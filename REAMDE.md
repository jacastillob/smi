## ANGULAR 2 CRM


Requirements

- Node version : ^6.1.0 -> 32 Bits
- npm install -g @angular/cli
- npm install --global http-server

# HOW TO RUN CODE

Open two terminal , in one npm install need to be run and in the second one  ng serve

- npm install (on master folder)
- http-server (on main folder)

# INSTALL AND RUN  NVM ON WINDOWS


After install zsh 

- brew update
- brew install nvm
- mkdir ~/.nvm

after in your ~/.zshrc or in .bash_profile if your use bash shell: 

    export NVM_DIR=~/.nvm
    source $(brew --prefix nvm)/nvm.sh


## NVM COMMANDS

- nvm list
- nvm install 6.1.0
- nvm use 6.1.0

# INSTALL AND RUN  NVM ON MACOS


- /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
- brew install node@6.1.0
- node -v
- which node 
alias node10='export PATH="/usr/local/opt/node@10/bin:$PATH"'
alias node6='export PATH="/usr/local/opt/node@6.1.0/bin:$PATH"'