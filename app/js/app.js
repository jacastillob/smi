/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS
 * 
 * Version: 3.0.0
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 * 
 */

// APP START
// ----------------------------------- 

(function() {
    'use strict';

    angular
        .module('SmiApp', [
            'app.core',
            'app.routes',
            'app.sidebar',
            'app.navsearch',
            'app.preloader',
            'app.translate',
            'app.settings',
            'app.pages',    
            'app.utils'
			
        ]);
})();


(function() {
    'use strict';

    angular
        .module('app.agenda', []);
})();
(function() {
    'use strict';

    angular
        .module('app.calendar', []);
})();
(function() {
    'use strict';

    angular
        .module('app.colors', []);
})();
(function() {
    'use strict';

    angular
        .module('app.consecutivo', []);
})();
(function() {
    'use strict';

    angular
        .module('app.contrato', []);
})();
(function() {
  'use strict';
  angular
    .module('app.core', [
    'ngRoute',
    'ngAnimate',
    'ngStorage',
    'ngCookies',
    'pascalprecht.translate',
    'ui.bootstrap',
    'ui.router',
    'oc.lazyLoad',
    'cfp.loadingBar',
    'ngSanitize',
    'ngResource',
    'tmh.dynamicLocale',
    'ui.utils',
    'app.fileManager',
    'app.routes',  
    'app.security',
    'app.mailbox',  
    'ngImgCrop',    
    'app.setMenu',
	  'app.funcionario',
    'app.tercero',
    'app.oportunidad',
    'app.cotizacion',
    'app.pedido',
    'app.factura',
    'app.contrato',
    'app.fileManager',
    'app.seguimiento',
    'app.producto',
    'app.calendar',
    'app.agenda',
    'app.consecutivo',
    'app.rol',
    'app.setMenu',       
    'app.mensajeria',
    'app.usuario',
    'app.entrega',
    'app.pasajero',
    'app.trm',
    'app.tarea',
    'app.recibo'
    
  ]);

})();
(function() {
    'use strict';

    angular
        .module('app.cotizacion', []);
})();
(function() {
    'use strict';

    angular
        .module('app.entrega', []);
})();
(function() {
    'use strict';

    angular
        .module('app.factura', []);
})();
(function() {
    'use strict';

    angular
        .module('app.fileManager', []);
})();
(function() {
    'use strict';

    angular
        .module('app.funcionario', []);
})();
(function() {
    'use strict';

    angular
        .module('app.lazyload', []);
})();
(function() {
    'use strict';

    angular
        .module('app.loadingbar', []);
})();
(function() {
    'use strict';

    angular
        .module('app.mailbox', []);
})();
(function() {
    'use strict';

    angular
        .module('app.mensajeria', []);
})();
(function() {
    'use strict';

    angular
        .module('app.setMenu', []);
})();
(function() {
    'use strict';

    angular
        .module('app.navsearch', []);
})();
(function() {
    'use strict';

    angular
        .module('app.oportunidad', []);
})();
(function() {
    'use strict';

    angular
        .module('app.pages', []);
})();
(function() {
    'use strict';

    angular
        .module('app.pasajero', []);
})();
(function() {
    'use strict';

    angular
        .module('app.pedido', []);
})();
(function() {
    'use strict';

    angular
        .module('app.preloader', []);
})();


(function() {
    'use strict';

    angular
        .module('app.producto', []);
})();
(function() {
    'use strict';

    angular
        .module('app.recibo', []);
})();
(function() {
    'use strict';

    angular
        .module('app.rol', []);
})();
(function() {
    'use strict';

    angular
        .module('app.routes', [
            'app.lazyload'
        ]);
})();
(function() {
    'use strict';

    angular
        .module('app.security', []);
})();
(function() {
    'use strict';

    angular
        .module('app.seguimiento', []);
})();
(function() {
    'use strict';

    angular
        .module('app.settings', []);
})();
(function() {
    'use strict';

    angular
        .module('app.sidebar', []);
})();
(function() {
    'use strict';
    angular
        .module('app.tarea', []);
})();
(function() {
    'use strict';

    angular
        .module('app.tercero', []);
})();
(function() {
    'use strict';

    angular
        .module('app.translate', []);
})();
(function() {
    'use strict';
    angular
        .module('app.trm', []);
})();
(function() {
    'use strict';

    angular
        .module('app.usuario', []);
})();
(function() {
    'use strict';

    angular
        .module('app.utils', [
          'app.colors'
          ]);
})();

/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaController', agendaController);

  agendaController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'agendaHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal','$filter'];


  function agendaController($scope, $rootScope, $state, $modalInstance, agendaHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window, $modal,$filter) {    
      
    $scope.agenda = {      
      model : {        
        id : "",        
        referenciaId : "",        
        asunto : '',
        descripcion : '',
        fechaInicial: '',
        fechaFinal: '',
        estado: '',          
        fechaAct: '',
        usuarioAct: '',
        fechaReg: '',
        usuarioReg: '',
        responsable:''
        
      },        
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1          
        },
        open : function($event) {
          $scope.agenda.fechaInicial.isOpen = true;
        }
      },
        horaInicial : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        horaFinal : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agenda.fechaFinal.isOpen = true;
        }
      },        
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.agenda.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.agenda.dataSource, paramFilter);
        $scope.agenda.paginations.totalItems = $scope.agenda.selectedItems.length;
        $scope.agenda.paginations.currentPage = 1;
        $scope.agenda.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.agenda.paginations.currentPage == 1 ) ? 0 : ($scope.agenda.paginations.currentPage * $scope.agenda.paginations.itemsPerPage) - $scope.agenda.paginations.itemsPerPage;
        $scope.agenda.data = $scope.agenda.selectedItems.slice(firstItem , $scope.agenda.paginations.currentPage * $scope.agenda.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.agenda.model.referenciaId,
          tipoReferencia : $scope.agenda.model.tipoReferencia
        };
        $scope.agenda.getData(params);
      },
      setData : function(data) {
        $scope.agenda.selectedItems = data;
        $scope.agenda.dataSource = data;
        $scope.agenda.paginations.totalItems = $scope.agenda.selectedItems.length;
        $scope.agenda.paginations.currentPage = 1;
        $scope.agenda.changePage();
        $scope.agenda.noData = false;
        $scope.agenda.loading = false;
        ($scope.agenda.dataSource.length < 1) ? $scope.agenda.noData = true : null;
      },
      getData : function(params) {
        $scope.agenda.data = [];
        $scope.agenda.loading = true;
        $scope.agenda.noData = false;

        agendaHttp.getAgendas({}, params, function(response) {
          $scope.agenda.setData(response);
        })
      },
      editar : function(id) {
        var params = {
          id : id 
        };
          
        agendaHttp.getAgenda({}, params, function(response) {
            $scope.agenda.model=response;
            
            
            $scope.modoEdicion=true;
               
			if($scope.agenda.model.fechaInicial){
                
                $scope.agenda.fechaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                $scope.agenda.horaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
            }    
			if($scope.agenda.model.fechaFinal){
                
                $scope.agenda.fechaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                $scope.agenda.horaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
            }  
            
            $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
            $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.agenda.model.responsable })[0];
            
        }, function() {
          message.show("error", "Ocurrio un error al intentar cargar la agenda");
          $rootScope.loadingVisible = false;
        }) 
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;

        agendaHttp.removeDocumento(params, function(response) {
          $scope.agenda.refresh();
          message.show("info", "Adjunto eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },    
      close : function() {
        $modalInstance.dismiss('cancel');
      }, seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        save : function() {
       
         
            if($scope.agenda.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
            else{$scope.agenda.model.fechaInicial=Date.parse(new Date($scope.agenda.fechaInicial.value));}	

            if($scope.agenda.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
            else{$scope.agenda.model.fechaFinal=Date.parse(new Date($scope.agenda.fechaFinal.value));}	
            
            if($scope.agenda.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
            if($scope.agenda.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
            
            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.agenda.model.estado= $scope.estados.current.value;}
            
            if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;}
            else{$scope.agenda.model.responsable= $scope.funcionarios.current.id;}
            
            $scope.agenda.model.referenciaId = parameters.id;
            
            var _http = new agendaHttp($scope.agenda.model);
             if(  $scope.agenda.model.id==0){
                 
                  _http.$addAgenda(function(response){                  
                      message.show("success", "Agenda creada satisfactoriamente!!");           
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editAgenda(function(response){                  
                      message.show("success", "Agenda actualizada satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }
            
            $scope.agenda.cancel();
            $scope.agenda.refresh();
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.funcionarios.current=$scope.funcionarios.data[0]; 
            
      },
        cancel : function(){
            
             var   model = {        
                id : 0,              
                asunto : '',
                tipoId : 0,
                descripcion : '',
                fechaInicial: '',
                referenciaId : parameters.id,
                fechaFinal: '',
                estado: '',          
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''
            };
            
           $scope.agenda.fechaInicial.value='';
           $scope.agenda.horaInicial.value='';
            
            $scope.agenda.model=model;
            $scope.modoEdicion= false;
            $scope.agenda.refresh();
            
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.funcionarios.current=$scope.funcionarios.data[0]; 
            
        }
    }    
      //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            
            $scope.estados.data.push({value:1, descripcion: 'Abierta'});
            $scope.estados.data.push({value:0, descripcion: 'Cerrada'});           
            
            if(!$scope.agenda.model){$scope.estados.current = $scope.estados.data[0];}
            else{$scope.estados.current = $filter('filter')($scope.estados.data, { id : $scope.agenda.model.estado })[0];}
                        
        }        
    }
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;             
            agendaHttp.getFuncionarios({}, {}, function(response) {
            
                $scope.funcionarios.data = response;                    
                $scope.funcionarios.current=$scope.funcionarios.data[0];

            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
        setFuncionario : function(){
            
        }
    }        
    $scope.funcionarios.getData();
    $scope.estados.getData();
      
    $scope.modoEdicion= false;
    $scope.agenda.model.referenciaId = parameters.id;
    $scope.agenda.model.tipoReferencia = parameters.referencia;
      
      
    $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
       
    //CARGAMOS LOS DATOS
     $scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;

    $scope.agenda.refresh();

  }

})();
/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaCalendarioController', agendaCalendarioController);

  agendaCalendarioController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'agendaHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal','$filter','cotizacionHttp','facturaHttp','pedidoHttp','oportunidadHttp'];


  function agendaCalendarioController($scope, $rootScope, $state, $modalInstance, agendaHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window, $modal,$filter,cotizacionHttp,facturaHttp,pedidoHttp,oportunidadHttp) {    
      
    $scope.agenda = {      
      model : {        
        id : "",        
        referenciaId : "",  
        referencia : "",
        asunto : '',
        descripcion : '',
        fechaInicial: '',
        fechaFinal: '',
        estado: '',          
        fechaAct: '',
        usuarioAct: '',
        fechaReg: '',
        usuarioReg: '',
        responsable:''
        
      },        
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1          
        },
        open : function($event) {
          $scope.agenda.fechaInicial.isOpen = true;
        }
      },
        horaInicial : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        horaFinal : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agenda.fechaFinal.isOpen = true;
        }
      },             
      getData : function() {
          
          
          
        var params = {
          id : $scope.agenda.model.id
        };          
        agendaHttp.getAgenda({}, params, function(response) {
            
            $scope.agenda.model=response;           
            
            $scope.modoEdicion=true;
               
			if($scope.agenda.model.fechaInicial){
                
                $scope.agenda.fechaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                $scope.agenda.horaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
            }    
			if($scope.agenda.model.fechaFinal){
                
                $scope.agenda.fechaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                $scope.agenda.horaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
            }  
            
            $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
            $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.agenda.model.responsable })[0];
            
        }, function() {
          message.show("error", "Ocurrio un error al intentar cargar la agenda");
          $rootScope.loadingVisible = false;
        }) 
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;

        agendaHttp.removeDocumento(params, function(response) {
          
          message.show("info", "Adjunto eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },    
      close : function() {
        $modalInstance.dismiss('cancel');
      }, seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                          referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        save : function() {
       
         
            if($scope.agenda.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
            else{$scope.agenda.model.fechaInicial=Date.parse(new Date($scope.agenda.fechaInicial.value));}	

            if($scope.agenda.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
            else{$scope.agenda.model.fechaFinal=Date.parse(new Date($scope.agenda.fechaFinal.value));}	
            
            if($scope.agenda.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
            if($scope.agenda.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
            
            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.agenda.model.estado= $scope.estados.current.value;}
            
            if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;}
            else{$scope.agenda.model.responsable= $scope.funcionarios.current.id;}            
            
            
            var _http = new agendaHttp($scope.agenda.model);
            $rootScope.loadingVisible = true;
             if(  $scope.agenda.model.id==0){
                 
                  _http.$addAgenda(function(response){                  
                      message.show("success", "Agenda creada satisfactoriamente!!"); 
                      $scope.agenda.close();
                      $rootScope.loadingVisible = false;
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editAgenda(function(response){                  
                      message.show("success", "Agenda actualizada satisfactoriamente");
                      $scope.agenda.close();
                      $rootScope.loadingVisible = false;
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }
            
            $scope.agenda.cancel();            
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.funcionarios.current=$scope.funcionarios.data[0]; 
            
      },
        cancel : function(){
            
             var   model = {        
                id : 0,              
                asunto : '',
                tipoId : 0,
                descripcion : '',
                fechaInicial: '',
                referenciaId : parameters.id,
                referencia : parameters.reference,
                fechaFinal: '',
                estado: '',          
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''
            };
            
           $scope.agenda.fechaInicial.value='';
           $scope.agenda.horaInicial.value='';
           $scope.agenda.model=model;
           $scope.modoEdicion= false; 
           $scope.estados.current=$scope.estados.data[0]; 
           $scope.funcionarios.current=$scope.funcionarios.data[0]; 
            
        },
        openReference:function(){       
             
           
             var params = {
                              id : $scope.agenda.model.codigoReferencia
                          };
            
            if(parameters.referencia=="OP"){  
                    $rootScope.loadingVisible = true;
                	oportunidadHttp.read({},params, function (data) {
                            
							
								    var modalInstance = $modal.open({
                                          templateUrl: 'app/views/oportunidad/oportunidad_form.html',
                                          controller: 'oportunidadController',
                                          size: 'lg',
                                          resolve: {
                                            parameters: { oportunidad: data }
                                          }
                                        });
                                        modalInstance.result.then(function (parameters) {
                                            
                                        }); 
											
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
                
            }
            if(parameters.referencia=="COT"){  
                $rootScope.loadingVisible = true;
                cotizacionHttp.read({},params, function (data) { 
                    
                    
                      var modalInstance = $modal.open({
                          templateUrl: 'app/views/cotizacion/cotizacion_form.html',
                          controller: 'cotizacionController',
                          size: 'lg',
                          resolve: {
                            parameters: { cotizacion: data,vcotizaciones:null }
                          }
                        });
                        modalInstance.result.then(function (parameters) {

                        }); 
                    
                    
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", faild.Message);
                });   
                
            }
            if(parameters.referencia=="PE"){
                
                $rootScope.loadingVisible = true;
                pedidoHttp.read({},params, function (data) { 
                
                    var modalInstance = $modal.open({
                          templateUrl: 'app/views/pedido/pedido_form.html',
                          controller: 'pedidoController',
                          size: 'lg',
                          resolve: {
                            parameters: { pedido: data,vpedidos:null }
                          }
                        });
                        modalInstance.result.then(function (parameters) {
                            $scope.pedidos.getData();
                        });   

                   $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", faild.Message);
                });                   
            }
            if(parameters.referencia=="FAC"){
                
                $rootScope.loadingVisible = true;
                facturaHttp.read({},params, function (data) {  
                
                       var modalInstance = $modal.open({
                          templateUrl: 'app/views/factura/factura_form.html',
                          controller: 'facturaController',
                          size: 'lg',
                          resolve: {
                            parameters: { factura: data,vfacturas:null }
                          }                           
                        });
                        modalInstance.result.then(function (parameters) {
                            
                        }); 
                
                   $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", faild.Message);
                });                   
            }            
            
        }
    }    
      //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            
            $scope.estados.data.push({value:1, descripcion: 'Abierta'});
            $scope.estados.data.push({value:0, descripcion: 'Cerrada'});           
            
            if(!$scope.agenda.model){$scope.estados.current = $scope.estados.data[0];}
            else{$scope.estados.current = $filter('filter')($scope.estados.data, { id : $scope.agenda.model.estado })[0];}
                        
        }        
    }
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;             
            agendaHttp.getFuncionarios({}, {}, function(response) {
            
                $scope.funcionarios.data = response;                    
                $scope.funcionarios.current=$scope.funcionarios.data[0];

            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
        setFuncionario : function(){            
        }
    }        
    $scope.funcionarios.getData();
    $scope.estados.getData();
      
    $scope.modoEdicion= true;
 
    $scope.agenda.model.id = parameters.id;
    
    $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
       
    //CARGAMOS LOS DATOS
    $scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;

    $scope.agenda.getData();
    

  }

})();
/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .service('agendaHttp', agendaHttp);

  agendaHttp.$inject = ['$resource', 'END_POINT'];


  function agendaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getAgendas : {
        method : 'POST',
        isArray : true
      },
      getAgenda : {
        method : 'GET',
        url : END_POINT + '/General.svc/agenda/:id'
      },
      removeAgenda : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/agenda/:id'
      },
      addAgenda: {
        method:'POST',
        url : END_POINT + '/General.svc/addAgenda'
      },
      editAgenda: {
        method:'PUT',
        url : END_POINT + '/General.svc/editAgenda'
      },
      removeAgenda: {
        method:'DELETE',
        params : {
            agendaId : '@agendaId'           
          },
        url : END_POINT + '/General.svc/agenda/:agendaId'
      },
     getFuncionarios : {
        method : 'GET',
        isArray : true,
        url : END_POINT + '/General.svc/funcionarioBusqueda'
      },        
        'getTipoAgenda' : {
        'method' : 'GET',
        'isArray' : true,
         'params' : {
            tipoAgenda : '@tipoAgenda'           
          },        
        'url' : END_POINT + '/General.svc/tipoAgenda/:tipoAgenda'
      },
        'getListaAgendas' : {
        'method': 'POST',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/listaAgenda'
      } 
        
        
    };

    return $resource(END_POINT + '/General.svc/agenda', paramDefault, actions);
  }

})();

/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaFormController', agendaFormController);

  agendaFormController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'agendaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function agendaFormController($scope, $filter, $state, LDataSource, agendaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      

     
      var agenda = $state.params.agenda;
      var referenciaId = agenda.referenciaId;
      var tipoReferencia = agenda.tipoReferencia;
 
      
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.agenda = {
		model : {
            id : 0,
            tipoId : 0,
            referenciaId : 0,
            tipoReferencia : '',
            asunto : '',
            descripcion : '',
            fechaInicial: '',
            fechaFinal: '',
            estado: '',          
            fechaAct: '',
            usuarioAct: '',
            fechaReg: '',
            usuarioReg: '',
            responsables : [],
            consecutivos : []	
		}, 
      fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.agenda.fechaInicial.isOpen = true;
        }
      },
        horaInicial : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        horaFinal : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agenda.fechaFinal.isOpen = true;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.agenda', params : { filters : {objFiltro:agenda.objFiltro}, data : []} });
        $state.go('app.agenda');
          
      },
      save : function() {		  
       
          if($scope.agenda.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
            else{$scope.agenda.model.fechaInicial=Date.parse(new Date($scope.agenda.fechaInicial.value));}	

            if($scope.agenda.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
            else{$scope.agenda.model.fechaFinal=Date.parse(new Date($scope.agenda.fechaFinal.value));}	
            
            if($scope.agenda.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
            if($scope.agenda.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
            
            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.agenda.model.estado= $scope.estados.current.value;}

            if(!$scope.tipoAgenda.current){message.show("warning", "Tipo agenda requerido");return;}
            else{$scope.agenda.model.tipoId= $scope.tipoAgenda.current.id;}	
            
            if($scope.agenda.model.responsables.length==0){message.show("warning", "Debe especificar un responsable");return;}
            
            
             $scope.agenda.model.referenciaId = referenciaId;
             $scope.agenda.model.tipoReferencia = tipoReferencia;
            
            
            var _http = new agendaHttp($scope.agenda.model);
          
             if(  $scope.agenda.model.id==0){
                 
                  _http.$addAgenda(function(response){                  
                      message.show("success", "Agenda creada satisfactoriamente!!"); 
                      $scope.modoCreacion=true;
                      $scope.agenda.model=response;
                      
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editAgenda(function(response){                  
                      message.show("success", "Agenda actualizada satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }
             
        
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
          seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        cancel : function(){
            
             var   model = {        
                id : 0,              
                asunto : '',
                tipoId : 0,
                descripcion : '',
                fechaInicial: '',
                referenciaId : referenciaId,
                tipoReferencia : tipoReferencia,
                fechaFinal: '',
                estado: '',          
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: '',
                responsables : [],
                consecutivos : []
            };
            
           $scope.agenda.fechaInicial.value='';
           $scope.agenda.horaInicial.value='';
            
            $scope.agenda.model=model;
            //$scope.modoEdicion= false;
           
            
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.tipoAgenda.current=$scope.tipoAgenda.data[0]; 
            
        },
        removeResponsable: function(item){          
                    
            
            for (var i = 0; i < $scope.agenda.model.responsables.length; i++) { 
                
                    if($scope.agenda.model.responsables[i].funcionarioId ==item.funcionarioId){
                        
                        $scope.agenda.model.responsables.splice(i, 1);
                        break;
                    }
                
                }
        }  ,        
        addResponsable: function(){  
          
         if(!$scope.funcionarios.current){message.show("warning", "Resposable requerido");return;}
            
            var guarda=0;
            for (var i = 0; i < $scope.agenda.model.responsables.length; i++) { 
                
                    if($scope.agenda.model.responsables[i].funcionarioId ==$scope.funcionarios.current.id){
                        
                      guarda++;
                    }
                
                }
            
            if(guarda==0){
                
                var nuevoResponsable={
                    agendaId:$scope.agenda.model.id,
                    funcionarioId: $scope.funcionarios.current.funcionarioId,
                    nombre: $scope.funcionarios.current.nombre                    
                }
                
                $scope.agenda.model.responsables.push(nuevoResponsable);
                
                
            }else{                
                message.show("warning", "El responsable ya se encuentra asociado en la agenda");
            }
        },          
        removeConsecutivo: function(item){          
                    
            
            for (var i = 0; i < $scope.agenda.model.consecutivos.length; i++) { 
                
                    if($scope.agenda.model.consecutivos[i].consecutivoId ==item.consecutivoId){
                        
                        $scope.agenda.model.consecutivos.splice(i, 1);
                        break;
                    }
                
                }
        }  ,        
        addConsecutivo: function(){  
          
         if(!$scope.consecutivos.current){message.show("warning", "Consecutivo requerido");return;}
           if(!$scope.agenda.model.consecutivos){$scope.agenda.model.consecutivos=[]} 
            var guarda=0;
            for (var i = 0; i < $scope.agenda.model.consecutivos.length; i++) { 
                
                    if($scope.agenda.model.consecutivos[i].consecutivoId ==$scope.consecutivos.current.consecutivoId){
                        
                      guarda++;
                    }
                
                }
            
            if(guarda==0){
                
                var nuevoConsecutivo={
                    agendaId:$scope.agenda.model.id,
                    consecutivoId: $scope.consecutivos.current.consecutivoId,
                    consecutivo: $scope.consecutivos.current.consecutivo                    
                }
                
                $scope.agenda.model.consecutivos.push(nuevoConsecutivo);
                
                
            }else{                
                message.show("warning", "El consecutivo ya se encuentra asociado en la agenda");
            }
        }
          
        
      }
   
         //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'A', descripcion: 'Abierta'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrada'});
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelada'}); 
            $scope.estados.data.push({value:'AN', descripcion: 'Anulada'});  
            
            if(!$scope.agenda.model){$scope.estados.current = $scope.estados.data[0];}
            else{$scope.estados.current = $filter('filter')($scope.estados.data, { id : $scope.agenda.model.estado })[0];}
                        
        }        
    }
    
        
    //TIPO AGENDA
    $scope.tipoAgenda = {
      current : {},
      data:[],
      getData : function() {
            $scope.tipoAgenda.data=agenda.tipoAgenda;          
         
                for (var i = 0; i < $scope.tipoAgenda.data.length; i++) { 

                    if($scope.tipoAgenda.data[i].codigo ==-1){
                        $scope.tipoAgenda.data.splice(i, 1);
                        break;
                    }

                }
      },
      setTipo : function(){
          $scope.agenda.model.agenda=$scope.tipoAgenda.current.id;
    }}
    
    
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $scope.funcionarios.data = agenda.participantes;
          
           for (var i = 0; i < $scope.funcionarios.data.length; i++) { 

                    if($scope.funcionarios.data[i].id ==-1){
                        $scope.funcionarios.data.splice(i, 1);
                        break;
                    }

                }
      }
    } 
    
       //FUNCIONARIOS
    $scope.consecutivos = {
      current : {}, data:[],
      getData : function() {
                      
           $rootScope.loadingVisible = true;
            agendaHttp.getConsecutivos({}, {}, function(response) {                    
            $scope.consecutivos.data = response ;                  
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }   
    
    
    $scope.tipoAgenda.getData();
    $scope.funcionarios.getData();
    $scope.estados.getData();
      $scope.consecutivos.getData();
    
	//CARGAMOS LOS DATOS DEL agenda	
	
	if(agenda.id==0){
    
        $scope.estados.current=$scope.estados.data[0]; 
        $scope.tipoAgenda.current = $scope.tipoAgenda.data[0];
        $scope.funcionarios.current= $scope.funcionarios.data[0];        
        $scope.modoCreacion=false;
        
        
      //$scope.objetoagenda.current = $scope.objetoagenda.data[0];
      //$scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : agenda.tipo})[0]; 
      //$scope.agenda.model.tipo=agenda.tipo;    
        
        
    }
    else{   
        $scope.modoCreacion=true;
        $rootScope.loadingVisible = true;        
        //$scope.divParticipanteagenda=true;
		agendaHttp.getAgenda({},$state.params.agenda, function (data) { 
            $scope.agenda.model = data;	
            $scope.funcionarios.current= $scope.funcionarios.data[0];

            if($scope.agenda.model.fechaInicial){

                    $scope.agenda.fechaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                    $scope.agenda.horaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                }    
                if($scope.agenda.model.fechaFinal){

                    $scope.agenda.fechaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                    $scope.agenda.horaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                }  

            $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
            $scope.tipoAgenda.current = $filter('filter')($scope.tipoAgenda.data, { id : $scope.agenda.model.tipoId })[0];   
            
                var obj= {
                    idProceso : data.referenciaId,
                    proceso : data.tipoReferencia
                }  
              
                $rootScope.loadingVisible = true;
                agendaHttp.getResponsablesProceso({}, obj, function(response) {                      
                    
                    
                    if(response){                        
                        
                        agenda.participantes = response;                          
                        $scope.funcionarios.getData();              
                    
                    }
                    else{
                            message.show("warning", "El proceso asociado no tiene responsables posibles");  
                    }
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });  
            
       
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      
    
  }
})();
/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaListController', agendaListController);

  agendaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'agendaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function agendaListController($scope, $filter, $state, ngDialog, tpl, agendaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
	
    var objFiltro= $state.params.filters.objFiltro;        
        
     var objF={              

          idTipoAgenda:0,
          fechaInicial:'',
          fechaFinal:'',
          idResponsable:0,
          idReferencia:0,
          tipoReferencia:'',
          estado:''
      }          
          
        
	$scope.ESTADO_ABIERTA = 'A';    
    $scope.ESTADO_CERRADO= 'C';	    
    $scope.ESTADO_APROBADA= 'AP';	       
        
        
    $scope.agendas = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      procesoAsociado:'',
      fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.agendas.fechaInicial.isOpen = true;
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agendas.fechaFinal.isOpen = true;
        }
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreagendas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.agendas.selectedAll = !$scope.agendas.selectedAll; 
        for (var key in $scope.agendas.selectedItems) {
          $scope.agendas.selectedItems[key].check = $scope.agendas.selectedAll;
        }
      },
      add : function() {           
          
          if($scope.procesoAsociado.current.id!=-1)
          {
              var cont = {
                    id : 0,
                    tipoId : 0,
                    referenciaId :$scope.procesoAsociado.current.id ,
                    tipoReferencia : $scope.procesoAgenda.current.proceso,
                    asunto : '',
                    descripcion : '',
                    fechaInicial: '',
                    fechaFinal: '',
                    estado: '',          
                    fechaAct: '',
                    usuarioAct: '',
                    fechaReg: '',
                    usuarioReg: '',
                    responsables : [],
                    tipoAgenda : $scope.tipoAgenda.data,
                    participantes : $scope.responsableProceso.data,
                    consecutivos : []
              };
            
              cont.objFiltro=objF;
            parametersOfState.set({ name : 'app.agenda_add', 
               params : { agenda: cont } 
                });
            $state.go('app.agenda_add'); 
              }
          else            
          {
                 message.show("warning", "Debe seleccionar un proceso asociado");
          }
      },
      edit : function(item) {
          
          item.objFiltro=objF;
          item.tipoAgenda = $scope.tipoAgenda.data;
          item.participantes = [];
          item.consecutivos = [];
            parametersOfState.set({ name : 'app.agenda_edit', params : { agenda: item } });
            $state.go('app.agenda_edit');            
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            agendaHttp.remove({}, { id: id }, function(response) {
                $scope.agendas.getData();
                message.show("success", "agenda eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.agendas.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    agendaHttp.remove({}, { id: id }, function(response) {
                        $scope.agendas.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.agendas.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.agendas.filterText,
          "precision": false
        }];
        $scope.agendas.selectedItems = $filter('arrayFilter')($scope.agendas.dataSource, paramFilter);
        $scope.agendas.paginations.totalItems = $scope.agendas.selectedItems.length;
        $scope.agendas.paginations.currentPage = 1;
        $scope.agendas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.agendas.paginations.currentPage == 1 ) ? 0 : ($scope.agendas.paginations.currentPage * $scope.agendas.paginations.itemsPerPage) - $scope.agendas.paginations.itemsPerPage;
        $scope.agendas.data = $scope.agendas.selectedItems.slice(firstItem , $scope.agendas.paginations.currentPage * $scope.agendas.paginations.itemsPerPage);
      },
      getData : function() {
          
       
          
            if($scope.agendas.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{objF.fechaInicial=Date.parse(new Date($scope.agendas.fechaInicial.value));}	
          
            if($scope.agendas.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{objF.fechaFinal=Date.parse(new Date($scope.agendas.fechaFinal.value));}	  
          
              
              objF.tipoReferencia=$scope.procesoAgenda.current.proceso;
              objF.idResponsable=$scope.responsableProceso.current.id;
              objF.estado=$scope.estadoAgenda.current.value;
              objF.idReferencia=$scope.procesoAsociado.current.id;              
              objF.idTipoAgenda=$scope.tipoAgenda.current.codigo;
       
          
            $scope.agendas.data = [];
            $scope.agendas.loading = true;
            $scope.agendas.noData = false;             
          
         agendaHttp.getListaAgendas( {},objF,function(response) {
             
              $scope.agendas.selectedItems = response;
              $scope.agendas.dataSource = response;
              for(var i=0; i<$scope.agendas.dataSource.length; i++){
                $scope.agendas.nombreagendas.push({id: i, nombre: $scope.agendas.dataSource[i]});
              }
              $scope.agendas.paginations.totalItems = $scope.agendas.selectedItems.length;
              $scope.agendas.paginations.currentPage = 1;
              $scope.agendas.changePage();
              $scope.agendas.loading = false;
              ($scope.agendas.dataSource.length < 1) ? $scope.agendas.noData = true : null;
              $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
        
    }  
    
      //CARGAMOS PROCESOS
    $scope.procesoAgenda = {
      current : {},
      data:[],
      getData : function() {
        $rootScope.loadingVisible = true;
        agendaHttp.getProcesosAgenda({}, { }, function(response) { 

            $scope.procesoAgenda.data = $filter('orderBy')(response, 'proceso');  
            if(objFiltro){
                
                $scope.procesoAgenda.current=  $filter('filter')($scope.procesoAgenda.data, {proceso : objFiltro.tipoReferencia})[0]; 
                $scope.procesoAgenda.setProceso();       
                
            }
            else{
                 
                $scope.procesoAgenda.current =$scope.procesoAgenda.data[0];
                $scope.procesoAgenda.setProceso();  
            }           
            
            
            $rootScope.loadingVisible = false;
            
            
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
         'setProceso' : function() {
                 
                  
                  $scope.tipoAgenda.getData();
                  $scope.procesoAsociado.getData();    

                  if($scope.procesoAgenda.current.proceso=='CONTRATO')
                        $scope.agendas.procesoAsociado='Contratos:';   
           
      }
    }
    //CARGAMOS TIPO AGENDAMIENTOS POR PROCESO
    $scope.tipoAgenda = {
        current : {},
        data:[],
        getData : function() {
            var pAgen="";
            
            
            if(objFiltro){                
                pAgen=objFiltro.tipoReferencia;                
            }
            else{                
                pAgen=$scope.procesoAgenda.current.proceso;                
            }            
            
            
            if(pAgen!=""){
                
                $rootScope.loadingVisible = true;
                agendaHttp.getTipoAgenda({}, {tipoAgenda:pAgen }, function(response) {
                    
                    
                    $scope.tipoAgenda.data = $filter('orderBy')(response, 'codigo'); 
                    $scope.tipoAgenda.data.push({codigo:(-1), descripcion:"Todos"});
                    $scope.tipoAgenda.data=$filter('orderBy')($scope.tipoAgenda.data, 'codigo');
                    
                    if(objFiltro){
                
                        $scope.tipoAgenda.current=  $filter('filter')($scope.tipoAgenda.data, {codigo : objFiltro.idTipoAgenda})[0]; 
                        
                    }
                    else{

                        $scope.tipoAgenda.current = $scope.tipoAgenda.data[0];
                    }                     
                    
                    $rootScope.loadingVisible = false;

                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", faild.Message);
                });
            }  
        }
    }
    //ESTADOS
    $scope.estadoAgenda = {
        current : {}, data:[],
        getData : function() {
            $scope.estadoAgenda.data.push({value:'', descripcion: 'Todos'});
            $scope.estadoAgenda.data.push({value:'A', descripcion: 'Abierta'});
            $scope.estadoAgenda.data.push({value:'C', descripcion: 'Cerrada'});
            $scope.estadoAgenda.data.push({value:'CA', descripcion: 'Cancelada'}); 
            $scope.estadoAgenda.data.push({value:'AN', descripcion: 'Anulada'});  
            $scope.estadoAgenda.data.push({value:'AP', descripcion: 'Aprobada'});   
            
            if(objFiltro){            
                $scope.estadoAgenda.current=  $filter('filter')($scope.estadoAgenda.data, {value : objFiltro.estado})[0];
            }
            else{
                
                $scope.estadoAgenda.current = $scope.estadoAgenda.data[0];
            }
                        
        }        
    }  
    
    $scope.procesoAsociado = {
      current : {}, data:[],
      getData : function() {
         
          
          if($scope.tipoAgenda.current){
              
                $rootScope.loadingVisible = true;
                agendaHttp.getProcesosAsociado({}, {proceso:$scope.procesoAgenda.current.proceso}, function(response) {  
                    
                   
                    $scope.procesoAsociado.data = response ;   
                    $scope.procesoAsociado.data.push({id:(-1), descripcion:"Todos",proceso:""});
                    $scope.procesoAsociado.data=$filter('orderBy')($scope.procesoAsociado.data, 'id');  
                    $scope.procesoAsociado.current=$scope.procesoAsociado.data[0];  
                     
                    
                  if(objFiltro){            
                    $scope.procesoAsociado.current=  $filter('filter')($scope.procesoAsociado.data, {id : objFiltro.idReferencia})[0]; 
                    $scope.responsableProceso.getData();
                    }
                    else{

                        $scope.procesoAsociado.current = $scope.procesoAsociado.data[0];
                        $scope.responsableProceso.getData();
                    }
                    
                    
                    
                    
                    $rootScope.loadingVisible = false;
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      },
        setProcesoAsociado: function() {             
           $scope.responsableProceso.getData();            
      }
    }
    
    
    //RESPONSABLE
    $scope.responsableProceso = {
      current : {}, data:[],
      getData : function() {
        
          if($scope.procesoAsociado.current){              
              
                var obj= {
                    idProceso : $scope.procesoAsociado.current.id,
                    proceso : $scope.procesoAgenda.current.proceso
                }  
              
                $rootScope.loadingVisible = true;
                agendaHttp.getResponsablesProceso({}, obj, function(response) {                      
                    
                    
                    if(response){
                        
                        $scope.responsableProceso.data = response ;   
                        $scope.responsableProceso.data.push({id:(-1), nombre:"Todos"});
                        $scope.responsableProceso.data=$filter('orderBy')($scope.responsableProceso.data, 'id'); 
                        
                        
                    }
                    else{
                         $scope.responsableProceso.data.push({id:(-1), nombre:"Todos"});                         
                    }
                    
                    if(objFiltro){            
                        $scope.responsableProceso.current=  $filter('filter')($scope.responsableProceso.data, {id : objFiltro.idResponsable})[0]; 

                    }
                    else{

                            $scope.responsableProceso.current = $scope.responsableProceso.data[0];

                        }
                    
                    
                    
                    $rootScope.loadingVisible = false;
                    
                    
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      }
    }  
    
    
	//CARGAMOS DATA       
    
    $scope.agendas.procesoAsociado='Contratos:';   
        
    var date = new Date();    
    $scope.agendas.fechaInicial.value=new Date(date.getFullYear(), date.getMonth(), 1);
    $scope.agendas.fechaFinal.value=new Date(date.getFullYear(), date.getMonth() + 1, 0);
        
    $scope.procesoAgenda.getData();  
    $scope.estadoAgenda.getData();  
    
        
        if (objFiltro){          

            if(objFiltro.fechaInicial){$scope.agendas.fechaInicial.value=new Date(parseFloat(objFiltro.fechaInicial));}
            if(objFiltro.fechaFinal){$scope.agendas.fechaFinal.value=new Date(parseFloat(objFiltro.fechaFinal));}  
                setTimeout(function() { $scope.agendas.getData(); }, 1000);
            
        } 
     
 
    }
  
  
  })();
(function() {
    'use strict';

    angular
        .module('app.calendar')
        .directive('calendar', calendar);

    calendar.$inject = [ '$rootScope','calendarHttp','message','$modal'];
    
    function calendar ($rootScope,calendarHttp,message,$modal) {
            
        var directive = {
            link: link, 
            restrict: 'EA'        
        };  
        return directive;  
        
        function link(scope, element) {
          
          if(!$.fn.fullCalendar) return;  
            
            var calendar = element;            
            initCalendar(calendar, null, $rootScope.app.layout.isRTL,calendarHttp,$rootScope,message,$modal);
                    
        }  
    }
      /**
     * Invoke full calendar plugin and attach behavior
     * @param  jQuery [calElement] The calendar dom element wrapped into jQuery
     * @param  EventObject [events] An object with the event list to load when the calendar displays
     */
    function initCalendar(calElement, events, isRTL,calendarHttp,$rootScope,message,$modal) { 
        
           $rootScope.loadingVisible = true;
           calendarHttp.getAgendas({}, {}, function(response) {        
               
                    message.show("success", "Agendas cargadas");
                
                    var eve = [];
                    response.forEach(function(entry) {
                        
                        eve.push( {
                                id:entry.id,
                                referenceId:entry.referenciaId,
                                reference:entry.referencia,
                                title: entry.title,
                                start: new Date(entry.start),
                                end: new Date(entry.end),
                                backgroundColor: entry.backgroundColor, //red                                
                            });
                       
                    });
               
                     calElement.fullCalendar({
                        isRTL: isRTL,
                        header: {
                            left:   'prev,next today',
                            center: 'title',
                            right:  'month,agendaWeek,agendaDay'
                        },
                        buttonIcons: { // note the space at the beginning
                            prev:    ' fa fa-caret-left',
                            next:    ' fa fa-caret-right'
                        },
                        buttonText: {
                            today: 'Hoy',
                            month: 'Mes',
                            week:  'Semana',
                            day:   'Dia'
                        },
                        editable: true,
                        droppable: false, // this allows things to be dropped onto the calendar 
                        // This array is the events sources
                        events: eve,
                        eventClick: function(calEvent, jsEvent, view) {                            
                            
                                    var modalInstance = $modal.open({
                                      templateUrl: 'app/views/agenda/agendaCalendario_form.html',
                                      controller: 'agendaCalendarioController',
                                      size: 'lg',
                                      resolve: {
                                        parameters: { id : calEvent.id,referencia:calEvent.reference}
                                      }
                                    });
                                    modalInstance.result.then(function (parameters) {
                                    });
                              }
                    });       
               $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                console.log(faild.Message);
                //message.show("error", faild.Message);
            });            
           
      }    
    
     

})();
/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.calendar')
    .service('calendarHttp', calendarHttp);

  calendarHttp.$inject = ['$resource', 'END_POINT'];


  function calendarHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getAgendas' : {
        'method' : 'GET',
        'isArray' : true,          
        'url' : END_POINT + '/General.svc/agenda/calendario'
      }
    };
    return $resource( END_POINT + '/General.svc/agenda/calendario', {}, actions, {}); 
  }

})();
(function() {
    'use strict';

    angular
        .module('app.colors')
        .constant('APP_COLORS', {
          'primary':                '#5d9cec',
          'success':                '#27c24c',
          'info':                   '#23b7e5',
          'warning':                '#ff902b',
          'danger':                 '#f05050',
          'inverse':                '#131e26',
          'green':                  '#37bc9b',
          'pink':                   '#f532e5',
          'purple':                 '#7266ba',
          'dark':                   '#3a3f51',
          'yellow':                 '#fad732',
          'gray-darker':            '#232735',
          'gray-dark':              '#3a3f51',
          'gray':                   '#dde6e9',
          'gray-light':             '#e4eaec',
          'gray-lighter':           '#edf1f2'
        
        })
        ;
})();
/**=========================================================
 * Module: colors.js
 * Services to retrieve global colors
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.colors')
        .service('Colors', Colors);

    Colors.$inject = ['APP_COLORS'];
    function Colors(APP_COLORS) {
        this.byName = byName;

        ////////////////

        function byName(name) {
          return (APP_COLORS[name] || '#fff');
        }
    }

})();

/**=========================================================
 * Module: app.consecutivo.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .controller('consecutivoController', consecutivoController);

  consecutivoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'consecutivoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function consecutivoController($scope, $filter, $state, LDataSource, consecutivoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var consecutivo = $state.params.consecutivo;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.consecutivo = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,   
              funcionarioId: 0,   
              tipo: 0,
              codigo: '',			  
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''	
          
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.consecutivo.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.consecutivo.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.consecutivo', params : { filters : {procesoSeleccionado:consecutivo.tipo}, data : []} });
        $state.go('app.consecutivo');
          
      },
      save : function() {		  
       
		  
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.consecutivo.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.consecutivo.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
				
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.consecutivo.model.funcionarioId = $scope.funcionarios.current.id;}
          
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.consecutivo.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoConsecutivo.current){message.show("warning", "Tipo consecutivo");return;}
				else{$scope.consecutivo.model.tipo= $scope.tipoConsecutivo.current.id;}	
   	
          
			//INSERTAR
            if($scope.consecutivo.model.id==0){
				
				$rootScope.loadingVisible = true;
                
				consecutivoHttp.save({}, $scope.consecutivo.model, function (data) { 
					
						consecutivoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.consecutivo.model=data;
							consecutivo.id=$scope.consecutivo.model.id; 
							if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
							if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));} 		
								
							message.show("success", "consecutivo creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.consecutivo.model.id){
					$state.go('app.consecutivo');
				}else{
					
					consecutivoHttp.update({}, $scope.consecutivo.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.consecutivo.model=data;
						if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
						if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "consecutivo actualizado satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/consecutivo/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.consecutivo.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }        
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'AN', descripcion: 'Anulado'});            
            $scope.estados.data.push({value:'AP', descripcion: 'Aprobado'}); 
        }        
    }
	//TIPO PROCESO
      $scope.tipoConsecutivo = {
            current: {},
            data: [],
            getData: function() { 
               consecutivoHttp.getTipoConsecutivos({}, {}, function(response) {
                      
                     $scope.tipoConsecutivo.data = $filter('orderBy')(response, 'id');                     
                     $scope.tipoConsecutivo.current= $filter('filter')($scope.tipoConsecutivo.data, {id : consecutivo.tipo})[0];  
                     $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });
                    
            }
        }
      

    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            consecutivoHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.consecutivo.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.consecutivo.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.consecutivo.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.consecutivo.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                consecutivoHttp.getContactosEmpresa({}, { terceroId: $scope.consecutivo.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.consecutivo.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.consecutivo.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.consecutivo.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            
            consecutivoHttp.getFuncionarios({}, {}, function(response) {
            
                $scope.funcionarios.data = response;  
                $scope.funcionarios.data.push({id:(-1), funcionario:"Seleccione"});
                $scope.funcionarios.data=$filter('orderBy')($scope.funcionarios.data, 'id');                
                $rootScope.loadingVisible = false;
                
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    } 
    

	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoConsecutivo.getData();
    $scope.tercerosOp.getData();   
    $scope.funcionarios.getData();   
    
      
   
  
	
	//CARGAMOS LOS DATOS DEL consecutivo	
	
	if(consecutivo.id==0){        
        
        
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.consecutivo.model.tipo=consecutivo.tipo;  
      $scope.funcionarios.current= $scope.funcionarios.data[0];
               
      
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            consecutivoHttp.read({},$state.params.consecutivo, function (data) { 
                
            $scope.consecutivo.model = data;	
            $scope.btnAdjunto=true;          
               
			if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
			if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));}  
            
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.consecutivo.model.terceroId })[0];  
            $scope.contactosOp.getData();               
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.consecutivo.model.contactoId })[0]; 
                
            $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.consecutivo.model.funcionarioId })[0]; 
                
			//$scope.estados.current=$scope.consecutivo.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.consecutivo.model.estado})[0];
            $scope.tipoConsecutivo.current = $filter('filter')($scope.tipoConsecutivo.data, {id : $scope.consecutivo.model.tipoProceso})[0];
        
                          
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
    
    
  }
})();
/**=========================================================
 * Module: app.consecutivo.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .service('consecutivoHttp', consecutivoHttp);

  consecutivoHttp.$inject = ['$resource', 'END_POINT'];


  function consecutivoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            tipo : '@tipo'           
          },
          'url' : END_POINT + '/Cliente.svc/consecutivo/tipo/:tipo'
          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/consecutivo/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/consecutivo'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/consecutivo'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/consecutivo/:id/tipo'
      },
        'getTipoConsecutivos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Cliente.svc/tipoConsecutivo'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroconsecutivo'
      },
        getFuncionarios : {
        method : 'GET',
        isArray : true,
        url : END_POINT + '/General.svc/funcionarioBusqueda'
      }
     
    };
    return $resource( END_POINT + '/Cliente.svc/consecutivo/tipo', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.consecutivo.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .controller('consecutivoListController', consecutivoListController);

  consecutivoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'consecutivoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function consecutivoListController($scope, $filter, $state, ngDialog, tpl, consecutivoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
        
    
    var procesoSeleccionado= $state.params.filters.procesoSeleccionado;
	
		
	$scope.ESTADO_PROCESO = 'P'; 
    $scope.ESTADO_APROBADO= 'AP';	
    $scope.ESTADO_ANULADO= 'AN';	
    		

    $scope.consecutivos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreconsecutivos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.consecutivos.selectedAll = !$scope.consecutivos.selectedAll; 
        for (var key in $scope.consecutivos.selectedItems) {
          $scope.consecutivos.selectedItems[key].check = $scope.consecutivos.selectedAll;
        }
      },
      add : function() {
        
        if($scope.tipoConsecutivo.current.id){        
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      funcionarioId: 0,
                      tipo: $scope.tipoConsecutivo.current.id,
                      codigo: '',                      
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.consecutivo_add', params : { consecutivo: oport } });
            $state.go('app.consecutivo_add');
          
          }
          else{
              
              message.show("warning", "Debe seleccionar el tipo de proceso!!")
          }
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.consecutivo_edit', params : { consecutivo: item } });
        $state.go('app.consecutivo_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            consecutivoHttp.remove({}, { id: id }, function(response) {
                $scope.consecutivos.getData();
                message.show("success", "consecutivo eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.consecutivos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    consecutivoHttp.remove({}, { id: id }, function(response) {
                        $scope.consecutivos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.consecutivos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.consecutivos.filterText,
          "precision": false
        }];
        $scope.consecutivos.selectedItems = $filter('arrayFilter')($scope.consecutivos.dataSource, paramFilter);
        $scope.consecutivos.paginations.totalItems = $scope.consecutivos.selectedItems.length;
        $scope.consecutivos.paginations.currentPage = 1;
        $scope.consecutivos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.consecutivos.paginations.currentPage == 1 ) ? 0 : ($scope.consecutivos.paginations.currentPage * $scope.consecutivos.paginations.itemsPerPage) - $scope.consecutivos.paginations.itemsPerPage;
        $scope.consecutivos.data = $scope.consecutivos.selectedItems.slice(firstItem , $scope.consecutivos.paginations.currentPage * $scope.consecutivos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.consecutivos.data = [];
        $scope.consecutivos.loading = true;
        $scope.consecutivos.noData = false;
        var parametros= {
            "tipo":$scope.tipoConsecutivo.current.id                
        };          
        consecutivoHttp.getList({}, parametros,function(response) {
              $scope.consecutivos.selectedItems = response;
              $scope.consecutivos.dataSource = response;
            
              for(var i=0; i<$scope.consecutivos.dataSource.length; i++){
                $scope.consecutivos.nombreconsecutivos.push({id: i, nombre: $scope.consecutivos.dataSource[i]});
              }
            
              $scope.consecutivos.paginations.totalItems = $scope.consecutivos.selectedItems.length;
              $scope.consecutivos.paginations.currentPage = 1;
              $scope.consecutivos.changePage();
              $scope.consecutivos.loading = false;
              ($scope.consecutivos.dataSource.length < 1) ? $scope.consecutivos.noData = true : null;
              $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
      //CARGAMOS PROCESOS
    $scope.tipoConsecutivo = {
        current : {}, data:[],
        getData : function() {
            
            $rootScope.loadingVisible = true;
                 consecutivoHttp.getTipoConsecutivos({}, {}, function(response) {
                     $scope.tipoConsecutivo.data = $filter('orderBy')(response, 'id');
                     
                     if(procesoSeleccionado){        
      
                            $scope.tipoConsecutivo.current = $filter('filter')($scope.tipoConsecutivo.data, {id : procesoSeleccionado})[0];  
                    }
                     else{
                         $scope.tipoConsecutivo.current =$scope.tipoConsecutivo.data[0];
                     }
                     
                     $scope.consecutivos.getData();  
                    $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });
            
        }        
    }	   
    
	//CARGAMOS LOS TIPOS DE PROCESO
    $scope.tipoConsecutivo.getData();
    
        
   
        
	}
  
  
  })();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoController', contratoController);

  contratoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'contratoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function contratoController($scope, $filter, $state, LDataSource, contratoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var contrato = $state.params.contrato;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.contrato = {
		model : {
              id: 0,
              terceroId: 0,
              tipoContratoId: 0,            
              objetoId: 0,
              descripcion: '',
              fechaInicial: '',
              horaInicial: '',
              fechaFinal: '',
              estado: '',
              consecutivo: '',
              fechaAct: '',
              usuarioAct: '',
              fechaReg: '',
              usuarioReg: '',                        
              longitude: 0,
              latitude: 0
		}, 
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaInicial.isOpen = true;
        }
      },  
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaFinal.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.contrato', params : { filters : {procesoSeleccionado:contrato.tipo}, data : []} });
        $state.go('app.contrato');
          
      },
      save : function() {		  
       
		  
          
          if($scope.contrato.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{$scope.contrato.model.fechaInicial=Date.parse(new Date($scope.contrato.fechaInicial.value));}	
          
             if($scope.contrato.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{$scope.contrato.model.fechaFinal=Date.parse(new Date($scope.contrato.fechaFinal.value));}	
          	
          
                if(!$scope.tercerosCon.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.contrato.model.terceroId = $scope.tercerosCon.current.id;}
          
          		if($scope.contrato.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				if($scope.contrato.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.contrato.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoContrato.current){message.show("warning", "Tipo contrato requerido");return;}
				else{$scope.contrato.model.tipoContratoId= $scope.tipoContrato.current.codigo;}	
          
                if(!$scope.objetoContrato.current){message.show("warning", "Objeto contrato requerido");return;}
				else{$scope.contrato.model.objetoId= $scope.objetoContrato.current.codigo;}	
          
          
			//INSERTAR
            if($scope.contrato.model.id==0){
				
				$rootScope.loadingVisible = true;
				contratoHttp.save({}, $scope.contrato.model, function (data) { 
					
						contratoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.contrato.model=data;
							contrato.id=$scope.contrato.model.id; 
							if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}    
							if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));} 
                            
                            if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
							if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));} 	
                            
								
							message.show("success", "contrato creada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.contrato.model.id){
					$state.go('app.contrato');
				}else{
					
					contratoHttp.update({}, $scope.contrato.model, function (data) {                           
					  $rootScope.loadingVisible = false;
					       $scope.contrato.model=data;
						if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}    
						if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));}   		
                        
                        if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
                        if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));} 	
                        
                            
					   message.show("success", "contrato actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
          seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
          
        
      }
    //TIPO CONTRATO
    $scope.tipoContrato = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getTipoContrato({}, {}, function(response) {
            $scope.tipoContrato.data = $filter('orderBy')(response, 'codigo');
            if(!$scope.contrato.model){$scope.tipoContrato.current = $scope.tipoContrato.data[0];}
            else{$scope.tipoContrato.current = $filter('filter')($scope.tipoContrato.data, { codigo : $scope.contrato.model.tipoContratoId })[0];}
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setTipo : function(){
          $scope.contrato.model.tipoContratoId=$scope.tipoContrato.current.codigo;
    }}
    
    
    //OBJETO CONTRATO
    $scope.objetoContrato = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getObjetoContrato({}, {}, function(response) {
            $scope.objetoContrato.data = $filter('orderBy')(response, 'codigo');
            if(!$scope.contrato.model){$scope.objetoContrato.current = $scope.objetoContrato.data[0];}
            else{$scope.objetoContrato.current = $filter('filter')($scope.objetoContrato.data, { codigo : $scope.contrato.model.objetoId })[0];}
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setObjeto : function(){
          $scope.contrato.model.objetoId=$scope.objetoContrato.current.codigo;
    }}
    
    
$scope.participanteContrato = {
    model : {  
            id: 0,
            funcionarioId: 0,
            contratoId: 0,
            cargoId: 0,
            funcionario: '',
            cargo: ''		
		},
      current : {},
      data:[],
      getData : function() {
          if(contrato){
              
            if($scope.contrato.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              contratoHttp.getParticipantesContrato({}, { contratoId: $scope.contrato.model.id }, function(response) {
                  $scope.participanteContrato.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },
       add : function() {    
           
        var parameter = {
              participanteContrato : {              
                        id: 0,
                        funcionarioId: 0,
                        cargoId: 0,
                        contratoId: $scope.contrato.model.id,
                        funcionario: '',
                        cargo: ''
              }            
        };

        var modalInstance = $modal.open({
          templateUrl: 'app/views/contrato/participanteContrato_edit.html',
          controller: 'participanteContratoEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {
             $scope.participanteContrato.getData();
        });
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          contratoHttp.deleteParticipanteContrato({}, {participanteContratoId: item.id}, function(response){
            $scope.participanteContrato.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
          
        var parameter = {
          participanteContrato : item
        };
        var modalInstance = $modal.open({
        templateUrl: 'app/views/contrato/participanteContrato_edit.html',
          controller: 'participanteContratoEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {
            
             $scope.participanteContrato.getData();
        });
      }       
    }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'E', descripcion: 'Ejecución'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'});             
   
        }        
    } 
        //TERCEROS
    $scope.tercerosCon = {
      current : {}, data:[],
      getData : function() {
        $rootScope.loadingVisible = true;
            contratoHttp.getTerceros({}, {}, function(response) {  
            $scope.tercerosCon.data = response ;              
                
           if($scope.contrato.model.terceroId ){

                $scope.tercerosCon.current = $filter('filter')($scope.tercerosCon.data, { id : $scope.contrato.model.terceroId })[0];                   
           }
            else{
                $scope.tercerosCon.current=$scope.tercerosCon.data[0];                     
            }     
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
	//CARGAMOS LOS LISTADOS	
    
	$scope.tipoContrato.getData();
	$scope.objetoContrato.getData();    
    $scope.estados.getData();	
    $scope.tercerosCon.getData();      
	
	//CARGAMOS LOS DATOS DEL contrato	
	
	if(contrato.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.tipoContrato.current = $scope.tipoContrato.data[0];
      $scope.objetoContrato.current = $scope.objetoContrato.data[0];
      //$scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : contrato.tipo})[0]; 
      //$scope.contrato.model.tipo=contrato.tipo;    
        
        
    }
    else{   
        $rootScope.loadingVisible = true;        
        $scope.divParticipanteContrato=true;
		
        contratoHttp.read({},$state.params.contrato, function (data) { 
        $scope.contrato.model = data;		

        if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}
        if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));}  
        
        		
        if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
        if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));}   
            
        $scope.tercerosCon.current = $filter('filter')($scope.tercerosCon.data, { id : $scope.contrato.model.terceroId })[0];  
     
        //$scope.estados.current=$scope.contrato.model.estado;	            
        $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.contrato.model.estado})[0];
        $scope.tipoContrato.current = $filter('filter')($scope.tipoContrato.data, {codigo : $scope.contrato.model.tipoContratoId})[0];
        $scope.objetoContrato.current = $filter('filter')($scope.objetoContrato.data, {codigo : $scope.contrato.model.objetoId})[0];            
        
        $scope.participanteContrato.getData();
        
       
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      
    
  }
})();
/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .service('contratoHttp', contratoHttp);

  contratoHttp.$inject = ['$resource', 'END_POINT'];


  function contratoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getList' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/contrato/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/contrato'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/contrato'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/contrato/:id'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'getTipoContrato' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/TIPOCONTRATO'
      },
        'getObjetoContrato' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/OBJETOCONTRATO'
      },
        'getCargos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/CARGO'
      },
       'getFuncionarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/funcionarioBusqueda'
      },
        'getParticipantesContrato': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            contratoId : '@contratoId'           
          },
          'url' : END_POINT + '/Cliente.svc/contrato/:contratoId/participante'
      },
      'addParticipanteContrato': {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/contrato/participante'
      },
      'editParticipanteContrato': {
        'method':'PUT',
        'url' : END_POINT + '/Cliente.svc/contrato/participante'
      },
      'deleteParticipanteContrato': {
        'method':'DELETE',
        'params' : {
            participanteContratoId : '@participanteContratoId'           
          },
        'url' : END_POINT + '/Cliente.svc/contrato/participante/:participanteContratoId'
      }
        
     
    };
    return $resource( END_POINT + '/Cliente.svc/contrato', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.CONTRATO.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoListController', contratoListController);

  contratoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'contratoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function contratoListController($scope, $filter, $state, ngDialog, tpl, contratoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
		
	$scope.ESTADO_EJECUCION = 'E';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.ESTADO_CANCELADO= 'CA';		

    $scope.contratos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrecontratos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.contratos.selectedAll = !$scope.contratos.selectedAll; 
        for (var key in $scope.contratos.selectedItems) {
          $scope.contratos.selectedItems[key].check = $scope.contratos.selectedAll;
        }
      },
      add : function() {  
              var cont = {
                      id: 0,
                      terceroId: 0,
                      tipoContratoId: 0,
                      objetoId: 0,
                      descripcion: '',
                      fechaInicial: '',
                      fechaFinal: '',
                      estado: '',
                      consecutivo: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.contrato_add', params : { contrato: cont } });
            $state.go('app.contrato_add');  
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.contrato_edit', params : { contrato: item } });
        $state.go('app.contrato_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            contratoHttp.remove({}, { id: id }, function(response) {
                $scope.contratos.getData();
                message.show("success", "Contrato eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.contratos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    contratoHttp.remove({}, { id: id }, function(response) {
                        $scope.contratos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.contratos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.contratos.filterText,
          "precision": false
        }];
        $scope.contratos.selectedItems = $filter('arrayFilter')($scope.contratos.dataSource, paramFilter);
        $scope.contratos.paginations.totalItems = $scope.contratos.selectedItems.length;
        $scope.contratos.paginations.currentPage = 1;
        $scope.contratos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.contratos.paginations.currentPage == 1 ) ? 0 : ($scope.contratos.paginations.currentPage * $scope.contratos.paginations.itemsPerPage) - $scope.contratos.paginations.itemsPerPage;
        $scope.contratos.data = $scope.contratos.selectedItems.slice(firstItem , $scope.contratos.paginations.currentPage * $scope.contratos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.contratos.data = [];
        $scope.contratos.loading = true;
        $scope.contratos.noData = false;      
         contratoHttp.getList(function(response) {
             
          $scope.contratos.selectedItems = response;
          $scope.contratos.dataSource = response;
          for(var i=0; i<$scope.contratos.dataSource.length; i++){
            $scope.contratos.nombrecontratos.push({id: i, nombre: $scope.contratos.dataSource[i]});
          }
          $scope.contratos.paginations.totalItems = $scope.contratos.selectedItems.length;
          $scope.contratos.paginations.currentPage = 1;
          $scope.contratos.changePage();
          $scope.contratos.loading = false;
          ($scope.contratos.dataSource.length < 1) ? $scope.contratos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }  
    
	//CARGAMOS DATA    
    $scope.contratos.getData();  
        
 
	}
  
  
  })();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('participanteContratoEditController', participanteContratoEditController);

  participanteContratoEditController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'contratoHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION'];


  function participanteContratoEditController($scope, $filter, $state, $modalInstance, LDataSource, contratoHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION) {
      
    
      
      $scope.participanteContrato = {     
          model:{
            id: 0,
            funcionarioId: 0,
            cargoId: 0,
            contratoId: 0,
            funcionario: '',
            cargo: ''
          },
		fechaReg : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.contrato.fechaReg.isOpen = false;
            }
          },
         save:  function(){       
             
             
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.participanteContrato.model.funcionarioId = $scope.funcionarios.current.id;}
          
                if(!$scope.cargo.current){message.show("warning", "Cargo requerido");return;}
                else{$scope.participanteContrato.model.cargoId= $scope.cargo.current.codigo;}	

             var _http = new contratoHttp($scope.participanteContrato.model);
             if(  $scope.participanteContrato.model.id==0){
                 
                  _http.$addParticipanteContrato(function(response){                  
                      message.show("success", "Participante creado satisfactoriamente!!");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editParticipanteContrato(function(response){                  
                      message.show("success", "Participante actualizado satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
              $modalInstance.close();
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      $scope.participanteContrato.model=parameters.participanteContrato;
      
     //TIPO CONTRATO
    $scope.cargo = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getCargos({}, {}, function(response) {
            $scope.cargo.data = $filter('orderBy')(response, 'codigo');
            
            if(!$scope.participanteContrato.model){
                $scope.cargo.current = $scope.cargo.data[0];
                }
            else{
                $scope.cargo.current = $filter('filter')($scope.cargo.data, { codigo : $scope.participanteContrato.model.cargoId })[0];
                }
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setCargo : function(){
          $scope.participanteContrato.cargoId=$scope.cargo.current.codigo;
    }}
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            contratoHttp.getFuncionarios({}, {}, function(response) {
            
            $scope.funcionarios.data = response;            
            
            if ($scope.participanteContrato.model.funcionarioId!=0)
            {
              $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.participanteContrato.model.funcionarioId })[0];   
            }
            else{
                
                $scope.funcionarios.current=$scope.funcionarios.data[0];
            }

            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setFuncionario' : function() {
          $scope.participanteContrato.funcionarioId=$scope.funcionarios.current.id;
      }
    }   
	   //CARGAMOS LOS LISTADOS	
	   $scope.cargo.getData();
       $scope.funcionarios.getData();
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
    
       if($scope.participanteContrato.model.fechaReg){$scope.participanteContrato.fechaReg.value=new Date(parseFloat($scope.participanteContrato.model.fechaReg));}   
    
      
  }
})();
(function() {
    'use strict';

    angular
        .module('app.core')
        .config(coreConfig);

    coreConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$httpProvider'];
    function coreConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $httpProvider){
      
      var core = angular.module('app.core');
      // registering components after bootstrap
      core.controller = $controllerProvider.register;
      core.directive  = $compileProvider.directive;
      core.filter     = $filterProvider.register;
      core.factory    = $provide.factory;
      core.service    = $provide.service;
      core.constant   = $provide.constant;
      core.value      = $provide.value;
      
    }

})();
/**=========================================================
 * Module: constants.js
 * Define constants to inject across the application
 https://crossorigin.me/https://ec2-54-213-134-94.us-west-2.compute.amazonaws.com/LyqService/Services
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('APP_MEDIAQUERY', {
          'desktopLG':             1200,
          'desktop':                992,
          'tablet':                 768,
          'mobile':                 480
        })
    .constant('END_POINT',  'https://www.segurosmedicosinternacionales.com.co/rs/Services')
    //.constant('END_POINT','http://localhost:20559/Services')   
    .constant('REPORT_URL',  'http://54.213.134.94:82/ReportServer?/ServidorInformes/')   
    .constant('SERVER_URL', 'https://www.segurosmedicosinternacionales.com.co/rs/')
    .constant('TO_STATE', '')
    .constant('REGULAR_EXPRESION', {
      POSITIVES_INTEGERS : /^\d{0,10}?$/g,
      POSTIVES_DECIMALS : /(^[0-9]+(\.[0-9]+)?$)|(^[0-9]+\.$)/g, 
      ONLY_UPPERCASE : /\b^[A-Z]+$\b/g 
    });
})();
(function() {
  'use strict';

  angular
    .module('app.core')
    .run(appRun);

  appRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$templateCache', 'Colors', 'parametersOfState', 'TO_STATE'];

  function appRun($rootScope, $state, $stateParams, $window, $templateCache, Colors, parametersOfState, TO_STATE) {


    // Set reference to access them from any scope
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$storage = $window.localStorage;

    // Uncomment this to disable template cache
    /*$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
          if (typeof(toState) !== 'undefined'){
            $templateCache.remove(toState.templateUrl);
          }
      });*/

    // Allows to use branding color with interpolation
    // {{ colorByName('primary') }}
    $rootScope.colorByName = Colors.byName;

    // cancel click event easily
    $rootScope.cancel = function($event) {                                                                                             $event.stopPropagation();
                                         };

    // Hooks Example
    // ----------------------------------- 

    // Hook not found
    $rootScope.$on('$stateNotFound',
                   function(event, unfoundState/*, fromState, fromParams*/) {
      console.log(unfoundState.to); // "lazy.state"
      console.log(unfoundState.toParams); // {a:1, b:2}
      console.log(unfoundState.options); // {inherit:false} + default options
    });
    // Hook error
    $rootScope.$on('$stateChangeError',
                   function(event, toState, toParams, fromState, fromParams, error){
    });
    // Hook success

    TO_STATE = '';
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
      if (parametersOfState.get(toState) != undefined) {
    //    debugger;
        if (TO_STATE != toState.name) {
          TO_STATE = toState.name;
          var params = parametersOfState.get(toState);
          $state.go(toState.name, params );
          event.preventDefault();  
        } else {
          TO_STATE = "";
        }
      }
    });

  }



})();


  angular
    .module('app.core')
    .factory('message', ["$timeout", function ($timeout) {

    var instance = {}
    instance.messages = [];

    instance.show = function(type, message) {
      if (message !== null) {

      toastr.options.closeButton = (true);
      toastr.options.positionClass = 'toast-bottom-right';
      toastr.options.progressBar = true;
      toastr.options.preventDuplicates = true;

      toastr[type](message, '');

/*    toastr.options.closeButton = false;
    toastr.options.progressBar = false;
    toastr.options.debug = false;
    toastr.options.positionClass = 'toast-bottom-left';
    toastr.options.showDuration = 333;
    toastr.options.hideDuration = 333;
    toastr.options.timeOut = 4000;
    toastr.options.extendedTimeOut = 4000;
    toastr.options.showEasing = 'swing';
    toastr.options.hideEasing = 'swing';
    toastr.options.showMethod = 'slideDown';
    toastr.options.hideMethod = 'slideUp';*/


      }
    }
    
    return instance;
  }]);

/**=========================================================
 * Module: app.core.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('parametersOfState', parametersOfState);

  parametersOfState.$inject = [];


  function parametersOfState() {
    
    var _parametersOfState = {};
      
    return {
      set: function (state) {
        _parametersOfState[state.name] = JSON.stringify(state.params);
      },
      get : function (state) {
        return _parametersOfState[state.name] != undefined ? JSON.parse(_parametersOfState[state.name]) : state.params;
      }
    }
  }

})();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('buscarClienteController', buscarClienteController);

  buscarClienteController.$inject = ['$scope','$rootScope', '$filter', '$state', '$modalInstance', 'LDataSource', 'cotizacionHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function buscarClienteController($scope,$rootScope, $filter, $state, $modalInstance, LDataSource, cotizacionHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
            
      
      $scope.clientes = {   
            paginations : {
                maxSize : 3,
                itemsPerPage : 8,
                currentPage : 0,
                totalItems : 0
              },            
              selectedAll : false,
              filterText :'',
              dataSource : [],
              nombreclientes:[],
              selectedItems : [],
              data : [],
              noData : false,
              loading : false,
              selectAll : function() {
                $scope.productos.selectedAll = !$scope.productos.selectedAll; 
                for (var key in $scope.productos.selectedItems) {
                  $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
                }
              },
          model:{
             
              razonSocial:'', 
              identificacion:'',              
              email:'',
              telefono:'',
              tipo:-1,
              estado:-1     
          },   
           filter : function() {
            var paramFilter = [{
              "key": "$",
              "value": $scope.clientes.filterText,
              "precision": false
            }];
            $scope.clientes.selectedItems = $filter('arrayFilter')($scope.clientes.dataSource, paramFilter);
            $scope.clientes.paginations.totalItems = $scope.clientes.selectedItems.length;
            $scope.clientes.paginations.currentPage = 1;
            $scope.clientes.changePage();
          },
          changePage : function() {
            var firstItem = ($scope.clientes.paginations.currentPage == 1 ) ? 0 : ($scope.clientes.paginations.currentPage * $scope.clientes.paginations.itemsPerPage) - $scope.clientes.paginations.itemsPerPage;
            $scope.clientes.data = $scope.clientes.selectedItems.slice(firstItem , $scope.clientes.paginations.currentPage * $scope.clientes.paginations.itemsPerPage);
          }, 
          
      getData : function() {
          
        $scope.clientes.data = [];
        $scope.clientes.loading = true;
        $scope.clientes.noData = false;
          
        cotizacionHttp.getTerceros({}, $scope.clientes.model,function(response) {
          $scope.clientes.selectedItems = response;
          $scope.clientes.dataSource = response;
            
          for(var i=0; i<$scope.clientes.dataSource.length; i++){
            $scope.clientes.nombreclientes.push({id: i, nombre: $scope.clientes.dataSource[i]});
          }
          $scope.clientes.paginations.totalItems = $scope.clientes.selectedItems.length;
          $scope.clientes.paginations.currentPage = 1;
          $scope.clientes.changePage();
          $scope.clientes.loading = false;
          ($scope.clientes.dataSource.length < 1) ? $scope.clientes.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
          
          
      },               
      close : function() {
          $modalInstance.dismiss('cancel');
      },
      send : function() {
          $modalInstance.close();
      },
        setClient: function(item){
            
            parameters.cotizacion.identificacion=item.identificacion;
            parameters.cotizacion.terceroId=item.codigo;
            parameters.cotizacion.tercero=item.nombre;
            parameters.cotizacion.movil=item.telefono;
            parameters.cotizacion.direccion=item.direccion;
            parameters.cotizacion.email=item.email;
            parameters.cotizacion.crearCliente=0;
            
            
            $modalInstance.close();
        }
      }
      
  }
})();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('buscarPasajeroController', buscarPasajeroController);

  buscarPasajeroController.$inject = ['$scope','$rootScope', '$filter', '$state', '$modalInstance', 'LDataSource', 'cotizacionHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];

  function buscarPasajeroController($scope,$rootScope, $filter, $state, $modalInstance, LDataSource, cotizacionHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {            
      
      $scope.pasajeros = {   
            paginations : {
                maxSize : 3,
                itemsPerPage : 8,
                currentPage : 0,
                totalItems : 0
              },            
              selectedAll : false,
              filterText :'',
              dataSource : [],
              nombrepasajeros:[],
              selectedItems : [],
              data : [],
              noData : false,
              loading : false,
              selectAll : function() {
                $scope.productos.selectedAll = !$scope.productos.selectedAll; 
                for (var key in $scope.productos.selectedItems) {
                  $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
                }
              },
          model:{
             
              nombre:'', 
              apellido:'', 
              identificacion:'',              
              email:'',
              telefono:'',
              direccion:''                
          },   
           filter : function() {
            var paramFilter = [{
              "key": "$",
              "value": $scope.pasajeros.filterText,
              "precision": false
            }];
            $scope.pasajeros.selectedItems = $filter('arrayFilter')($scope.pasajeros.dataSource, paramFilter);
            $scope.pasajeros.paginations.totalItems = $scope.pasajeros.selectedItems.length;
            $scope.pasajeros.paginations.currentPage = 1;
            $scope.pasajeros.changePage();
          },
          changePage : function() {
            var firstItem = ($scope.pasajeros.paginations.currentPage == 1 ) ? 0 : ($scope.pasajeros.paginations.currentPage * $scope.pasajeros.paginations.itemsPerPage) - $scope.pasajeros.paginations.itemsPerPage;
            $scope.pasajeros.data = $scope.pasajeros.selectedItems.slice(firstItem , $scope.pasajeros.paginations.currentPage * $scope.pasajeros.paginations.itemsPerPage);
          }, 
          
      getData : function() {
          
        $scope.pasajeros.data = [];
        $scope.pasajeros.loading = true;
        $scope.pasajeros.noData = false;
          
        $scope.pasajeros.model.estado=$scope.estadoPasajero.current.value;
          
        cotizacionHttp.getPasajeros({}, $scope.pasajeros.model,function(response) {
            
          $scope.pasajeros.selectedItems = response;
          $scope.pasajeros.dataSource = response;
            
          for(var i=0; i<$scope.pasajeros.dataSource.length; i++){
            $scope.pasajeros.nombrepasajeros.push({id: i, nombre: $scope.pasajeros.dataSource[i]});
          }
          $scope.pasajeros.paginations.totalItems = $scope.pasajeros.selectedItems.length;
          $scope.pasajeros.paginations.currentPage = 1;
          $scope.pasajeros.changePage();
          $scope.pasajeros.loading = false;
          ($scope.pasajeros.dataSource.length < 1) ? $scope.pasajeros.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });          
          
      },               
      close : function() {
          $modalInstance.dismiss('cancel');
      },
      send : function() {
          $modalInstance.close();
      },
      setPasajero: function(item){  
            item.polizaId=parameters.poliza.idPoliza;
            parameters.poliza.pasajeros.push(item);
            $modalInstance.close();
        }
      }
            //CARGAMOS ESTADOS
    $scope.estadoPasajero = {             
          current : {}, data:[],
        getData : function() {            
            $scope.estadoPasajero.data.push({value:'1', descripcion: 'Activos'});
            $scope.estadoPasajero.data.push({value:'0', descripcion: 'Inactivos'});          
            $scope.estadoPasajero.current =$scope.estadoPasajero.data[0];
        }          
    }
       $scope.estadoPasajero.getData(); 	
  }
})();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('buscarPolizaController', buscarPolizaController);

  buscarPolizaController.$inject = ['$scope','$rootScope', '$filter', '$state', '$modalInstance', 'LDataSource', 'cotizacionHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function buscarPolizaController($scope,$rootScope, $filter, $state, $modalInstance, LDataSource, cotizacionHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
            
      
      $scope.polizas = {   
              paginations : {
                maxSize : 3,
                itemsPerPage : 8,
                currentPage : 0,
                totalItems : 0
              },            
              selectedAll : false,
              filterText :'',
              dataSource : [],
              nombrepolizas:[],
              selectedItems : [],
              data : [],
              noData : false,
              loading : false,
              selectAll : function() {
                $scope.productos.selectedAll = !$scope.productos.selectedAll; 
                for (var key in $scope.productos.selectedItems) {
                  $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
                }
              },
          model:{
              origen:0,
              destino:0,
              tipo:0, 
              inicio:'', 
              fin:'',              
              email:'',
              estadia:1,
              e1:'',
              e2:'',
              e3:'',
              e4:'',
              e5:'',
              e6:'',
              e7:'',
              e8:'',
              aseguradora:'',
              ordenamiento:'',
              dias:0,
              pasajeros:0
          },   
           filter : function() {
            var paramFilter = [{
              "key": "$",
              "value": $scope.polizas.filterText,
              "precision": false
            }];
            $scope.polizas.selectedItems = $filter('arrayFilter')($scope.polizas.dataSource, paramFilter);
            $scope.polizas.paginations.totalItems = $scope.polizas.selectedItems.length;
            $scope.polizas.paginations.currentPage = 1;
            $scope.polizas.changePage();
          },
          changePage : function() {
            var firstItem = ($scope.polizas.paginations.currentPage == 1 ) ? 0 : ($scope.polizas.paginations.currentPage * $scope.polizas.paginations.itemsPerPage) - $scope.polizas.paginations.itemsPerPage;
            $scope.polizas.data = $scope.polizas.selectedItems.slice(firstItem , $scope.polizas.paginations.currentPage * $scope.polizas.paginations.itemsPerPage);
          }, 
          
      getData : function() {
        $scope.polizas.data = [];
        $scope.polizas.loading = true;
        $scope.polizas.noData = false;
          
        $scope.polizas.model.origen=parameters.cotizacion.origen;
        $scope.polizas.model.destino=parameters.cotizacion.destino;
        $scope.polizas.model.tipo=parameters.cotizacion.tipo;
        $scope.polizas.model.inicio= $scope.polizas.formatdate (parameters.cotizacion.fechaInicio);
        $scope.polizas.model.fin=    $scope.polizas.formatdate (parameters.cotizacion.fechaFin);             
        $scope.polizas.model.email='';
        $scope.polizas.model.estadia=1;
        $scope.polizas.model.e1=parameters.cotizacion.e1!=undefined?parameters.cotizacion.e1:"";
        $scope.polizas.model.e2=parameters.cotizacion.e2!=undefined?parameters.cotizacion.e2:"";
        $scope.polizas.model.e3=parameters.cotizacion.e3!=undefined?parameters.cotizacion.e3:"";
        $scope.polizas.model.e4=parameters.cotizacion.e4!=undefined?parameters.cotizacion.e4:"";
        $scope.polizas.model.e5=parameters.cotizacion.e5!=undefined?parameters.cotizacion.e5:"";
        $scope.polizas.model.e6=parameters.cotizacion.e6!=undefined?parameters.cotizacion.e6:"";
        $scope.polizas.model.e7=parameters.cotizacion.e7!=undefined?parameters.cotizacion.e7:"";
        $scope.polizas.model.e8=parameters.cotizacion.e8!=undefined?parameters.cotizacion.e8:"";
        $scope.polizas.model.aseguradora='';
        $scope.polizas.model.ordenamiento=''; 
          
        cotizacionHttp.getPolizas({}, $scope.polizas.model,function(response) {
          $scope.polizas.selectedItems = response.polizas;
          $scope.polizas.dataSource = response.polizas;
            
            $scope.polizas.model.dias=response.dias;
            $scope.polizas.model.pasajeros=response.pasajeros;
            
            
          for(var i=0; i<$scope.polizas.dataSource.length; i++){
            $scope.polizas.nombrepolizas.push({id: i, nombre: $scope.polizas.dataSource[i]});
          }
          $scope.polizas.paginations.totalItems = $scope.polizas.selectedItems.length;
          $scope.polizas.paginations.currentPage = 1;
          $scope.polizas.changePage();
          $scope.polizas.loading = false;
          ($scope.polizas.dataSource.length < 1) ? $scope.polizas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      },
               
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          },
           formatdate(t){    

            //var today = new Date();
            var dd = t.getDate();
            var mm = t.getMonth()+1; //January is 0!

            var yyyy = t.getFullYear();
            if(dd<10){
                dd='0'+dd;
            } 
            if(mm<10){
                mm='0'+mm;
            } 
            //2018-01-15
            return yyyy+'-'+mm+'-'+dd;         
       
        },
          save : function() {             
          
          
            var  lst= $scope.polizas.dataSource.filter( element => element.check==true  );
            parameters.cotizacion.polizas=lst;
            $modalInstance.close();
          }
          
      }
       	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      $scope.polizas.getData()
      
             
        
     
        
      
  }
})();
/**=========================================================
 * Module: app.cotizacion.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('cotizacionController', cotizacionController);

 cotizacionController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'cotizacionHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal'];   

  function cotizacionController($scope, $rootScope, $state,$modalInstance,$filter,cotizacionHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window,$modal) {      
      
     
      var cotizacion =parameters.cotizacion;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.cotizacion = {
		model : {
              id: 0,
              idRow: "",
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,            
              padreId: 0,
              crearCliente:0,              
			  observaciones: '',
			  estado: 1,			  
              fechaVencimiento: '',
              fechaInicio: '',
              fechaFin: '' ,
              cotizacionId:0, 
              polizas:[]
		},deletePoliza: function(p) {
               $scope.cotizacion.model.polizas.splice($scope.cotizacion.model.polizas.indexOf(p), 1);     
          },
      back : function() {
          parametersOfState.set({ name : 'app.cotizacion', params : { filters : {procesoSeleccionado:cotizacion.tipo}, data : []} });
        $state.go('app.cotizacion');
          
      },
      save : function() {	
            
            //if($scope.cotizacion.model.identificacion==""){message.show("warning", "Identifiación del cliente requerido");return;}
          
            if($scope.cotizacion.model.tercero==""){message.show("warning", "Razón social del cliente requerido");return;}
            if($scope.cotizacion.model.email==""){message.show("warning", "Email del cliente requerido");return; }
            if( $scope.cotizacion.model.polizas.length ==0){message.show("warning", "Debe agregar polizas");return;}  
                
            try{
                if($scope.cotizacion.model.fechaVencimiento==""){message.show("warning", "Debe establecer una fecha de vencimiento");return;}
                else{$scope.cotizacion.model.fechaVencimiento=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaVencimiento);}
            }
            catch(err) {    
            }
            
            try{                
                $scope.cotizacion.model.fechaInicio=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaInicio);
            }
            catch(err) {
            }
            try{
                
                $scope.cotizacion.model.fechaFin=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaFin);
            }
            catch(err) {
            }
            
            $scope.cotizacion.model.grupoId= $scope.grupos.current.value;
            $scope.cotizacion.model.prioridadId= $scope.prioridades.current.value;
            $scope.cotizacion.model.viaContactoId= $scope.viasContacto.current.value;
            $scope.cotizacion.model.interes= $scope.interes.current.value;
            $scope.cotizacion.model.formaPagoId= $scope.formasPago.current.value;
            $scope.cotizacion.model.monedaId= $scope.monedas.current.value;
            $scope.cotizacion.model.destino= $scope.destinos.current.value;
            $scope.cotizacion.model.tipoViaje= $scope.tiposViaje.current.value;
          
            if($scope.universidades.current){$scope.cotizacion.model.universidadId = $scope.universidades.current.codigo;
            }else{$scope.cotizacion.model.universidadId =0;}   
          
          
            if($scope.tipoCliente.current){$scope.cotizacion.model.tipoCliente = $scope.tipoCliente.current.value;
            }else{message.show("warning", "Debe especificar el tipo de cliente");return}   
       
            if($scope.intermediarios.current){$scope.cotizacion.model.intermediarioId = $scope.intermediarios.current.codigo;
            }else{$scope.cotizacion.model.intermediarioId ='00000000-0000-0000-0000-000000000000';} 
				
            $rootScope.loadingVisible = true;
            cotizacionHttp.save({}, $scope.cotizacion.model, function (data) { 	
                       
                       
                        if ($scope.cotizacion.model.id==0){
                            message.show("success", "Cotización creada satisfactoriamente!!");
                        }
                        else{message.show("success", "Cotización actualizada satisfactoriamente!!");}
                        // $scope.cotizacion.model=data;                        
                        $scope.cotizacion.model.id=data.id;		
                        $scope.cotizacion.model.intermediarioId=data.intermediarioId;
                
                        $scope.cotizacion.model.fechaVencimiento=new Date(data.fechaVencimiento);
                        $scope.cotizacion.model.fechaInicio=new Date(data.fechaInicio);
                        $scope.cotizacion.model.fechaFin=new Date(data.fechaFin);
                        	
                        $scope.btnEnviarCotizacion=true;
                        $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });				
		
		},          
          anular: function() {   
              
            if (confirm("Desea anular la cotización?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.cotizacion.model.estado=3;//estado anulado

                try{                
                    $scope.cotizacion.model.fechaInicio=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.cotizacion.model.fechaFin=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaFin);
                }
                catch(err) {
                }
                       cotizacionHttp.save({}, $scope.cotizacion.model, function (data) {

                            $rootScope.loadingVisible = false;
                            message.show("success", "Cotización anulada satisfactoriamente!!");	
                            $scope.cotizacion.close();               


                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
          generarPedido: function() {   
              
            if (confirm("Desea crear el pedido?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.cotizacion.model.estado=4;//estado anulado

                try{                
                    $scope.cotizacion.model.fechaInicio=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.cotizacion.model.fechaFin=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaFin);
                }
                catch(err) {
                }
                       cotizacionHttp.save({}, $scope.cotizacion.model, function (data) {

                            $rootScope.loadingVisible = false;
                            message.show("success", "Pedido creado satisfactoriamente!!");	
                             var modalInstance = $modal.open({
                                  templateUrl: 'app/views/pedido/pedido_form.html',
                                  controller: 'pedidoController',
                                  size: 'lg',
                                  resolve: {
                                    parameters: { pedido: data,vpedidos:null}
                                  }
                                });
                                modalInstance.result.then(function (parameters) {
                                    $scope.pedidos.getData();
                                });      
                           
                            $scope.cotizacion.close();     

                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.id,
                         referencia : 'cotizacion' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
      
      ,agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.idRow,
                         referencia : 'cotizacion' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.cotizacion.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.padreId,
                         referencia : 'cotizacion' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },          
      buscarCliente : function() { 

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion : $scope.cotizacion.model}
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },   
    verSeguimientoHistorico : function() {            
    var modalInstance = $modal.open({
      templateUrl: 'app/views/seguimiento/seguimiento.html',
      controller: 'seguimientoController',
      size: 'lg',
      resolve: {
        parameters: { id : $scope.cotizacion.model.padreId,
                     referencia : 'cotizacion' }
      }
    });
    modalInstance.result.then(function (parameters) {
    });

    }
  ,buscarPolizas : function() {  
      
            if(!$scope.cotizacion.model.fechaInicio){message.show("warning", "Fecha inicial requerida");return;}
            if(!$scope.cotizacion.model.fechaFin){message.show("warning", "Fecha final requerida");return;}               
            $scope.cotizacion.model.origen= $scope.origenes.current.value;
            $scope.cotizacion.model.destino= $scope.destinos.current.value;
            $scope.cotizacion.model.tipo= $scope.tiposViaje.current.value;
          
            var modalInstance = $modal.open({
              templateUrl: 'app/views/cotizacion/buscarPolizas.html',
              controller: 'buscarPolizaController',
              size: 'lg',
              resolve: {
                parameters: { cotizacion : $scope.cotizacion.model}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.padreId,
                         referencia : 'cotizacion' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },  
        close : function() {
        
            
          if(parameters.vcotizaciones!=null && parameters.vcotizaciones!=undefined)
            parameters.vcotizaciones.getData();
            
        $modalInstance.dismiss('cancel');
      },
        formatdate(t){    
            try{ 
                    var dd = t.getDate();
                    var mm = t.getMonth()+1; //January is 0!

                    var yyyy = t.getFullYear();
                    if(dd<10){
                        dd='0'+dd;
                    } 
                    if(mm<10){
                        mm='0'+mm;
                    } 
                    //2018-01-15
                    return yyyy+'-'+mm+'-'+dd;   
                }
                    catch(err) {
                        return t;
                    }
       
        },
      enviarCotizacion(){

                $rootScope.loadingVisible = true;   
                    
                    if($scope.cotizacion.model.intermediarioId=="00000000-0000-0000-0000-000000000000"){
                      $rootScope.loadingVisible = true;
                       cotizacionHttp.enviarCotizacionCrm({}, {codigo: $scope.cotizacion.model.id }, function(response){
                          message.show("success", "Cotización enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
                    else{

                    $rootScope.loadingVisible = true;
                     cotizacionHttp.enviarCotizacionIntermediario({}, {codigo: $scope.cotizacion.model.id }, function(response){
                          message.show("success", "Cotización enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
      },
        agregarCliente: function(data){
            
            $scope.crearCliente=true;
            $scope.cotizacion.model.crearCliente=1;
        },
          calcularDescuento:function(data){            
            
               if( data.descuento>0 && data.precioOriginal){
                   data.precio=data.precioOriginal - (data.precioOriginal* (data.descuento/100));
                   data.precioCop=data.precio*data.trm;
               }
        }
          
    }
    //TERCEROS
    $scope.clientes = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            generalHttp.getTerceros({}, {}, function(response) {                
            $scope.clientes.data = response ;              
                
               if($scope.cotizacion.model){
                    
                    $scope.clientes.current = $filter('filter')($scope.clientes.data, { id : $scope.cotizacion.model.clienteId })[0];                   
               }
                else{
                    $scope.clientes.current=$scope.Clientes.data[0];                     
                }               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.cotizacion.model.terceroId=$scope.clientes.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.cotizacion.model.terceroId=$scope.clientes.current.id;
         // $scope.contactosOp.getData();
      }
    }
    
    //VIA CONTACTO
    $scope.viasContacto = {
      current : {},
      data:[],
      getData : function() {
                $scope.viasContacto.data.push({value:1, descripcion: 'Cliente'});
                $scope.viasContacto.data.push({value:2, descripcion: 'Facebook'});     
                $scope.viasContacto.data.push({value:3, descripcion: 'E-mail'}); 
                $scope.viasContacto.data.push({value:4, descripcion: 'Directorio'});  
                $scope.viasContacto.data.push({value:5, descripcion: 'Referido'});  
                $scope.viasContacto.data.push({value:6, descripcion: 'Compra online'});  
                $scope.viasContacto.data.push({value:7, descripcion: 'Intermediario'});  
                $scope.viasContacto.data.push({value:8, descripcion: 'Google'});
                $scope.viasContacto.data.push({value:9, descripcion: 'Whatsapp'});
                $scope.viasContacto.data.push({value:10, descripcion: 'Zopim'});  
                $scope.viasContacto.data.push({value:11, descripcion: 'Ori/Volante'}); 
                $scope.viasContacto.current=$scope.viasContacto.data[0];   
      },
      setViaContacto : function(){
          $scope.cotizacion.model.viaContactoId=$scope.viasContacto.current.codigo;
    }}
    //PRIORIDADES
    $scope.prioridades = {
      current : {},
      data:[],
      getData : function() {
                $scope.prioridades.data.push({value:1, descripcion: 'NORMAL(15DIAS)'});
                $scope.prioridades.data.push({value:2, descripcion: 'ALTA(48HORAS)'});     
                $scope.prioridades.data.push({value:3, descripcion: 'CRITICA(8HORAS)'}); 
                $scope.prioridades.data.push({value:4, descripcion: 'BAJA(1MES)'});  
                $scope.prioridades.data.push({value:5, descripcion: 'BAJA(2MESES)'});  
                $scope.prioridades.data.push({value:6, descripcion: 'BAJA(3MESES)'});  
                $scope.prioridades.data.push({value:7, descripcion: 'INTERM( 6MESES)'}); 
                $scope.prioridades.current=$scope.prioridades.data[0];   
      },
      setPrioridad : function(){
          $scope.cotizacion.model.prioridadId=$scope.prioridades.current.value;
    }}
    //PORCENTAJE DE INTERES
    $scope.interes = {
      current : {},
      data:[],
      getData : function() {
                $scope.interes.data.push({value:10, descripcion: '10'});
                $scope.interes.data.push({value:20, descripcion: '20'});     
                $scope.interes.data.push({value:30, descripcion: '30'});     
                $scope.interes.data.push({value:40, descripcion: '40'});     
                $scope.interes.data.push({value:50, descripcion: '50'});     
                $scope.interes.data.push({value:60, descripcion: '60'});     
                $scope.interes.data.push({value:70, descripcion: '70'});
                $scope.interes.data.push({value:80, descripcion: '80'});
                $scope.interes.data.push({value:90, descripcion: '90'});
                $scope.interes.data.push({value:100, descripcion: '100'});
                $scope.interes.current=$scope.interes.data[0];    
      },
      setInteres : function(){
          $scope.cotizacion.model.interes=$scope.interes.current.value;
    }}
    //TIPO DE CLIENTE
    $scope.tipoCliente = {
      current : {},
      data:[],
      getData : function() {
                $scope.tipoCliente.data.push({value:1, descripcion: 'CLIENTE CON FECHA DE VIAJE'});
                $scope.tipoCliente.data.push({value:2, descripcion: 'ESTUDIANTES'});     
                $scope.tipoCliente.data.push({value:3, descripcion: 'PROMOCION'});
                $scope.tipoCliente.data.push({value:4, descripcion: 'TRAMITE DE VISA'});
                $scope.tipoCliente.current=$scope.tipoCliente.data[0];    
      },
      settipoCliente : function(){
          $scope.cotizacion.model.tipoCliente=$scope.tipoCliente.current.value;
    }}
    //GRUPO
    $scope.grupos = {
      current : {},
      data:[],
      getData : function() {
                $scope.grupos.data.push({value:2, descripcion: 'Asesores'});    
                $scope.grupos.data.push({value:1, descripcion: 'Administrativo'});                 
                $scope.grupos.data.push({value:3, descripcion: 'Lista de pólizas'});     
                $scope.grupos.data.push({value:4, descripcion: 'Servicio al Cliente'});
                $scope.grupos.current=$scope.grupos.data[0];                   
          },
      setGrupo : function(){
          $scope.cotizacion.model.grupoId=$scope.grupos.current.value;
    }}
    //MONEDA
    $scope.monedas  = {
            current: {},
            data: [],
            getData: function() {
                $scope.monedas.data.push({value:2, descripcion: 'Dolar'});
                $scope.monedas.data.push({value:4, descripcion: 'Pesos Colombianos'});    
                $scope.monedas.current=$scope.monedas.data[0];    
            }
        }
    

   
      
    $scope.formasPago  = {
            current: {},
            data: [],
            getData: function() {
                $scope.formasPago.data.push({value:0, descripcion: 'Pesos en efectivo'});
                $scope.formasPago.data.push({value:1, descripcion: 'Tarjeta de credito'});     
                $scope.formasPago.data.push({value:2, descripcion: 'Pse'});     
                $scope.formasPago.data.push({value:3, descripcion: 'Consig Bogota'});     
                $scope.formasPago.data.push({value:4, descripcion: 'Consig Bancolombia'});     
                $scope.formasPago.data.push({value:5, descripcion: 'Consig Colpatria'});     
                $scope.formasPago.data.push({value:6, descripcion: 'Cheque'});
                $scope.formasPago.data.push({value:8, descripcion: 'Dolares'});  
                $scope.formasPago.data.push({value:9, descripcion: 'Tarjeta debito'});  
                $scope.formasPago.data.push({value:10, descripcion: 'Credito'});  
                $scope.formasPago.data.push({value:11, descripcion: 'Online'});  
                $scope.formasPago.data.push({value:12, descripcion: 'Giro'});  
                $scope.formasPago.current=$scope.formasPago.data[0];    
            }
        }
      
        $scope.origenes  = {
            current: {},
            data: [],
            getData: function() {
      
                        $scope.origenes.data.push({value: 48, descripcion: 'Colombia' });
                        $scope.origenes.data.push({value: 1, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 2, descripcion: 'Argentina' });
                        $scope.origenes.data.push({value: 6, descripcion: 'Alemania' });
                        $scope.origenes.data.push({value: 12, descripcion: 'Arabia Saudita' });
                        $scope.origenes.data.push({value: 15, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 16, descripcion: 'Australia' });
                        $scope.origenes.data.push({value: 19, descripcion: 'Bahamas' });
                        $scope.origenes.data.push({value: 21, descripcion: 'Barbados' });
                        $scope.origenes.data.push({value: 25, descripcion: 'Bemudas' });
                        $scope.origenes.data.push({value: 28, descripcion: 'Bolivia' });
                        $scope.origenes.data.push({value: 31, descripcion: 'Brasil' });
                        $scope.origenes.data.push({value: 33, descripcion: 'Bulgaria' });
                        $scope.origenes.data.push({value: 37, descripcion: 'Belgica' });
                        $scope.origenes.data.push({value: 38, descripcion: 'Cabo Verde' });
                        $scope.origenes.data.push({value: 39, descripcion: 'Islas Caiman' });
                        $scope.origenes.data.push({value: 40, descripcion: 'Camboya' });
                        $scope.origenes.data.push({value: 41, descripcion: 'Camerun' });
                        $scope.origenes.data.push({value: 42, descripcion: 'Canada' });
                        $scope.origenes.data.push({value: 45, descripcion: 'Chile' });
                        $scope.origenes.data.push({value: 52, descripcion: 'Corea del Norte' });
                        $scope.origenes.data.push({value: 53, descripcion: 'Corea del Sur' });
                        $scope.origenes.data.push({value: 54, descripcion: 'Costa de Marfil' });
                        $scope.origenes.data.push({value: 55, descripcion: 'Costa Rica' });
                        $scope.origenes.data.push({value: 56, descripcion: 'Croacia' });
                        $scope.origenes.data.push({value: 57, descripcion: 'Cuba' });
                        $scope.origenes.data.push({value: 59, descripcion: 'Dinamarca' });
                        $scope.origenes.data.push({value: 61, descripcion: 'Ecuador' });
                        $scope.origenes.data.push({value: 62, descripcion: 'Egipto' });
                        $scope.origenes.data.push({value: 63, descripcion: 'El Salvador' });
                        $scope.origenes.data.push({value: 64, descripcion: 'Emiratos Arabes Unvalueos' });
                        $scope.origenes.data.push({value: 66, descripcion: 'Eslovaquia' });
                        $scope.origenes.data.push({value: 67, descripcion: 'Eslovenia' });
                        $scope.origenes.data.push({value: 68, descripcion: 'España' });
                        $scope.origenes.data.push({value: 69, descripcion: 'Estados Unvalueos' });
                        $scope.origenes.data.push({value: 70, descripcion: 'Estonia' });
                        $scope.origenes.data.push({value: 71, descripcion: 'Etiopia' });
                        $scope.origenes.data.push({value: 72, descripcion: 'Islas Feroe' });
                        $scope.origenes.data.push({value: 73, descripcion: 'Filipinas' });
                        $scope.origenes.data.push({value: 74, descripcion: 'Finlandia' });
                        $scope.origenes.data.push({value: 75, descripcion: 'Fiji' });
                        $scope.origenes.data.push({value: 76, descripcion: 'Francia'});
                        $scope.origenes.data.push({value: 83, descripcion: 'Grecia' });
                        $scope.origenes.data.push({value: 84, descripcion: 'Groenlandia' });
                        $scope.origenes.data.push({value: 86, descripcion: 'Guatemala' });
                        $scope.origenes.data.push({value: 87, descripcion: 'Guernsey' });
                        $scope.origenes.data.push({value: 88, descripcion: 'Guinea' });
                        $scope.origenes.data.push({value: 89, descripcion: 'Guinea Ecuatorial' });
                        $scope.origenes.data.push({value: 90, descripcion: 'Guinea-Bissau' });
                        $scope.origenes.data.push({value: 91, descripcion: 'Guyana' });
                        $scope.origenes.data.push({value: 92, descripcion: 'Haiti' });
                        $scope.origenes.data.push({value: 93, descripcion: 'Honduras' });
                        $scope.origenes.data.push({value: 94, descripcion: 'Hong kong' });
                        $scope.origenes.data.push({value: 95, descripcion: 'Hungria' });
                        $scope.origenes.data.push({value: 96, descripcion: 'India' });
                        $scope.origenes.data.push({value: 97, descripcion: 'Indonesia' });
                        $scope.origenes.data.push({value: 98, descripcion: 'Iraq' });
                        $scope.origenes.data.push({value: 99, descripcion: 'Irlanda' });
                        $scope.origenes.data.push({value: 100, descripcion: 'Iran' });
                        $scope.origenes.data.push({value: 101, descripcion: 'Islandia' });
                        $scope.origenes.data.push({value: 102, descripcion: 'Israel' });
                        $scope.origenes.data.push({value: 103, descripcion: 'Italia' });
                        $scope.origenes.data.push({value: 104, descripcion: 'Jamaica' });
                        $scope.origenes.data.push({value: 105, descripcion: 'Japon' });
                        $scope.origenes.data.push({value: 117, descripcion: 'Liberia' });
                        $scope.origenes.data.push({value: 118, descripcion: 'Libia' });
                        $scope.origenes.data.push({value: 120, descripcion: 'Lituania' });
                        $scope.origenes.data.push({value: 121, descripcion: 'Luxemburgo' });
                        $scope.origenes.data.push({value: 122, descripcion: 'Libano' });
                        $scope.origenes.data.push({value: 128, descripcion: 'Maldivas' });
                        $scope.origenes.data.push({value: 129, descripcion: 'Malta' });
                        $scope.origenes.data.push({value: 130, descripcion: 'Islas Malvinas' });
                        $scope.origenes.data.push({value: 131, descripcion: 'Mali' });
                        $scope.origenes.data.push({value: 134, descripcion: 'Marruecos' });
                        $scope.origenes.data.push({value: 144, descripcion: 'Mexico' });
                        $scope.origenes.data.push({value: 145, descripcion: 'Monaco' });
                        $scope.origenes.data.push({value: 146, descripcion: 'Namibia' });
                        $scope.origenes.data.push({value: 147, descripcion: 'Nauru' });
                        $scope.origenes.data.push({value: 149, descripcion: 'Nepal' });
                        $scope.origenes.data.push({value: 150, descripcion: 'Nicaragua' });
                        $scope.origenes.data.push({value: 151, descripcion: 'Nigeria' });
                        $scope.origenes.data.push({value: 152, descripcion: 'Niue' });
                        $scope.origenes.data.push({value: 154, descripcion: 'Noruega' });
                        $scope.origenes.data.push({value: 155, descripcion: 'Nueva Caledonia' });
                        $scope.origenes.data.push({value: 156, descripcion: 'Nueva Zelanda' });
                        $scope.origenes.data.push({value: 157, descripcion: 'Niger' });
                        $scope.origenes.data.push({value: 158, descripcion: 'Oman' });
                        $scope.origenes.data.push({value: 160, descripcion: 'Pakistan' });
                        $scope.origenes.data.push({value: 161, descripcion: 'Palaos' });
                        $scope.origenes.data.push({value: 162, descripcion: 'Palestina' });
                        $scope.origenes.data.push({value: 163, descripcion: 'Panama' });
                        $scope.origenes.data.push({value: 165, descripcion: 'Paraguay' });
                        $scope.origenes.data.push({value: 167, descripcion: 'Peru' });
                        $scope.origenes.data.push({value: 170, descripcion: 'Polonia' });
                        $scope.origenes.data.push({value: 171, descripcion: 'Portugal' });
                        $scope.origenes.data.push({value: 172, descripcion: 'Puerto Rico' });
                        $scope.origenes.data.push({value: 173, descripcion: 'Reino Unvalueo' });
                        $scope.origenes.data.push({value: 175, descripcion: 'Republica Checa' });
                        $scope.origenes.data.push({value: 176, descripcion: 'Republica Dominicana' });
                        $scope.origenes.data.push({value: 178, descripcion: 'Rumania' });
                        $scope.origenes.data.push({value: 179, descripcion: 'Rusia' });
                        $scope.origenes.data.push({value: 197, descripcion: 'Singapur' });
                        $scope.origenes.data.push({value: 198, descripcion: 'Siria' });
                        $scope.origenes.data.push({value: 201, descripcion: 'Sri Lanka' });
                        $scope.origenes.data.push({value: 203, descripcion: 'Sudafrica' });
                        $scope.origenes.data.push({value: 206, descripcion: 'Suecia' });
                        $scope.origenes.data.push({value: 207, descripcion: 'Suiza' });
                        $scope.origenes.data.push({value: 210, descripcion: 'Tailandia' });
                        $scope.origenes.data.push({value: 211, descripcion: 'Taiwan' });
                        $scope.origenes.data.push({value: 219, descripcion: 'Trinvaluead y Tobajo' });
                        $scope.origenes.data.push({value: 222, descripcion: 'Turquia' });
                        $scope.origenes.data.push({value: 225, descripcion: 'Ucrania' });
                        $scope.origenes.data.push({value: 227, descripcion: 'Uruguay' });
                        $scope.origenes.data.push({value: 230, descripcion: 'Ciudad del Vaticano' });
                        $scope.origenes.data.push({value: 231, descripcion: 'Venezuela' });
                        $scope.origenes.data.push({value: 239, descripcion: 'Chipre' });
                        $scope.origenes.data.push({value: 240, descripcion: 'Guam' });
                        $scope.origenes.data.push({value: 241, descripcion: 'Hawai' });
                        $scope.origenes.data.push({value: 246, descripcion: 'Boniaire' });
                        $scope.origenes.current=$scope.origenes.data[0];    
            }
        }  
      
        $scope.destinos  = {
            current: {},
            data: [],
            getData: function() {
                
                $scope.destinos.data.push({value:1, descripcion: 'Sur América'});
                $scope.destinos.data.push({value:2, descripcion: 'Centro América'});     
                $scope.destinos.data.push({value:3, descripcion: 'Norte América'});                         
                $scope.destinos.data.push({value:5, descripcion: 'Europa'});     
                $scope.destinos.data.push({value:6, descripcion: 'Asia'});     
                $scope.destinos.data.push({value:7, descripcion: 'África'}); 
                $scope.destinos.data.push({value:8, descripcion: 'Oceanía'}); 
                $scope.destinos.data.push({value:9, descripcion: 'Antillas Holandesas'}); 
                $scope.destinos.data.push({value:10, descripcion: 'Internacional'}); 
                $scope.destinos.data.push({value:11, descripcion: 'Colombia'}); 
                $scope.destinos.data.push({value:13, descripcion: 'Parques Nacionales'}); 
                $scope.destinos.current=$scope.destinos.data[0]; 
             }
        }
      $scope.tiposViaje  = {
            current: {},
            data: [],
            getData: function() {  
                
                $scope.tiposViaje.data.push({value:11, descripcion: 'Estudio'});
                $scope.tiposViaje.data.push({value:12, descripcion: 'Larga Estadía (más de 60 días).'});     
                $scope.tiposViaje.data.push({value:13, descripcion: 'Corta Estadia (menos de 60 días)'});     
                $scope.tiposViaje.data.push({value:14, descripcion: 'MultiViajes'});     
                $scope.tiposViaje.data.push({value:15, descripcion: 'Estudio Con Responsabilidad Civil'});     
                $scope.tiposViaje.data.push({value:16, descripcion: 'Futura Mamá'});     
                $scope.tiposViaje.data.push({value:17, descripcion: 'Atención médica por Pre-existencia'});   
                $scope.tiposViaje.data.push({value:18, descripcion: 'Seguro obligatorio Parques Nacionales'});   
                $scope.tiposViaje.current=$scope.tiposViaje.data[0];
            }
      }
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            cotizacionHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.cotizacion.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.cotizacion.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.cotizacion.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.cotizacion.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }

    //INTERMEDIARIOS
    $scope.intermediarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            cotizacionHttp.getIntermediarios({}, {}, function(response) {                
            $scope.intermediarios.data = response ;              
                
               if($scope.cotizacion.model){
                    
                    //$scope.intermediarios.current = $filter('filter')($scope.intermediarios.data, { id : $scope.cotizacion.model.intermediarioId })[0];                   
               }                           
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setIntermediario' : function() {
          $scope.cotizacion.model.intermediarioId=$scope.intermediarios.current.id;
      }
    }      
    //UNIVERSIDADES
    $scope.universidades = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            cotizacionHttp.getUniversidades({}, {}, function(response) {                
            $scope.universidades.data = response ;              
                
               if($scope.cotizacion.model){                    
                    //$scope.universidades.current = $filter('filter')($scope.universidades.data, { id : $scope.cotizacion.model.universidadId })[0];                   
               }                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setUniversidad' : function() {
          $scope.cotizacion.model.universidadId=$scope.universidades.current.id;
      }
    }    

 
	
	//CARGAMOS LOS LISTADOS	
      
      
	  $scope.monedas.getData();
    $scope.formasPago.getData();      
    $scope.origenes.getData();
    $scope.destinos.getData();
    $scope.tiposViaje.getData();
    $scope.universidades.getData();
    $scope.intermediarios.getData();
    $scope.prioridades.getData();
    $scope.grupos.getData();
    $scope.interes.getData();
    $scope.viasContacto.getData();
    $scope.tipoCliente.getData();
  
      
    $scope.btnHistorico=true;
    $scope.btnAdjunto=true;
      
   
	//CARGAMOS LOS DATOS DEL cotizacion	
	
	if(cotizacion.id==0){
        
          
          $scope.btnModificar=true;
          $scope.btnAnular=false;
        
    }
    else{   
        $rootScope.loadingVisible = true;    
        
        
        $scope.btnEditar=false;
        $scope.btnEnviarCotizacion=true;       
        
		
            cotizacionHttp.read({},cotizacion, function (data) { 
                
                $scope.cotizacion.model = data;	
                
                $scope.cotizacion.model.fechaVencimiento=new Date(data.fechaVencimiento);
                $scope.cotizacion.model.fechaInicio=new Date(data.fechaInicio);
                $scope.cotizacion.model.fechaFin=new Date(data.fechaFin);
                $scope.monedas.current = $filter('filter')($scope.monedas.data, { value : $scope.cotizacion.model.monedaId })[0];
                $scope.formasPago.current = $filter('filter')($scope.formasPago.data, { value : $scope.cotizacion.model.formaPagoId })[0];
                $scope.destinos.current = $filter('filter')($scope.destinos.data, { value : $scope.cotizacion.model.destino })[0];               
                
                $scope.tiposViaje.current = $filter('filter')($scope.tiposViaje.data, { value : $scope.cotizacion.model.tipoViaje })[0];
                
                $scope.interes.current = $filter('filter')($scope.interes.data, { value : $scope.cotizacion.model.interes })[0];

                $scope.tipoCliente.current = $filter('filter')($scope.tipoCliente.data, { value : $scope.cotizacion.model.tipoCliente })[0];

               
                $scope.prioridades.current = $filter('filter')($scope.prioridades.data, { value : $scope.cotizacion.model.prioridadId })[0];
                $scope.grupos.current = $filter('filter')($scope.grupos.data, { value : $scope.cotizacion.model.grupoId })[0];
                $scope.viasContacto.current = $filter('filter')($scope.viasContacto.data, { value : $scope.cotizacion.model.viaContactoId })[0];
                
                $scope.universidades.current = $filter('filter')($scope.universidades.data, { codigo : $scope.cotizacion.model.universidadId })[0];
                $scope.intermediarios.current = $filter('filter')($scope.intermediarios.data, { codigo : $scope.cotizacion.model.intermediarioId })[0];
                
                $scope.btnModificar=true;
                $scope.btnAnular=true;
                
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
        
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.cotizacion.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .service('cotizacionHttp', cotizacionHttp);

  cotizacionHttp.$inject = ['$resource', 'END_POINT'];


  function cotizacionHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'POST',
        'isArray' : true,          
        'url' : END_POINT + '/Comercial.svc/cotizacion/busqueda'       
      },
      'read' : {
        'method' : 'GET',
        'params' : {
            id : '@id'           
          },              
        'url' : END_POINT + '/Comercial.svc/cotizacion/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/cotizacion'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/cotizacion'          
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/cotizacion/:id'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
     
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercerocotizacion'
      },
         'getIntermediarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/intermediario'
      },
         'getUniversidades' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/universidad'
      },
        'getPolizas' : {
        'method' : 'POST',        
        'url' : END_POINT + '/Web.svc/poliza'
      },'getTerceros':   
        {
            'method':'POST',
            'isArray' : true,
            'url' : END_POINT + '/Comercial.svc/tercero/busqueda'
        },'getPasajeros':   
        {
            'method':'POST',
            'isArray' : true,
            'url' : END_POINT + '/Comercial.svc/pasajero/busqueda'
        },
        'enviarCotizacionCrm' : {
        'method' : 'GET', 
        'params' : {
            codigo : '@codigo',              
        },
        'url' : END_POINT + '/Web.svc/cotizacion/crm/:codigo'
      },
        'enviarCotizacionIntermediario' : {
        'method' : 'GET',
        'params' : {
            codigo : '@codigo',              
          },
        'url' : END_POINT + '/Web.svc/cotizacion/interm/:codigo'
      },
        
    };
    return $resource( END_POINT + '/Comercial.svc/cotizacion', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.cotizacion.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('cotizacionListController', cotizacionListController);

  cotizacionListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'cotizacionHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function cotizacionListController($scope, $filter,$state,$modal, ngDialog, tpl, cotizacionHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        
		
	  $scope.VERDE = 0;
    $scope.AMARILLO = 1;
    $scope.ROJO= 2;	    	

    $scope.cotizaciones = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrecotizaciones:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.cotizaciones.selectedAll = !$scope.cotizaciones.selectedAll; 
        for (var key in $scope.cotizaciones.selectedItems) {
          $scope.cotizaciones.selectedItems[key].check = $scope.cotizaciones.selectedAll;
        }
      },
      add : function() {
        
              var coti = {
                      id: 0,
                      terceroId: 0,                      
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
          
            var modalInstance = $modal.open({
            templateUrl: 'app/views/cotizacion/cotizacion_form.html',
            controller: 'cotizacionController',
            size: 'lg',
            resolve: {
                parameters: { cotizacion: coti,vcotizaciones:$scope.cotizaciones }
            }
            });
            modalInstance.result.then(function (parameters) {
            });      
        
          
      },
      edit : function(item) {
      
          
        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/cotizacion_form.html',
          controller: 'cotizacionController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion: item,vcotizaciones:$scope.cotizaciones }
          }
        });
        modalInstance.result.then(function (parameters) {
            $scope.cotizaciones.getData();
        }); 
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            cotizacionHttp.remove({}, { id: id }, function(response) {
                $scope.cotizaciones.getData();
                message.show("success", "cotizacion eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.cotizaciones.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    cotizacionHttp.remove({}, { id: id }, function(response) {
                        $scope.cotizaciones.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.cotizaciones.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.cotizaciones.filterText,
          "precision": false
        }];
        $scope.cotizaciones.selectedItems = $filter('arrayFilter')($scope.cotizaciones.dataSource, paramFilter);
        $scope.cotizaciones.paginations.totalItems = $scope.cotizaciones.selectedItems.length;
        $scope.cotizaciones.paginations.currentPage = 1;
        $scope.cotizaciones.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.cotizaciones.paginations.currentPage == 1 ) ? 0 : ($scope.cotizaciones.paginations.currentPage * $scope.cotizaciones.paginations.itemsPerPage) - $scope.cotizaciones.paginations.itemsPerPage;
        $scope.cotizaciones.data = $scope.cotizaciones.selectedItems.slice(firstItem , $scope.cotizaciones.paginations.currentPage * $scope.cotizaciones.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.cotizaciones.data = [];
        $scope.cotizaciones.loading = true;
        $scope.cotizaciones.noData = false;      
        
        debugger;

            var parametros= {
              "estado": $scope.estado.current.value,
              "fechainicial": $scope.cotizaciones.formatdate( $scope.busqueda.model.fechainicial),
              "fechafinal": $scope.cotizaciones.formatdate($scope.busqueda.model.fechafinal),   
                        
          };
          
        cotizacionHttp.getList({}, parametros,function(response) {
          $scope.cotizaciones.selectedItems = response;
          $scope.cotizaciones.dataSource = response;
          for(var i=0; i<$scope.cotizaciones.dataSource.length; i++){
            $scope.cotizaciones.nombrecotizaciones.push({id: i, nombre: $scope.cotizaciones.dataSource[i]});
          }
          $scope.cotizaciones.paginations.totalItems = $scope.cotizaciones.selectedItems.length;
          $scope.cotizaciones.paginations.currentPage = 1;
          $scope.cotizaciones.changePage();
          $scope.cotizaciones.loading = false;
          ($scope.cotizaciones.dataSource.length < 1) ? $scope.cotizaciones.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      },
      formatdate(t){    
        try{ 
                var dd = t.getDate();
                var mm = t.getMonth()+1; //January is 0!

                var yyyy = t.getFullYear();
                if(dd<10){
                    dd='0'+dd;
                } 
                if(mm<10){
                    mm='0'+mm;
                } 
                //2018-01-15
                return dd+'-'+mm+'-'+yyyy;   
            }
                catch(err) {
                    return t;
                }
   
    }
    }   

    
     //ESTADOS
     $scope.estado = {
      current : {}, data:[],
      getData : function() {
          $scope.estado.data.push({value:'1', descripcion: 'En Proceso'});
          $scope.estado.data.push({value:'2', descripcion: 'Cerrado'});
          $scope.estado.data.push({value:'3', descripcion: 'Anulado'}); 
          $scope.estado.current =$scope.estado.data[0];
      }    
  }

  $scope.busqueda = {
    model : {
          fechainicial: '',
          fechafinal: ''
          
            },
      init(){

        var dt = new Date();
        dt.setMonth( dt.getMonth() - 1 );
        $scope.busqueda.model.fechainicial=dt;
        $scope.busqueda.model.fechafinal=new Date();
        $scope.cotizaciones.getData();   
      } 
    }
	//CARGAMOS DATA
          
    $scope.estado.getData();  
    $scope.busqueda.init();  
    
        
	}
  
  
  })();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('crearClienteController', crearClienteController);

  crearClienteController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'oportunidadHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function crearClienteController($scope, $filter, $state, $modalInstance, LDataSource, oportunidadHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
      
      
      
      $scope.cliente = {     
          model:{
              identificacion: '',
              nombre: '',
              telefono: '', 
              direccion: '', 
              email: '',              
              nombreContacto: '',
              telefonoContacto: '',
              cargoContacto: ''
          },         
          
         save:  function(){ 
                
                if($scope.cliente.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
                
                   
             var _http = new oportunidadHttp($scope.cliente.model);
                  _http.$addTercero(function(response){                  
                      
                      
                      message.show("success", "Cliente creado satisfactoriamente!!");
                      $modalInstance.close(response);
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
                 
              
              
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }      
       	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      //$scope.cliente.model=parameters.cliente;
  }
})();
/**=========================================================
 * Module: app.entrega.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.entrega')
    .controller('entregaController', entregaController);

  entregaController.$inject = ['$scope', '$filter', '$state', 'LDataSource','base64', 'entregaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION','SERVER_URL','$window'];


  function entregaController($scope, $filter, $state, LDataSource,base64, entregaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION,SERVER_URL,$window) {
      
     
      var entrega = $state.params.entrega;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.entrega = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,
              tipo: '',
              consecutivo: '',
			  asunto: '',
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
              fechaEmision: '',
              fechaVencimiento: ''
              
		},  
        fechaEmision : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.entrega.fechaEmision.isOpen = true;
        }
      },  
        fechaVencimiento : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.entrega.fechaVencimiento.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.entrega.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.entrega.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.entrega', params : { filters : {procesoSeleccionado:entrega.tipo}, data : []} });
        $state.go('app.entrega');
          
      },
      save : function() {		  
       
		        if($scope.entrega.fechaEmision.value==""){message.show("warning", "Fecha de emisión requerida");return;}
				else{$scope.entrega.model.fechaEmision=Date.parse(new Date($scope.entrega.fechaEmision.value));}	
          
                if($scope.entrega.fechaVencimiento.value==""){message.show("warning", "Fecha de vencimiento requerida");return;}
				else{$scope.entrega.model.fechaVencimiento=Date.parse(new Date($scope.entrega.fechaVencimiento.value));}	
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.entrega.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.entrega.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.entrega.model.estado= $scope.estados.current.value;}
			
          
			//INSERTAR
            if($scope.entrega.model.id==0){
				
				$rootScope.loadingVisible = true;
				entregaHttp.save({}, $scope.entrega.model, function (data) { 
					
						entregaHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.entrega.model=data;
							entrega.id=$scope.entrega.model.id; 
							if($scope.entrega.model.fechaReg){$scope.entrega.fechaReg.value=new Date(parseFloat($scope.entrega.model.fechaReg));}    
							if($scope.entrega.model.fechaAct){$scope.entrega.fechaAct.value=new Date(parseFloat($scope.entrega.model.fechaAct));} 		
                            
                                $scope.btnAdjunto=true;
                                $scope.divProductosentrega=true;
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.entrega.model.id){
					$state.go('app.entrega');
				}else{
					
					entregaHttp.update({}, $scope.entrega.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.entrega.model=data;
						if($scope.entrega.model.fechaReg){$scope.entrega.fechaReg.value=new Date(parseFloat($scope.entrega.model.fechaReg));}    
						if($scope.entrega.model.fechaAct){$scope.entrega.fechaAct.value=new Date(parseFloat($scope.entrega.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.entrega.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.entrega.model.id,
                         referencia : 'entrega' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/entrega/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.entrega.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.entrega.model.id,
                         referencia : 'entrega' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      } ,imprimir : function() {            
        	entregaHttp.getFormato({},{ id : $scope.entrega.model.id,formato:'entrega'}, function (data) {
							
                            var urlDownload = SERVER_URL + base64.decode(data.url);
                            $window.open(urlDownload);                           
											
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
          
          
          
      },
          verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.entrega.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          }, 
   
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'PA', descripcion: 'Pagada'});             
        }        
    }

    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            entregaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.entrega.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.entrega.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.entrega.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.entrega.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                entregaHttp.getContactosEmpresa({}, { terceroId: $scope.entrega.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.entrega.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.entrega.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.entrega.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    
      //PRODUCTOS
    $scope.productosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            entregaHttp.getProductos({}, {}, function(response) {   
                
                $scope.productosOp.data = response ;                 
                $scope.productosOp.current=$scope.productosOp.data[0];
                $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
    
    
    $scope.productoentrega = {
    model : {  
            productoId: 0,
            oportunidadId :0,
            total :0,
            totalPendiente :0,
            valor :0,
            cantidad :0,
            valorIva :0,
            producto: '',
            comentario: ''
			
		},
      current : {},
      data:[],
      getData : function() {
          
          
          if(entrega){
              
            if($scope.entrega.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              entregaHttp.getProductosentrega({}, { entregaId: $scope.entrega.model.id }, function(response) {
                  
                  $scope.productoentrega.data = response;
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },        
        add: function(){           
            
              
            if($scope.entrega.model.id!=0){
                if($scope.productosOp.current){
                    $rootScope.loadingVisible = true;                    
                    
                    var data ={
                         productoId: $scope.productosOp.current.id,
                         oportunidadId :$scope.entrega.model.id,
                         total :  $scope.productosOp.current.valor,
                         totalPendiente :  $scope.productosOp.current.valor,
                         valor :$scope.productosOp.current.valor,
                         cantidad :1,
                         valorIva :0,
                         producto: $scope.productosOp.current.nombre,
                         comentario: ''
                        
                    } ;
                      entregaHttp.addProductoentrega({}, data, function(response) {

                          $scope.productoentrega.getData();
                          $rootScope.loadingVisible = false;
                          message.show("success", "Producto agregado satisfactoriamente");	
                      }, function(faild) {
                          $rootScope.loadingVisible = false;
                          message.show("error", faild.Message);
                      });    
                }
                else{message.show("warning", "Debe seleccionar un producto");}

            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
          
        }
        ,        
        update: function(data){
            
            
            if($scope.entrega.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              entregaHttp.editProductoentrega({}, data, function(response) {
                  
                  $scope.productoentrega.getData();
                  $rootScope.loadingVisible = false;
                  message.show("success", "Producto actualizado satisfactoriamente");	
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
        }
        ,delete: function(data){            
            
            $rootScope.loadingVisible = true;    
            debugger;
            entregaHttp.removeProductoentrega({}, {oportunidadId: $scope.entrega.model.id,productoId: data.productoId }, function(response){
             $scope.productoentrega.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
        }
    }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	    
    $scope.tercerosOp.getData();   
    $scope.productosOp.getData();
	
	//CARGAMOS LOS DATOS DEL entrega	
	
	if(entrega.id==0){
        
      $scope.estados.current=$scope.estados.data[0];             
      $scope.entrega.model.tipo=entrega.tipo;             
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            entregaHttp.read({},$state.params.entrega, function (data) { 
                
            $scope.entrega.model = data;	
            $scope.btnAdjunto=true;
            $scope.divProductosentrega=true;
               
			if($scope.entrega.model.fechaReg){$scope.entrega.fechaReg.value=new Date(parseFloat($scope.entrega.model.fechaReg));}    
			if($scope.entrega.model.fechaAct){$scope.entrega.fechaAct.value=new Date(parseFloat($scope.entrega.model.fechaAct));}  
            
                
            if($scope.entrega.model.fechaEmision){$scope.entrega.fechaEmision.value=new Date(parseFloat($scope.entrega.model.fechaEmision));}    
			if($scope.entrega.model.fechaVencimiento){$scope.entrega.fechaVencimiento.value=new Date(parseFloat($scope.entrega.model.fechaVencimiento));}     
                
                
                
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.entrega.model.terceroId })[0];  
            $scope.contactosOp.getData();
            $scope.productoentrega.getData();                
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.entrega.model.contactoId })[0];    
                
			//$scope.estados.current=$scope.entrega.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.entrega.model.estado})[0];
               
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.entrega')
    .service('entregaHttp', entregaHttp);

  entregaHttp.$inject = ['$resource', 'END_POINT'];


  function entregaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            proceso : '@proceso'           
          },
          'url' : END_POINT + '/Comercial.svc/entrega'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/entrega/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/entrega'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/entrega'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/entrega'
      }  
    };
    return $resource( END_POINT + '/Comercial.svc/entrega', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.entrega.js
 =========================================================*/

(function() {
  'use strict';
  angular
    .module('app.entrega')
    .controller('entregaListController', entregaListController);

  entregaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'entregaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function entregaListController($scope, $filter, $state, ngDialog, tpl, entregaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
    
    $scope.PROCESO = 1;
    $scope.CERRADA = 2;

    $scope.entregas = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreentregas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.entregas.selectedAll = !$scope.entregas.selectedAll; 
        for (var key in $scope.entregas.selectedItems) {
          $scope.entregas.selectedItems[key].check = $scope.entregas.selectedAll;
        }
      },
      add : function() {
        
       
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: 'FAC',
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',                  
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.entrega_add', params : { entrega: oport } });
            $state.go('app.entrega_add');
      
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.entrega_edit', params : { entrega: item } });
        $state.go('app.entrega_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            entregaHttp.remove({}, { id: id }, function(response) {
                $scope.entregas.getData();
                message.show("success", "entrega eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.entregas.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    entregaHttp.remove({}, { id: id }, function(response) {
                        $scope.entregas.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.entregas.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.entregas.filterText,
          "precision": false
        }];
        $scope.entregas.selectedItems = $filter('arrayFilter')($scope.entregas.dataSource, paramFilter);
        $scope.entregas.paginations.totalItems = $scope.entregas.selectedItems.length;
        $scope.entregas.paginations.currentPage = 1;
        $scope.entregas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.entregas.paginations.currentPage == 1 ) ? 0 : ($scope.entregas.paginations.currentPage * $scope.entregas.paginations.itemsPerPage) - $scope.entregas.paginations.itemsPerPage;
        $scope.entregas.data = $scope.entregas.selectedItems.slice(firstItem , $scope.entregas.paginations.currentPage * $scope.entregas.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.entregas.data = [];
        $scope.entregas.loading = true;
        $scope.entregas.noData = false;
        var parametros= {
            "proceso":'FAC'                
        };
          
        entregaHttp.getList({}, parametros,function(response) {
          $scope.entregas.selectedItems = response;
          $scope.entregas.dataSource = response;
          for(var i=0; i<$scope.entregas.dataSource.length; i++){
            $scope.entregas.nombreentregas.push({id: i, nombre: $scope.entregas.dataSource[i]});
          }
          $scope.entregas.paginations.totalItems = $scope.entregas.selectedItems.length;
          $scope.entregas.paginations.currentPage = 1;
          $scope.entregas.changePage();
          $scope.entregas.loading = false;
          ($scope.entregas.dataSource.length < 1) ? $scope.entregas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }

	//CARGAMOS LOS TIPOS DE PROCESO
    
    $scope.entregas.getData();  

        
	}
  
  
  })();
/**=========================================================
 * Module: app.factura.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .controller('facturaController', facturaController);

 facturaController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'facturaHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal','REPORT_URL'];   

  function facturaController($scope, $rootScope, $state,$modalInstance,$filter,facturaHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window,$modal,REPORT_URL) {      
      
     
      var factura =parameters.factura;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.factura = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              entregaId: 0,
              viaContactoId: 0,            
              padreId: 0,
              crearCliente:0,              
			  observaciones: '',
			  estado: 1,	
              fechaInicio: '',
              fechaFin: '' ,              
              polizas:[],
              ciudadEntregaId:0,
              fechaEntrega: '' ,
              direccionEntrega: '' ,
              observacionEntrega: '',
              facturaId:0
		},
      pasajeros:[],
      polizaSeleccionada:undefined,
      deletePoliza: function(p) {
           $scope.factura.model.polizas.splice($scope.factura.model.polizas.indexOf(p), 1);     
      },
      back : function() {
          parametersOfState.set({ name : 'app.factura', params : { filters : {procesoSeleccionado:factura.tipo}, data : []} });
        $state.go('app.factura');
          
      },
      save : function() {	
        
            if($scope.factura.model.identificacion==""){message.show("warning", "Identifiación del cliente requerido");return;}
            if($scope.factura.model.tercero==""){message.show("warning", "Razón social del cliente requerido");return;}
            if($scope.factura.model.email==""){message.show("warning", "Email del cliente requerido");return; }
            if( $scope.factura.model.polizas.length ==0){message.show("warning", "Debe agregar polizas");return;}  
        
            try{                
              $scope.factura.model.fechaInicio=$scope.factura.formatdate($scope.factura.model.fechaInicio);
              //$scope.factura.model.fechaInicio=$scope.factura.formatdate(new Date($scope.factura.model.fechaInicio));
            }
            catch(err) {
            }
            try{
                
              $scope.factura.model.fechaFin=$scope.factura.formatdate($scope.factura.model.fechaFin);
              //$scope.factura.model.fechaFin=$scope.factura.formatdate(new Date($scope.factura.model.fechaFin));
            }
            catch(err) {
            }
          
           try{                
                $scope.factura.model.fechaEntrega=$scope.factura.formatdate($scope.factura.model.fechaEntrega);
                //$scope.factura.model.fechaEntrega=$scope.factura.formatdate(new Date($scope.factura.model.fechaEntrega));
            }
            catch(err) {
                $scope.factura.model.fechaEntrega='';
            }
            
            $scope.factura.model.grupoId= $scope.grupos.current.value;
            $scope.factura.model.prioridadId= $scope.prioridades.current.value;
            $scope.factura.model.viaContactoId= $scope.viasContacto.current.value;
            $scope.factura.model.entregaId= $scope.entregas.current.value;
            $scope.factura.model.formaPagoId= $scope.formasPago.current.value;
            $scope.factura.model.monedaId= $scope.monedas.current.value;
            $scope.factura.model.destino= $scope.destinos.current.value;
            $scope.factura.model.tipoViaje= $scope.tiposViaje.current.value;
          
            $scope.factura.model.entregaId= $scope.entregas.current.value;
            $scope.factura.model.ciudadEntregaId= $scope.ciudades.current.value;          
          
          
            
            if($scope.intermediarios.current){$scope.factura.model.intermediarioId = $scope.intermediarios.current.codigo;
            }else{$scope.factura.model.intermediarioId ='00000000-0000-0000-0000-000000000000';} 
				
            $rootScope.loadingVisible = true;
          
            facturaHttp.save({}, $scope.factura.model, function (data) { 	
                       
                
                        if ($scope.factura.model.id==0){
                            message.show("success", "Factura creada satisfactoriamente!!");
                        }
                        else{message.show("success", "Factura actualizada satisfactoriamente!!");}
                        
                
                        $scope.factura.model.id=data.id;		
                        $scope.factura.model.intermediarioId=data.intermediarioId;
                        	
                        $scope.btnEnviarCotizacion=true;
                        $scope.crearCliente=false;
                        $scope.factura.model.crearCliente=0;                        
                      
                
                        $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });				
		
		},          
          anular: function() {   
              
            if (confirm("Desea anular el factura?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.factura.model.estado=3;//estado anulado

                try{                
                    $scope.factura.model.fechaInicio=$scope.factura.formatdate($scope.factura.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.factura.model.fechaFin=$scope.factura.formatdate($scope.factura.model.fechaFin);
                }
                catch(err) {
                }
                       facturaHttp.save({}, $scope.factura.model, function (data) {
                            $rootScope.loadingVisible = false;
                            message.show("success", "Factura anulada satisfactoriamente!!");	
                            $scope.factura.close(); 
                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.idRow,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.id,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }      
      ,agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.idRow,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.factura.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.padreId,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },          
      buscarCliente : function() { 

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion : $scope.factura.model}
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },   
    verSeguimientoHistorico : function() {            
    var modalInstance = $modal.open({
      templateUrl: 'app/views/seguimiento/seguimiento.html',
      controller: 'seguimientoController',
      size: 'lg',
      resolve: {
        parameters: { id : $scope.factura.model.padreId,
                     referencia : 'factura' }
      }
    });
    modalInstance.result.then(function (parameters) {
    });

    }
  ,buscarPolizas : function() {  
      
            if(!$scope.factura.model.fechaInicio){message.show("warning", "Fecha inicial requerida");return;}
            if(!$scope.factura.model.fechaFin){message.show("warning", "Fecha final requerida");return;}               
            $scope.factura.model.origen= $scope.origenes.current.value;
            $scope.factura.model.destino= $scope.destinos.current.value;
            $scope.factura.model.tipo= $scope.tiposViaje.current.value;
          
            var modalInstance = $modal.open({
              templateUrl: 'app/views/cotizacion/buscarPolizas.html',
              controller: 'buscarPolizaController',
              size: 'lg',
              resolve: {
                parameters: { cotizacion : $scope.factura.model}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.padreId,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },  
        close : function() {
        if(parameters.vfacturas!=null && parameters.vfacturas!=undefined)
            parameters.vfacturas.getData();
            
        $modalInstance.dismiss('cancel');
      },
        formatdate(t){    

            //var today = new Date();
            var dd = t.getDate();
            var mm = t.getMonth()+1; //January is 0!

            var yyyy = t.getFullYear();
            if(dd<10){
                dd='0'+dd;
            } 
            if(mm<10){
                mm='0'+mm;
            } 
            //2018-01-15
            return yyyy+'-'+mm+'-'+dd;         
       
        },
      enviarCotizacion(){

                $rootScope.loadingVisible = true;   
                    
                    if($scope.factura.model.intermediarioId=="00000000-0000-0000-0000-000000000000"){
                      $rootScope.loadingVisible = true;
                       facturaHttp.enviarCotizacionCrm({}, {codigo: $scope.factura.model.id }, function(response){
                          message.show("success", "Pedido enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
                    else{

                    $rootScope.loadingVisible = true;
                     facturaHttp.enviarCotizacionIntermediario({}, {codigo: $scope.factura.model.id }, function(response){
                          message.show("success", "Pedido enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
      },
        agregarCliente: function(data){
            
            $scope.crearCliente=true;
            $scope.factura.model.crearCliente=1;
        },
          selectPoliza: function(polizaSeleccionada){            
              
              $scope.factura.pasajeros=polizaSeleccionada.pasajeros; 
              $scope.polizaSeleccionada=polizaSeleccionada;
              
        },buscarPasajero: function(data){
            
            if($scope.polizaSeleccionada!=null && $scope.polizaSeleccionada!=undefined){

                      var modalInstance = $modal.open({
                      templateUrl: 'app/views/cotizacion/buscarPasajero.html',
                      controller: 'buscarPasajeroController',
                      size: 'lg',
                      resolve: {
                        parameters: { cotizacion : $scope.factura.model,poliza:$scope.polizaSeleccionada}
                      }
                    });
                    modalInstance.result.then(function (parameters) {
                    });
                }
            else{                
                message.show("warning", "Debe seleccionar una poliza!!");
            }
            
        },imprimir: function(data){
            
             var targeturl = REPORT_URL + "Factura&NumCotizacion=" + $scope.factura.model.id+"&rs:Format=PDF";
             window.open(targeturl, '_blank');
        } 
    }
      
      
      
    //TERCEROS
    $scope.clientes = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            generalHttp.getTerceros({}, {}, function(response) {                
            $scope.clientes.data = response ;              
                
               if($scope.factura.model){
                    
                    $scope.clientes.current = $filter('filter')($scope.clientes.data, { id : $scope.factura.model.clienteId })[0];                   
               }
                else{
                    $scope.clientes.current=$scope.Clientes.data[0];                     
                }               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.factura.model.terceroId=$scope.clientes.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.factura.model.terceroId=$scope.clientes.current.id;
         // $scope.contactosOp.getData();
      }
    }

    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'}); 
        }        
    }

    //VIA CONTACTO
    $scope.viasContacto = {
      current : {},
      data:[],
      getData : function() {
                $scope.viasContacto.data.push({value:1, descripcion: 'Cliente'});
                $scope.viasContacto.data.push({value:2, descripcion: 'Facebook'});     
                $scope.viasContacto.data.push({value:3, descripcion: 'E-mail'}); 
                $scope.viasContacto.data.push({value:4, descripcion: 'Directorio'});  
                $scope.viasContacto.data.push({value:5, descripcion: 'Referido'});  
                $scope.viasContacto.data.push({value:6, descripcion: 'Compra online'});  
                $scope.viasContacto.data.push({value:7, descripcion: 'Intermediario'});  
                $scope.viasContacto.data.push({value:8, descripcion: 'Google'});
                $scope.viasContacto.data.push({value:9, descripcion: 'Whatsapp'});
                $scope.viasContacto.data.push({value:10, descripcion: 'Zopim'});  
                $scope.viasContacto.data.push({value:11, descripcion: 'Ori/Volante'}); 
                $scope.viasContacto.current=$scope.viasContacto.data[0];   
      },
      setViaContacto : function(){
          $scope.factura.model.viaContactoId=$scope.viasContacto.current.codigo;
    }}
    //PRIORIDADES
    $scope.prioridades = {
      current : {},
      data:[],
      getData : function() {
                $scope.prioridades.data.push({value:1, descripcion: 'NORMAL(15DIAS)'});
                $scope.prioridades.data.push({value:2, descripcion: 'ALTA(48HORAS)'});     
                $scope.prioridades.data.push({value:3, descripcion: 'CRITICA(8HORAS)'}); 
                $scope.prioridades.data.push({value:4, descripcion: 'BAJA(1MES)'});  
                $scope.prioridades.data.push({value:5, descripcion: 'BAJA(2MESES)'});  
                $scope.prioridades.data.push({value:6, descripcion: 'BAJA(3MESES)'});  
                $scope.prioridades.current=$scope.prioridades.data[0];   
      },
      setPrioridad : function(){
          $scope.factura.model.prioridadId=$scope.prioridades.current.value;
    }}
   
    //GRUPO
    $scope.grupos = {
      current : {},
      data:[],
      getData : function() {
                $scope.grupos.data.push({value:2, descripcion: 'Asesores'});    
                $scope.grupos.data.push({value:1, descripcion: 'Administrativo'});                 
                $scope.grupos.data.push({value:3, descripcion: 'Lista de pólizas'});     
                $scope.grupos.data.push({value:4, descripcion: 'Servicio al Cliente'});
                $scope.grupos.current=$scope.grupos.data[0];                   
          },
      setGrupo : function(){
          $scope.factura.model.grupoId=$scope.grupos.current.value;
    }}
    //ENTREGAS
    $scope.entregas = {
      current : {},
      data:[],
      getData : function() {
                $scope.entregas.data.push({value:1, descripcion: 'Entrega Oficina'});    
                $scope.entregas.data.push({value:2, descripcion: 'Entrega Domicilio'});                 
                $scope.entregas.data.push({value:3, descripcion: 'Pago Contra Entrega'});     
                $scope.entregas.data.push({value:4, descripcion: 'Consignación Bancaria'});
                $scope.entregas.current=$scope.entregas.data[0];                   
          },
      setEntrega : function(){
          $scope.factura.model.entregaId=$scope.entregas.current.value;
    }}
      
      
    //MONEDA
    $scope.monedas  = {
            current: {},
            data: [],
            getData: function() {
                $scope.monedas.data.push({value:2, descripcion: 'Dolar'});
                $scope.monedas.data.push({value:4, descripcion: 'Pesos Colombianos'});    
                $scope.monedas.current=$scope.monedas.data[0];    
            }
        }
      
    $scope.formasPago  = {
            current: {},
            data: [],
            getData: function() {
                $scope.formasPago.data.push({value:0, descripcion: 'Pesos'});
                $scope.formasPago.data.push({value:1, descripcion: 'Visa Tarjeta de credito'});     
                $scope.formasPago.data.push({value:2, descripcion: 'Pse'});     
                $scope.formasPago.data.push({value:3, descripcion: 'Consig  Bogota'});     
                $scope.formasPago.data.push({value:4, descripcion: 'Consig  Bancolombia'});     
                $scope.formasPago.data.push({value:5, descripcion: 'Consig  Colpatria'});     
                $scope.formasPago.data.push({value:6, descripcion: 'Consig  Davivienda'});
                $scope.formasPago.data.push({value:8, descripcion: 'Dolares'});  
                $scope.formasPago.data.push({value:10, descripcion: 'Mastercard Tarjeta'});  
                $scope.formasPago.data.push({value:11, descripcion: 'Diners Tarjeta'});  
                $scope.formasPago.data.push({value:12, descripcion: 'Amex Tarjeta'});  
                $scope.formasPago.data.push({value:13, descripcion: 'CREDITO/SIN RECIBO'});  
                
                $scope.formasPago.current=$scope.formasPago.data[0]; 
                   
            }
        }
      
        $scope.ciudades  = {
            current: {},
            data: [],
            getData: function() {
                $scope.ciudades.data.push({value:0, descripcion: 'Bogotá D.C'});
                $scope.ciudades.data.push({value:1, descripcion: 'Medellín'});     
                $scope.ciudades.data.push({value:2, descripcion: 'Barranquilla'});     
                $scope.ciudades.data.push({value:3, descripcion: 'Calí'});     
                $scope.ciudades.data.push({value:4, descripcion: 'Bucaramanga'});     
                $scope.ciudades.data.push({value:5, descripcion: 'Consig Colpatria'});     
                $scope.ciudades.data.push({value:6, descripcion: 'Santa Marta'});
                
                $scope.ciudades.current=$scope.ciudades.data[0];    
            }
        }
      
      
      
        $scope.origenes  = {
            current: {},
            data: [],
            getData: function() {
      
                        $scope.origenes.data.push({value: 48, descripcion: 'Colombia' });
                        $scope.origenes.data.push({value: 1, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 2, descripcion: 'Argentina' });
                        $scope.origenes.data.push({value: 6, descripcion: 'Alemania' });
                        $scope.origenes.data.push({value: 12, descripcion: 'Arabia Saudita' });
                        $scope.origenes.data.push({value: 15, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 16, descripcion: 'Australia' });
                        $scope.origenes.data.push({value: 19, descripcion: 'Bahamas' });
                        $scope.origenes.data.push({value: 21, descripcion: 'Barbados' });
                        $scope.origenes.data.push({value: 25, descripcion: 'Bemudas' });
                        $scope.origenes.data.push({value: 28, descripcion: 'Bolivia' });
                        $scope.origenes.data.push({value: 31, descripcion: 'Brasil' });
                        $scope.origenes.data.push({value: 33, descripcion: 'Bulgaria' });
                        $scope.origenes.data.push({value: 37, descripcion: 'Belgica' });
                        $scope.origenes.data.push({value: 38, descripcion: 'Cabo Verde' });
                        $scope.origenes.data.push({value: 39, descripcion: 'Islas Caiman' });
                        $scope.origenes.data.push({value: 40, descripcion: 'Camboya' });
                        $scope.origenes.data.push({value: 41, descripcion: 'Camerun' });
                        $scope.origenes.data.push({value: 42, descripcion: 'Canada' });
                        $scope.origenes.data.push({value: 45, descripcion: 'Chile' });
                        $scope.origenes.data.push({value: 52, descripcion: 'Corea del Norte' });
                        $scope.origenes.data.push({value: 53, descripcion: 'Corea del Sur' });
                        $scope.origenes.data.push({value: 54, descripcion: 'Costa de Marfil' });
                        $scope.origenes.data.push({value: 55, descripcion: 'Costa Rica' });
                        $scope.origenes.data.push({value: 56, descripcion: 'Croacia' });
                        $scope.origenes.data.push({value: 57, descripcion: 'Cuba' });
                        $scope.origenes.data.push({value: 59, descripcion: 'Dinamarca' });
                        $scope.origenes.data.push({value: 61, descripcion: 'Ecuador' });
                        $scope.origenes.data.push({value: 62, descripcion: 'Egipto' });
                        $scope.origenes.data.push({value: 63, descripcion: 'El Salvador' });
                        $scope.origenes.data.push({value: 64, descripcion: 'Emiratos Arabes Unvalueos' });
                        $scope.origenes.data.push({value: 66, descripcion: 'Eslovaquia' });
                        $scope.origenes.data.push({value: 67, descripcion: 'Eslovenia' });
                        $scope.origenes.data.push({value: 68, descripcion: 'España' });
                        $scope.origenes.data.push({value: 69, descripcion: 'Estados Unvalueos' });
                        $scope.origenes.data.push({value: 70, descripcion: 'Estonia' });
                        $scope.origenes.data.push({value: 71, descripcion: 'Etiopia' });
                        $scope.origenes.data.push({value: 72, descripcion: 'Islas Feroe' });
                        $scope.origenes.data.push({value: 73, descripcion: 'Filipinas' });
                        $scope.origenes.data.push({value: 74, descripcion: 'Finlandia' });
                        $scope.origenes.data.push({value: 75, descripcion: 'Fiji' });
                        $scope.origenes.data.push({value: 76, descripcion: 'Francia'});
                        $scope.origenes.data.push({value: 83, descripcion: 'Grecia' });
                        $scope.origenes.data.push({value: 84, descripcion: 'Groenlandia' });
                        $scope.origenes.data.push({value: 86, descripcion: 'Guatemala' });
                        $scope.origenes.data.push({value: 87, descripcion: 'Guernsey' });
                        $scope.origenes.data.push({value: 88, descripcion: 'Guinea' });
                        $scope.origenes.data.push({value: 89, descripcion: 'Guinea Ecuatorial' });
                        $scope.origenes.data.push({value: 90, descripcion: 'Guinea-Bissau' });
                        $scope.origenes.data.push({value: 91, descripcion: 'Guyana' });
                        $scope.origenes.data.push({value: 92, descripcion: 'Haiti' });
                        $scope.origenes.data.push({value: 93, descripcion: 'Honduras' });
                        $scope.origenes.data.push({value: 94, descripcion: 'Hong kong' });
                        $scope.origenes.data.push({value: 95, descripcion: 'Hungria' });
                        $scope.origenes.data.push({value: 96, descripcion: 'India' });
                        $scope.origenes.data.push({value: 97, descripcion: 'Indonesia' });
                        $scope.origenes.data.push({value: 98, descripcion: 'Iraq' });
                        $scope.origenes.data.push({value: 99, descripcion: 'Irlanda' });
                        $scope.origenes.data.push({value: 100, descripcion: 'Iran' });
                        $scope.origenes.data.push({value: 101, descripcion: 'Islandia' });
                        $scope.origenes.data.push({value: 102, descripcion: 'Israel' });
                        $scope.origenes.data.push({value: 103, descripcion: 'Italia' });
                        $scope.origenes.data.push({value: 104, descripcion: 'Jamaica' });
                        $scope.origenes.data.push({value: 105, descripcion: 'Japon' });
                        $scope.origenes.data.push({value: 117, descripcion: 'Liberia' });
                        $scope.origenes.data.push({value: 118, descripcion: 'Libia' });
                        $scope.origenes.data.push({value: 120, descripcion: 'Lituania' });
                        $scope.origenes.data.push({value: 121, descripcion: 'Luxemburgo' });
                        $scope.origenes.data.push({value: 122, descripcion: 'Libano' });
                        $scope.origenes.data.push({value: 128, descripcion: 'Maldivas' });
                        $scope.origenes.data.push({value: 129, descripcion: 'Malta' });
                        $scope.origenes.data.push({value: 130, descripcion: 'Islas Malvinas' });
                        $scope.origenes.data.push({value: 131, descripcion: 'Mali' });
                        $scope.origenes.data.push({value: 134, descripcion: 'Marruecos' });
                        $scope.origenes.data.push({value: 144, descripcion: 'Mexico' });
                        $scope.origenes.data.push({value: 145, descripcion: 'Monaco' });
                        $scope.origenes.data.push({value: 146, descripcion: 'Namibia' });
                        $scope.origenes.data.push({value: 147, descripcion: 'Nauru' });
                        $scope.origenes.data.push({value: 149, descripcion: 'Nepal' });
                        $scope.origenes.data.push({value: 150, descripcion: 'Nicaragua' });
                        $scope.origenes.data.push({value: 151, descripcion: 'Nigeria' });
                        $scope.origenes.data.push({value: 152, descripcion: 'Niue' });
                        $scope.origenes.data.push({value: 154, descripcion: 'Noruega' });
                        $scope.origenes.data.push({value: 155, descripcion: 'Nueva Caledonia' });
                        $scope.origenes.data.push({value: 156, descripcion: 'Nueva Zelanda' });
                        $scope.origenes.data.push({value: 157, descripcion: 'Niger' });
                        $scope.origenes.data.push({value: 158, descripcion: 'Oman' });
                        $scope.origenes.data.push({value: 160, descripcion: 'Pakistan' });
                        $scope.origenes.data.push({value: 161, descripcion: 'Palaos' });
                        $scope.origenes.data.push({value: 162, descripcion: 'Palestina' });
                        $scope.origenes.data.push({value: 163, descripcion: 'Panama' });
                        $scope.origenes.data.push({value: 165, descripcion: 'Paraguay' });
                        $scope.origenes.data.push({value: 167, descripcion: 'Peru' });
                        $scope.origenes.data.push({value: 170, descripcion: 'Polonia' });
                        $scope.origenes.data.push({value: 171, descripcion: 'Portugal' });
                        $scope.origenes.data.push({value: 172, descripcion: 'Puerto Rico' });
                        $scope.origenes.data.push({value: 173, descripcion: 'Reino Unvalueo' });
                        $scope.origenes.data.push({value: 175, descripcion: 'Republica Checa' });
                        $scope.origenes.data.push({value: 176, descripcion: 'Republica Dominicana' });
                        $scope.origenes.data.push({value: 178, descripcion: 'Rumania' });
                        $scope.origenes.data.push({value: 179, descripcion: 'Rusia' });
                        $scope.origenes.data.push({value: 197, descripcion: 'Singapur' });
                        $scope.origenes.data.push({value: 198, descripcion: 'Siria' });
                        $scope.origenes.data.push({value: 201, descripcion: 'Sri Lanka' });
                        $scope.origenes.data.push({value: 203, descripcion: 'Sudafrica' });
                        $scope.origenes.data.push({value: 206, descripcion: 'Suecia' });
                        $scope.origenes.data.push({value: 207, descripcion: 'Suiza' });
                        $scope.origenes.data.push({value: 210, descripcion: 'Tailandia' });
                        $scope.origenes.data.push({value: 211, descripcion: 'Taiwan' });
                        $scope.origenes.data.push({value: 219, descripcion: 'Trinvaluead y Tobajo' });
                        $scope.origenes.data.push({value: 222, descripcion: 'Turquia' });
                        $scope.origenes.data.push({value: 225, descripcion: 'Ucrania' });
                        $scope.origenes.data.push({value: 227, descripcion: 'Uruguay' });
                        $scope.origenes.data.push({value: 230, descripcion: 'Ciudad del Vaticano' });
                        $scope.origenes.data.push({value: 231, descripcion: 'Venezuela' });
                        $scope.origenes.data.push({value: 239, descripcion: 'Chipre' });
                        $scope.origenes.data.push({value: 240, descripcion: 'Guam' });
                        $scope.origenes.data.push({value: 241, descripcion: 'Hawai' });
                        $scope.origenes.data.push({value: 246, descripcion: 'Boniaire' });
                        $scope.origenes.current=$scope.origenes.data[0];    
            }
        }  
      
        $scope.destinos  = {
            current: {},
            data: [],
            getData: function() {
                
                $scope.destinos.data.push({value:1, descripcion: 'Sur América'});
                $scope.destinos.data.push({value:2, descripcion: 'Centro América'});     
                $scope.destinos.data.push({value:3, descripcion: 'Norte América'});                         
                $scope.destinos.data.push({value:5, descripcion: 'Europa'});     
                $scope.destinos.data.push({value:6, descripcion: 'Asia'});     
                $scope.destinos.data.push({value:7, descripcion: 'África'}); 
                $scope.destinos.data.push({value:8, descripcion: 'Oceanía'}); 
                $scope.destinos.data.push({value:9, descripcion: 'Antillas Holandesas'}); 
                $scope.destinos.data.push({value:10, descripcion: 'Internacional'}); 
                $scope.destinos.data.push({value:11, descripcion: 'Colombia'}); 
                $scope.destinos.current=$scope.destinos.data[0];    
                
             }
        }
      
      $scope.tiposViaje  = {
            current: {},
            data: [],
            getData: function() {   
                
                $scope.tiposViaje.data.push({value:11, descripcion: 'Estudio'});
                $scope.tiposViaje.data.push({value:12, descripcion: 'Larga Estadía (más de 60 días).'});     
                $scope.tiposViaje.data.push({value:13, descripcion: 'Corta Estadia (menos de 60 días)'});     
                $scope.tiposViaje.data.push({value:14, descripcion: 'MultiViajes'});     
                $scope.tiposViaje.data.push({value:15, descripcion: 'Estudio Con Responsabilidad Civil'});     
                $scope.tiposViaje.data.push({value:16, descripcion: 'Futura Mamá'});     
                $scope.tiposViaje.data.push({value:17, descripcion: 'Atención médica por Pre-existencia'});   
                $scope.tiposViaje.current=$scope.tiposViaje.data[0];
          
            }
      }
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            facturaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.factura.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.factura.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.factura.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.factura.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }

    //INTERMEDIARIOS
    $scope.intermediarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            facturaHttp.getIntermediarios({}, {}, function(response) {                
            $scope.intermediarios.data = response ;              
                
               if($scope.factura.model){
                                     
               }                           
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setIntermediario' : function() {
          $scope.factura.model.intermediarioId=$scope.intermediarios.current.id;
      }
    }      
 

 
	
	//CARGAMOS LOS LISTADOS	
      
      
	$scope.monedas.getData();
    $scope.formasPago.getData();      
    $scope.origenes.getData();
    $scope.destinos.getData();
    $scope.tiposViaje.getData();    
    $scope.intermediarios.getData();
    $scope.prioridades.getData();
    $scope.grupos.getData();
    $scope.entregas.getData();
    $scope.viasContacto.getData();
    $scope.ciudades.getData();
      
    $scope.btnHistorico=true;
    $scope.btnAdjunto=true;
      
   
	//CARGAMOS LOS DATOS DEL factura	
	
	if(factura.id==0){
          $scope.btnModificar=true;
          $scope.btnAnular=false;        
    }
    else{   
        $rootScope.loadingVisible = true;  
        $scope.btnEditar=false;
        $scope.btnEnviarCotizacion=true;
        
        
		
            facturaHttp.read({},factura, function (data) { 
                
            $scope.factura.model = data;	
            $scope.factura.model.fechaInicio=new Date(data.fechaInicio);
            $scope.factura.model.fechaFin=new Date(data.fechaFin);  
           
            try{                
                 $scope.factura.model.fechaEntrega=new Date(data.fechaEntrega);  
            }
            catch(err) {
                $scope.factura.model.fechaEntrega='';
            }

            $scope.monedas.current = $filter('filter')($scope.monedas.data, { value : $scope.factura.model.monedaId })[0];
            debugger;
            $scope.formasPago.current = $filter('filter')($scope.formasPago.data, { value : $scope.factura.model.formaPagoId })[0];

            $scope.destinos.current = $filter('filter')($scope.destinos.data, { value : $scope.factura.model.destino })[0];
            $scope.entregas.current = $filter('filter')($scope.entregas.data, { value : $scope.factura.model.entregaId })[0];


            $scope.entregas.current = $filter('filter')($scope.entregas.data, { value : $scope.factura.model.entregaId })[0];
            $scope.ciudades.current = $filter('filter')($scope.ciudades.data, { value : $scope.factura.model.ciudadEntregaId })[0];

            $scope.prioridades.current = $filter('filter')($scope.prioridades.data, { value : $scope.factura.model.prioridadId })[0];

            $scope.grupos.current = $filter('filter')($scope.grupos.data, { value : $scope.factura.model.grupoId })[0];
            $scope.viasContacto.current = $filter('filter')($scope.viasContacto.data, { value : $scope.factura.model.viaContactoId })[0];        

            $scope.intermediarios.current = $filter('filter')($scope.intermediarios.data, { codigo : $scope.factura.model.intermediarioId })[0];

            $scope.btnModificar=true;
            $scope.btnAnular=true;
                
            if($scope.factura.model.polizas.length>0)
            $scope.factura.selectPoliza($scope.factura.model.polizas[0]);
                
                
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
        
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .service('facturaHttp', facturaHttp);

  facturaHttp.$inject = ['$resource', 'END_POINT'];


  function facturaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            proceso : '@proceso'           
          },
          'url' : END_POINT + '/Comercial.svc/factura'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/factura/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/factura'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/factura'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/factura'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/factura'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      },
        'getIntermediarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/intermediario'
      },    
    };
    return $resource( END_POINT + '/Comercial.svc/factura', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.factura.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .controller('facturaListController', facturaListController);

  facturaListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'facturaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function facturaListController($scope, $filter, $state,$modal, ngDialog, tpl, facturaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
    
    $scope.VERDE = 0;
    $scope.AMARILLO = 1;
    $scope.ROJO= 2;	    	
	

    $scope.facturas = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrefacturas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.facturas.selectedAll = !$scope.facturas.selectedAll; 
        for (var key in $scope.facturas.selectedItems) {
          $scope.facturas.selectedItems[key].check = $scope.facturas.selectedAll;
        }
      },
      add : function() {
        
       
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: 'FAC',
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',                  
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.factura_add', params : { factura: oport } });
            $state.go('app.factura_add');
      
          
      },
      edit : function(item) {
          
          
         var modalInstance = $modal.open({
          templateUrl: 'app/views/factura/factura_form.html',
          controller: 'facturaController',
          size: 'lg',
          resolve: {
            parameters: { factura: item,vfacturas:$scope.facturas }
          }
        });
        modalInstance.result.then(function (parameters) {
            $scope.cotizaciones.getData();
        }); 
          
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            facturaHttp.remove({}, { id: id }, function(response) {
                $scope.facturas.getData();
                message.show("success", "factura eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.facturas.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    facturaHttp.remove({}, { id: id }, function(response) {
                        $scope.facturas.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.facturas.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.facturas.filterText,
          "precision": false
        }];
        $scope.facturas.selectedItems = $filter('arrayFilter')($scope.facturas.dataSource, paramFilter);
        $scope.facturas.paginations.totalItems = $scope.facturas.selectedItems.length;
        $scope.facturas.paginations.currentPage = 1;
        $scope.facturas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.facturas.paginations.currentPage == 1 ) ? 0 : ($scope.facturas.paginations.currentPage * $scope.facturas.paginations.itemsPerPage) - $scope.facturas.paginations.itemsPerPage;
        $scope.facturas.data = $scope.facturas.selectedItems.slice(firstItem , $scope.facturas.paginations.currentPage * $scope.facturas.paginations.itemsPerPage);
      },
      getData : function() {
      $scope.facturas.data = [];
        $scope.facturas.loading = true;
        $scope.facturas.noData = false;        
          
        facturaHttp.getList({}, {},function(response) {
          $scope.facturas.selectedItems = response;
          $scope.facturas.dataSource = response;
          for(var i=0; i<$scope.facturas.dataSource.length; i++){
            $scope.facturas.nombrefacturas.push({id: i, nombre: $scope.facturas.dataSource[i]});
          }
          $scope.facturas.paginations.totalItems = $scope.facturas.selectedItems.length;
          $scope.facturas.paginations.currentPage = 1;
          $scope.facturas.changePage();
          $scope.facturas.loading = false;
          ($scope.facturas.dataSource.length < 1) ? $scope.facturas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }

	//CARGAMOS LOS TIPOS DE PROCESO
    
    $scope.facturas.getData();  

        
	}
  
  
  })();
/**=========================================================
 * Module: app.forma30.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.fileManager')
    .controller('fileManagerController', fileManagerController);

  fileManagerController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'fileManagerHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];


  function fileManagerController($scope, $rootScope, $state, $modalInstance, fileManagerHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {


    $scope.fileManager = {
      model : {
        file : {},
        referenciaId : '',
        tipoReferencia : '',
        comentario : ''
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.fileManager.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.fileManager.dataSource, paramFilter);
        $scope.fileManager.paginations.totalItems = $scope.fileManager.selectedItems.length;
        $scope.fileManager.paginations.currentPage = 1;
        $scope.fileManager.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.fileManager.paginations.currentPage == 1 ) ? 0 : ($scope.fileManager.paginations.currentPage * $scope.fileManager.paginations.itemsPerPage) - $scope.fileManager.paginations.itemsPerPage;
        $scope.fileManager.data = $scope.fileManager.selectedItems.slice(firstItem , $scope.fileManager.paginations.currentPage * $scope.fileManager.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.fileManager.model.referenciaId,
          tipoReferencia : $scope.fileManager.model.tipoReferencia
        };
        $scope.fileManager.getData(params);
      },
      setData : function(data) {
        $scope.fileManager.selectedItems = data;
        $scope.fileManager.dataSource = data;
        $scope.fileManager.paginations.totalItems = $scope.fileManager.selectedItems.length;
        $scope.fileManager.paginations.currentPage = 1;
        $scope.fileManager.changePage();
        $scope.fileManager.noData = false;
        $scope.fileManager.loading = false;
        ($scope.fileManager.dataSource.length < 1) ? $scope.fileManager.noData = true : null;
      },
      getData : function(params) {
        $scope.fileManager.data = [];
        $scope.fileManager.loading = true;
        $scope.fileManager.noData = false;

        fileManagerHttp.getDocumentos({}, params, function(response) {
          $scope.fileManager.setData(response);
        })
      },
      visualizar : function(id) {
        
        
        var params = {
          id : id 
        };

        fileManagerHttp.getDocumento({}, params, function(response) {
          if (response.url != '') {
            var urlDownload = SERVER_URL + base64.decode(response.url);
            $window.open(urlDownload);
          }
        })
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;

        fileManagerHttp.removeDocumento(params, function(response) {
          $scope.fileManager.refresh();
          message.show("info", "Adjunto eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },
      uploader : function(file) {

        $rootScope.loadingVisible = true;

        file.upload = Upload.upload({
          url: END_POINT + '/General.svc/uploadDocument',
          headers: {
            Authorization : tokenManager.get()
          },
          data: { file: file, 
                 referenciaId: base64.encode($scope.fileManager.model.referenciaId),
                 tipoReferencia : base64.encode($scope.fileManager.model.tipoReferencia),
                 comentario : base64.encode($scope.fileManager.model.comentario)
                }
        });

        file.upload.then(function (response) {
          message.show("success", "Archivo almacenado correctamente");
          $scope.fileManager.model.file = {};
          $scope.fileManager.model.comentario = '';
          $scope.fileManager.refresh();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", "Ocurrio un error al intentar guardar el archivo");
        });

      },
      close : function() {
        $modalInstance.dismiss('cancel');
      }
    }

    $scope.fileManager.model.referenciaId = parameters.id.toString();
    $scope.fileManager.model.tipoReferencia = parameters.referencia.toString();
    $scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;

    $scope.fileManager.refresh();

  }

})();
/**=========================================================
 * Module: app.fileManager.js
 =========================================================*/
(function() {
    'use strict';
    angular.module('app.fileManager').controller('fileManagerDynamicController', fileManagerDynamicController);
    fileManagerDynamicController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'fileManagerHttp', 'parameters', 'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];

    function fileManagerDynamicController($scope, $rootScope, $state, $modalInstance, fileManagerHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {
        $scope.motrarTabla = false;
        $scope.fileManager = {
                model: {
                    file: {},
                    tipoReferencia: null,
                    anio: null,
                    mes: null,
                    liquidacion: null,
                    tipoPrecio: null,
                    curvaId: null,
                    tipoHdr: null
                },
                error: [],
                uploader: function(file) {
                    $rootScope.loadingVisible = true;
                    cargueArchivo(file);
                },
                close: function() {
                    $modalInstance.dismiss('cancel');
                }
            }
            /**
             * Objeto de la tabla
             */
        $scope.dataPage = {
            paginations: {
                maxSize: 3,
                itemsPerPage: 10,
                currentPage: 0,
                totalItems: 0
            },
            selectedAll: false,
            filterText: '',
            dataSource: [],
            selectedItems: [],
            data: [],
            noData: false,
            loading: false,
            selectAll: function() {
                $scope.dataPage.selectedAll = !$scope.dataPage.selectedAll;
                for (var key in $scope.dataPage.selectedItems) {
                    $scope.dataPage.selectedItems[key].check = $scope.dataPage.selectedAll;
                }
            },
            filter: function() {
                var paramFilter = [{
                    "key": "$",
                    "value": $scope.dataPage.filterText,
                    "precision": false
                }];
                $scope.dataPage.selectedItems = $filter('arrayFilter')($scope.dataPage.dataSource, paramFilter);
                $scope.dataPage.paginations.totalItems = $scope.dataPage.selectedItems.length;
                $scope.dataPage.paginations.currentPage = 1;
                $scope.dataPage.changePage();
            },
            changePage: function() {
                var firstItem = ($scope.dataPage.paginations.currentPage == 1) ? 0 : ($scope.dataPage.paginations.currentPage * $scope.dataPage.paginations.itemsPerPage) - $scope.dataPage.paginations.itemsPerPage;
                $scope.dataPage.data = $scope.dataPage.selectedItems.slice(firstItem, $scope.dataPage.paginations.currentPage * $scope.dataPage.paginations.itemsPerPage);
            },
            getData: function() {
                $scope.dataPage.data = [];
                $scope.dataPage.loading = true;
                $scope.dataPage.noData = false;
            }
        }
        $scope.fileManager.model.tipoReferencia = parameters.item.tipoReferencia;
        if ($scope.fileManager.model.tipoReferencia == "TRM") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
        }
        if ($scope.fileManager.model.tipoReferencia == "OIL") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
            $scope.fileManager.model.liquidacion = parameters.item.liquidacion;
            $scope.fileManager.model.tipoPrecio = parameters.item.tipoPrecio;
        }
        if ($scope.fileManager.model.tipoReferencia == "GAS") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
            $scope.fileManager.model.liquidacion = parameters.item.liquidacion;
        }
        if ($scope.fileManager.model.tipoReferencia == "CURVA") {
            $scope.motrarTabla = false;
            $scope.fileManager.model.curvaId = parameters.item.curvaId;
            $scope.fileManager.model.tipoHdr = parameters.item.tipoHdr;
        }
        /**
         * Metodo que carga un archivo dependiendo del tipo de cargue
         *
         * @method     cargueArchivo
         * @param      {<Object>}  file    { Contiene el archivo a cargar }
         */
        function cargueArchivo(file) {
            if ($scope.fileManager.model.tipoReferencia == "CURVA") {
                $rootScope.loadingVisible = true;
                console.log("Entro CURVA");
                file.upload = Upload.upload({
                    url: END_POINT + '/General.svc/uploadCurvaBase',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        curvaId: base64.encode('' + $scope.fileManager.model.curvaId),
                        tipoHdr: base64.encode($scope.fileManager.model.tipoHdr)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "TRM") {
                $rootScope.loadingVisible = true;
                console.log("Entro TRM");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadTrm',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "OIL") {
                $rootScope.loadingVisible = true;
                console.log("Entro OIL");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadPrecioOil',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes),
                        liquidacion: base64.encode($scope.fileManager.model.liquidacion),
                        tipoPrecio: base64.encode($scope.fileManager.model.tipoPrecio)
                    }
                });
                file.upload.then(function(response) {
                   if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "GAS") {
                $rootScope.loadingVisible = true;
                console.log("Entro GAS");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadPrecioGas',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes),
                        liquidacion: base64.encode($scope.fileManager.model.liquidacion)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            $rootScope.loadingVisible = false;
        }
    }
})();
/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.fileManager')
    .service('fileManagerHttp', fileManagerHttp);

  fileManagerHttp.$inject = ['$resource', 'END_POINT'];


  function fileManagerHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getDocumentos : {
        method : 'POST',
        isArray : true
      },
      getDocumento : {
        method : 'GET',
        url : END_POINT + '/General.svc/document/:id'
      },
      removeDocumento : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/document/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/document', paramDefault, actions);
  }

})();

/**=========================================================
 * Module: app.funcionario.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .controller('funcionarioController', funcionarioController);

  funcionarioController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'funcionarioHttp', 'ngDialog', 'tpl', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function funcionarioController($scope, $filter, $state, LDataSource, funcionarioHttp, ngDialog, tpl, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
      var funcionario = $state.params.funcionario;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      $scope.funcionario = {
		model : {
				  id: 0,
				  nombre: '',
				  empresaId: 0,
				  matricula: '',
				  email: '',
				  documento: '',
				  direccion: '',
				  estado: '',
				  fechaIngreso: '',
				  codigo: '',
				  telefono: '',
				  fechaAct: '',
				  usuarioAct: '',
				  fechaReg: '',
				  usuarioReg: '',
				  usuario: '',
				  contrasenia: '',
				  tipoAuth: '',
                  roles:[]
		},
        fechaIngreso : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaIngreso.isOpen = true;
        }
      },   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.funcionario', params : { filters : {}, data : []} });
        $state.go('app.funcionario');
      },
      save : function() {		  
       
		  
          		if($scope.funcionario.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.funcionario.model.documento ==""){message.show("warning", "Documento requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.funcionario.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoAuth.current){message.show("warning", "Tipo autenticación requerido");return;}
				else{$scope.funcionario.model.tipoAuth= $scope.tipoAuth.current.value;}
				
				if($scope.funcionario.fechaIngreso.value==""){message.show("warning", "Fecha de ingreso requerido");return;}
				else{$scope.funcionario.model.fechaIngreso=Date.parse(new Date($scope.funcionario.fechaIngreso.value));}				
			
				$scope.funcionario.model.empresaId=1;

			
          
			//INSERTAR
            if($scope.funcionario.model.id==0){
				
				$rootScope.loadingVisible = true;
				funcionarioHttp.save({}, $scope.funcionario.model, function (data) { 
					
						funcionarioHttp.read({},{ id : data.id}, function (data) {
							$scope.funcionario.model=data;
							funcionario.id=$scope.funcionario.model.id; 
							if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
							if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));} 		
								
							message.show("success", "Funcionario creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.funcionario.model.id){
					$state.go('app.funcionario');
				}else{
					
					funcionarioHttp.update({}, $scope.funcionario.model, function (data) {
					  $rootScope.loadingVisible = false;
					  $scope.funcionario.model=data;
						if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
						if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));}   					  
					   message.show("success", "Funcionario actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
								
					
				   
				}
			}
		}
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'D', descripcion: 'Eliminado'});   
        }        
    }
	//TIPO AUTENTICACION
      $scope.tipoAuth = {
            current: {},
            data: [],
            getData: function() {
                $scope.tipoAuth.data.push({value: 'APP',descripcion: 'Aplicación'});
                $scope.tipoAuth.data.push({value: 'LDAP',descripcion: 'Directorio Activo'});
            }
        }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoAuth.getData();
	
	
	//CARGAMOS LOS DATOS DEL FUNCIONARIO
	
	
	if(funcionario.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
    }
    else{
        $rootScope.loadingVisible = true;
		
        funcionarioHttp.read({},$state.params.funcionario, function (data) { 
            $scope.funcionario.model = data;
			
            if($scope.funcionario.model.fechaIngreso){$scope.funcionario.fechaIngreso.value=new Date(parseFloat($scope.funcionario.model.fechaIngreso));}    
			if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
			if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));}   
            
			//$scope.estados.current=$scope.funcionario.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.funcionario.model.estado})[0];
            $scope.tipoAuth.current = $filter('filter')($scope.tipoAuth.data, {value : $scope.funcionario.model.tipoAuth})[0];
			
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
	
    
    
  }
})();
/**=========================================================
 * Module: app.funcionario.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .service('funcionarioHttp', funcionarioHttp);

  funcionarioHttp.$inject = ['$resource', 'END_POINT'];


  function funcionarioHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/funcionario/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/funcionario/:id'
      }        
     
    };
    return $resource( END_POINT + '/General.svc/funcionario', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.funcionario.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .controller('funcionarioListController', funcionarioListController);

  funcionarioListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'funcionarioHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function funcionarioListController($scope, $filter, $state, ngDialog, tpl, funcionarioHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	$scope.ESTADO_INACTIVO = 'I';
    $scope.ESTADO_ACTIVO = 'A';
    $scope.ESTADO_ELIMINADO = 'D';
	
	
	//$rootScope.loadingVisible = true;

    $scope.funcionarios = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrefuncionarios:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.funcionarios.selectedAll = !$scope.funcionarios.selectedAll; 
        for (var key in $scope.funcionarios.selectedItems) {
          $scope.funcionarios.selectedItems[key].check = $scope.funcionarios.selectedAll;
        }
      },
      add : function() {
          var func = {
              id: 0,
              nombre: '',
              empresaId: 0,
			  matricula: '',
			  email: '',
			  documento: '',
			  direccion: '',
			  estado: '',
			  fechaIngreso: '',
			  codigo: '',
			  telefono: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
			  usuario: '',
			  contrasenia: '',
			  tipoAuth: ''
          };
        parametersOfState.set({ name : 'app.funcionario_add', params : { funcionario: func } });
        $state.go('app.funcionario_add');
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.funcionario_edit', params : { funcionario: item } });
        $state.go('app.funcionario_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            funcionarioHttp.remove({}, { id: id }, function(response) {
                $scope.funcionarios.getData();
                message.show("success", "Funcionario eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.funcionarios.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    funcionarioHttp.remove({}, { id: id }, function(response) {
                        $scope.funcionarios.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.funcionarios.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.funcionarios.filterText,
          "precision": false
        }];
        $scope.funcionarios.selectedItems = $filter('arrayFilter')($scope.funcionarios.dataSource, paramFilter);
        $scope.funcionarios.paginations.totalItems = $scope.funcionarios.selectedItems.length;
        $scope.funcionarios.paginations.currentPage = 1;
        $scope.funcionarios.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.funcionarios.paginations.currentPage == 1 ) ? 0 : ($scope.funcionarios.paginations.currentPage * $scope.funcionarios.paginations.itemsPerPage) - $scope.funcionarios.paginations.itemsPerPage;
        $scope.funcionarios.data = $scope.funcionarios.selectedItems.slice(firstItem , $scope.funcionarios.paginations.currentPage * $scope.funcionarios.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.funcionarios.data = [];
        $scope.funcionarios.loading = true;
        $scope.funcionarios.noData = false;
        funcionarioHttp.getList(function(response) {
          $scope.funcionarios.selectedItems = response;
          $scope.funcionarios.dataSource = response;
          for(var i=0; i<$scope.funcionarios.dataSource.length; i++){
            $scope.funcionarios.nombrefuncionarios.push({id: i, nombre: $scope.funcionarios.dataSource[i]});
          }
          $scope.funcionarios.paginations.totalItems = $scope.funcionarios.selectedItems.length;
          $scope.funcionarios.paginations.currentPage = 1;
          $scope.funcionarios.changePage();
          $scope.funcionarios.loading = false;
          ($scope.funcionarios.dataSource.length < 1) ? $scope.funcionarios.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
	//CARGAMOS LOS LISTADOS
    $scope.funcionarios.getData();
	}
  
  
  })();
(function() {
    'use strict';

    angular
        .module('app.lazyload')
        .config(lazyloadConfig);

    lazyloadConfig.$inject = ['$ocLazyLoadProvider', 'APP_REQUIRES'];
    function lazyloadConfig($ocLazyLoadProvider, APP_REQUIRES){

      // Lazy Load modules configuration
      $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: APP_REQUIRES.modules
      });

    }
})();
(function() {
  'use strict';

  angular
    .module('app.lazyload')
    .constant('APP_REQUIRES', {
    // jQuery based and standalone scripts
    scripts: {
      'whirl':              ['vendor/whirl/dist/whirl.css'],
      'classyloader':       ['vendor/jquery-classyloader/js/jquery.classyloader.min.js'],
      'animo':              ['vendor/animo.js/animo.js'],
      'fastclick':          ['vendor/fastclick/lib/fastclick.js'],
      'modernizr':          ['vendor/modernizr/modernizr.js'],
      'animate':            ['vendor/animate.css/animate.min.css'],
      'skycons':            ['vendor/skycons/skycons.js'],
      'icons':              ['vendor/fontawesome/css/font-awesome.min.css',
                             'vendor/simple-line-icons/css/simple-line-icons.css'],
      'weather-icons':      ['vendor/weather-icons/css/weather-icons.min.css'],
      'sparklines':         ['app/vendor/sparklines/jquery.sparkline.min.js'],
      'wysiwyg':            ['vendor/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                             'vendor/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
      'slimscroll':         ['vendor/slimScroll/jquery.slimscroll.min.js'],
      'screenfull':         ['vendor/screenfull/dist/screenfull.js'],
      'vector-map':         ['vendor/ika.jvectormap/jquery-jvectormap-1.2.2.min.js',
                             'vendor/ika.jvectormap/jquery-jvectormap-1.2.2.css'],
      'vector-map-maps':    ['vendor/ika.jvectormap/jquery-jvectormap-world-mill-en.js',
                             'vendor/ika.jvectormap/jquery-jvectormap-us-mill-en.js'],
      'loadGoogleMapsJS':   ['app/vendor/gmap/load-google-maps.js'],
      'flot-chart':         ['vendor/Flot/jquery.flot.js'],
      'flot-chart-plugins': ['vendor/flot.tooltip/js/jquery.flot.tooltip.min.js',
                             'vendor/Flot/jquery.flot.resize.js',
                             'vendor/Flot/jquery.flot.pie.js',
                             'vendor/Flot/jquery.flot.time.js',
                             'vendor/Flot/jquery.flot.categories.js',
                             'vendor/flot-spline/js/jquery.flot.spline.min.js'],
      // jquery core and widgets
      'jquery-ui':          ['vendor/jquery-ui/ui/core.js',
                             'vendor/jquery-ui/ui/widget.js'],
      // loads only jquery required modules and touch support
      'jquery-ui-widgets':  ['vendor/jquery-ui/ui/core.js',
                             'vendor/jquery-ui/ui/widget.js',
                             'vendor/jquery-ui/ui/mouse.js',
                             'vendor/jquery-ui/ui/draggable.js',
                             'vendor/jquery-ui/ui/droppable.js',
                             'vendor/jquery-ui/ui/sortable.js',
                             'vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min.js'],
      'moment' :            ['vendor/moment/min/moment-with-locales.min.js'],
      'inputmask':          ['vendor/jquery.inputmask/dist/jquery.inputmask.bundle.min.js'],
      'flatdoc':            ['vendor/flatdoc/flatdoc.js'],
      'codemirror':         ['vendor/codemirror/lib/codemirror.js',
                             'vendor/codemirror/lib/codemirror.css'],
      // modes for common web files
      'codemirror-modes-web': ['vendor/codemirror/mode/javascript/javascript.js',
                               'vendor/codemirror/mode/xml/xml.js',
                               'vendor/codemirror/mode/htmlmixed/htmlmixed.js',
                               'vendor/codemirror/mode/css/css.js'],
      'taginput' :          ['vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                             'vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'],
      'filestyle':          ['vendor/bootstrap-filestyle/src/bootstrap-filestyle.js'],
      'parsley':            ['vendor/parsleyjs/dist/parsley.min.js'],
      'fullcalendar':       ['vendor/fullcalendar/dist/fullcalendar.min.js',
                             'vendor/fullcalendar/dist/fullcalendar.css'],
      'gcal':               ['vendor/fullcalendar/dist/gcal.js'],
      'chartjs':            ['vendor/Chart.js/Chart.js'],
      'morris':             ['vendor/raphael/raphael.js',
                             'vendor/morris.js/morris.js',
                             'vendor/morris.js/morris.css'],
      'loaders.css':          ['vendor/loaders.css/loaders.css'],
      'spinkit':              ['vendor/spinkit/css/spinkit.css'],
      'ngImgCrop':            ['vendor/ng-img-crop/compile/unminified/ng-img-crop.js',
                               'vendor/ng-img-crop/compile/unminified/ng-img-crop.css'],
      'toastr':               ['vendor/toastr/toastr.js',
                              'vendor/toastr/toastr.css']
    },
    // Angular based script (use the right module name)
    modules: [
      {name: 'localytics.directives',     files: ['vendor/chosen_v1.2.0/chosen.jquery.min.js',
                                                  'vendor/chosen_v1.2.0/chosen.min.css',
                                                  'vendor/angular-chosen-localytics/chosen.js']},
      {name: 'ngDialog',                  files: ['vendor/ngDialog/js/ngDialog.min.js',
                                                  'vendor/ngDialog/css/ngDialog.min.css',
                                                  'vendor/ngDialog/css/ngDialog-theme-default.min.css'] },
      {name: 'ngWig',                     files: ['vendor/ngWig/dist/ng-wig.min.js'] },
      {name: 'ngTable',                   files: ['vendor/ng-table/dist/ng-table.min.js',
                                                  'vendor/ng-table/dist/ng-table.min.css']},
      {name: 'ngTableExport',             files: ['vendor/ng-table-export/ng-table-export.js']},
      {name: 'angularBootstrapNavTree',   files: ['vendor/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
                                                  'vendor/angular-bootstrap-nav-tree/dist/abn_tree.css']},
      {name: 'htmlSortable',              files: ['vendor/html.sortable/dist/html.sortable.js',
                                                  'vendor/html.sortable/dist/html.sortable.angular.js']},
      {name: 'xeditable',                 files: ['vendor/angular-xeditable/dist/js/xeditable.js',
                                                  'vendor/angular-xeditable/dist/css/xeditable.css']},
      {name: 'angularFileUpload',         files: ['vendor/angular-file-upload/angular-file-upload.js']},
      {name: 'ngFileUpload',              files: ['vendor/ng-file-upload-master/dist/ng-file-upload.min.js']},
      {name: 'ngImgCrop',                 files: ['vendor/ng-img-crop/compile/unminified/ng-img-crop.js',
                                                  'vendor/ng-img-crop/compile/unminified/ng-img-crop.css']},
      {name: 'ui.select',                 files: ['vendor/angular-ui-select/dist/select.js',
                                                  'vendor/angular-ui-select/dist/select.css']},
      {name: 'ui.codemirror',             files: ['vendor/angular-ui-codemirror/ui-codemirror.js']},
      {name: 'angular-carousel',          files: ['vendor/angular-carousel/dist/angular-carousel.css',
                                                  'vendor/angular-carousel/dist/angular-carousel.js']},
      {name: 'ngGrid',                    files: ['vendor/ng-grid/build/ng-grid.min.js',
                                                  'vendor/ng-grid/ng-grid.css' ]},
      {name: 'infinite-scroll',           files: ['vendor/ngInfiniteScroll/build/ng-infinite-scroll.js']},
      {name: 'ui.bootstrap-slider',       files: ['vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js',
                                                  'vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css',
                                                  'vendor/angular-bootstrap-slider/slider.js']},
      {name: 'ui.grid',                   files: ['vendor/angular-ui-grid/ui-grid.min.css',
                                                  'vendor/angular-ui-grid/ui-grid.min.js']},
      {name: 'textAngular',               files: ['vendor/textAngular/dist/textAngular.css',
                                                  'vendor/textAngular/dist/textAngular-rangy.min.js',
                                                  'vendor/textAngular/dist/textAngular-sanitize.js',
                                                  'vendor/textAngular/src/globals.js',
                                                  'vendor/textAngular/src/factories.js',
                                                  'vendor/textAngular/src/DOM.js',
                                                  'vendor/textAngular/src/validators.js',
                                                  'vendor/textAngular/src/taBind.js',
                                                  'vendor/textAngular/src/main.js',
                                                  'vendor/textAngular/dist/textAngularSetup.js'
                                                 ], serie: true},
      {name: 'angular-rickshaw',          files: ['vendor/d3/d3.min.js',
                                                  'vendor/rickshaw/rickshaw.js',
                                                  'vendor/rickshaw/rickshaw.min.css',
                                                  'vendor/angular-rickshaw/rickshaw.js'], serie: true},
      {name: 'angular-chartist',          files: ['vendor/chartist/dist/chartist.min.css',
                                                  'vendor/chartist/dist/chartist.js',
                                                  'vendor/angular-chartist.js/dist/angular-chartist.js'], serie: true},
      {name: 'ui.map',                    files: ['vendor/angular-ui-map/ui-map.js']},
      {name: 'datatables',                files: ['vendor/datatables/media/css/jquery.dataTables.css',
                                                  'vendor/datatables/media/js/jquery.dataTables.js',
                                                  'vendor/angular-datatables/dist/angular-datatables.js'], serie: true},
      {name: 'angular-jqcloud',           files: ['vendor/jqcloud2/dist/jqcloud.css',
                                                  'vendor/jqcloud2/dist/jqcloud.js',
                                                  'vendor/angular-jqcloud/angular-jqcloud.js']},
      {name: 'angularGrid',               files: ['vendor/ag-grid/dist/angular-grid.css',
                                                  'vendor/ag-grid/dist/angular-grid.js',
                                                  'vendor/ag-grid/dist/theme-dark.css',
                                                  'vendor/ag-grid/dist/theme-fresh.css']},
      {name: 'ng-nestable',               files: ['vendor/ng-nestable/src/angular-nestable.js',
                                                  'vendor/nestable/jquery.nestable.js']},
      {name: 'akoenig.deckgrid',          files: ['vendor/angular-deckgrid/angular-deckgrid.js']},
      {name: 'oitozero.ngSweetAlert',     files: ['vendor/sweetalert/dist/sweetalert.css',
                                                  'vendor/sweetalert/dist/sweetalert.min.js',
                                                  'vendor/angular-sweetalert/SweetAlert.js']},
      {name: 'bm.bsTour',                 files: ['vendor/bootstrap-tour/build/css/bootstrap-tour.css',
                                                  'vendor/bootstrap-tour/build/js/bootstrap-tour-standalone.js',
                                                  'vendor/angular-bootstrap-tour/dist/angular-bootstrap-tour.js'], serie: true},
      {name: 'al-ui', files : ['vendor/al-ui/al-ui.js']},
      {name: 'ab-base64', files : ['vendor/angular-utf8-base64/angular-utf8-base64.js']},
    { name: 'ngProgress', files : ['vendor/ngprogress/build/ngprogress.js',
                             'vendor/ngprogress/ngProgress.css']},
      {name: 'angular.bootstrap.datepicker',     files: ['vendor/angular-bootstrap-datepicker/ui-bootstrap-datepicker-0.14.3','vendor/angular-bootstrap-datepicker/ui-bootstrap-datepicker-tpls-0.14.3']},
      {name: 'al-ui', files : ['vendor/al-ui/al-ui.js']},
      {name: 'angular.morris-chart', files : ['vendor/angular-morris-chart/src/angular-morris-chart.min.js']},
      {name: 'ui.utils.masks', files : ['vendor/angular-input-masks/angular-input-masks-standalone.min.js']}
    ]
  })
  ;

})();

(function() {
    'use strict';

    angular
        .module('app.loadingbar')
        .config(loadingbarConfig)
        ;
    loadingbarConfig.$inject = ['cfpLoadingBarProvider'];
    function loadingbarConfig(cfpLoadingBarProvider){
      cfpLoadingBarProvider.includeBar = true;
      cfpLoadingBarProvider.includeSpinner = false;
      cfpLoadingBarProvider.latencyThreshold = 500;
      cfpLoadingBarProvider.parentSelector = '.wrapper > section';
    }
})();
(function() {
    'use strict';

    angular
        .module('app.loadingbar')
        .run(loadingbarRun)
        ;
    loadingbarRun.$inject = ['$rootScope', '$timeout', 'cfpLoadingBar'];
    function loadingbarRun($rootScope, $timeout, cfpLoadingBar){

      // Loading bar transition
      // ----------------------------------- 
      var thBar;
      $rootScope.$on('$stateChangeStart', function() {
        if($('.wrapper > section').length) // check if bar container exists
            thBar = $timeout(function() {
              cfpLoadingBar.start();
            }, 0); // sets a latency Threshold
      });
      $rootScope.$on('$stateChangeSuccess', function(event) {
          event.targetScope.$watch('$viewContentLoaded', function () {
            $timeout.cancel(thBar);
            cfpLoadingBar.complete();
          });
      });

    }

})();
/**=========================================================
 * Module: demo-pagination.js
 * Provides a simple demo for pagination
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.mailbox')
        .controller('MailboxController', MailboxController);

    function MailboxController() {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          vm.folders = [
            {name: 'Bandeja Entrada',   folder: 'inbox',   alert: 42, icon: 'fa-inbox' },
            {name: 'Notificaciones', folder: 'notificacion', alert: 10, icon: 'fa-bell-o' },
            {name: 'Enviados',    folder: 'enviados',    alert: 0,  icon: 'fa-paper-plane-o' },
            {name: 'Destacados',   folder: 'destacados',   alert: 5,  icon: 'fa-star-o' },
            {name: 'Eliminados',   folder: 'eliminados',   alert: 0,  icon: 'fa-trash-o'}
          ];

          /*
          vm.labels = [
            {name: 'Red',     color: 'danger'},
            {name: 'Pink',    color: 'pink'},
            {name: 'Blue',    color: 'info'},
            {name: 'Yellow',  color: 'warning'}
          ];
          */

          vm.mail = {
            cc: false,
            bcc: false
          };
          
          
          // Mailbox editr initial content
          vm.content = '<p>Type something..</p>';
          
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('app.mailbox')
        .controller('MailFolderController', MailFolderController);

    MailFolderController.$inject = ['mails', '$stateParams'];
    function MailFolderController(mails, $stateParams) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          debugger;
          vm.folder = {};
          // no filter for inbox
          vm.folder.folder = $stateParams.folder === 'inbox' ? '' : $stateParams.folder;

          mails.all().then(function(mails){
            vm.mails = mails;
          });
        }
    }
})();

// A RESTful factory for retrieving mails from json file

(function() {
    'use strict';

    angular
        .module('app.mailbox')
        .factory('mails', mails);

    mails.$inject = ['$http'];
    function mails($http) {
        var service = {
            all: all,
            get: get
        };
        return service;

        ////////////////
        
        function readMails() {
          var path = 'http://north-legacy.codio.io:8080/api/v1/anh/news';
          return $http.get(path).then(function (resp) {
            return resp.data.mails;
          });
        }

        function all() {
          return readMails();
        }

        function get(id) {
          return readMails().then(function(mails){
            for (var i = 0; i < mails.length; i++) {
              if (+mails[i].id === +id) return mails[i];
            }
            return null;
          });
        }
    }
})();


(function() {
  'use strict';

  angular
    .module('app.mailbox')
    .controller('MailViewController', MailViewController);

  MailViewController.$inject = ['mails', '$stateParams'];
  function MailViewController(mails, $stateParams) {
    debugger;
    var vm = this;

    activate();

    ////////////////

    function activate() {
      debugger;
      mails.get($stateParams.mid).then(function(mail){
        vm.mail = mail;
      });
    }
    
    
  }
})();

/**=========================================================
 * Module: app.mensajeria.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .controller('mensajeriaController', mensajeriaController);

  mensajeriaController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'mensajeriaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function mensajeriaController($scope, $filter, $state, LDataSource, mensajeriaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var mensajeria = $state.params.mensajeria;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.mensajeria = {
		model : {
                id: 0,
                terceroId: 0,                      
                consecutivo: '',                     
                estado: '',
                descripcion: '',                  
                fechaRec: '',    
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''		
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaAct.isOpen = false;
        }
      },	
       fechaRec : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaRec.isOpen = true;
        }
      },
      back : function() {
          parametersOfState.set({ name : 'app.mensajeria', params : { filters : {}, data : []} });
        $state.go('app.mensajeria');
          
      },
      save : function() {		  
       
		  
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.mensajeria.model.terceroId = $scope.tercerosOp.current.id;}          
          		
				if($scope.mensajeria.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.mensajeria.model.estado= $scope.estados.current.value;}
			
          
                 $scope.mensajeria.model.fechaRec=Date.parse(new Date($scope.mensajeria.fechaRec.value));
          
			//INSERTAR
            if($scope.mensajeria.model.id==0){
				
				$rootScope.loadingVisible = true;
				mensajeriaHttp.save({}, $scope.mensajeria.model, function (data) { 
					
						mensajeriaHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.mensajeria.model=data;
							mensajeria.id=$scope.mensajeria.model.id; 
							if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}    
							if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));} 	
                            if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));} 	
                           
                            
                                $scope.btnAdjunto=true;
                                
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.mensajeria.model.id){
					$state.go('app.mensajeria');
				}else{
					
					mensajeriaHttp.update({}, $scope.mensajeria.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.mensajeria.model=data;
						if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}    
						if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));} 
                        if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));} 
                        
                        
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                            message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'mensajeria' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/mensajeria/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.mensajeria.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'mensajeria' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }        
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'});             
        }        
    }

    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            mensajeriaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.mensajeria.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.mensajeria.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.mensajeria.model.terceroId=$scope.tercerosOp.current.id;
      }
    }

	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	    
    $scope.tercerosOp.getData();     
      
   
  
	
	//CARGAMOS LOS DATOS DEL mensajeria	
	
	if(mensajeria.id==0){
        
      $scope.estados.current=$scope.estados.data[0];    
      var date = new Date(); 
        
    $scope.mensajeria.fechaRec.value=new Date(date.getFullYear(), date.getMonth(), date.getDate());
          
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            mensajeriaHttp.read({},$state.params.mensajeria, function (data) { 
                
            $scope.mensajeria.model = data;	
            $scope.btnAdjunto=true;            
               
			if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}    
			if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));}  
            if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));}  
     
            
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.mensajeria.model.terceroId })[0]; 
			//$scope.estados.current=$scope.mensajeria.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.mensajeria.model.estado})[0];
               
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .service('mensajeriaHttp', facturaHttp);

  facturaHttp.$inject = ['$resource', 'END_POINT'];


  function facturaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,       
          'url' : END_POINT + '/Cliente.svc/mensajeria/:id'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/mensajeria/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/mensajeria'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/mensajeria'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/mensajeria/:id'
      }, 
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      }
     
    };
    return $resource( END_POINT + '/Cliente.svc/mensajeria', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.mensajeria.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .controller('mensajeriaListController', mensajeriaListController);

  mensajeriaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'mensajeriaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function mensajeriaListController($scope, $filter, $state, ngDialog, tpl, mensajeriaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
		
	$scope.ESTADO_PROCESO = 'P';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    	

    $scope.mensajerias = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombremensajerias:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.mensajerias.selectedAll = !$scope.mensajerias.selectedAll; 
        for (var key in $scope.mensajerias.selectedItems) {
          $scope.mensajerias.selectedItems[key].check = $scope.mensajerias.selectedAll;
        }
      },
      add : function() {
        
       
            
              var mens = {
                id: 0,
                terceroId: 0,                      
                consecutivo: '',                     
                estado: '',
                descripcion: '',                  
                fechaRec: '',    
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.mensajeria_add', params : { mensajeria: mens } });
            $state.go('app.mensajeria_add');
      
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.mensajeria_edit', params : { mensajeria: item } });
        $state.go('app.mensajeria_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            mensajeriaHttp.remove({}, { id: id }, function(response) {
                $scope.mensajerias.getData();
                message.show("success", "mensajeria eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.mensajerias.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    mensajeriaHttp.remove({}, { id: id }, function(response) {
                        $scope.mensajerias.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.mensajerias.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.mensajerias.filterText,
          "precision": false
        }];
        $scope.mensajerias.selectedItems = $filter('arrayFilter')($scope.mensajerias.dataSource, paramFilter);
        $scope.mensajerias.paginations.totalItems = $scope.mensajerias.selectedItems.length;
        $scope.mensajerias.paginations.currentPage = 1;
        $scope.mensajerias.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.mensajerias.paginations.currentPage == 1 ) ? 0 : ($scope.mensajerias.paginations.currentPage * $scope.mensajerias.paginations.itemsPerPage) - $scope.mensajerias.paginations.itemsPerPage;
        $scope.mensajerias.data = $scope.mensajerias.selectedItems.slice(firstItem , $scope.mensajerias.paginations.currentPage * $scope.mensajerias.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.mensajerias.data = [];
        $scope.mensajerias.loading = true;
        $scope.mensajerias.noData = false;
       
        mensajeriaHttp.getList({},{} ,function(response) {
          $scope.mensajerias.selectedItems = response;
          $scope.mensajerias.dataSource = response;
          for(var i=0; i<$scope.mensajerias.dataSource.length; i++){
            $scope.mensajerias.nombremensajerias.push({id: i, nombre: $scope.mensajerias.dataSource[i]});
          }
          $scope.mensajerias.paginations.totalItems = $scope.mensajerias.selectedItems.length;
          $scope.mensajerias.paginations.currentPage = 1;
          $scope.mensajerias.changePage();
          $scope.mensajerias.loading = false;
          ($scope.mensajerias.dataSource.length < 1) ? $scope.mensajerias.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }

	//CARGAMOS LOS TIPOS DE PROCESO
    
    $scope.mensajerias.getData();  

        
	}
  
  
  })();
/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('menuAddController', menuAddController);

  menuAddController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'menuRolHttp', 'message', '$rootScope', 'parameters', '$modalInstance'];


  function menuAddController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, menuRolHttp, message, $rootScope, parameters, $modalInstance){
    $scope.datosParams= parameters;
    var datos=$scope.datosParams.detalle;
    
    $scope.menu= {
        model : {
            alert: '',
            icon: '',
            index: 0,
            label: '',
            orden: 0,
            padreId: 0,
            param: '',
            sref: '',
            submenu: null,
            text: '',
            urlParam: '',
            menuName:''
        },
        add: function(){
          if(!$scope.menu.model.label){message.show("error", "Label Requerido..");return;}
          if($scope.menu.model.label==""){message.show("error", "Label Requerido");return;}
          if(!$scope.menu.model.text){message.show("error", "Texto Requerido");return;}
          if($scope.menu.model.text==""){message.show("error", "Texto Requerido");return;}
          
          var paramatros = {
            alert: '',
            icon: $scope.menu.model.name,
            index: $scope.menu.model.index,
            label: $scope.menu.model.label,
            orden: $scope.menu.model.index,
            padreId: $scope.menu.model.padreId,
            param: $scope.menu.model.param,
            sref: $scope.menu.model.sref,
            submenu: null,
            text: $scope.menu.model.text,
            urlParam: $scope.menu.model.urlParam
          };
          
          $rootScope.loadingVisible = true;
          
          var _Http = new menuRolHttp($scope.menu.model);
           _Http.$newMenu (function(response) {
             var parametros = { menuId:response.menuId, rolId:1, seleccionado:true};
             var _HttpRol = new menuRolHttp(parametros);
             $rootScope.loadingVisible = true;  
             _HttpRol.$assignRol(function(response) {}, function(faild) {});
            $rootScope.loadingVisible = false;
            if(datos.padreId!=0){
              message.show("success", "Submenú Agregado");
            }else{
              message.show("success", "Menú Agregado");
            }
            $scope.menu.close();

          }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", "No se creó el Menu: " + faild.Message);
          })//*/
        },
        edit: function(){
          
          $rootScope.loadingVisible = true;
            
          var _Http = new menuRolHttp($scope.menu.model);
          
          _Http.$updateMenu(function(response) {
            $rootScope.loadingVisible = false;
            if(datos.padreId!=0){
              message.show("success", "Submenú Editado");
            }else{
              message.show("success", "Menú Editado");
            }
            $scope.menu.close();

          }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", "No se cargaron los Menus: " + faild.Message);
          })
        },
        dismiss : function() {
            $modalInstance.dismiss('cancel');
        },
        close : function() {
            $modalInstance.close('close');
        }
    }
    $scope.menu.model=datos;
  }
})();
/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('menuRolController', menuRolController);

  menuRolController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'sidebarHttp', 'UsuarioHttp'];


  function menuRolController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, sidebarHttp, UsuarioHttp){
    
    $scope.menu= {
        current : {},
        data:[],
        getData: function(){
            sidebarHttp.getMenu({},{},function(response) {
                $scope.menu.data = $filter('orderBy')(response, 'menuId');
                for(var i = 0; i < $scope.menu.data.length; i++){
                    $scope.menu.data[i].submenu = $filter('orderBy')($scope.menu.data[i].submenu, 'menuId');
                }
            }, function(faild) {
                $scope.menu.data = [];
                message.show("error", "No se cargaron los Menus: " + faild.Message);
            })
        }, edit: function (item){
            var parameter = {id:0, nombreOperadora:$scope.operadora.model.nombre, empresaId:$scope.operadora.model.id};
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menuRol_edit.html',
                controller: 'menuRolController',
                size: 'lg',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {$scope.menu.getData();});
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        getData: function(){
            UsuarioHttp.consultarPerfilesAll({}, {}, function(response) {
                $scope.roles.data = response;
            }, function(faild) {
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        }
    }
    
    $scope.menu.getData();
    $scope.roles.getData();
  }
})();
/**=========================================================
 * Module: app.setMenu.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .service('menuRolHttp', menuRolHttp);

  menuRolHttp.$inject = ['$resource', 'END_POINT'];


  function menuRolHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getListMenu' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/menu'
      },
      'getMenuRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/menu/:id/rol'
      },
      'getRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/rol'
      },
      'updateMenu' : {///
        'method' : 'PUT',
        'url' : END_POINT + '/General.svc/menu'
      },
      'removeMenu':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/menu/:id'
      },
      'newMenu':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/menu/'
      },
      'deleteMenu': {
          'method':'DELETE',
          'isArray' : true,
          'params' : {
            menuId : '@menuId'
          },
          'url' : END_POINT + '/General.svc/menu/:menuId'
      },
      'deleteRol': {
          'method':'DELETE',
          'params' : {
            rolId : '@rolId'
          },
          'url' : END_POINT + '/General.svc/rol/:rolId'
      },
      'assignRol':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/menu/rol'
      },
      'unassignRol':   {
        'method':'PUT',
        'url' : END_POINT + '/General.svc/menu/rol'
      }
    };
    return $resource( END_POINT + '/General.svc/menu', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('setMenuController', setMenuController);

  setMenuController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'tpl', 'ngDialog', 'parametersOfState', 'REGULAR_EXPRESION', 'menuRolHttp', 'message', '$rootScope'];


  function setMenuController($scope, $filter, $state, LDataSource, $modal, tpl, ngDialog, parametersOfState, REGULAR_EXPRESION, menuRolHttp, message, $rootScope){
    $scope.menuName='';
    $scope.menuId=0;
    $scope.nameMenu='';
    $scope.menu= {
        current : {
          menuName:'',
          menuId:0
        },
        data:[],
        getData: function(){
            $scope.menuName='';
            $scope.menuId=0;
            $scope.roles.data=[];
            $rootScope.loadingVisible = true;
            menuRolHttp.getListMenu({},{},function(response) {
                $scope.menu.data = $filter('orderBy')(response, 'menuId');
                for(var i = 0; i < $scope.menu.data.length; i++){
                    $scope.menu.data[i].submenu = $filter('orderBy')($scope.menu.data[i].submenu, 'menuId');
                    $scope.menu.data[i].menuName= $scope.menu.data[i].label;
                    $scope.menu.data[i].show=false;
                    if($scope.menu.data[i].submenu!=null){
                        for(var j=0; j<$scope.menu.data[i].submenu.length; j++){
                            $scope.menu.data[i].submenu[j].menuName=$scope.menu.data[i].menuName + "/" + $scope.menu.data[i].submenu[j].label;
                            $scope.menu.data[i].submenu[j].show=false;
                            if($scope.menu.data[i].submenu[j].submenu!=null){
                                $scope.menu.data[i].submenu[j].submenu = $filter('orderBy')($scope.menu.data[i].submenu[j].submenu, 'menuId');
                                for(var k=0; k<$scope.menu.data[i].submenu[j].submenu.length; k++){
                                    $scope.menu.data[i].submenu[j].submenu[k].show=false;
                                    $scope.menu.data[i].submenu[j].submenu[k].menuName=$scope.menu.data[i].label + "/" + $scope.menu.data[i].submenu[j].label + "/" + $scope.menu.data[i].submenu[j].submenu[k].label;
                                }
                            }
                        }
                    }
                }
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $scope.menu.data = [];
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los Menus: " + faild.Message);
            })
        }, edit: function (item){
            var parametros={
              detalle:{},
              edit:true,
              menuName:'Menú'
            };
            parametros.detalle=item;
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
        }, addSubmenu: function (item){
            var parametros={
              detalle:{
                alert: '',
                icon: '',
                index: 0,
                label: '',
                orden: 0,
                padreId: item.menuId,
                param: '',
                sref: '',
                submenu: null,
                text: '',
                urlParam: ''
              },
              edit:false,
              menuName:'Submenú'
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
            
        }, addMenu: function (){
            var parametros={
              detalle:{
                alert: '',
                icon: '',
                index: 0,
                label: '',
                orden: 0,
                padreId: 0,
                param: '',
                sref: '',
                submenu: null,
                text: '',
                urlParam: ''
              },
              edit:false,
              menuName:'Menú',
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
            
        }, getRol: function(item){
            $scope.menuName=item.menuName;
            $scope.menuId=item.menuId;
            $scope.roles.getData();
            $scope.nameMenu=item.label;
        }, remove: function(item){
            ngDialog.openConfirm({
                template: tpl.path,
                className: 'ngdialog-theme-default',
                scope: $scope
            }).then(function (value) {
                var parametros={menuId: $scope.roles.menuId, rol: 'ADMINISTRADOR', rolId: 1, seleccionado: false};
                var _Http = new menuRolHttp(parametros);
                _Http.$unassignRol(function(response) {
                  $rootScope.loadingVisible = false;
                }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se Desasignó el Rol: " + faild.Message);
                })
                var id = item.menuId;
                console.log(id);
                $rootScope.loadingVisible = true;
                menuRolHttp.removeMenu({}, { id: id }, function(response) {
                    $scope.menu.getData();
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", "No se Eliminó el Menú: " + faild.Message);
                });
            });
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        menuId: 0,
        getData: function(){
            $rootScope.loadingVisible = true;
            menuRolHttp.getMenuRol({},{ id : $scope.menuId}, function (response) {
                $scope.roles.data = response;
                $scope.roles.menuId = $scope.menuId;
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        },
        checkRol: function(item){
            var _Http = new menuRolHttp(item);
            $rootScope.loadingVisible = true;
            if(item.seleccionado==true){
              $rootScope.loadingVisible = true;
              _Http.$assignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Asignó el Rol: " + faild.Message);
              })
            }
            else{
              _Http.$unassignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Desasignó el Rol: " + faild.Message);
              })
            }
        }
    }
    
    $scope.menu.getData();
  }
})();
/**=========================================================
 * Module: navbar-search.js
 * Navbar search toggler * Auto dismiss on ESC key
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.navsearch')
        .directive('searchOpen', searchOpen)
        .directive('searchDismiss', searchDismiss);

    //
    // directives definition
    // 
    
    function searchOpen () {
        var directive = {
            controller: searchOpenController,
            restrict: 'A'
        };
        return directive;

    }

    function searchDismiss () {
        var directive = {
            controller: searchDismissController,
            restrict: 'A'
        };
        return directive;
        
    }

    //
    // Contrller definition
    // 
    
    searchOpenController.$inject = ['$scope', '$element', 'NavSearch'];
    function searchOpenController ($scope, $element, NavSearch) {
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', NavSearch.toggle);
    }

    searchDismissController.$inject = ['$scope', '$element', 'NavSearch'];
    function searchDismissController ($scope, $element, NavSearch) {
      
      var inputSelector = '.navbar-form input[type="text"]';

      $(inputSelector)
        .on('click', function (e) { e.stopPropagation(); })
        .on('keyup', function(e) {
          if (e.keyCode === 27) // ESC
            NavSearch.dismiss();
        });
        
      // click anywhere closes the search
      $(document).on('click', NavSearch.dismiss);
      // dismissable options
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', NavSearch.dismiss);
    }

})();


/**=========================================================
 * Module: nav-search.js
 * Services to share navbar search functions
 =========================================================*/
 
(function() {
    'use strict';

    angular
        .module('app.navsearch')
        .service('NavSearch', NavSearch);

    function NavSearch() {
        this.toggle = toggle;
        this.dismiss = dismiss;

        ////////////////

        var navbarFormSelector = 'form.navbar-form';

        function toggle() {
          var navbarForm = $(navbarFormSelector);

          navbarForm.toggleClass('open');
          
          var isOpen = navbarForm.hasClass('open');
          
          navbarForm.find('input')[isOpen ? 'focus' : 'blur']();
        }

        function dismiss() {
          $(navbarFormSelector)
            .removeClass('open') // Close control
            .find('input[type="text"]').blur() // remove focus
            .val('') // Empty input
            ;
        }        
    }
})();

/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('crearClienteController', crearClienteController);

  crearClienteController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'oportunidadHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function crearClienteController($scope, $filter, $state, $modalInstance, LDataSource, oportunidadHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
      
      
      
      $scope.cliente = {     
          model:{
              identificacion: '',
              nombre: '',
              telefono: '', 
              direccion: '', 
              email: '',              
              nombreContacto: '',
              telefonoContacto: '',
              cargoContacto: ''
          },         
          
         save:  function(){ 
                
                if($scope.cliente.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
                
                   
             var _http = new oportunidadHttp($scope.cliente.model);
                  _http.$addTercero(function(response){                  
                      
                      
                      message.show("success", "Cliente creado satisfactoriamente!!");
                      $modalInstance.close(response);
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
                 
              
              
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }      
       	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      //$scope.cliente.model=parameters.cliente;
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('oportunidadController', oportunidadController);

  oportunidadController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'oportunidadHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];

  function oportunidadController($scope, $rootScope, $state,$modalInstance,oportunidadHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {
      
      
      var oportunidad = parameters.oportunidad;
      var currentDate = new Date();     
      
      $scope.oportunidad = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,
              padreId: 0,
              tipo: '',
              consecutivo: '',
			  asunto: '',
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
              fechaEmision: '',
              fechaVencimiento: ''
            
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.oportunidad.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.oportunidad.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.oportunidad', params : { filters : {procesoSeleccionado:oportunidad.tipo}, data : []} });
        $state.go('app.oportunidad');
          
      },
      save : function() {		  
       
		  
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.oportunidad.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.oportunidad.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				if($scope.oportunidad.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.oportunidad.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoProceso.current){message.show("warning", "Tipo proceso requerido");return;}
				else{$scope.oportunidad.model.tipo= $scope.tipoProceso.current.value;}	
          
                if(!$scope.viaContacto.current){message.show("warning", "Vía de contacto requerido");return;}
				else{$scope.oportunidad.model.viaContactoId= $scope.viaContacto.current.codigo;}	
          
			//INSERTAR
            if($scope.oportunidad.model.id==0){
				
				$rootScope.loadingVisible = true;
				oportunidadHttp.save({}, $scope.oportunidad.model, function (data) { 
					
						oportunidadHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.oportunidad.model=data;
							oportunidad.id=$scope.oportunidad.model.id; 
							if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
							if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));} 		
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.oportunidad.model.id){
					$state.go('app.oportunidad');
				}else{
					
					oportunidadHttp.update({}, $scope.oportunidad.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.oportunidad.model=data;
						if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
						if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/oportunidad/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.oportunidad.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.oportunidad.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.padreId,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      }
    }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'}); 
        }        
    }
	//TIPO PROCESO
      $scope.tipoProceso = {
            current: {},
            data: [],
            getData: function() {
                $scope.tipoProceso.data.push({value:'O', descripcion: 'Oportunidad'});
                $scope.tipoProceso.data.push({value:'POR', descripcion: 'Portafolio'});
                $scope.tipoProceso.data.push({value:'PRO', descripcion: 'Propuesta'});  
                    
            }
        }
      
    //VIA CONTACTO
    $scope.viaContacto = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        oportunidadHttp.getViaContacto({}, {}, function(response) {
            $scope.viaContacto.data = $filter('orderBy')(response, 'codigo');
            
            if(!$scope.oportunidad.model){$scope.viaContacto.current = $scope.viaContacto.data[0];}
            else{$scope.viaContacto.current = $filter('filter')($scope.viaContacto.data, { codigo : $scope.oportunidad.model.viaContactoId })[0];}
            
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setViaContacto : function(){
          $scope.oportunidad.model.viaContactoId=$scope.viaContacto.current.codigo;
    }}
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            oportunidadHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.oportunidad.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.oportunidad.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.oportunidad.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.oportunidad.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                oportunidadHttp.getContactosEmpresa({}, { terceroId: $scope.oportunidad.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.oportunidad.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.oportunidad.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.oportunidad.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    
      //PRODUCTOS
    $scope.productosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            oportunidadHttp.getProductos({}, {}, function(response) {   
                
                $scope.productosOp.data = response ;                 
                $scope.productosOp.current=$scope.productosOp.data[0];
                $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
    
    
    $scope.productoOportunidad = {
    model : {  
            productoId: 0,
            oportunidadId :0,
            total :0,
            valor :0,
            cantidad :0,
            valorIva :0,
            producto: '',
            comentario: ''
			
		},
      current : {},
      data:[],
      getData : function() {
          
          
          if(oportunidad){
              
            if($scope.oportunidad.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              oportunidadHttp.getProductosOportunidad({}, { oportunidadId: $scope.oportunidad.model.id }, function(response) {
                  
                  $scope.productoOportunidad.data = response;
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },        
        add: function(){           
            
              
            if($scope.oportunidad.model.id!=0){
                if($scope.productosOp.current){
                    $rootScope.loadingVisible = true;                    
                    
                    var data ={
                         productoId: $scope.productosOp.current.id,
                         oportunidadId :$scope.oportunidad.model.id,
                         total :  $scope.productosOp.current.valor,
                         valor :$scope.productosOp.current.valor,
                         cantidad :1,
                         valorIva :0,
                         producto: $scope.productosOp.current.nombre,
                         comentario: ''
                        
                    } ;
                      oportunidadHttp.addProductoOportunidad({}, data, function(response) {

                          $scope.productoOportunidad.getData();
                          $rootScope.loadingVisible = false;
                          message.show("success", "Producto agregado satisfactoriamente");	
                      }, function(faild) {
                          $rootScope.loadingVisible = false;
                          message.show("error", faild.Message);
                      });    
                }
                else{message.show("warning", "Debe seleccionar un producto");}

            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
          
        }
        ,        
        update: function(data){
            
            
            if($scope.oportunidad.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              oportunidadHttp.editProductoOportunidad({}, data, function(response) {
                  
                  $scope.productoOportunidad.getData();
                  $rootScope.loadingVisible = false;
                  message.show("success", "Producto actualizado satisfactoriamente");	
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
        }
        ,delete: function(data){            
            
            $rootScope.loadingVisible = true;          
            oportunidadHttp.removeProductoOportunidad({}, {oportunidadId: data.oportunidadId,productoId: data.productoId }, function(response){
             $scope.productoOportunidad.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
        }
    }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoProceso.getData();	
    $scope.viaContacto.getData();
    //$scope.tercerosOp.getData();   
    //$scope.productosOp.getData();
      
   
  
	
	//CARGAMOS LOS DATOS DEL OPORTUNIDAD	
	
	if(oportunidad.id==0){
        
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : oportunidad.tipo})[0]; 
      $scope.viaContacto.current=  $scope.viaContacto[0];   
      $scope.oportunidad.model.tipo=oportunidad.tipo;             
        
    }
    else{   
        $rootScope.loadingVisible = true;
		      
            debugger;
            oportunidadHttp.read({},$state.params.oportunidad, function (data) { 
                
            $scope.oportunidad.model = data;	
            $scope.btnAdjunto=true;
            $scope.divProductosOportunidad=true;
               
			if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
			if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));}  
            
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.oportunidad.model.terceroId })[0];  
            $scope.contactosOp.getData();
            $scope.productoOportunidad.getData();                
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.oportunidad.model.contactoId })[0];    
                
			//$scope.estados.current=$scope.oportunidad.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.oportunidad.model.estado})[0];
            $scope.tipoProceso.current = $filter('filter')($scope.tipoProceso.data, {value : $scope.oportunidad.model.tipo})[0];
            $scope.viaContacto.current = $filter('filter')($scope.viaContacto.data, {codigo : $scope.oportunidad.model.viaContactoId})[0];
                
             if  ($scope.oportunidad.model.padreId!=0){
                 
                 $scope.btnHistorico=true;
             }     
                
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .service('oportunidadHttp', oportunidadHttp);

  oportunidadHttp.$inject = ['$resource', 'END_POINT'];


  function oportunidadHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            proceso : '@proceso'           
          },
          'url' : END_POINT + '/Comercial.svc/oportunidad'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      },
       'getProductos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/productoBusqueda'
      },
       'getProductosOportunidad' : {
       'params' : {
            oportunidadId : '@oportunidadId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:oportunidadId/producto'
      },
        'addProductoOportunidad': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
        'editProductoOportunidad': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
      'removeProductoOportunidad':  {
        'params' : {
            oportunidadId : '@oportunidadId',
            productoId : '@productoId'   
          },
        'method':'DELETE',        
        'url' : END_POINT + '/Comercial.svc/oportunidad/:oportunidadId/producto/:productoId'
      }
     
    };
    return $resource( END_POINT + '/Comercial.svc/oportunidad', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.oportunidad.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('oportunidadListController', oportunidadListController);

  oportunidadListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'oportunidadHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function oportunidadListController($scope, $filter,$state,$modal, ngDialog, tpl, oportunidadHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        
		
	$scope.VERDE = 0;
    $scope.AMARILLO = 1;
    $scope.ROJO= 2;	    	

    $scope.oportunidades = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreoportunidades:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.oportunidades.selectedAll = !$scope.oportunidades.selectedAll; 
        for (var key in $scope.oportunidades.selectedItems) {
          $scope.oportunidades.selectedItems[key].check = $scope.oportunidades.selectedAll;
        }
      },
      add : function() {
        
        if($scope.tipoProceso.current.value){        
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: $scope.tipoProceso.current.value,
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.oportunidad_add', params : { oportunidad: oport } });
            $state.go('app.oportunidad_add');
          
          }
          else{
              
              message.show("warning", "Debe seleccionar el tipo de proceso!!")
          }
          
      },
      edit : function(item) {
      
          
        var modalInstance = $modal.open({
          templateUrl: 'app/views/oportunidad/oportunidad_form.html',
          controller: 'oportunidadController',
          size: 'lg',
          resolve: {
            parameters: { oportunidad: item }
          }
        });
        modalInstance.result.then(function (parameters) {
        }); 
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            oportunidadHttp.remove({}, { id: id }, function(response) {
                $scope.oportunidades.getData();
                message.show("success", "Oportunidad eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.oportunidades.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    oportunidadHttp.remove({}, { id: id }, function(response) {
                        $scope.oportunidades.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.oportunidades.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.oportunidades.filterText,
          "precision": false
        }];
        $scope.oportunidades.selectedItems = $filter('arrayFilter')($scope.oportunidades.dataSource, paramFilter);
        $scope.oportunidades.paginations.totalItems = $scope.oportunidades.selectedItems.length;
        $scope.oportunidades.paginations.currentPage = 1;
        $scope.oportunidades.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.oportunidades.paginations.currentPage == 1 ) ? 0 : ($scope.oportunidades.paginations.currentPage * $scope.oportunidades.paginations.itemsPerPage) - $scope.oportunidades.paginations.itemsPerPage;
        $scope.oportunidades.data = $scope.oportunidades.selectedItems.slice(firstItem , $scope.oportunidades.paginations.currentPage * $scope.oportunidades.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.oportunidades.data = [];
        $scope.oportunidades.loading = true;
        $scope.oportunidades.noData = false;        
          
        oportunidadHttp.getList({}, {},function(response) {
          $scope.oportunidades.selectedItems = response;
          $scope.oportunidades.dataSource = response;
          for(var i=0; i<$scope.oportunidades.dataSource.length; i++){
            $scope.oportunidades.nombreoportunidades.push({id: i, nombre: $scope.oportunidades.dataSource[i]});
          }
          $scope.oportunidades.paginations.totalItems = $scope.oportunidades.selectedItems.length;
          $scope.oportunidades.paginations.currentPage = 1;
          $scope.oportunidades.changePage();
          $scope.oportunidades.loading = false;
          ($scope.oportunidades.dataSource.length < 1) ? $scope.oportunidades.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }   
	//CARGAMOS DATA
    $scope.oportunidades.getData();         
   
        
	}
  
  
  })();
/**=========================================================
 * Module: access-login.js
 * Demo for login api
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.pages')
        .controller('LoginFormController', LoginFormController);

    LoginFormController.$inject = ['$http', '$state'];
    function LoginFormController($http, $state) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          // bind here all data from the form
          vm.account = {};
          // place the message if something goes wrong
          vm.authMsg = '';

          vm.login = function() {
            vm.authMsg = '';

            if(vm.loginForm.$valid) {

              $http
                .post('api/account/login', {email: vm.account.email, password: vm.account.password})
                .then(function(response) {
                  // assumes if ok, response is an object with some data, if not, a string with error
                  // customize according to your api
                  if ( !response.account ) {
                    vm.authMsg = 'Incorrect credentials.';
                  }else{
                    $state.go('app.dashboard');
                  }
                }, function() {
                  vm.authMsg = 'Server Request Error';
                });
            }
            else {
              // set as dirty if the user click directly to login so we show the validation messages
              /*jshint -W106*/
              vm.loginForm.account_email.$dirty = true;
              vm.loginForm.account_password.$dirty = true;
            }
          };
        }
    }
})();

/**=========================================================
 * Module: access-register.js
 * Demo for register account api
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.pages')
        .controller('RegisterFormController', RegisterFormController);

    RegisterFormController.$inject = ['$http', '$state'];
    function RegisterFormController($http, $state) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          // bind here all data from the form
          vm.account = {};
          // place the message if something goes wrong
          vm.authMsg = '';
            
          vm.register = function() {
            vm.authMsg = '';

            if(vm.registerForm.$valid) {

              $http
                .post('api/account/register', {email: vm.account.email, password: vm.account.password})
                .then(function(response) {
                  // assumes if ok, response is an object with some data, if not, a string with error
                  // customize according to your api
                  if ( !response.account ) {
                    vm.authMsg = response;
                  }else{
                    $state.go('app.dashboard');
                  }
                }, function() {
                  vm.authMsg = 'Server Request Error';
                });
            }
            else {
              // set as dirty if the user click directly to login so we show the validation messages
              /*jshint -W106*/
              vm.registerForm.account_email.$dirty = true;
              vm.registerForm.account_password.$dirty = true;
              vm.registerForm.account_agreed.$dirty = true;
              
            }
          };
        }
    }
})();

/**=========================================================
 * Module: app.pasajero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pasajero')
    .controller('pasajeroController', pasajeroController);

  pasajeroController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'pasajeroHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];   

  function pasajeroController($scope, $rootScope, $state,$modalInstance,$filter,pasajeroHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {
      
      
      var pasajero = parameters.pasajero;
      var currentDate = new Date();
      
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?.¿ ]+$/;
      $scope.pasajero = {
		model : {
				  id: 0,
                  identificacion: '', 
                  nombre: '', 
                  telefono: '',
                  direccion: '',
                  correo: '',
                  tipoId: 0,
                  estado: '',			  
                  fechaAct: '',
                  usuarioAct: '',
                  fechaReg: '',
                  usuarioReg: '',
                  fechaNacimiento: ''
		},  
        fechaNacimiento : {
        isOpen : false,
        value:'',
        dateOptions : {          
          startingDay: 1         
          
        },
        open : function($event) {
          $scope.pasajero.fechaNacimiento.isOpen = true;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.pasajero', params : { filters : {estadoSeleccionado:pasajero.estado}, data : []} });
        $state.go('app.pasajero');
      },
      save : function() {		  
       
		        if($scope.pasajero.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.pasajero.model.identificacion ==""){message.show("warning", "Identificación requerido");return;}
                
                if($scope.pasajero.fechaNacimiento.value==""){message.show("warning", "Fecha de nacimiento requerida");return;}
                else{
                    
                    $scope.pasajero.model.fechaNacimiento=Date.parse(new Date($scope.pasajero.fechaNacimiento.value));
                }	

                	
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.pasajero.model.estado= $scope.estados.current.value;}			
				
			
          
			//INSERTAR
            if($scope.pasajero.model.id==0){
				
				$rootScope.loadingVisible = true;
				pasajeroHttp.save({}, $scope.pasajero.model, function (data) { 
					
						pasajeroHttp.read({},{ id : data.id}, function (data) {
							$scope.pasajero.model=data;
							pasajero.id=$scope.pasajero.model.id; 
							if($scope.pasajero.model.fechaReg){$scope.pasajero.fechaReg.value=new Date(parseFloat($scope.pasajero.model.fechaReg));}    
							if($scope.pasajero.model.fechaAct){$scope.pasajero.fechaAct.value=new Date(parseFloat($scope.pasajero.model.fechaAct));} 		
								
							message.show("success", "pasajero creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.pasajero.model.id){
					$state.go('app.pasajero');
				}else{
					 
					pasajeroHttp.update({}, $scope.pasajero.model, function (data) {
					  $rootScope.loadingVisible = false;
					  $scope.pasajero.model=data;
						if($scope.pasajero.model.fechaReg){$scope.pasajero.fechaReg.value=new Date(parseFloat($scope.pasajero.model.fechaReg));}    
						if($scope.pasajero.model.fechaAct){$scope.pasajero.fechaAct.value=new Date(parseFloat($scope.pasajero.model.fechaAct));}   					  
					   message.show("success", "pasajero actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pasajero.model.id,
                         referencia : 'pasajero' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pasajero.model.id,
                         referencia : 'pasajero' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        close : function() {
        $modalInstance.dismiss('cancel');
      }
    }
      
    $scope.contactoPasajero = {
        
    model : {              
            id: 0,
            nombre: '',
            telefono: '',
			ext: '',
			email: ''
		},
      current : {
          id: 0,
          comision:false,
          cotizacion:false,
          activo:false
      },
      data:[],
      getData : function() {
          if(pasajero){
              
            if($scope.pasajero.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              pasajeroHttp.getContactosPasajero({}, { pasajeroId: $scope.pasajero.model.id }, function(response) {
                  $scope.contactoPasajero.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },
       save : function() {   
           
        
           if($scope.contactoPasajero.current.nombre==""){message.show("warning", "Debe especificar el nombre");return;}
           if($scope.contactoPasajero.current.email==""){message.show("warning", "Debe especificar el email");return;} 
           
        $rootScope.loadingVisible = true;
           
           if($scope.contactoPasajero.current.id==0){
               $scope.contactoPasajero.current.terceroId= $scope.pasajero.model.id;
               	pasajeroHttp.addContactoPasajero({}, $scope.contactoPasajero.current, function (data) {
					  $rootScope.loadingVisible = false;
					  
                      $scope.contactoPasajero.current={  
                            id: 0,
                            nombre: '',
                            telefono: '',
                            ext: '',
                            email: '',
                            comision:false,
                            cotizacion:false,
                            activo:false
                      };
                        
                       $scope.contactoPasajero.getData();
               
					   message.show("success", "Contacto actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
           
               
           }
           else if ($scope.contactoPasajero.current.id > 0){
               
               	pasajeroHttp.editContactoPasajero({}, $scope.contactoPasajero.current, function (data) {
					  $rootScope.loadingVisible = false;
					  
                      $scope.contactoPasajero.current={  
                            id: 0,
                            nombre: '',
                            telefono: '',
                            ext: '',
                            email: '',
                            comision:false,
                            cotizacion:false,
                            activo:false
                      };
                        
                       $scope.contactoPasajero.getData();
               
					   message.show("success", "Contacto actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
           
           }
				
         
           
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          pasajeroHttp.deleteContactoPasajero({}, {contactoPasajeroId: item.id}, function(response){
            //$scope.contactoPasajero.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
         $scope.contactoPasajero.current=item;
      },
        cancel : function(item) {
         $scope.contactoPasajero.current={};
      }
    }
    //TAB
     $scope.showTabs= function(){
        if ($scope.pasajero.id==0)
          return false;
        else
          return true;
    }
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:0 , descripcion: 'Inactivo'});
            $scope.estados.data.push({value:1 , descripcion: 'Activo'});
              
        }        
    }	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	
	
	//CARGAMOS LOS DATOS DEL pasajero
	
	if(pasajero.id==0){
        $scope.estados.current=$scope.estados.data[1];     
        $scope.pasajero.model.direccion=pasajero.direccion;
        $scope.pasajero.model.correo=pasajero.correo;
        $scope.pasajero.model.telefono=pasajero.telefono;
    }
    else{        
        $rootScope.loadingVisible = true;
		$scope.divContactopasajero=true;
        pasajeroHttp.read({}, { id : pasajero.id} , function (data) { 
            
            $scope.pasajero.model = data;	

                  
           
            if($scope.pasajero.model.fechaNacimiento){
              var d = new Date(parseFloat($scope.pasajero.model.fechaNacimiento));
              d.setMinutes( d.getMinutes() + d.getTimezoneOffset() );   
              $scope.pasajero.fechaNacimiento.value=d;
            }   
			       
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.pasajero.model.estado})[0];            
			$scope.contactoPasajero.getData();
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
	
    
    
  }
})();
/**=========================================================
 * Module: app.pasajero.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pasajero')
    .service('pasajeroHttp', pasajeroHttp);

  pasajeroHttp.$inject = ['$resource', 'END_POINT'];


  function pasajeroHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {     
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/pasajero/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/pasajero/:id'
      },
      'getList' : {
        'method' : 'POST',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/pasajero/busqueda'          
        },        
      'getContactosPasajero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            pasajeroId : '@pasajeroId'           
          },
          'url' : END_POINT + '/Comercial.svc/pasajero/:pasajeroId/contacto'
      },
      'addContactoPasajero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/pasajero/contacto'
      },
      'editContactoPasajero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/pasajero/contacto'
      },
      'deleteContactoPasajero': {
        'method':'DELETE',
        'params' : {
            contactoPasajeroId : '@contactoPasajeroId'           
          },
        'url' : END_POINT + '/Comercial.svc/pasajero/contacto/:contactoPasajeroId'
      } 
    };
    return $resource( END_POINT + '/Comercial.svc/pasajero', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.pasajero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pasajero')
    .controller('pasajeroListController', pasajeroListController);

  pasajeroListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl','$modal', 'pasajeroHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function pasajeroListController($scope, $filter, $state, ngDialog, tpl,$modal, pasajeroHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	
    $scope.ESTADO_ACTIVO = '1';
    $scope.ESTADO_INACTIVO = '0';
        
	//$rootScope.loadingVisible = true;

 
    $scope.pasajeros = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrepasajeros:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.pasajeros.selectedAll = !$scope.pasajeros.selectedAll; 
        for (var key in $scope.pasajeros.selectedItems) {
          $scope.pasajeros.selectedItems[key].check = $scope.pasajeros.selectedAll;
        }
      },
      add : function() {
          var item = {
              codigo: 0,
              id: 0,
              identificacion: '', 
              nombre: '', 
              apellido: '', 
              fechaNacimiento:'',
			  telefono: '',
			  direccion: '',
			  correo: '',
              tipoId: 0,
			  estado: '',			  
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''			 
          };
         var modalInstance = $modal.open({
            templateUrl: 'app/views/pasajero/pasajero_form.html',
            controller: 'pasajeroController',
            size: 'lg',
            resolve: {
                
                        parameters: { pasajero: item }
                
            }
            });
            modalInstance.result.then(function (parameters) {
            });  
      },
      edit : function(item) {
          
        var modalInstance = $modal.open({
        templateUrl: 'app/views/pasajero/pasajero_form.html',
        controller: 'pasajeroController',
        size: 'lg',
        resolve: {
            parameters: { pasajero: item }
        }
        });
        modalInstance.result.then(function (parameters) {
        });          
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            pasajeroHttp.remove({}, { id: id }, function(response) {
                $scope.pasajeros.getData();
                message.show("success", "pasajero eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.pasajeros.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    pasajeroHttp.remove({}, { id: id }, function(response) {
                        $scope.pasajeros.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.pasajeros.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.pasajeros.filterText,
          "precision": false
        }];
        $scope.pasajeros.selectedItems = $filter('arrayFilter')($scope.pasajeros.dataSource, paramFilter);
        $scope.pasajeros.paginations.totalItems = $scope.pasajeros.selectedItems.length;
        $scope.pasajeros.paginations.currentPage = 1;
        $scope.pasajeros.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.pasajeros.paginations.currentPage == 1 ) ? 0 : ($scope.pasajeros.paginations.currentPage * $scope.pasajeros.paginations.itemsPerPage) - $scope.pasajeros.paginations.itemsPerPage;
        $scope.pasajeros.data = $scope.pasajeros.selectedItems.slice(firstItem , $scope.pasajeros.paginations.currentPage * $scope.pasajeros.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.pasajeros.data = [];
        $scope.pasajeros.loading = true;
        $scope.pasajeros.noData = false;
          
        var parametros= {
            "estado":$scope.estadoPasajero.current.value,            
            "nombre":$scope.busquedaPasajero.model.nombre,
            "apellido":$scope.busquedaPasajero.model.apellido,
            "identificacion":$scope.busquedaPasajero.model.identificacion,
            "email":$scope.busquedaPasajero.model.email,
            "direccion":$scope.busquedaPasajero.model.direccion,
            "telefono":$scope.busquedaPasajero.model.telefono,
            "movil":$scope.busquedaPasajero.model.movil,
            "intermediario":$scope.busquedaPasajero.model.intermediario            
        };
          
        pasajeroHttp.getList({}, parametros,function(response) {
          $scope.pasajeros.selectedItems = response;
          $scope.pasajeros.dataSource = response;
          for(var i=0; i<$scope.pasajeros.dataSource.length; i++){
            $scope.pasajeros.nombrepasajeros.push({id: i, nombre: $scope.pasajeros.dataSource[i]});
          }
          $scope.pasajeros.paginations.totalItems = $scope.pasajeros.selectedItems.length;
          $scope.pasajeros.paginations.currentPage = 1;
          $scope.pasajeros.changePage();
          $scope.pasajeros.loading = false;
          ($scope.pasajeros.dataSource.length < 1) ? $scope.pasajeros.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }       
    //CARGAMOS PROCESOS
    $scope.estadoPasajero = {             
          current : {}, data:[],
        getData : function() {            
            $scope.estadoPasajero.data.push({value:'1', descripcion: 'Activos'});
            $scope.estadoPasajero.data.push({value:'0', descripcion: 'Inactivos'});          
            $scope.estadoPasajero.current =$scope.estadoPasajero.data[0];
        }          
    }
    
    
    $scope.busquedaPasajero = {
    model : {
          nombre: '',
          apellido: '',
          identificacion: '',
          email: '',
          direccion: '',
          telefono: '',
          movil: '',
          estado: '',
          intermediario:''
            } 
    }
	//CARGAMOS LOS LISTADOS
    $scope.estadoPasajero.getData();    
    $scope.pasajeros.getData();
        
        
        
	}
  
  
  })();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('crearClienteController', crearClienteController);

  crearClienteController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'oportunidadHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function crearClienteController($scope, $filter, $state, $modalInstance, LDataSource, oportunidadHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
      
      
      
      $scope.cliente = {     
          model:{
              identificacion: '',
              nombre: '',
              telefono: '', 
              direccion: '', 
              email: '',              
              nombreContacto: '',
              telefonoContacto: '',
              cargoContacto: ''
          },         
          
         save:  function(){ 
                
                if($scope.cliente.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
                
                   
             var _http = new oportunidadHttp($scope.cliente.model);
                  _http.$addTercero(function(response){                  
                      
                      
                      message.show("success", "Cliente creado satisfactoriamente!!");
                      $modalInstance.close(response);
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
                 
              
              
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }      
       	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      //$scope.cliente.model=parameters.cliente;
  }
})();
/**=========================================================
 * Module: app.pedido.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pedido')
    .controller('pedidoController', pedidoController);

 pedidoController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'pedidoHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal','REPORT_URL'];   

  function pedidoController($scope, $rootScope, $state,$modalInstance,$filter,pedidoHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window,$modal,REPORT_URL) {      
      
     
      var pedido =parameters.pedido;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.pedido = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              entregaId: 0,
              viaContactoId: 0,            
              padreId: 0,
              crearCliente:0,    
              pasajeros:0,    
			  observaciones: '',
			  estado: 1,	
              fechaInicio: '',
              fechaFin: '' ,              
              polizas:[],
              ciudadEntregaId:0,
              fechaEntrega: '' ,
              direccionEntrega: '' ,
              observacionEntrega: '',
              pedidoId:0
        },
        file : {},
        pasajeros:[],
        polizaSeleccionada:undefined,
      deletePoliza: function(p) {
           $scope.pedido.model.polizas.splice($scope.pedido.model.polizas.indexOf(p), 1);     
      },
      back : function() {
          parametersOfState.set({ name : 'app.pedido', params : { filters : {procesoSeleccionado:pedido.tipo}, data : []} });
        $state.go('app.pedido');
          
      },
      save : function() {		
          
            
            if($scope.pedido.model.identificacion==""){message.show("warning", "Identifiación del cliente requerido");return;}
            if($scope.pedido.model.tercero==""){message.show("warning", "Razón social del cliente requerido");return;}
            if($scope.pedido.model.email==""){message.show("warning", "Email del cliente requerido");return; }
            if($scope.pedido.model.email==""){message.show("warning", "Email del cliente requerido");return; }
            if( $scope.pedido.model.polizas.length ==0){message.show("warning", "Debe agregar polizas");return;}  
        
            try{                
                $scope.pedido.model.fechaInicio=$scope.pedido.formatdate($scope.pedido.model.fechaInicio);
            }
            catch(err) {
            }
            try{
                
                $scope.pedido.model.fechaFin=$scope.pedido.formatdate($scope.pedido.model.fechaFin);
            }
            catch(err) {
            }
          
           try{
                
                $scope.pedido.model.fechaEntrega=$scope.pedido.formatdate($scope.pedido.model.fechaEntrega);
            }
            catch(err) {
                $scope.pedido.model.fechaEntrega='';
            }
            
            $scope.pedido.model.grupoId= $scope.grupos.current.value;
            $scope.pedido.model.prioridadId= $scope.prioridades.current.value;
            $scope.pedido.model.viaContactoId= $scope.viasContacto.current.value;
            $scope.pedido.model.entregaId= $scope.entregas.current.value;
            $scope.pedido.model.formaPagoId= $scope.formasPago.current.value;
            $scope.pedido.model.monedaId= $scope.monedas.current.value;
            $scope.pedido.model.destino= $scope.destinos.current.value;
            $scope.pedido.model.tipoViaje= $scope.tiposViaje.current.value;
          
            $scope.pedido.model.entregaId= $scope.entregas.current.value;
            $scope.pedido.model.ciudadEntregaId= $scope.ciudades.current.value;  
          
            if($scope.universidades.current){$scope.pedido.model.universidadId = $scope.universidades.current.codigo;
            }else{$scope.pedido.model.universidadId =0;}  
       
            if($scope.intermediarios.current){$scope.pedido.model.intermediarioId = $scope.intermediarios.current.codigo;
            }else{$scope.pedido.model.intermediarioId ='00000000-0000-0000-0000-000000000000';} 
				
            $rootScope.loadingVisible = true;
          
           
           
            pedidoHttp.save({}, $scope.pedido.model, function (data) { 	
                       
                        
                        if ($scope.pedido.model.id==0){
                            message.show("success", "Pedido creado satisfactoriamente!!");
                        }
                        else{message.show("success", "Pedido actualizado satisfactoriamente!!");
                            }
                        $scope.pedido.model=data;       
                
                        $scope.pedido.model.fechaInicio=new Date(data.fechaInicio);
                        $scope.pedido.model.fechaFin=new Date(data.fechaFin);
                        $scope.pedido.model.fechaEntrega=new Date(data.fechaEntrega);
                        	
                        $scope.btnEnviarCotizacion=true;
                        $scope.crearCliente=false;
                        $scope.pedido.model.crearCliente=0;                        
                      
                
                        $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });				
		
		},          
          anular: function() {   
              
            if (confirm("Desea anular el pedido?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.pedido.model.estado=3;//estado anulado

                          try{                
                              $scope.pedido.model.fechaInicio=$scope.pedido.formatdate($scope.pedido.model.fechaInicio);
                          }
                          catch(err) {
                          }
                          try{
                              $scope.pedido.model.fechaFin=$scope.pedido.formatdate($scope.pedido.model.fechaFin);
                          }
                          catch(err) {
                          }
              
                       pedidoHttp.save({}, $scope.pedido.model, function (data) {

                            $rootScope.loadingVisible = false;
                            message.show("success", "Pedido anulado satisfactoriamente!!");	
                            $scope.pedido.close();               


                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pedido.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pedido.model.id,
                         referencia : 'pedido' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }      
      ,agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pedido.model.idRow,
                         referencia : 'pedido' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.pedido.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pedido.model.padreId,
                         referencia : 'pedido' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },         
      uploader : function(file) {
        
        if($scope.polizaSeleccionada!= undefined){
            $rootScope.loadingVisible = true;
            file.upload = Upload.upload({
            url: END_POINT + '/General.svc/UploadPasajeros',
            headers: {
                Authorization : tokenManager.get()
            },
            data: { 
                    file: file, 
                    codigo: base64.encode(String($scope.pedido.model.id)),  
                    poliza: base64.encode($scope.polizaSeleccionada.id),                 
                  }
            });

            file.upload.then(function (response) {
                message.show("success", "Archivo procesado correctamente");
                $scope.pedido.file = {};  
                $rootScope.loadingVisible = false;
                $scope.pedido.loadPedido();                

            }, function (faild) {
            $rootScope.loadingVisible = false;
            message.show("error", "Ocurrio un error al procesar el archivo");
            });
        }
        else{
            message.show("error", "Debe seleccionar la poliza y cargar el archivo");
        }
      }, 
      buscarCliente : function() { 

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion : $scope.pedido.model}
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },   
    verSeguimientoHistorico : function() {            
    var modalInstance = $modal.open({
      templateUrl: 'app/views/seguimiento/seguimiento.html',
      controller: 'seguimientoController',
      size: 'lg',
      resolve: {
        parameters: { id : $scope.pedido.model.padreId,
                     referencia : 'pedido' }
      }
    });
    modalInstance.result.then(function (parameters) {
    });

    }
  ,buscarPolizas : function() {  
      
            if(!$scope.pedido.model.fechaInicio){message.show("warning", "Fecha inicial requerida");return;}
            if(!$scope.pedido.model.fechaFin){message.show("warning", "Fecha final requerida");return;}               
            $scope.pedido.model.origen= $scope.origenes.current.value;
            $scope.pedido.model.destino= $scope.destinos.current.value;
            $scope.pedido.model.tipo= $scope.tiposViaje.current.value;
          
            var modalInstance = $modal.open({
              templateUrl: 'app/views/cotizacion/buscarPolizas.html',
              controller: 'buscarPolizaController',
              size: 'lg',
              resolve: {
                parameters: { cotizacion : $scope.pedido.model}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pedido.model.padreId,
                         referencia : 'pedido' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },  
        close : function() {
        if(parameters.vpedidos!=null && parameters.vpedidos!=undefined)
            parameters.vpedidos.getData();
            
        $modalInstance.dismiss('cancel');
      },formatdate(t){    
            try{ 
                    var dd = t.getDate();
                    var mm = t.getMonth()+1; //January is 0!

                    var yyyy = t.getFullYear();
                    if(dd<10){
                        dd='0'+dd;
                    } 
                    if(mm<10){
                        mm='0'+mm;
                    } 
                    //2018-01-15
                    return yyyy+'-'+mm+'-'+dd;   
                }
                    catch(err) {
                        return t;
                    }
       
        },enviarCotizacion(){

                $rootScope.loadingVisible = true;   
                    
                    if($scope.pedido.model.intermediarioId=="00000000-0000-0000-0000-000000000000"){
                      $rootScope.loadingVisible = true;
                       pedidoHttp.enviarCotizacionCrm({}, {codigo: $scope.pedido.model.id }, function(response){
                          message.show("success", "Pedido enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
                    else{

                    $rootScope.loadingVisible = true;
                     pedidoHttp.enviarCotizacionIntermediario({}, {codigo: $scope.pedido.model.id }, function(response){
                          message.show("success", "Pedido enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
      },
        agregarCliente: function(data){
            
            $scope.crearCliente=true;
            $scope.pedido.model.crearCliente=1;
        },
          selectPoliza: function(polizaSeleccionada){  
              
            if(polizaSeleccionada!=null){
                $scope.pedido.pasajeros=polizaSeleccionada.pasajeros; 
                $scope.polizaSeleccionada=polizaSeleccionada;                

            }
            else{
                $scope.pedido.pasajeros=[];                
            }
              
             
        },buscarPasajero: function(data){
            
            if($scope.polizaSeleccionada!=null && $scope.polizaSeleccionada!=undefined){

                      var modalInstance = $modal.open({
                      templateUrl: 'app/views/cotizacion/buscarPasajero.html',
                      controller: 'buscarPasajeroController',
                      size: 'lg',
                      resolve: {
                        parameters: { cotizacion : $scope.pedido.model,poliza:$scope.polizaSeleccionada}
                      }
                    });
                    modalInstance.result.then(function (parameters) {
                    });
                }
            else{                
                message.show("warning", "Debe seleccionar una poliza!!");
            }
            
        },agregarPasajero: function(data){           
            
             var item = {
              codigo: 0,
              id: 0,
              identificacion: '', 
              nombre: '', 
              apellido: '', 
              fechaNacimiento:'',
			  telefono: $scope.pedido.model.movil,
			  direccion:  $scope.pedido.model.direccion,
			  correo: $scope.pedido.model.email,
              tipoId: 0,
			  estado: '',			  
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''			 
          };

         var modalInstance = $modal.open({
            templateUrl: 'app/views/pasajero/pasajero_form.html',
            controller: 'pasajeroController',
            size: 'lg',
            resolve: {
               parameters: { pasajero: item }
            }
            });
            modalInstance.result.then(function (parameters) {
            });        
            
            
        },abrirPasajero: function(item){
            
            var modalInstance = $modal.open({
                templateUrl: 'app/views/pasajero/pasajero_form.html',
                controller: 'pasajeroController',
                size: 'lg',
                resolve: {
                    parameters: { pasajero: item }
                }
                });
                modalInstance.result.then(function (parameters) {
                });   
                  
        },eliminarPasajero: function(item){
            
            
            pedidoHttp.deletepasajero({id:item.idRow}, {}, function (data) {

                $rootScope.loadingVisible = false;
                message.show("success", "Pasajero eliminado satisfactoriamente!!");	 
                
                $scope.pedido.pasajeros.splice($scope.pedido.pasajeros.indexOf(item), 1 );

        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });		
                  
        },generarFactura:function(data){          
          
            if (confirm("Desea crear la factura?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.pedido.model.estado=4;//estado anulado

                try{                
                    $scope.pedido.model.fechaInicio=$scope.pedido.formatdate($scope.pedido.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.pedido.model.fechaFin=$scope.pedido.formatdate($scope.pedido.model.fechaFin);
                }
                catch(err) {
                }
                       pedidoHttp.save({}, $scope.pedido.model, function (data) {

                            $rootScope.loadingVisible = false;
                            message.show("success", "Factura creada satisfactoriamente!!");	
                             var modalInstance = $modal.open({
                                  templateUrl: 'app/views/factura/factura_form.html',
                                  controller: 'facturaController',
                                  size: 'lg',
                                  resolve: {
                                    parameters: { factura: data,vfacturas:null}
                                  }
                                });
                                modalInstance.result.then(function (parameters) {
                                    $scope.pedidos.getData();
                                });      
                           
                            $scope.pedido.close();     

                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
      },generarCertificado:function(data){ 

        var targeturl = REPORT_URL + "CertificadoPnn&CodigoInterno=" + $scope.pedido.model.id+"&rs:Format=PDF";
        window.open(targeturl, '_blank');

      },generarCertificadoZebra:function(data){ 

        var targeturl = REPORT_URL + "CertificadoZebra&CodigoInterno=" + $scope.pedido.model.id+"&rs:Format=PDF";
        window.open(targeturl, '_blank');

      },loadPedido(){
        pedidoHttp.read({},pedido, function (data) { 
                
            $scope.pedido.model = data;	                

            $scope.pedido.model.fechaInicio=new Date(data.fechaInicio);
            $scope.pedido.model.fechaFin=new Date(data.fechaFin);
            $scope.pedido.model.fechaEntrega=new Date(data.fechaEntrega);

            $scope.monedas.current = $filter('filter')($scope.monedas.data, { value : $scope.pedido.model.monedaId })[0];
            $scope.formasPago.current = $filter('filter')($scope.formasPago.data, { value : $scope.pedido.model.formaPagoId })[0];

            $scope.destinos.current = $filter('filter')($scope.destinos.data, { value : $scope.pedido.model.destino })[0];
            $scope.entregas.current = $filter('filter')($scope.entregas.data, { value : $scope.pedido.model.entregaId })[0];

            $scope.entregas.current = $filter('filter')($scope.entregas.data, { value : $scope.pedido.model.entregaId })[0];
            $scope.ciudades.current = $filter('filter')($scope.ciudades.data, { value : $scope.pedido.model.ciudadEntregaId })[0];

            $scope.prioridades.current = $filter('filter')($scope.prioridades.data, { value : $scope.pedido.model.prioridadId })[0];

            $scope.grupos.current = $filter('filter')($scope.grupos.data, { value : $scope.pedido.model.grupoId })[0];
            $scope.viasContacto.current = $filter('filter')($scope.viasContacto.data, { value : $scope.pedido.model.viaContactoId })[0];

            $scope.universidades.current = $filter('filter')($scope.universidades.data, { codigo : $scope.pedido.model.universidadId })[0];

            $scope.intermediarios.current = $filter('filter')($scope.intermediarios.data, { codigo : $scope.pedido.model.intermediarioId })[0];

            $scope.btnModificar=true;
            $scope.btnAnular=true;

            if($scope.pedido.model.polizas.length>0)
                $scope.pedido.selectPoliza($scope.pedido.model.polizas[0]);



           $rootScope.loadingVisible = false;

           
    }, function(faild) {
        $rootScope.loadingVisible = false;
        message.show("error", faild.Message);
    });   
      }
    }      
      
      
    //TERCEROS
    $scope.clientes = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            generalHttp.getTerceros({}, {}, function(response) {                
            $scope.clientes.data = response ;              
                
               if($scope.pedido.model){
                    
                    $scope.clientes.current = $filter('filter')($scope.clientes.data, { id : $scope.pedido.model.clienteId })[0];                   
               }
                else{
                    $scope.clientes.current=$scope.Clientes.data[0];                     
                }               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.pedido.model.terceroId=$scope.clientes.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.pedido.model.terceroId=$scope.clientes.current.id;
         // $scope.contactosOp.getData();
      }
    }

    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'}); 
        }        
    }

    //VIA CONTACTO
    $scope.viasContacto = {
      current : {},
      data:[],
      getData : function() {
                $scope.viasContacto.data.push({value:1, descripcion: 'Cliente'});
                $scope.viasContacto.data.push({value:2, descripcion: 'Facebook'});     
                $scope.viasContacto.data.push({value:3, descripcion: 'E-mail'}); 
                $scope.viasContacto.data.push({value:4, descripcion: 'Directorio'});  
                $scope.viasContacto.data.push({value:5, descripcion: 'Referido'});  
                $scope.viasContacto.data.push({value:6, descripcion: 'Compra online'});  
                $scope.viasContacto.data.push({value:7, descripcion: 'Intermediario'});  
                $scope.viasContacto.data.push({value:8, descripcion: 'Google'});
                $scope.viasContacto.data.push({value:9, descripcion: 'Whatsapp'});
                $scope.viasContacto.data.push({value:10, descripcion: 'Zopim'});  
                $scope.viasContacto.data.push({value:11, descripcion: 'Ori/Volante'}); 
                $scope.viasContacto.current=$scope.viasContacto.data[0];   
      },
      setViaContacto : function(){
          $scope.pedido.model.viaContactoId=$scope.viasContacto.current.codigo;
    }}
    //PRIORIDADES
    $scope.prioridades = {
      current : {},
      data:[],
      getData : function() {
                $scope.prioridades.data.push({value:1, descripcion: 'NORMAL(15DIAS)'});
                $scope.prioridades.data.push({value:2, descripcion: 'ALTA(48HORAS)'});     
                $scope.prioridades.data.push({value:3, descripcion: 'CRITICA(8HORAS)'}); 
                $scope.prioridades.data.push({value:4, descripcion: 'BAJA(1MES)'});  
                $scope.prioridades.data.push({value:5, descripcion: 'BAJA(2MESES)'});  
                $scope.prioridades.data.push({value:6, descripcion: 'BAJA(3MESES)'});  
                $scope.prioridades.current=$scope.prioridades.data[0];   
      },
      setPrioridad : function(){
          $scope.pedido.model.prioridadId=$scope.prioridades.current.value;
        }
    }
   
    //GRUPO
    $scope.grupos = {
      current : {},
      data:[],
      getData : function() {
                $scope.grupos.data.push({value:2, descripcion: 'Asesores'});    
                $scope.grupos.data.push({value:1, descripcion: 'Administrativo'});                 
                $scope.grupos.data.push({value:3, descripcion: 'Lista de pólizas'});     
                $scope.grupos.data.push({value:4, descripcion: 'Servicio al Cliente'});
                $scope.grupos.current=$scope.grupos.data[0];                   
          },
      setGrupo : function(){
          $scope.pedido.model.grupoId=$scope.grupos.current.value;
    }}
    //ENTREGAS
    $scope.entregas = {
      current : {},
      data:[],
      getData : function() {
                $scope.entregas.data.push({value:1, descripcion: 'Entrega Oficina'});    
                $scope.entregas.data.push({value:2, descripcion: 'Entrega Domicilio'});                 
                $scope.entregas.data.push({value:3, descripcion: 'Pago Contra Entrega'});     
                $scope.entregas.data.push({value:4, descripcion: 'Consignación Bancaria'});
                $scope.entregas.current=$scope.entregas.data[0];                   
          },
      setEntrega : function(){
          $scope.pedido.model.entregaId=$scope.entregas.current.value;
    }}
      
      
    //MONEDA
    $scope.monedas  = {
            current: {},
            data: [],
            getData: function() {
                $scope.monedas.data.push({value:2, descripcion: 'Dolar'});
                $scope.monedas.data.push({value:4, descripcion: 'Pesos Colombianos'});    
                $scope.monedas.current=$scope.monedas.data[0];    
            }
        }
      
    $scope.formasPago  = {
            current: {},
            data: [],
            getData: function() {
                $scope.formasPago.data.push({value:0, descripcion: 'Pesos en efectivo'});
                $scope.formasPago.data.push({value:1, descripcion: 'Tarjeta de credito'});     
                $scope.formasPago.data.push({value:2, descripcion: 'Pse'});     
                $scope.formasPago.data.push({value:3, descripcion: 'Consig Bogota'});     
                $scope.formasPago.data.push({value:4, descripcion: 'Consig Bancolombia'});     
                $scope.formasPago.data.push({value:5, descripcion: 'Consig Colpatria'});     
                $scope.formasPago.data.push({value:6, descripcion: 'Cheque'});
                $scope.formasPago.data.push({value:8, descripcion: 'Dolares'});  
                $scope.formasPago.data.push({value:9, descripcion: 'Tarjeta debito'});  
                $scope.formasPago.data.push({value:10, descripcion: 'Credito'});  
                $scope.formasPago.data.push({value:11, descripcion: 'Online'});  
                $scope.formasPago.data.push({value:12, descripcion: 'Giro'});  
                $scope.formasPago.current=$scope.formasPago.data[0];    
            }
        }
      
        $scope.ciudades  = {
            current: {},
            data: [],
            getData: function() {
                $scope.ciudades.data.push({value:0, descripcion: 'Bogotá D.C'});
                $scope.ciudades.data.push({value:1, descripcion: 'Medellín'});     
                $scope.ciudades.data.push({value:2, descripcion: 'Barranquilla'});     
                $scope.ciudades.data.push({value:3, descripcion: 'Calí'});     
                $scope.ciudades.data.push({value:4, descripcion: 'Bucaramanga'});     
                $scope.ciudades.data.push({value:5, descripcion: 'Consig Colpatria'});     
                $scope.ciudades.data.push({value:6, descripcion: 'Santa Marta'});
                
                $scope.ciudades.current=$scope.ciudades.data[0];    
            }
        }
      
      
      
        $scope.origenes  = {
            current: {},
            data: [],
            getData: function() {
      
                        $scope.origenes.data.push({value: 48, descripcion: 'Colombia' });
                        $scope.origenes.data.push({value: 1, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 2, descripcion: 'Argentina' });
                        $scope.origenes.data.push({value: 6, descripcion: 'Alemania' });
                        $scope.origenes.data.push({value: 12, descripcion: 'Arabia Saudita' });
                        $scope.origenes.data.push({value: 15, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 16, descripcion: 'Australia' });
                        $scope.origenes.data.push({value: 19, descripcion: 'Bahamas' });
                        $scope.origenes.data.push({value: 21, descripcion: 'Barbados' });
                        $scope.origenes.data.push({value: 25, descripcion: 'Bemudas' });
                        $scope.origenes.data.push({value: 28, descripcion: 'Bolivia' });
                        $scope.origenes.data.push({value: 31, descripcion: 'Brasil' });
                        $scope.origenes.data.push({value: 33, descripcion: 'Bulgaria' });
                        $scope.origenes.data.push({value: 37, descripcion: 'Belgica' });
                        $scope.origenes.data.push({value: 38, descripcion: 'Cabo Verde' });
                        $scope.origenes.data.push({value: 39, descripcion: 'Islas Caiman' });
                        $scope.origenes.data.push({value: 40, descripcion: 'Camboya' });
                        $scope.origenes.data.push({value: 41, descripcion: 'Camerun' });
                        $scope.origenes.data.push({value: 42, descripcion: 'Canada' });
                        $scope.origenes.data.push({value: 45, descripcion: 'Chile' });
                        $scope.origenes.data.push({value: 52, descripcion: 'Corea del Norte' });
                        $scope.origenes.data.push({value: 53, descripcion: 'Corea del Sur' });
                        $scope.origenes.data.push({value: 54, descripcion: 'Costa de Marfil' });
                        $scope.origenes.data.push({value: 55, descripcion: 'Costa Rica' });
                        $scope.origenes.data.push({value: 56, descripcion: 'Croacia' });
                        $scope.origenes.data.push({value: 57, descripcion: 'Cuba' });
                        $scope.origenes.data.push({value: 59, descripcion: 'Dinamarca' });
                        $scope.origenes.data.push({value: 61, descripcion: 'Ecuador' });
                        $scope.origenes.data.push({value: 62, descripcion: 'Egipto' });
                        $scope.origenes.data.push({value: 63, descripcion: 'El Salvador' });
                        $scope.origenes.data.push({value: 64, descripcion: 'Emiratos Arabes Unvalueos' });
                        $scope.origenes.data.push({value: 66, descripcion: 'Eslovaquia' });
                        $scope.origenes.data.push({value: 67, descripcion: 'Eslovenia' });
                        $scope.origenes.data.push({value: 68, descripcion: 'España' });
                        $scope.origenes.data.push({value: 69, descripcion: 'Estados Unvalueos' });
                        $scope.origenes.data.push({value: 70, descripcion: 'Estonia' });
                        $scope.origenes.data.push({value: 71, descripcion: 'Etiopia' });
                        $scope.origenes.data.push({value: 72, descripcion: 'Islas Feroe' });
                        $scope.origenes.data.push({value: 73, descripcion: 'Filipinas' });
                        $scope.origenes.data.push({value: 74, descripcion: 'Finlandia' });
                        $scope.origenes.data.push({value: 75, descripcion: 'Fiji' });
                        $scope.origenes.data.push({value: 76, descripcion: 'Francia'});
                        $scope.origenes.data.push({value: 83, descripcion: 'Grecia' });
                        $scope.origenes.data.push({value: 84, descripcion: 'Groenlandia' });
                        $scope.origenes.data.push({value: 86, descripcion: 'Guatemala' });
                        $scope.origenes.data.push({value: 87, descripcion: 'Guernsey' });
                        $scope.origenes.data.push({value: 88, descripcion: 'Guinea' });
                        $scope.origenes.data.push({value: 89, descripcion: 'Guinea Ecuatorial' });
                        $scope.origenes.data.push({value: 90, descripcion: 'Guinea-Bissau' });
                        $scope.origenes.data.push({value: 91, descripcion: 'Guyana' });
                        $scope.origenes.data.push({value: 92, descripcion: 'Haiti' });
                        $scope.origenes.data.push({value: 93, descripcion: 'Honduras' });
                        $scope.origenes.data.push({value: 94, descripcion: 'Hong kong' });
                        $scope.origenes.data.push({value: 95, descripcion: 'Hungria' });
                        $scope.origenes.data.push({value: 96, descripcion: 'India' });
                        $scope.origenes.data.push({value: 97, descripcion: 'Indonesia' });
                        $scope.origenes.data.push({value: 98, descripcion: 'Iraq' });
                        $scope.origenes.data.push({value: 99, descripcion: 'Irlanda' });
                        $scope.origenes.data.push({value: 100, descripcion: 'Iran' });
                        $scope.origenes.data.push({value: 101, descripcion: 'Islandia' });
                        $scope.origenes.data.push({value: 102, descripcion: 'Israel' });
                        $scope.origenes.data.push({value: 103, descripcion: 'Italia' });
                        $scope.origenes.data.push({value: 104, descripcion: 'Jamaica' });
                        $scope.origenes.data.push({value: 105, descripcion: 'Japon' });
                        $scope.origenes.data.push({value: 117, descripcion: 'Liberia' });
                        $scope.origenes.data.push({value: 118, descripcion: 'Libia' });
                        $scope.origenes.data.push({value: 120, descripcion: 'Lituania' });
                        $scope.origenes.data.push({value: 121, descripcion: 'Luxemburgo' });
                        $scope.origenes.data.push({value: 122, descripcion: 'Libano' });
                        $scope.origenes.data.push({value: 128, descripcion: 'Maldivas' });
                        $scope.origenes.data.push({value: 129, descripcion: 'Malta' });
                        $scope.origenes.data.push({value: 130, descripcion: 'Islas Malvinas' });
                        $scope.origenes.data.push({value: 131, descripcion: 'Mali' });
                        $scope.origenes.data.push({value: 134, descripcion: 'Marruecos' });
                        $scope.origenes.data.push({value: 144, descripcion: 'Mexico' });
                        $scope.origenes.data.push({value: 145, descripcion: 'Monaco' });
                        $scope.origenes.data.push({value: 146, descripcion: 'Namibia' });
                        $scope.origenes.data.push({value: 147, descripcion: 'Nauru' });
                        $scope.origenes.data.push({value: 149, descripcion: 'Nepal' });
                        $scope.origenes.data.push({value: 150, descripcion: 'Nicaragua' });
                        $scope.origenes.data.push({value: 151, descripcion: 'Nigeria' });
                        $scope.origenes.data.push({value: 152, descripcion: 'Niue' });
                        $scope.origenes.data.push({value: 154, descripcion: 'Noruega' });
                        $scope.origenes.data.push({value: 155, descripcion: 'Nueva Caledonia' });
                        $scope.origenes.data.push({value: 156, descripcion: 'Nueva Zelanda' });
                        $scope.origenes.data.push({value: 157, descripcion: 'Niger' });
                        $scope.origenes.data.push({value: 158, descripcion: 'Oman' });
                        $scope.origenes.data.push({value: 160, descripcion: 'Pakistan' });
                        $scope.origenes.data.push({value: 161, descripcion: 'Palaos' });
                        $scope.origenes.data.push({value: 162, descripcion: 'Palestina' });
                        $scope.origenes.data.push({value: 163, descripcion: 'Panama' });
                        $scope.origenes.data.push({value: 165, descripcion: 'Paraguay' });
                        $scope.origenes.data.push({value: 167, descripcion: 'Peru' });
                        $scope.origenes.data.push({value: 170, descripcion: 'Polonia' });
                        $scope.origenes.data.push({value: 171, descripcion: 'Portugal' });
                        $scope.origenes.data.push({value: 172, descripcion: 'Puerto Rico' });
                        $scope.origenes.data.push({value: 173, descripcion: 'Reino Unvalueo' });
                        $scope.origenes.data.push({value: 175, descripcion: 'Republica Checa' });
                        $scope.origenes.data.push({value: 176, descripcion: 'Republica Dominicana' });
                        $scope.origenes.data.push({value: 178, descripcion: 'Rumania' });
                        $scope.origenes.data.push({value: 179, descripcion: 'Rusia' });
                        $scope.origenes.data.push({value: 197, descripcion: 'Singapur' });
                        $scope.origenes.data.push({value: 198, descripcion: 'Siria' });
                        $scope.origenes.data.push({value: 201, descripcion: 'Sri Lanka' });
                        $scope.origenes.data.push({value: 203, descripcion: 'Sudafrica' });
                        $scope.origenes.data.push({value: 206, descripcion: 'Suecia' });
                        $scope.origenes.data.push({value: 207, descripcion: 'Suiza' });
                        $scope.origenes.data.push({value: 210, descripcion: 'Tailandia' });
                        $scope.origenes.data.push({value: 211, descripcion: 'Taiwan' });
                        $scope.origenes.data.push({value: 219, descripcion: 'Trinvaluead y Tobajo' });
                        $scope.origenes.data.push({value: 222, descripcion: 'Turquia' });
                        $scope.origenes.data.push({value: 225, descripcion: 'Ucrania' });
                        $scope.origenes.data.push({value: 227, descripcion: 'Uruguay' });
                        $scope.origenes.data.push({value: 230, descripcion: 'Ciudad del Vaticano' });
                        $scope.origenes.data.push({value: 231, descripcion: 'Venezuela' });
                        $scope.origenes.data.push({value: 239, descripcion: 'Chipre' });
                        $scope.origenes.data.push({value: 240, descripcion: 'Guam' });
                        $scope.origenes.data.push({value: 241, descripcion: 'Hawai' });
                        $scope.origenes.data.push({value: 246, descripcion: 'Boniaire' });
                        $scope.origenes.current=$scope.origenes.data[0];    
            }
        }  
      
        $scope.destinos  = {
            current: {},
            data: [],
            getData: function() {
                
                $scope.destinos.data.push({value:1, descripcion: 'Sur América'});
                $scope.destinos.data.push({value:2, descripcion: 'Centro América'});     
                $scope.destinos.data.push({value:3, descripcion: 'Norte América'});                         
                $scope.destinos.data.push({value:5, descripcion: 'Europa'});     
                $scope.destinos.data.push({value:6, descripcion: 'Asia'});     
                $scope.destinos.data.push({value:7, descripcion: 'África'}); 
                $scope.destinos.data.push({value:8, descripcion: 'Oceanía'}); 
                $scope.destinos.data.push({value:9, descripcion: 'Antillas Holandesas'}); 
                $scope.destinos.data.push({value:10, descripcion: 'Internacional'});             
                $scope.destinos.data.push({value:11, descripcion: 'Colombia'});                 
                $scope.destinos.data.push({value:13, descripcion: 'Parque Nacionales'}); 
                $scope.destinos.current=$scope.destinos.data[0];                    
             }
        }      
      $scope.tiposViaje  = {
            current: {},
            data: [],
            getData: function() {  
                $scope.tiposViaje.data.push({value:11, descripcion: 'Estudio'});
                $scope.tiposViaje.data.push({value:12, descripcion: 'Larga Estadía (más de 60 días).'});     
                $scope.tiposViaje.data.push({value:13, descripcion: 'Corta Estadia (menos de 60 días)'});     
                $scope.tiposViaje.data.push({value:14, descripcion: 'MultiViajes'});     
                $scope.tiposViaje.data.push({value:15, descripcion: 'Estudio Con Responsabilidad Civil'});     
                $scope.tiposViaje.data.push({value:16, descripcion: 'Futura Mamá'});     
                $scope.tiposViaje.data.push({value:17, descripcion: 'Atención médica por Pre-existencia'});   
                $scope.tiposViaje.data.push({value:18, descripcion: 'Seguro obligatorio Parques Nacionales'});  
                $scope.tiposViaje.current=$scope.tiposViaje.data[0];          
            }
      }
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            pedidoHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.pedido.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.pedido.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.pedido.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.pedido.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }

    //INTERMEDIARIOS
    $scope.intermediarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            pedidoHttp.getIntermediarios({}, {}, function(response) {                
            $scope.intermediarios.data = response ;              
                
               if($scope.pedido.model){
                                     
               }                           
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setIntermediario' : function() {
          $scope.pedido.model.intermediarioId=$scope.intermediarios.current.id;
      }
    }      
    //UNIVERSIDADES
    $scope.universidades = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            pedidoHttp.getUniversidades({}, {}, function(response) {                
            $scope.universidades.data = response ;              
                
               if($scope.pedido.model){                    
                    //$scope.universidades.current = $filter('filter')($scope.universidades.data, { id : $scope.pedido.model.universidadId })[0];                   
               }                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setUniversidad' : function() {
          $scope.pedido.model.universidadId=$scope.universidades.current.id;
      }
    }    

 
	
	//CARGAMOS LOS LISTADOS	
      
      
	$scope.monedas.getData();
    $scope.formasPago.getData();      
    $scope.origenes.getData();
    $scope.destinos.getData();
    $scope.tiposViaje.getData();
    $scope.universidades.getData();
    $scope.intermediarios.getData();
    $scope.prioridades.getData();
    $scope.grupos.getData();
    $scope.entregas.getData();
    $scope.viasContacto.getData();
    $scope.ciudades.getData();
      
    $scope.btnHistorico=true;
    $scope.btnAdjunto=true;
      
   
	//CARGAMOS LOS DATOS DEL pedido	
	
	if(pedido.id==0){
          $scope.btnModificar=true;
          $scope.btnAnular=false;        
    }
    else{   
        $rootScope.loadingVisible = true;  
        $scope.btnEditar=false;
        $scope.btnEnviarCotizacion=true;
        $scope.pedido.loadPedido();
        
		
           
        
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.pedido.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pedido')
    .service('pedidoHttp', pedidoHttp);

  pedidoHttp.$inject = ['$resource', 'END_POINT'];


  function pedidoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'POST',
        'isArray' : true,
          'params' : {
            estado : '@estado'           
          },
          'url' : END_POINT + '/Comercial.svc/pedidos'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/pedido/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/pedido'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/pedido'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },      
      'deletepasajero':  {
        'method':'DELETE',
        'params' : {
          id : '@id'           
        },
        'url' : END_POINT + '/Comercial.svc/pedido/pasajero/:id'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceropedido'
      },
         'getIntermediarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/intermediario'
      },
         'getUniversidades' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/universidad'
      },
     
    };
    return $resource( END_POINT + '/Comercial.svc/pedido', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.pedido.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pedido')
    .controller('pedidoListController', pedidoListController);

  pedidoListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'pedidoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function pedidoListController($scope, $filter,$state,$modal, ngDialog, tpl, pedidoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        
		
	  $scope.VERDE = 0;
    $scope.AMARILLO = 1;
    $scope.ROJO= 2;	    	

    $scope.pedidos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrepedidos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.pedidos.selectedAll = !$scope.pedidos.selectedAll; 
        for (var key in $scope.pedidos.selectedItems) {
          $scope.pedidos.selectedItems[key].check = $scope.pedidos.selectedAll;
        }
      },
      add : function() {
        
        var ped = {
                      id: 0,
                      terceroId: 0,                      
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
          
            var modalInstance = $modal.open({
            templateUrl: 'app/views/pedido/pedido_form.html',
            controller: 'pedidoController',
            size: 'lg',
            resolve: {
                parameters: { pedido: ped,vpedidos:$scope.pedidos }
            }
            });
            modalInstance.result.then(function (parameters) {
            });  
          
      },
      edit : function(item) {
      
        var modalInstance = $modal.open({
          templateUrl: 'app/views/pedido/pedido_form.html',
          controller: 'pedidoController',
          size: 'lg',
          resolve: {
            parameters: { pedido: item,vpedidos:$scope.pedidos }
          }
        });
        modalInstance.result.then(function (parameters) {
            $scope.pedidos.getData();
        });       
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            pedidoHttp.remove({}, { id: id }, function(response) {
                $scope.pedidos.getData();
                message.show("success", "pedido eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.pedidos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    pedidoHttp.remove({}, { id: id }, function(response) {
                        $scope.pedidos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.pedidos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.pedidos.filterText,
          "precision": false
        }];
        $scope.pedidos.selectedItems = $filter('arrayFilter')($scope.pedidos.dataSource, paramFilter);
        $scope.pedidos.paginations.totalItems = $scope.pedidos.selectedItems.length;
        $scope.pedidos.paginations.currentPage = 1;
        $scope.pedidos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.pedidos.paginations.currentPage == 1 ) ? 0 : ($scope.pedidos.paginations.currentPage * $scope.pedidos.paginations.itemsPerPage) - $scope.pedidos.paginations.itemsPerPage;
        $scope.pedidos.data = $scope.pedidos.selectedItems.slice(firstItem , $scope.pedidos.paginations.currentPage * $scope.pedidos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.pedidos.data = [];
        $scope.pedidos.loading = true;
        $scope.pedidos.noData = false;        
        
        pedidoHttp.getList({}, {estado:$scope.estados.current.value},function(response) {
          $scope.pedidos.selectedItems = response;
          $scope.pedidos.dataSource = response;
          for(var i=0; i<$scope.pedidos.dataSource.length; i++){
            $scope.pedidos.nombrepedidos.push({id: i, nombre: $scope.pedidos.dataSource[i]});
          }
          $scope.pedidos.paginations.totalItems = $scope.pedidos.selectedItems.length;
          $scope.pedidos.paginations.currentPage = 1;
          $scope.pedidos.changePage();
          $scope.pedidos.loading = false;
          ($scope.pedidos.dataSource.length < 1) ? $scope.pedidos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
    
    
 //VIA CONTACTO
 $scope.estados = {
  current : {},
  data:[],
  getData : function() {
            $scope.estados.data.push({value:1, descripcion: 'En Proceso'});
            $scope.estados.data.push({value:2, descripcion: 'Cerrada'});     
            $scope.estados.data.push({value:3, descripcion: 'Anulada'}); 
            
            $scope.estados.current=$scope.estados.data[0];   
        
  }
 }



  //CARGAMOS DATA
    $scope.estados.getData();     
    $scope.pedidos.getData();
        
   
        
	}
  
  
  })();
(function() {
    'use strict';

    angular
        .module('app.preloader')
        .directive('preloader', preloader);

    preloader.$inject = ['$animate', '$timeout', '$q'];
    function preloader ($animate, $timeout, $q) {

        var directive = {
            restrict: 'EAC',
            template: 
              '<div class="preloader-progress">' +
                  '<div class="preloader-progress-bar" ' +
                       'ng-style="{width: loadCounter + \'%\'}"></div>' +
              '</div>'
            ,
            link: link
        };
        return directive;

        ///////

        function link(scope, el) {

          scope.loadCounter = 0;

          var counter  = 0,
              timeout;

          // disables scrollbar
          angular.element('body').css('overflow', 'hidden');
          // ensure class is present for styling
          el.addClass('preloader');

          appReady().then(endCounter);

          timeout = $timeout(startCounter);

          ///////

          function startCounter() {

            var remaining = 100 - counter;
            counter = counter + (0.015 * Math.pow(1 - Math.sqrt(remaining), 2));

            scope.loadCounter = parseInt(counter, 10);

            timeout = $timeout(startCounter, 20);
          }

          function endCounter() {

            $timeout.cancel(timeout);

            scope.loadCounter = 100;

            $timeout(function(){
              // animate preloader hiding
              $animate.addClass(el, 'preloader-hidden');
              // retore scrollbar
              angular.element('body').css('overflow', '');
            }, 300);
          }

          function appReady() {
            var deferred = $q.defer();
            var viewsLoaded = 0;
            // if this doesn't sync with the real app ready
            // a custom event must be used instead
            var off = scope.$on('$viewContentLoaded', function () {
              viewsLoaded ++;
              // we know there are at least two views to be loaded 
              // before the app is ready (1-index.html 2-app*.html)
              if ( viewsLoaded === 2) {
                // with resolve this fires only once
                $timeout(function(){
                  deferred.resolve();
                }, 3000);

                off();
              }

            });

            return deferred.promise;
          }

        } //link
    }

})();
/**=========================================================
 * Module: app.producto.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.producto')
    .controller('productoController', productoController);

  productoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'productoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function productoController($scope, $filter, $state, LDataSource, productoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var producto = $state.params.producto;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.producto = {
		model : {
              id: 0,                            
			  nombre: '',
              descripcion: '',
              valor:0,			
              estado: '',
              fechaAct: '',
              usuarioAct: '',
              fechaReg: '',
              usuarioReg: ''			
		}, 
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaInicial.isOpen = true;
        }
      },  
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaFinal.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.producto', params : { filters : {procesoSeleccionado:producto.tipo}, data : []} });
        $state.go('app.producto');
          
      },
      save : function() {		  
       
		  
        
          		if($scope.producto.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.producto.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.producto.model.estado= $scope.estados.current.value;}
				
			 
			//INSERTAR
            if($scope.producto.model.id==0){
				
				$rootScope.loadingVisible = true;
				productoHttp.save({}, $scope.producto.model, function (data) { 
					
						productoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.producto.model=data;
							producto.id=$scope.producto.model.id; 
							if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
							if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));} 
                            
                                 
								
							message.show("success", "producto creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.producto.model.id){
					$state.go('app.producto');
				}else{
					
					productoHttp.update({}, $scope.producto.model, function (data) {                           
					  $rootScope.loadingVisible = false;
					       $scope.producto.model=data;
						if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
						if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));}   		
                                     
					   message.show("success", "producto actualizado satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		}
      }
        
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'E', descripcion: 'Eliminado'}); 
        }       
    }      
    //Estados
    $scope.estados.getData();	
	//CARGAMOS LOS DATOS DEL producto	
	
	if(producto.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
       
        
    }
    else{   
        $rootScope.loadingVisible = true;       
        
		
        productoHttp.read({},$state.params.producto, function (data) { 
        $scope.producto.model = data;		

        if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
        if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));}              
       
        //$scope.estados.current=$scope.producto.model.estado;	            
        $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.producto.model.estado})[0];
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
    
      

	
    
    
  }
})();
/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.producto')
    .service('productoHttp', contratoHttp);

  contratoHttp.$inject = ['$resource', 'END_POINT'];


  function contratoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getList' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/producto/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/producto'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/producto'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/producto/:id'
      }
        
     
    };
    return $resource( END_POINT + '/Comercial.svc/producto', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.producto.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.producto')
    .controller('productoListController', productoListController);

  productoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'productoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function productoListController($scope, $filter, $state, ngDialog, tpl, productoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
		
	$scope.ESTADO_ACTIVO = 'A';
    $scope.ESTADO_INACTIVO = 'I';
    $scope.ESTADO_ELIMINADO= 'E';	
    	

    $scope.productos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreproductos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.productos.selectedAll = !$scope.productos.selectedAll; 
        for (var key in $scope.productos.selectedItems) {
          $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
        }
      },
      add : function() {  
              var cont = {
                      id: 0,                    
                      nombre: '',
                      descripcion: '',
                      valor: 0,
                      estado: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.producto_add', params : { producto: cont } });
            $state.go('app.producto_add');  
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.producto_edit', params : { producto: item } });
        $state.go('app.producto_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            productoHttp.remove({}, { id: id }, function(response) {
                $scope.productos.getData();
                message.show("success", "producto eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.productos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    productoHttp.remove({}, { id: id }, function(response) {
                        $scope.productos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.productos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.productos.filterText,
          "precision": false
        }];
        $scope.productos.selectedItems = $filter('arrayFilter')($scope.productos.dataSource, paramFilter);
        $scope.productos.paginations.totalItems = $scope.productos.selectedItems.length;
        $scope.productos.paginations.currentPage = 1;
        $scope.productos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.productos.paginations.currentPage == 1 ) ? 0 : ($scope.productos.paginations.currentPage * $scope.productos.paginations.itemsPerPage) - $scope.productos.paginations.itemsPerPage;
        $scope.productos.data = $scope.productos.selectedItems.slice(firstItem , $scope.productos.paginations.currentPage * $scope.productos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.productos.data = [];
        $scope.productos.loading = true;
        $scope.productos.noData = false;      
         productoHttp.getList(function(response) {
          $scope.productos.selectedItems = response;
          $scope.productos.dataSource = response;
          for(var i=0; i<$scope.productos.dataSource.length; i++){
            $scope.productos.nombreproductos.push({id: i, nombre: $scope.productos.dataSource[i]});
          }
          $scope.productos.paginations.totalItems = $scope.productos.selectedItems.length;
          $scope.productos.paginations.currentPage = 1;
          $scope.productos.changePage();
          $scope.productos.loading = false;
          ($scope.productos.dataSource.length < 1) ? $scope.productos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }  
    
	//CARGAMOS DATA    
    $scope.productos.getData();  
        
 
	}
  
  
  })();
/**=========================================================
 * Module: app.recibo.js
 =========================================================*/
(function() {
  'use strict';

  angular
    .module('app.recibo')
    .controller('reciboController', reciboController);

 reciboController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'reciboHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal'];   

  function reciboController($scope, $rootScope, $state,$modalInstance,$filter,reciboHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window,$modal) {      
      
     
      var recibo =parameters.recibo;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.recibo = {
		model : {
              id: 0,
              terceroId: 0,                      
              consecutivo: '',                     
              estado: '',
              facturaId: 0,    
              fechaAct: '',
              usuarioAct: '',
              fechaReg: '',
              usuarioReg: ''
		},
      recibos:[],
      reciboSeleccionada:undefined,
      deleteRecibo: function(p) {
           $scope.recibo.model.recibos.splice($scope.recibo.model.recibos.indexOf(p), 1);     
      },
      back : function() {
          parametersOfState.set({ name : 'app.recibo', params : { filters : {procesoSeleccionado:recibo.tipo}, data : []} });
        $state.go('app.recibo');
          
      },        
      save : function() {		
          
            
            if($scope.recibo.model.identificacion==""){message.show("warning", "Identifiación del cliente requerido");return;}
            if($scope.recibo.model.tercero==""){message.show("warning", "Razón social del cliente requerido");return;}
            if($scope.recibo.model.email==""){message.show("warning", "Email del cliente requerido");return; }
            if($scope.recibo.model.polizas.length ==0){message.show("warning", "Debe agregar polizas");return;}  
                
         
            try{                
                $scope.recibo.model.fechaInicio=$scope.recibo.formatdate($scope.recibo.model.fecha);
            }
            catch(err) {
            }
          
           try{
                
                $scope.recibo.model.fechaEntrega=$scope.recibo.formatdate($scope.recibo.model.fechaEntrega);
            }
            catch(err) {
                $scope.recibo.model.fechaEntrega='';
            }
            
				
            $rootScope.loadingVisible = true;
          
            reciboHttp.save({}, $scope.recibo.model, function (data) { 	
                       
                
                        if ($scope.recibo.model.id==0){
                            message.show("success", "Recibo creado satisfactoriamente!!");
                        }
                        else{message.show("success", "Recibo actualizado satisfactoriamente!!");}
                        
                
                        $scope.pedido.model.id=data.id;		
                        $scope.pedido.model.intermediarioId=data.intermediarioId;
                        	
                        $scope.btnEnviarCotizacion=true;
                        $scope.crearCliente=false;
                        $scope.pedido.model.crearCliente=0;                        
                                     
                        $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });				
		
		},          
          anular: function() {   
              
            if (confirm("Desea anular el recibo?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.recibo.model.estado=3;//estado anulado

                try{                
                    $scope.recibo.model.fechaInicio=$scope.recibo.formatdate($scope.recibo.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.recibo.model.fechaFin=$scope.recibo.formatdate($scope.recibo.model.fechaFin);
                }
                catch(err) {
                }
                       reciboHttp.save({}, $scope.recibo.model, function (data) {

                            $rootScope.loadingVisible = false;
                            message.show("success", "Recibo anulado satisfactoriamente!!");	
                            $scope.recibo.close();               


                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
       
  
         
  
        close : function() {
        if(parameters.vrecibos!=null && parameters.vrecibos!=undefined)
            parameters.vrecibos.getData();
            
        $modalInstance.dismiss('cancel');
      },
        formatdate(t){    

            //var today = new Date();
            var dd = t.getDate();
            var mm = t.getMonth()+1; //January is 0!

            var yyyy = t.getFullYear();
            if(dd<10){
                dd='0'+dd;
            } 
            if(mm<10){
                mm='0'+mm;
            } 
            //2018-01-15
            return yyyy+'-'+mm+'-'+dd;         
       
        },
        
       buscarCliente : function() { 

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion : $scope.recibo.model}
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }, 
      
    }      
      
      
    $scope.tipoPagos  = {
            current: {},
            data: [],
            getData: function() {
                $scope.tipoPagos.data.push({value:0, descripcion: 'Tarjeta VISA'});
                $scope.tipoPagos.data.push({value:1, descripcion: 'Tarjeta MASTERCARD'});
                $scope.tipoPagos.data.push({value:2, descripcion: 'Tarjeta Diners'});
                $scope.tipoPagos.data.push({value:3, descripcion: 'Tarjeta AMERICAN'});
                $scope.tipoPagos.data.push({value:4, descripcion: 'Pse'});     
                $scope.tipoPagos.data.push({value:5, descripcion: 'Consig Bogota'});     
                $scope.tipoPagos.data.push({value:6, descripcion: 'Consig Bancolombia'});     
                $scope.tipoPagos.data.push({value:7, descripcion: 'Consig Colpatria'});     
                $scope.tipoPagos.data.push({value:8, descripcion: 'Cheque'});
                $scope.tipoPagos.current=$scope.tipoPagos.data[0];    
            }
        }
      
        $scope.ciudades  = {
            current: {},
            data: [],
            getData: function() {
                $scope.ciudades.data.push({value:0, descripcion: 'Bogotá D.C'});
                $scope.ciudades.data.push({value:1, descripcion: 'Medellín'});     
                $scope.ciudades.data.push({value:2, descripcion: 'Barranquilla'});     
                $scope.ciudades.data.push({value:3, descripcion: 'Calí'});     
                $scope.ciudades.data.push({value:4, descripcion: 'Bucaramanga'});      
                $scope.ciudades.data.push({value:5, descripcion: 'Santa Marta'});  
                $scope.ciudades.current=$scope.ciudades.data[0];    
            }
        }
      debugger
    //EMPLEADOS
    $scope.empleados = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            reciboHttp.getEmpleados({}, {}, function(response) {                
            $scope.empleados.data = response ;              
                
               if($scope.recibo.model){
                                     
               }                           
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setEmpleado' : function() {
          $scope.recibo.model.empleadoId=$scope.empleados.current.id;
      }
     } 
	
	//CARGAMOS LOS LISTADOS	
      
    $scope.tipoPagos.getData();
    $scope.ciudades.getData();
    $scope.empleados.getData();
   

	//CARGAMOS LOS DATOS DEL RECIBO	
	
	if(recibo.id==0){
          $scope.btnModificar=true;
          $scope.btnAnular=false;        
    }
    else{   
        $rootScope.loadingVisible = true;  
        $scope.btnEditar=false;
        $scope.btnEnviarCotizacion=true;
        
        
		
            reciboHttp.read({},pedido, function (data) { 
                
            $scope.recibo.model = data;	                

            $scope.recibo.model.fecha=new Date(data.fecha);

            $scope.tipoPagos.current = $filter('filter')($scope.tipoPagos.data, { value : $scope.pedido.model.tipoPagoId })[0];
            $scope.ciudades.current = $filter('filter')($scope.ciudades.data, { value : $scope.pedido.model.ciudadId })[0];



            $scope.btnModificar=true;
            $scope.btnAnular=true;
                
                
                
                
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
        
    }
      

        
    
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.recibo')
    .service('reciboHttp', reciboHttp);

  reciboHttp.$inject = ['$resource', 'END_POINT'];


  function reciboHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };      
    var actions = {
      'getList' : {
       'method' : 'GET',
        'isArray' : true,
        'params' : {           
      },
          'url' : END_POINT + '/Comercial.svc/recibo'          
      },
      
     'getEmpleados' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/recibo'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/recibo'
      }
    };
    return $resource( END_POINT + '/Comercial.svc/recibo', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.recibo.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.recibo')
    .controller('reciboListController', reciboListController);

  reciboListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'reciboHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function reciboListController($scope, $filter, $state,$modal, ngDialog, tpl, reciboHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
    
  	
    $scope.recibos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrerecibos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.recibos.selectedAll = !$scope.recibos.selectedAll; 
        for (var key in $scope.recibos.selectedItems) {
          $scope.recibos.selectedItems[key].check = $scope.recibos.selectedAll;
        }
      },

      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.recibos.filterText,
          "precision": false
        }];
        $scope.recibos.selectedItems = $filter('arrayFilter')($scope.recibos.dataSource, paramFilter);
        $scope.recibos.paginations.totalItems = $scope.recibos.selectedItems.length;
        $scope.recibos.paginations.currentPage = 1;
        $scope.recibos.changePage();
      },  
      changePage : function() {
        var firstItem = ($scope.recibos.paginations.currentPage == 1 ) ? 0 : ($scope.recibos.paginations.currentPage * $scope.recibos.paginations.itemsPerPage) - $scope.recibos.paginations.itemsPerPage;
        $scope.recibos.data = $scope.recibos.selectedItems.slice(firstItem , $scope.recibos.paginations.currentPage * $scope.recibos.paginations.itemsPerPage);
      },      
      getData : function() {
      $scope.recibos.data = [];
      $scope.recibos.loading = true;
      $scope.recibos.noData = false;        
          
        reciboHttp.getList({}, {},function(response) {
          $scope.recibos.selectedItems = response;
          $scope.recibos.dataSource = response;
          for(var i=0; i<$scope.recibos.dataSource.length; i++){
            $scope.recibos.nombrerecibos.push({id: i, nombre: $scope.recibos.dataSource[i]});
          }
          $scope.recibos.paginations.totalItems = $scope.recibos.selectedItems.length;
          $scope.recibos.paginations.currentPage = 1;
          $scope.recibos.changePage();
          $scope.recibos.loading = false;
          ($scope.recibos.dataSource.length < 1) ? $scope.recibos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      },
        
      add : function() {
        
        var rec = {
                      id: 0,
                      terceroId: 0,                      
                      consecutivo: '',                     
                      estado: '',
                      facturaId: 0,    
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
          
            var modalInstance = $modal.open({
            templateUrl: 'app/views/recibo/recibo_form.html',
            controller: 'reciboController',
            size: 'lg',
            resolve: {
                parameters: { recibo: rec,vrecibos:$scope.recibos }
            }
            });
            modalInstance.result.then(function (parameters) {
            });  
          
      },
      edit : function(item) {
      
        var modalInstance = $modal.open({
          templateUrl: 'app/views/recibo/recibo_form.html',
          controller: 'reciboController',
          size: 'lg',
          resolve: {
            parameters: { recibo: item,vrecibos:$scope.recibos }
          }
        });
        modalInstance.result.then(function (parameters) {
            $scope.recibos.getData();
        });       
          
      }
        
        
        
        
        
        
        
    }

	//CARGAMOS LOS TIPOS DE PROCESO
    
    $scope.recibos.getData();  

        
	}
  
  
  })();
/**=========================================================
 * Module: app.report.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .controller('rolController', rolController);

  rolController.$inject = ['$scope', '$rootScope', '$state', '$q', '$filter', 'roltHttp', 'base64', '$stateParams', 'parametersOfState', 'message', '$modal', 'ngDialog', 'tpl'];


  function rolController($scope, $rootScope, $state, $q, $filter, roltHttp, base64, $stateParams, parametersOfState, message, $modal, ngDialog, tpl) {
      
      
      $scope.gridOptions = {
        rowHeight: 34,
        data: [],
        subsections: [],
        setData: function(){
          $rootScope.loadingVisible = true;
          $scope.gridOptions.data = []; 
          roltHttp.getList({}, {}, function(response) {
              $scope.gridOptions.data = $filter('orderBy')(response, 'nombre');
              $rootScope.loadingVisible = false;
          }, function(faild) {
              $rootScope.loadingVisible = false;
              message.show("error", "No se Pudo Obtener el Listado de Roles: " + faild.Message);
          });
        },
        edit: function (item){
          var parameter = {datos: item, option: 0};
          var modalInstance = $modal.open({
              templateUrl: 'app/views/roles/roles_edit.html',
              controller: 'rolEditController',
              size: 'sm',
              resolve: {
                parameters: function () { return parameter; }
              }
          });
          modalInstance.result.then(function (parameters) {
            $scope.gridOptions.setData();
          }, function (parameters) {
          });
        },
        add: function (){
          var model = {id: 0, nombre: '', estado: 0};
          var parameter = {datos: model, option: 1};
          var modalInstance = $modal.open({
              templateUrl: 'app/views/roles/roles_edit.html',
              controller: 'rolEditController',
              size: 'sm',
              resolve: {
                parameters: function () { return parameter; }
              }
          });
          modalInstance.result.then(function (parameters) {
            $scope.gridOptions.setData();
          }, function (parameters) {
          });
        },
        remove: function(item){
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
              $rootScope.loadingVisible = true;
              roltHttp.removeRol({}, { id: item.id }, function(response) {
                  $scope.gridOptions.setData();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se Pudo Eliminar el Registro: " + faild.Message);
              });
          });
        }
      };
      $scope.gridOptions.setData();
  }

})();
/**=========================================================
 * Module: app.report.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .controller('rolEditController', rolEditController);

  rolEditController.$inject = ['$scope', '$rootScope', '$state', '$q', '$filter', 'roltHttp', 'base64', '$stateParams', 'parametersOfState', 'message', 'parameters', '$modalInstance'];


  function rolEditController($scope, $rootScope, $state, $q, $filter, roltHttp, base64, $stateParams, parametersOfState, message, parameters, $modalInstance) {
    
      $scope.estados= {
          current:{},
          data: [],
          setData: function() {
              $scope.estados.data.push({id: 0, value: 'Inactivo'});
              $scope.estados.data.push({id: 1, value: 'Activo'});
              $scope.estados.current = $scope.estados.data[0];
          }
      };
      $scope.estados.setData();
      
      $scope.rol={
          model:{
              id: 0,
              nombre: '',
              estado: 0
          },
          option: 0,
          secciones: [],
          save: function() {
            
            if(!$scope.rol.model.nombre){message.show("warning", "Nombre del Rol Requerido");return;}
            if($scope.rol.model.nombre == ""){message.show("warning", "Nombre del Rol Requerido");return;}
            if(!$scope.estados.current){message.show("warning", "Estado del Rol Requerido");return;}
            $scope.rol.model.estado = $scope.estados.current.id;
            
            if($scope.rol.option == 0){
              $rootScope.loadingVisible = true;
              var parametros = $scope.rol.model;
              roltHttp.editRol({}, parametros,  function(response) {
                  $scope.rol.close();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se pudo guardar el registro: " + faild.Message);
              });
            }
            if($scope.rol.option == 1){
              
              var parametros = {
                  nombre: $scope.rol.model.nombre,
                  estado: $scope.rol.model.estado
              };
              
              var _Http = new roltHttp(parametros);
              _Http.$newrol( function(response) {
                  $scope.rol.close();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });
            }
          },
          dismiss : function() {
              $modalInstance.dismiss('cancel');
          },
          close : function() {
              $modalInstance.close('close');
          }
      };
      
      $scope.rol.model = parameters.datos;
      $scope.rol.option = parameters.option;
      
      if($scope.rol.option==0){
        $scope.estados.current= $filter('filter')($scope.estados.data, {id: $scope.rol.model.estado})[0];
      }
  }

})();
/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .service('roltHttp', roltHttp);

  roltHttp.$inject = ['$resource', 'END_POINT'];


  function roltHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/rol'
      },
      'editRol' : {
        'method' : 'PUT',
        'url' : END_POINT + '/General.svc/rol'
      },
      'newrol' : {
        'method' : 'POST',
        'url' : END_POINT + '/General.svc/rol'
      },
      'removeRol': {
        'method':'DELETE',
        'params' : {
           id : '@id'
        },
        'url' : END_POINT + '/General.svc/rol/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/rol', paramDefault, actions);
  }

})();
/**=========================================================
 * Module: helpers.js
 * Provides helper functions for routes definition
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.routes')
        .provider('RouteHelpers', RouteHelpersProvider)
        ;

    RouteHelpersProvider.$inject = ['APP_REQUIRES'];
    function RouteHelpersProvider(APP_REQUIRES) {

      /* jshint validthis:true */
      return {
        // provider access level
        basepath: basepath,
        resolveFor: resolveFor,
        // controller access level
        $get: function() {
          return {
            basepath: basepath,
            resolveFor: resolveFor
          };
        }
      };

      // Set here the base of the relative path
      // for all app views
      function basepath(uri) {
        return 'app/views/' + uri;
      }

      // Generates a resolve object by passing script names
      // previously configured in constant.APP_REQUIRES
      function resolveFor() {
        var _args = arguments;
        return {
          deps: ['$ocLazyLoad','$q', function ($ocLL, $q) {
            // Creates a promise chain for each argument
            var promise = $q.when(1); // empty promise
            for(var i=0, len=_args.length; i < len; i ++){
              promise = andThen(_args[i]);
            }
            return promise;

            // creates promise to chain dynamically
            function andThen(_arg) {
              // also support a function that returns a promise
              if(typeof _arg === 'function')
                  return promise.then(_arg);
              else
                  return promise.then(function() {
                    // if is a module, pass the name. If not, pass the array
                    var whatToLoad = getRequired(_arg);
                    // simple error check
                    if(!whatToLoad) return $.error('Route resolve: Bad resource name [' + _arg + ']');
                    // finally, return a promise
                    return $ocLL.load( whatToLoad );
                  });
            }
            // check and returns required data
            // analyze module items with the form [name: '', files: []]
            // and also simple array of script files (for not angular js)
            function getRequired(name) {
              if (APP_REQUIRES.modules)
                  for(var m in APP_REQUIRES.modules)
                      if(APP_REQUIRES.modules[m].name && APP_REQUIRES.modules[m].name === name)
                          return APP_REQUIRES.modules[m];
              return APP_REQUIRES.scripts && APP_REQUIRES.scripts[name];
            }

          }]};
      } // resolveFor

    }


})();


/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.routes')
    .config(routesConfig);

  routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];
  function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper){

    // Set the following to true to enable the HTML5 Mode
    // You may have to set <base> tag in index and a routing configuration in your server
    $locationProvider.html5Mode(false);

    // defaults to dashboard
    $urlRouterProvider.otherwise('/page/login');
    //'toastr',

    $stateProvider
      .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: helper.basepath('app.html'),
      resolve: helper.resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'whirl', 'al-ui', 'ngProgress', 'ab-base64','angularFileUpload', 'ngFileUpload', 'flot-chart','flot-chart-plugins', 'ui.select', 'ngDialog', 'ui.utils.masks')
    })
      .state('app.home', {
      url: '/home',
      title: 'home',
      templateUrl: helper.basepath('home.html'),
      resolve: helper.resolveFor( 'weather-icons')
    })
      .state('app.template', {
      url: '/template',
      title: 'Blank Template',
      templateUrl: helper.basepath('template.html')
    })
    
    //FUNCIONARIO
	   .state('app.funcionario', {
      url: '/funcionario',
      controller : 'funcionarioListController',
      templateUrl: 'app/views/funcionario/funcionario_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
	
	 .state('app.funcionario_add', {
      url: '/funcionario_add',
      controller : 'funcionarioController',
      templateUrl: 'app/views/funcionario/funcionario_form.html',
      params : { funcionario: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
      .state('app.funcionario_edit', {
      url: '/funcionario_edit',
      controller : 'funcionarioController',
      templateUrl: 'app/views/funcionario/funcionario_form.html',
      params : { funcionario: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    //TERCERO
       .state('app.tercero', {
      url: '/tercero',
      controller : 'terceroListController',
      templateUrl: 'app/views/tercero/tercero_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
      
     .state('app.tercero_add', {
      url: '/tercero_add',
      controller : 'terceroController',
      templateUrl: 'app/views/tercero/tercero_form.html',
      params : { tercero: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
      .state('app.tercero_edit', {
      url: '/tercero_edit',
      controller : 'terceroController',
      templateUrl: 'app/views/tercero/tercero_form.html',
      params : { tercero: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    //PASAJEROS
       .state('app.pasajero', {
      url: '/pasajero',
      controller : 'pasajeroListController',
      templateUrl: 'app/views/pasajero/pasajero_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    //RECIBOS
       .state('app.recibo', {
      url: '/recibo',
      controller : 'reciboListController',
      templateUrl: 'app/views/recibo/recibo_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    //OPORTUNIDAD
    
     .state('app.oportunidad', {
      url: '/oportunidad',
      controller : 'oportunidadListController',
      templateUrl: 'app/views/oportunidad/oportunidad_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.oportunidad_add', {
      url: '/oportunidad_add',
      controller : 'oportunidadController',
      templateUrl: 'app/views/oportunidad/oportunidad_form.html',
      params : { oportunidad: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.oportunidad_edit', {
      url: '/oportunidad_edit',
      controller : 'oportunidadController',
      templateUrl: 'app/views/oportunidad/oportunidad_form.html',
      params : { oportunidad: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    }) 
     //TRM
     .state('app.trm', {
      url: '/trm',
      controller : 'trmListController',
      templateUrl: 'app/views/trm/trm_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
     //TAREA
     .state('app.tarea', {
      url: '/tarea',
      controller : 'tareaListController',
      templateUrl: 'app/views/tarea/tarea_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    //COTIZACION
     .state('app.cotizacion', {
      url: '/cotizacion',
      controller : 'cotizacionListController',
      templateUrl: 'app/views/cotizacion/cotizacion_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
      
   .state('app.cotizacion_add', {
      url: '/cotizacion_add',
      controller : 'cotizacionController',
      templateUrl: 'app/views/cotizacion/cotizacion_form.html',
      params : { cotizacion: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.cotizacion_edit', {
      url: '/cotizacion_edit',
      controller : 'cotizacionController',
      templateUrl: 'app/views/cotizacion/cotizacion_form.html',
      params : { cotizacion: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    }) 
      
      
    //PEDIDO
     .state('app.pedido', {
      url: '/pedido',
      controller : 'pedidoListController',
      templateUrl: 'app/views/pedido/pedido_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })   
     //FACTURA    
     .state('app.factura', {
      url: '/factura',
      controller : 'facturaListController',
      templateUrl: 'app/views/factura/factura_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.factura_add', {
      url: '/factura_add',
      controller : 'facturaController',
      templateUrl: 'app/views/factura/factura_form.html',
      params : { factura: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.factura_edit', {
      url: '/factura_edit',
      controller : 'facturaController',
      templateUrl: 'app/views/factura/factura_form.html',
      params : { factura: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    }) 
     //ENTREGA
     .state('app.entrega', {
      url: '/entrega',
      controller : 'entregaListController',
      templateUrl: 'app/views/entrega/entrega_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
    
   
    
    //PRODUCTO
    
     .state('app.producto', {
      url: '/producto',
      controller : 'productoListController',
      templateUrl: 'app/views/producto/producto_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.producto_add', {
      url: '/contrato_add',
      controller : 'productoController',
      templateUrl: 'app/views/producto/producto_form.html',
      params : { producto: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.producto_edit', {
      url: '/producto_edit',
      controller : 'productoController',
      templateUrl: 'app/views/producto/producto_form.html',
      params : { producto: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    // CALENDARIO
    .state('app.calendar', {
          url: '/calendar',
          title: 'Calendar',
          templateUrl: helper.basepath('/calendar/calendar.html'),
          resolve: helper.resolveFor('jquery-ui', 'jquery-ui-widgets', 'moment', 'fullcalendar')
      })
    
    
    //AGENDAS    
     .state('app.agenda', {
      url: '/agenda',
      controller : 'agendaListController',
      templateUrl: 'app/views/agenda/agenda_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.agenda_add', {
      url: '/agenda_add',
      controller : 'agendaFormController',
      templateUrl: 'app/views/agenda/agenda_form.html',
      params : { agenda: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.agenda_edit', {
      url: '/agenda_edit',
      controller : 'agendaFormController',
      templateUrl: 'app/views/agenda/agenda_form.html',
      params : { agenda: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    

   
     .state('app.setMenu', {
      url: '/Menu',
      controller : 'setMenuController',
      templateUrl: 'app/views/menu/setMenu.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
      .state('app.rol', {
      url: '/Roles',
      controller : 'rolController',
      templateUrl: 'app/views/roles/roles.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
    
    
        
   
    //MENSAJERIA
    
     .state('app.mensajeria', {
      url: '/mensajeria',
      controller : 'mensajeriaListController',
      templateUrl: 'app/views/mensajeria/mensajeria_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.mensajeria_add', {
      url: '/mensajeria_add',
      controller : 'mensajeriaController',
      templateUrl: 'app/views/mensajeria/mensajeria_form.html',
      params : { mensajeria: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.mensajeria_edit', {
      url: '/mensajeria_edit',
      controller : 'mensajeriaController',
      templateUrl: 'app/views/mensajeria/mensajeria_form.html',
      params : { mensajeria: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
    //     
    /*
     .state('app.registroUbicacion', {
      url: '/registroUbicacion',
      controller : 'registroUbicacionListController',
      templateUrl: 'app/views/usuario/registroUbicacion_list.html',
      params : { filters : {}, data : []},
      resolve: helper.resolveFor('loadGoogleMapsJS', function() { return  
                loadGoogleMaps(3.21,'AIzaSyAQMaVBZQTPu5tPLNA39ulXaNdUGGqSCSw'); }, 'ui.map')
    })   
    */     
      .state('app.registroUbicacion', {
      url: '/registroUbicacion',
      controller : 'registroUbicacionListController',
      templateUrl: 'app/views/usuario/registroUbicacion_list.html',
      params : { filters : {}, data : []},
        resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    }) 
    
    // 
    // Single Page Routes
    // -----------------------------------
      .state('page', {
      url: '/page',
      templateUrl: 'app/pages/page.html',
      resolve: helper.resolveFor('modernizr', 'icons'),
      controller: ['$rootScope', function($rootScope) {
        $rootScope.app.layout.isBoxed = false;
      }]
    })
      .state('page.login', {
      url: '/login',
      controller : 'LoginController',
      title: 'Login',
      templateUrl: 'app/views/security/login.html'
    })
      .state('page.register', {
      url: '/register',
      title: 'Register',
      templateUrl: 'app/views/security/register.html'
    })
      .state('page.recover', {
      url: '/recover',
      controller : 'RecoverController',
      title: 'Recover',
      templateUrl: 'app/views/security/recover.html'
    })
      .state('page.resetpassword', {
      url: '/resetpassword/:token',
      controller : 'ResetPasswordController',
      templateUrl: 'app/views/security/resetPassword.html'
    })
      .state('page.lock', {
      url: '/lock',
      title: 'Lock',
      templateUrl: 'app/views/security/lock.html'
    })
      .state('page.404', {
      url: '/404',
      title: 'Not Found',
      templateUrl: 'app/pages/404.html'
    });

  } // routesConfig

})();


(function() {
  'use strict';
  angular
    .module('app.security')
    .service('authRequestInterceptor', authRequestInterceptor);

  authRequestInterceptor.$inject = ['tokenManager'];


  function authRequestInterceptor(tokenManager) {
    return {
      request: function (config) {

        if (config.headers.Authorization == undefined) {
          config.headers = config.headers || {};
          if ( tokenManager.get() !== undefined || tokenManager.get() !== null ) {
            config.headers.Authorization = 'bearer '+ tokenManager.get();        
          }
        }

        return config;
      }
    }
  }

})();
(function() {
  'use strict';
  angular
    .module('app.security')
    .service('authResponseInterceptor', authResponseInterceptor);

  authResponseInterceptor.$inject = ['$q', '$timeout', '$location',  '$rootScope', '$window', 'tokenManager', 'message'];


  function authResponseInterceptor($q, $timeout, $location,  $rootScope, $window, tokenManager, message) {
    return {
      response: function(response) {
        return response;
      },
      responseError: function (response) {
        var INVALID_TOKEN =  1;
        if(response.status == 0) {
          if ($rootScope.updatingToken == false) {
            message.show("warning", "No fue posible conectarse al servidor");
          }
          return $q.reject(response.status);
        } else if (response.data.ErrorCode !== undefined) {
          if (response.data.ErrorCode === INVALID_TOKEN && !$rootScope.updatingToken){  
            tokenManager.resetToken();
            if ($rootScope.updatingToken == false) {
              message.show("error", "No fue posible conectarse al servidor");
            }
            $location.path('page/login');
            return $q.reject(null);
          } else {
            return $q.reject(response.data);
          }
        } else if (response.data !== undefined) {
          return $q.resolve(response.data);
        } else {
          return $q.resolve(response);
        }
      }
    }
  }
  
})();
/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$scope', '$rootScope', '$state', 'securityHttp', 'tokenManager', 'message'];


  function LoginController($scope, $rootScope, $state, securityHttp, tokenManager, message) {

    (tokenManager.getRefreshToken()) ? $state.go('app.home') : null;
      
      

      
    $scope.login = {
      account : {
        user : '',
        password : ''
      },
      login : function() {
        var params = {
          user : $scope.login.account.user,
          pass : $scope.login.account.password
        };

        $rootScope.loadingVisible = true;

        securityHttp.login({}, params, function(response) {
          $rootScope.perfil = response.perfil;
          tokenManager.set(response);
          $state.go('app.home');
          $rootScope.loadingVisible = false;
        }, function(faild) {
          message.show('error', 'Usuario o Clave incorrecta');
          $rootScope.loadingVisible = false;
        })


        $rootScope.$on('refresh_token', function(event, param) {
          if ($rootScope.updatingToken == false) {
            $rootScope.updatingToken = true;
            securityHttp.refreshToken({}, param, function(token) {
              tokenManager.set(token);
              $rootScope.updatingToken = false;
            }, function(faild) {
              message.show('error', "No fue posible ingresar, con credenciales actuales");
              $rootScope.updatingToken = false;
            });
          }
        })

      }
    }

  }

})();
/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('RecoverController', RecoverController);

  RecoverController.$inject = ['$scope', '$rootScope', '$state', 'securityHttp','message'];


  function RecoverController($scope, $rootScope, $state, securityHttp, message) {

    $scope.recover = {
      model : {
        email : ''
      },
      resetPassword : function() {
        $rootScope.loadingVisible = true;
        securityHttp.resetPassword({},{ email : $scope.recover.model.email}, function() {
          message.show("info","Hemos enviado un correo electronico para continuar con el proceso");
          $state.go('page.login');
          $rootScope.loadingVisible = false;
        }, function(faild) {
          $state.go('page.login');
          message.show("info","Ocurrio un error al intentar enviar el mensaje de confirmación.");
          $rootScope.loadingVisible = false;
        });
      }
    }


  }

})();
/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('ResetPasswordController', ResetPasswordController);

  ResetPasswordController.$inject = ['$scope', '$rootScope', '$state', '$location', 'securityHttp', 'tokenManager', 'message'];


  function ResetPasswordController($scope, $rootScope, $state, $location, securityHttp, tokenManager, message) {


    $scope.reset = {
        model : {
            token : '',
            password : '',
            repeatPassword: ''
        },
      resetPassword : function() {
        if ($scope.reset.model.password == $scope.reset.model.repeatPassword) {
        debugger;
        $rootScope.loadingVisible = true;
        tokenManager.set({ token : $scope.reset.model.token });
        securityHttp.confirmPassword({}, { password : $scope.reset.model.password }, function(response) {
          $state.go('page.login');
          $rootScope.loadingVisible = false;
          tokenManager.resetToken();
          message.show('info', 'Su contraseña ha sido actualizada exitosamente.');
        }, function(faild) {
          tokenManager.resetToken();
          $state.go('page.recover');
          message.show('error', faild.Message);
          $rootScope.loadingVisible = false;
        })
          
        } else {
          message.show("info", "Las claves no corresponden");
          $scope.reset.model.password = '';
          $scope.reset.model.repeatPassword = '';          
        }
      }
    }
    
    $scope.reset.model.token = $location.url().split("/")[3];

       
      
  }

})();
(function() {
    'use strict';

    angular
        .module('app.security')
        .config(securityConfig);

    securityConfig.$inject = ['$httpProvider'];
    function securityConfig($httpProvider) {
      
      $httpProvider.interceptors.push('authRequestInterceptor');
      $httpProvider.interceptors.push('authResponseInterceptor');
      
    }

})();
(function() {
  'use strict';

  angular
    .module('app.security')
    .run(securityRun);

  securityRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$q', 'tokenManager', 'securityHttp', 'message'];

  function securityRun($rootScope, $state, $stateParams, $window, $q, tokenManager, securityHttp, message) {

    // Hook not found
    $rootScope.$on('$stateNotFound',
                   function(event, unfoundState/*, fromState, fromParams*/) {
      console.log(unfoundState.to); // "lazy.state"
      console.log(unfoundState.toParams); // {a:1, b:2}
      console.log(unfoundState.options); // {inherit:false} + default options
    });
    // Hook error
    $rootScope.$on('$stateChangeError',
                   function(event, toState, toParams, fromState, fromParams, error){
      console.log(error);
    });
    // Hook success

    var LAST_STATE = '';
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        
      if ((toState.name == 'page.recover' && fromState.name == 'page.login') || toState.name == 'page.resetpassword') {
      } else {
        if ((tokenManager.get() == undefined && toState.name != 'page.login') || (tokenManager.get() == '' && toState.name != 'page.login')) {
          if (LAST_STATE != toState.name) {
            event.preventDefault();  
            refreshToken().then(function() {
              LAST_STATE = 'app.home';
              $state.go('app.home');
            }, function() {
              $state.go('page.login');
            });
          }
        };        
      }
    });



    function refreshToken() {
      var defer = $q.defer();
      if (!tokenManager.get() && tokenManager.getRefreshToken()) {
        $rootScope.updatingToken = true;
        var param = { refreshToken : tokenManager.getRefreshToken() };
        tokenManager.resetToken();
        securityHttp.refreshToken({}, param, function(token) {
          tokenManager.set(token);
          $rootScope.updatingToken = false;
          defer.resolve(true);
        }, function(faild) {
          message.show("error", "No fue posible ingresar, con credenciales actuales");
          $rootScope.updatingToken = false;
          defer.reject();
        }); 
      } else {
        defer.reject();
      }
      return defer.promise;
    }
  }

})();
/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .service('securityHttp', securityHttp);

  securityHttp.$inject = ['$resource', 'END_POINT'];


  function securityHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'login' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/authUser'
      },
      'refreshToken' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/refreshToken'        
      },
      'resetPassword' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/resetPassword'        
      },
      'confirmPassword' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/confirmPassword'        
      }
    };

    return $resource(END_POINT + '/Administracion.svc/authUser', paramDefault, actions);
  }

})();

(function() {
  'use strict';

  angular
    .module('app.security')
    .service('tokenManager', tokenManager);

  tokenManager.$inject = ['$window', '$rootScope'];

  function tokenManager($window, $rootScope) {
    return ({
      set: setToken,
      get: getToken,
      getRefreshToken : getRefreshToken, 
      resetToken : resetToken
    });

    var token = '';

    function expirationTime(time) {
      console.log("expirationTime" + time);
      //var _time = (time * 60) - 60;
      var _time = time * 3;
        
      setTimeout(function() {
        $rootScope.$broadcast('refresh_token', { refreshToken : $window.localStorage.token } );
      }, _time * 1000);
    }

    function setToken(_token) {
      $rootScope.perfil = _token.perfil;
      token = _token.token;
      $window.localStorage.token = _token.refreshToken;
      expirationTime(_token.timeOut);
    }

    function getToken() {
      return token;
    }

    function getRefreshToken() {
      return $window.localStorage.token;
    }

    function resetToken() {
      token = '';
      $window.localStorage.removeItem("token");
    }


  }

})();




/**=========================================================
 * Module: app.forma30.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.seguimiento')
    .controller('seguimientoController', seguimientoController);

  seguimientoController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'seguimientoHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];


  function seguimientoController($scope, $rootScope, $state, $modalInstance, seguimientoHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {


    $scope.seguimiento = {
      model : {     
        id:0,  
        idReferencia : '',
        tipoReferencia : '',
        descripcion : '',
        usuarioReg : '',
        fechaReg : ''
          
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.seguimiento.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.seguimiento.dataSource, paramFilter);
        $scope.seguimiento.paginations.totalItems = $scope.seguimiento.selectedItems.length;
        $scope.seguimiento.paginations.currentPage = 1;
        $scope.seguimiento.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.seguimiento.paginations.currentPage == 1 ) ? 0 : ($scope.seguimiento.paginations.currentPage * $scope.seguimiento.paginations.itemsPerPage) - $scope.seguimiento.paginations.itemsPerPage;
        $scope.seguimiento.data = $scope.seguimiento.selectedItems.slice(firstItem , $scope.seguimiento.paginations.currentPage * $scope.seguimiento.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.seguimiento.model.idReferencia,
          tipoReferencia : $scope.seguimiento.model.tipoReferencia
        };
        $scope.seguimiento.getData(params);
      },
      setData : function(data) {
        $scope.seguimiento.selectedItems = data;
        $scope.seguimiento.dataSource = data;
        $scope.seguimiento.paginations.totalItems = $scope.seguimiento.selectedItems.length;
        $scope.seguimiento.paginations.currentPage = 1;
        $scope.seguimiento.changePage();
        $scope.seguimiento.noData = false;
        $scope.seguimiento.loading = false;
        ($scope.seguimiento.dataSource.length < 1) ? $scope.seguimiento.noData = true : null;
      },
      getData : function(params) {
        $scope.seguimiento.data = [];
        $scope.seguimiento.loading = true;
        $scope.seguimiento.noData = false;

        seguimientoHttp.getSeguimientos({}, params, function(response) {
          $scope.seguimiento.setData(response);
        })
      },
      visualizar : function(id) {
        var params = {
          id : id 
        };

        seguimientoHttp.getSeguimiento({}, params, function(response) {
          if (response) {
              $scope.seguimiento.model.descripcion=response.descripcion;
              
          }
        })
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;
        seguimientoHttp.removeSeguimiento(params, function(response) {
          $scope.seguimiento.refresh();
          message.show("info", "Seguimiento eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },     
      close : function() {
        $modalInstance.dismiss('cancel');
      },        
      save : function() {	
          
        $rootScope.loadingVisible = true;
        seguimientoHttp.addSeguimiento({}, $scope.seguimiento.model, function (data) { 

             message.show("success", "Seguimiento creaado correctamente");
             $rootScope.loadingVisible = false;
             $scope.seguimiento.refresh();

        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      }
    }
    
    $scope.hideDelete=true;
    
    $scope.seguimiento.model.idReferencia = parameters.id.toString();
    $scope.seguimiento.model.tipoReferencia = parameters.referencia.toString();
    //$scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;
    $scope.seguimiento.refresh();

  }

})();
/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.seguimiento')
    .service('seguimientoHttp', seguimientoHttp);

  seguimientoHttp.$inject = ['$resource', 'END_POINT'];


  function seguimientoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
        
        'addSeguimiento': {
        'method':'POST',
        'url' : END_POINT + '/General.svc/addSeguimiento'
        },
      getSeguimientos : {
        method : 'POST',
        isArray : true
      },
      getSeguimiento : {
        method : 'GET',
        url : END_POINT + '/General.svc/seguimiento/:id'
      },
      removeSeguimiento : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/seguimiento/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/seguimiento', paramDefault, actions);
  }

})();

(function() {
    'use strict';

    angular
        .module('app.settings')
        .run(settingsRun);

    settingsRun.$inject = ['$rootScope', '$localStorage'];

    function settingsRun($rootScope, $localStorage){
      
      // Global Settings
      // ----------------------------------- 
      $rootScope.app = {
        name: 'SMI',
        description: 'Sistema-Información',
        year: ((new Date()).getFullYear()),
        layout: {
          isFixed: true,
          isCollapsed: true,
          isBoxed: false,
          isRTL: false,
          horizontal: false,
          isFloat: false,
          asideHover: false,
          theme: 'app/css/SMI.css'
        },
        useFullLayout: false,
        hiddenFooter: false,
        offsidebarOpen: false,
        asideToggled: false,
        viewAnimation: 'ng-fadeInUp'
      };

      // Setup the layout mode
      $rootScope.app.layout.horizontal = ( $rootScope.$stateParams.layout === 'app-h') ;

      // Restore layout settings
      if( angular.isDefined($localStorage.layout) )
        $rootScope.app.layout = $localStorage.layout;
      else
        $localStorage.layout = $rootScope.app.layout;

      $rootScope.$watch('app.layout', function () {
        $localStorage.layout = $rootScope.app.layout;
      }, true);

      // Close submenu when sidebar change from collapsed to normal
      $rootScope.$watch('app.layout.isCollapsed', function(newValue) {
        if( newValue === false )
          $rootScope.$broadcast('closeSidebarMenu');
      });

    }

})();

/**=========================================================
 * Module: sidebar-menu.js
 * Handle sidebar collapsible elements
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .controller('SidebarController', SidebarController);

  SidebarController.$inject = ['$rootScope', '$scope', '$state', 'Utils', 'sidebarHttp', 'parametersOfState', 'TO_STATE'];
  function SidebarController($rootScope, $scope, $state, Utils, sidebarHttp, parametersOfState, TO_STATE) {

    activate();

    function activate() {
      var collapseList = [];

      // demo: when switch from collapse to hover, close all items
      $rootScope.$watch('app.layout.asideHover', function(oldVal, newVal) {
        if ( newVal  === false && oldVal === true ) {
          closeAllBut(-1);
        }
      });

      $scope.changeOptions = function(item) {
        TO_STATE = '';
        if (item.sref != '') {
          var param = (item.param != '') ? JSON.parse(item.param) : null;
          parametersOfState.set({ name : item.sref, params : param });
          $state.go(item.sref);
          if ($state.current.name == 'app.reporte') {
            $rootScope.$broadcast('changeOptions', null);
          }
        }
      }


      // Load menu from json file
      // ----------------------------------- 

      sidebarHttp.getMenu({},{},function(response) {
        $scope.menuItems = response; 
      }, function(faild) {
        $scope.menuItems = [];
      })

      // Handle sidebar and collapse items
      // ----------------------------------

      $scope.getMenuItemPropClasses = function(item) {
        return (item.heading ? 'nav-heading' : '') +
          (isActive(item) ? ' active' : '') ;
      };

      $scope.addCollapse = function($index, item) {
        collapseList[$index] = $rootScope.app.layout.asideHover ? true : !isActive(item);
      };

      $scope.isCollapse = function($index) {
        return (collapseList[$index]);
      };

      $scope.toggleCollapse = function($index, isParentItem, param) {
        // collapsed sidebar doesn't toggle drodopwn
        if( Utils.isSidebarCollapsed() || $rootScope.app.layout.asideHover ) return true;

        // make sure the item index exists
        if( angular.isDefined( collapseList[$index] ) ) {
          if ( ! $scope.lastEventFromChild ) {
            collapseList[$index] = !collapseList[$index];
            closeAllBut($index);
          }
        }
        else if ( isParentItem ) {
          closeAllBut(-1);
        }

        $scope.lastEventFromChild = isChild($index);

        return true;

      };

      // Controller helpers
      // ----------------------------------- 

      // Check item and children active state
      function isActive(item) {

        if(!item) return;

        if( !item.sref || item.sref === '#') {
          var foundActive = false;
          angular.forEach(item.submenu, function(value) {
            if(isActive(value)) foundActive = true;
          });
          return foundActive;
        }
        else
          return $state.is(item.sref) || $state.includes(item.sref);
      }

      function closeAllBut(index) {
        index += '';
        for(var i in collapseList) {
          if(index < 0 || index.indexOf(i) < 0)
            collapseList[i] = true;
        }
      }

      function isChild($index) {
        /*jshint -W018*/
        return (typeof $index === 'string') && !($index.indexOf('-') < 0);
      }


    } // activate
  }

})();

/**=========================================================
 * Module: sidebar.js
 * Wraps the sidebar and handles collapsed state
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .directive('sidebar', sidebar);

    sidebar.$inject = ['$rootScope', '$timeout', '$window', 'Utils'];
    function sidebar ($rootScope, $timeout, $window, Utils) {
        var $win = angular.element($window);
        var directive = {
            // bindToController: true,
            // controller: Controller,
            // controllerAs: 'vm',
            link: link,
            restrict: 'EA',
            template: '<nav class="sidebar" ng-transclude></nav>',
            transclude: true,
            replace: true
            // scope: {}
        };
        return directive;

        function link(scope, element, attrs) {

          var currentState = $rootScope.$state.current.name;
          var $sidebar = element;

          var eventName = Utils.isTouch() ? 'click' : 'mouseenter' ;
          var subNav = $();

          $sidebar.on( eventName, '.nav > li', function() {

            if( Utils.isSidebarCollapsed() || $rootScope.app.layout.asideHover ) {

              subNav.trigger('mouseleave');
              subNav = toggleMenuItem( $(this), $sidebar);

              // Used to detect click and touch events outside the sidebar          
              sidebarAddBackdrop();

            }

          });

          scope.$on('closeSidebarMenu', function() {
            removeFloatingNav();
          });

          // Normalize state when resize to mobile
          $win.on('resize', function() {
            if( ! Utils.isMobile() )
          	asideToggleOff();
          });

          // Adjustment on route changes
          $rootScope.$on('$stateChangeStart', function(event, toState) {
            currentState = toState.name;
            // Hide sidebar automatically on mobile
            asideToggleOff();

            $rootScope.$broadcast('closeSidebarMenu');
          });

      	  // Autoclose when click outside the sidebar
          if ( angular.isDefined(attrs.sidebarAnyclickClose) ) {
            
            var wrapper = $('.wrapper');
            var sbclickEvent = 'click.sidebar';
            
            $rootScope.$watch('app.asideToggled', watchExternalClicks);

          }

          //////

          function watchExternalClicks(newVal) {
            // if sidebar becomes visible
            if ( newVal === true ) {
              $timeout(function(){ // render after current digest cycle
                wrapper.on(sbclickEvent, function(e){
                  // if not child of sidebar
                  if( ! $(e.target).parents('.aside').length ) {
                    asideToggleOff();
                  }
                });
              });
            }
            else {
              // dettach event
              wrapper.off(sbclickEvent);
            }
          }

          function asideToggleOff() {
            $rootScope.app.asideToggled = false;
            if(!scope.$$phase) scope.$apply(); // anti-pattern but sometimes necessary
      	  }
        }
        
        ///////

        function sidebarAddBackdrop() {
          var $backdrop = $('<div/>', { 'class': 'dropdown-backdrop'} );
          $backdrop.insertAfter('.aside-inner').on('click mouseenter', function () {
            removeFloatingNav();
          });
        }

        // Open the collapse sidebar submenu items when on touch devices 
        // - desktop only opens on hover
        function toggleTouchItem($element){
          $element
            .siblings('li')
            .removeClass('open')
            .end()
            .toggleClass('open');
        }

        // Handles hover to open items under collapsed menu
        // ----------------------------------- 
        function toggleMenuItem($listItem, $sidebar) {

          removeFloatingNav();

          var ul = $listItem.children('ul');
          
          if( !ul.length ) return $();
          if( $listItem.hasClass('open') ) {
            toggleTouchItem($listItem);
            return $();
          }

          var $aside = $('.aside');
          var $asideInner = $('.aside-inner'); // for top offset calculation
          // float aside uses extra padding on aside
          var mar = parseInt( $asideInner.css('padding-top'), 0) + parseInt( $aside.css('padding-top'), 0);
          var subNav = ul.clone().appendTo( $aside );
          
          toggleTouchItem($listItem);

          var itemTop = ($listItem.position().top + mar) - $sidebar.scrollTop();
          var vwHeight = $win.height();

          subNav
            .addClass('nav-floating')
            .css({
              position: $rootScope.app.layout.isFixed ? 'fixed' : 'absolute',
              top:      itemTop,
              bottom:   (subNav.outerHeight(true) + itemTop > vwHeight) ? 0 : 'auto'
            });

          subNav.on('mouseleave', function() {
            toggleTouchItem($listItem);
            subNav.remove();
          });

          return subNav;
        }

        function removeFloatingNav() {
          $('.dropdown-backdrop').remove();
          $('.sidebar-subnav.nav-floating').remove();
          $('.sidebar li.open').removeClass('open');
        }
    }


})();


(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .controller('UserBlockController', UserBlockController);

    UserBlockController.$inject = ['$scope','$rootScope', '$modal'];
    function UserBlockController($scope,$rootScope,$modal) {

        activate();

        ////////////////

        function activate() {
          $rootScope.user = {
            name:     'John',
            job:      'ng-developer',
            picture:  'app/img/user/02.jpg'
          };

          // Hides/show user avatar on sidebar
          $rootScope.toggleUserBlock = function(){
            $rootScope.$broadcast('toggleUserBlock');
          };

          $rootScope.userBlockVisible = true;
          
          $rootScope.$on('toggleUserBlock', function(/*event, args*/) {

            $rootScope.userBlockVisible = ! $rootScope.userBlockVisible;
            
          });
        }
      $rootScope.changeImg = function(){
        var modalInstance = $modal.open({
          templateUrl: 'app/views/partials/user-photo.html',
          controller: 'userPhotoController',
          size: 'lg',
          resolve: {
            parameters: function () { //return parameter; 
            }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
      
      
      $rootScope.myImage='';
    $rootScope.myCroppedImage='';
      
    $rootScope.setEditing=function(varEditing) {
      $scope.editing =varEditing;
    };
      
     $rootScope.handleFileSelect=function() {
    
      var file=event.target.files[0];
      var reader = new FileReader();
      reader.onload = function (event) {
        $rootScope.$apply(function($scope){
          debugger
          $scope.myImage=event.target.result;
        
          
        });
      };
         $scope.editing = true;
      reader.readAsDataURL(file);
    };
      
    }
})();

(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .controller('userPhotoController', userPhotoController);

  userPhotoController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'parameters'];


  function userPhotoController($scope, $filter, $state, $modalInstance, parameters) {
    $scope.myImage='';
    $scope.myCroppedImage='';

     $scope.handleFileSelect=function() {
      debugger;
       alert(event.target.files[0].name);
      var file=event.target.files[0];
      var reader = new FileReader();
      reader.onload = function (event) {
        $scope.$apply(function($scope){
          debugger;
          $scope.myImage=event.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
    
//    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
  

    
  }
})();
/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .service('sidebarHttp', sidebarHttp);

  sidebarHttp.$inject = ['$resource', 'END_POINT'];


  function sidebarHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'getMenu' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Administracion.svc/menu'
      }
    };

    return $resource(END_POINT + '/Administracion.svc/autmenuhUser', paramDefault, actions);
  }

})();
(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .controller('TopnavbarController', TopnavbarController);

  TopnavbarController.$inject = ['$scope', '$state', 'tokenManager', 'ngDialog'];
  function TopnavbarController($scope, $state, tokenManager, ngDialog) {

    $scope.logout = function() {

      ngDialog.openConfirm({
        template: 'app/views/templates/logout_confirmation.html',
        className: 'ngdialog-theme-default',
        scope: $scope
      }).then(function (value) {
        tokenManager.resetToken();
        $state.go('page.login');
      });        
    }

  }

})();
/**=========================================================
 * Module: app.tarea.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tarea')
    .service('tareaHttp', tareaHttp);

  tareaHttp.$inject = ['$resource', 'END_POINT'];


  function tareaHttp($resource, END_POINT) {

    var paramDefault = {
      tipoTarea : '@tipoTarea'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true, 
        'params': {
            tipoTarea:'@tipoTarea'
        },
        'url' : END_POINT + '/General.svc/tarea/:tipoTarea'          
      },     
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/tarea'
      }        
    };
    return $resource( END_POINT + '/General.svc/tarea', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.tarea.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tarea')
    .controller('tareaListController', tareaListController);

  tareaListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'tareaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function tareaListController($scope, $filter,$state,$modal, ngDialog, tpl, tareaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
         
    $scope.tareas = {
      model : {        
        tarea : 0    
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },     
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombretareas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.tareas.selectedAll = !$scope.tareas.selectedAll; 
        for (var key in $scope.tareas.selectedItems) {
          $scope.tareas.selectedItems[key].check = $scope.tareas.selectedAll;
        }
      },      
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.tareas.filterText,
          "precision": false
        }];
        $scope.tareas.selectedItems = $filter('arrayFilter')($scope.tareas.dataSource, paramFilter);
        $scope.tareas.paginations.totalItems = $scope.tareas.selectedItems.length;
        $scope.tareas.paginations.currentPage = 1;
        $scope.tareas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.tareas.paginations.currentPage == 1 ) ? 0 : ($scope.tareas.paginations.currentPage * $scope.tareas.paginations.itemsPerPage) - $scope.tareas.paginations.itemsPerPage;
        $scope.tareas.data = $scope.tareas.selectedItems.slice(firstItem , $scope.tareas.paginations.currentPage * $scope.tareas.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.tareas.data = [];
        $scope.tareas.loading = true;
        $scope.tareas.noData = false;
          
          
        tareaHttp.getList({}, {tipoTarea: $scope.filtrotarea.current.value},function(response) {
          $scope.tareas.selectedItems = response;
          $scope.tareas.dataSource = response;
          for(var i=0; i<$scope.tareas.dataSource.length; i++){
            $scope.tareas.nombretareas.push({id: i, nombre: $scope.tareas.dataSource[i]});
          }
          $scope.tareas.paginations.totalItems = $scope.tareas.selectedItems.length;
          $scope.tareas.paginations.currentPage = 1;
          $scope.tareas.changePage();
          $scope.tareas.loading = false;
          ($scope.tareas.dataSource.length < 1) ? $scope.tareas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      },edit : function(item) {
          
          var modalInstance = $modal.open({
                                      templateUrl: 'app/views/agenda/agendaCalendario_form.html',
                                      controller: 'agendaCalendarioController',
                                      size: 'lg',
                                      resolve: {
                                        parameters: { id : item.id,referencia:item.referencia}
                                      }
                                    });
                                    modalInstance.result.then(function (parameters) {
                                    });
          
      }
    }  
        
    //FILTRO TAREA
    $scope.filtrotarea = {             
        current : {}, data:[],
        getData : function() {            
            $scope.filtrotarea.data.push({value:'1', descripcion: 'Todos'});
            $scope.filtrotarea.data.push({value:'2', descripcion: 'Mes'});
            $scope.filtrotarea.data.push({value:'3', descripcion: 'Día'});
            $scope.filtrotarea.current =$scope.filtrotarea.data[2];
            $scope.tareas.getData();
            
        }          
    }
   // $scope.filtrotarea.getData();
	//CARGAMOS DATA
            
   $scope.filtrotarea.getData();
        
	}
  
  
  })();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('consultaTerceroController', consultaTerceroController);

  consultaTerceroController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'terceroHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION'];


  function consultaTerceroController($scope, $filter, $state, $modalInstance, LDataSource, terceroHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION) {      
    
      
      $scope.consultaTercero = {     
          model:{
                  id: 0,
                  identificacion: '', 
                  nombre: '', 
                  telefono: '',
                  direccion: '',
                  correo: '',
                  tipoId: 0,
                  estado: '',			  
                  fechaAct: '',
                  usuarioAct: '',
                  fechaReg: '',
                  usuarioReg: ''
          },	
          fechaReg : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.tercero.fechaReg.isOpen = false;
            }
          },   
            fechaAct : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.tercero.fechaAct.isOpen = false;
            }
          },
         save:  function(){                    
             
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.participanteContrato.model.funcionarioId = $scope.funcionarios.current.id;}
          
                if(!$scope.cargo.current){message.show("warning", "Cargo requerido");return;}
                else{$scope.participanteContrato.model.cargoId= $scope.cargo.current.codigo;}	

             var _http = new terceroHttp($scope.participanteContrato.model);
             if(  $scope.participanteContrato.model.id==0){
                 
                  _http.$addParticipanteContrato(function(response){                  
                      message.show("success", "Participante creado satisfactoriamente!!");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editParticipanteContrato(function(response){                  
                      message.show("success", "Participante actualizado satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
              $modalInstance.close();
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      
      
    
      //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'D', descripcion: 'Eliminado'});   
        }        
    }
    //CONTACTO
    $scope.contactoTercero = {
    model : {  
            id: 0,
            nombre: '',
            telefono: '',
			ext: '',
			email: ''
		},
      current : {},
      data:[],
      getData : function() {        
              
            
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              terceroHttp.getContactosTercero({}, { terceroId: parameters.id }, function(response) {
                  $scope.contactoTercero.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
          
      },
       add : function() {          
           
           
        var parameter = {
              contactoTercero : {              
                      id: 0,
                      terceroId:$scope.tercero.model.id,
                      nombre: '',
                      telefono: '',
                      ext: '',
                      email: ''
              }            
        };

        var modalInstance = $modal.open({
          templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {            
            $scope.contactoTercero.getData();
        });
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          terceroHttp.deleteContactoTercero({}, {contactoTerceroId: item.id}, function(response){
            $scope.contactoTercero.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
        var parameter = {
          contactoTercero : item
        };
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {            
            $scope.contactoTercero.getData();
        });
      }
    }
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	
    $scope.modoEdicion=true;
    $scope.modoActualizacion=false;
      
    //DEFINIMOS TITULO 
    if( parameters.referencia.toString()=="CLIENTE")
    {
        $scope.destino="Cliente";            
    }
      
      terceroHttp.read({},{ id : parameters.id}, function (data) { 
            $scope.consultaTercero.model = data;			
             
			if($scope.consultaTercero.model.fechaReg){$scope.consultaTercero.fechaReg.value=new Date(parseFloat($scope.consultaTercero.model.fechaReg));}    
			if($scope.consultaTercero.model.fechaAct){$scope.consultaTercero.fechaAct.value=new Date(parseFloat($scope.consultaTercero.model.fechaAct));}   
            
			       
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.consultaTercero.model.estado})[0];            
			$scope.contactoTercero.getData();
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });  
      
    
      
  }
})();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('contactoTerceroEditController', contactoTerceroEditController);

  contactoTerceroEditController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'terceroHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function contactoTerceroEditController($scope, $filter, $state, $modalInstance, LDataSource, terceroHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
      
      
      
      $scope.contactoTercero = {     
          model:{
              id: 0,
              terceroId: 0,
              nombre: '',
              telefono: '',
              ext: '',
              email: '',
              envioInf: ''
          },         
          
         save:  function(){            
             
             
                if($scope.contactoTercero.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
                if(!$scope.envioInf.current){message.show("warning", "Envio información requerido");return;}
                else{$scope.contactoTercero.model.envioInf= $scope.envioInf.current.value;}	
             
             var _http = new terceroHttp($scope.contactoTercero.model);
             if(  $scope.contactoTercero.model.id==0){
                 
                  _http.$addContactoTercero(function(response){                  
                      message.show("success", "Contacto creado satisfactoriamente!!");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editContactoTercero(function(response){                  
                      message.show("success", "Contacto actualizado satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
              $modalInstance.close();
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      
       //ESTADOS
        $scope.envioInf = {
            current : {}, data:[],
            getData : function() {
                $scope.envioInf.data.push({value:'NA', descripcion: 'No Autorizado'}); 
                $scope.envioInf.data.push({value:'A', descripcion: 'Autorizado'});
            }        
        }	
	   //CARGAMOS LOS LISTADOS	
	   $scope.envioInf.getData();	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      $scope.contactoTercero.model=parameters.contactoTercero;
      $scope.envioInf.current = $filter('filter')($scope.envioInf.data, {value :  $scope.contactoTercero.envioInf})[0];
  }
})();
/**=========================================================
 * Module: app.tercero.js
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.tercero')
    .controller('terceroController', terceroController);

  terceroController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', '$filter', 'terceroHttp', 'parameters', 'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window', '$modal'];

  function terceroController($scope, $rootScope, $state, $modalInstance, $filter, terceroHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window, $modal) {

    var tercero = parameters.tercero;
    var currentDate = new Date();

    $scope.tercero = {
      model: {
        id: 0,
        identificacion: '',
        nombre: '',
        telefono: '',
        direccion: '',
        correo: '',
        tipo: 0,
        moneda: '',
        sexo: '',
        tipoEmpresa: '',
        clasificacion: '',
        estado: '',
        fechaAct: '',
        usuarioAct: '',
        fechaReg: '',
        usuarioReg: '',
        comision: ''
      },
      fechaReg: {
        isOpen: false,
        value: '',
        dateOptions: {
          formatYear: 'yy',
          startingDay: 1
        },
        open: function ($event) {
          $scope.tercero.fechaReg.isOpen = false;
        }
      },
      fechaAct: {
        isOpen: false,
        value: '',
        dateOptions: {
          formatYear: 'yy',
          startingDay: 1
        },
        open: function ($event) {
          $scope.tercero.fechaAct.isOpen = false;
        }
      },
      back: function () {
        parametersOfState.set({ name: 'app.tercero', params: { filters: { estadoSeleccionado: tercero.estado }, data: [] } });
        $state.go('app.tercero');
      },
      save: function () {

        if ($scope.tercero.model.nombre == "") { message.show("warning", "Nombre requerido"); return; }
        if ($scope.tercero.model.identificacion == "") { message.show("warning", "Identificación requerido"); return; }
        if ($scope.tercero.model.direccion == "") { message.show("warning", "Dirección requerido"); return; }

        try {
          $scope.tercero.model.estado = $scope.estados.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.sexo = $scope.sexos.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.clasificacion = $scope.clasificaciones.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.moneda = $scope.monedas.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.tipoEmpresa = $scope.tipoEmpresas.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.tipo = $scope.tipos.current.value;
        }
        catch (err) {
        }
        //INSERTAR
        if ($scope.tercero.model.id == 0) {

          $rootScope.loadingVisible = true;
          terceroHttp.save({}, $scope.tercero.model, function (data) {

            terceroHttp.read({}, { id: data.id }, function (data) {
              $scope.tercero.model = data;
              message.show("success", "Cliente creado satisfactoriamente!!");
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });

          }, function (faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });

        }
        //ACTUALIZAR
        else {
          if (!$scope.tercero.model.id) {
            $state.go('app.tercero');
          } else {

            $rootScope.loadingVisible = true;
            terceroHttp.update({}, $scope.tercero.model, function (data) {
              $rootScope.loadingVisible = false;
              $scope.tercero.model = data;

              message.show("success", "Cliente actualizado satisfactoriamente");


            }, function (faild) {
              message.show("error", faild.Message);
              $rootScope.loadingVisible = false;
            });

          }
        }
      },
      uploadFile: function () {
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: {
              id: $scope.tercero.model.id,
              referencia: 'TERCERO'
            }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
      seguimiento: function () {
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: {
              id: $scope.tercero.model.id,
              referencia: 'TERCERO'
            }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
      close: function () {
        $modalInstance.dismiss('cancel');
      }
    }

    $scope.contactoTercero = {

      model: {
        id: 0,
        nombre: '',
        telefono: '',
        ext: '',
        email: '',
        terceroId: $scope.tercero.model.id
      },
      current: {

        id: 0,
        nombre: '',
        telefono: '',
        ext: '',
        email: '',
        comision: false,
        cotizacion: false,
        activo: false,
        terceroId: $scope.tercero.model.id
      },
      data: [],
      getData: function () {
        if (tercero) {

          if ($scope.tercero.model.id != 0) {
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
            terceroHttp.getContactosTercero({}, { terceroId: $scope.tercero.model.id }, function (response) {
              $scope.contactoTercero.data = $filter('orderBy')(response, 'nombre');
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });

          }
        }
      },
      save: function () {

        //INSERTAR
        if ($scope.tercero.model.id == 0) { message.show("warning", "Debe guardar primero el cliente"); return; }
        if ($scope.contactoTercero.current.nombre == "") { message.show("warning", "Debe especificar el nombre"); return; }
        if ($scope.contactoTercero.current.email == "") { message.show("warning", "Debe especificar el email"); return; }

        $rootScope.loadingVisible = true;
        $scope.contactoTercero.current.terceroId = $scope.tercero.model.id;
        if ($scope.contactoTercero.current.id == 0) {

          terceroHttp.addContactoTercero({}, $scope.contactoTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.contactoTercero.current = {
              id: 0,
              nombre: '',
              telefono: '',
              ext: '',
              email: '',
              comision: false,
              cotizacion: false,
              activo: false,
              terceroId: $scope.tercero.model.id
            };

            $scope.contactoTercero.getData();
            message.show("success", "Contacto actualizado satisfactoriamente");


          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
        else if ($scope.contactoTercero.current.id > 0) {

          terceroHttp.editContactoTercero({}, $scope.contactoTercero.current, function (data) {
            $rootScope.loadingVisible = false;
            $scope.contactoTercero.current = {
              id: 0,
              nombre: '',
              telefono: '',
              ext: '',
              email: '',
              comision: false,
              cotizacion: false,
              activo: false,
              terceroId: $scope.tercero.model.id
            };
            $scope.contactoTercero.getData();
            message.show("success", "Contacto actualizado satisfactoriamente");
          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
      },
      delete: function (item) {

        $rootScope.loadingVisible = true;
        terceroHttp.deleteContactoTercero({}, { contactoTerceroId: item.id }, function (response) {
          $scope.contactoTercero.getData();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", faild.Message);
        });
      },
      edit: function (item) {
        $scope.contactoTercero.current = item;
      },
      cancel: function (item) {
        $scope.contactoTercero.current = {};
      }
    }

    $scope.cuentaTercero = {

      model: {
        id: 0,
        banco: 1,
        tipoCuenta: 'AHORROS',
        numero: '',
        cedula: '',
        descripcion: '',
        activo: false,
        terceroId: $scope.tercero.model.id
      },
      current: {

        id: 0,
        banco: 1,
        tipoCuenta: 'AHORROS',
        numero: '',
        cedula: '',
        descripcion: '',
        activo: false,
        terceroId: $scope.tercero.model.id
      },
      data: [],
      getData: function () {
        if (tercero) {

          if ($scope.tercero.model.id != 0) {
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
            terceroHttp.getCuentasTercero({}, { terceroId: $scope.tercero.model.id }, function (response) {
              $scope.cuentaTercero.data = $filter('orderBy')(response, 'descripcion');
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });

          }
        }
      },
      save: function () {

        //INSERTAR
        if ($scope.tercero.model.id == 0) { message.show("warning", "Debe guardar primero el cliente"); return; }
        if ($scope.cuentaTercero.current.numero == "") { message.show("warning", "Debe especificar el número"); return; }
        if ($scope.cuentaTercero.current.descripcion == "") { message.show("warning", "Debe especificar la descripción"); return; }

        $scope.cuentaTercero.current.banco = $scope.bancos.current.value;
        $scope.cuentaTercero.current.tipoCuenta = $scope.tiposCuenta.current.value;

        $rootScope.loadingVisible = true;
        $scope.cuentaTercero.current.terceroId = $scope.tercero.model.id;
        if ($scope.cuentaTercero.current.id == 0) {

          terceroHttp.addCuentaTercero({}, $scope.cuentaTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.cuentaTercero.current = {
              id: 0,
              banco: 1,
              tipoCuenta: 'AHORROS',
              numero: '',
              cedula: '',
              descripcion: '',
              activo: false,
              terceroId: $scope.tercero.model.id
            };

            $scope.cuentaTercero.getData();

            message.show("success", "Cuenta actualizada satisfactoriamente");


          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });

        }
        else if ($scope.cuentaTercero.current.id > 0) {

          terceroHttp.editCuentaTercero({}, $scope.cuentaTercero.current, function (data) {
            $rootScope.loadingVisible = false;
            $scope.cuentaTercero.current = {
              id: 0,
              banco: 1,
              tipoCuenta: 'AHORROS',
              numero: '',
              cedula: '',
              descripcion: '',
              activo: false,
              terceroId: $scope.tercero.model.id
            };

            $scope.cuentaTercero.getData();

            message.show("success", "Cuenta actualizada satisfactoriamente");
          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
      },
      delete: function (item) {

        $rootScope.loadingVisible = true;
        terceroHttp.deleteCuentaTercero({}, { cuentaTerceroId: item.id }, function (response) {

          $scope.cuentaTercero.getData();
          $rootScope.loadingVisible = false;

        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", faild.Message);
        });
      },
      edit: function (item) {
        $scope.cuentaTercero.current = item;
        $scope.bancos.current = $filter('filter')($scope.bancos.data, { value: $scope.cuentaTercero.current.banco })[0];
        $scope.tiposCuenta.current = $filter('filter')($scope.tiposCuenta.data, { value: $scope.cuentaTercero.current.tipoCuenta })[0];

      },
      cancel: function (item) {
        $scope.cuentaTercero.current = {};
      }
    }
    //REFERIDOS
    $scope.referidoTercero = {

      current: {
        id: 0,
        identificacion: '',
        terceroId: 0,
        tercero: '',
        movil: '',
        direccion: '',
        email: '',
        porcentaje: 0,
        intermediarioId: $scope.tercero.model.id
      },
      data: [],
      getData: function () {
        if (tercero) {

          if ($scope.tercero.model.id != 0) {
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
            terceroHttp.getReferidosTercero({}, { terceroId: $scope.tercero.model.id }, function (response) {
              $scope.referidoTercero.data = $filter('orderBy')(response, 'nombre');
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });
          }
        }
      }, buscar: function () {

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion: $scope.referidoTercero.current }
          }
        });
        modalInstance.result.then(function (parameters) {
        });

      },
      save: function () {

        //INSERTAR                    
        if ($scope.referidoTercero.current.tercero == "") { message.show("warning", "Debe especificar el intermediario"); return; }
        if ($scope.referidoTercero.current.porcentaje == 0) { message.show("warning", "Debe especificar el porcentaje"); return; }

        $rootScope.loadingVisible = true;
        $scope.referidoTercero.current.intermediarioId = $scope.tercero.model.id;
        if ($scope.referidoTercero.current.id == 0) {

          terceroHttp.addReferidoTercero({}, $scope.referidoTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.referidoTercero.current = {

              id: 0,
              identificacion: '',
              terceroId: 0,
              tercero: '',
              movil: '',
              direccion: '',
              email: '',
              porcentaje: 0,
              intermediarioId: $scope.tercero.model.id

            };
            $scope.referidoTercero.getData();
            message.show("success", "Referido actualizado satisfactoriamente");

          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
        else if ($scope.referidoTercero.current.id > 0) {

          terceroHttp.editReferidoTercero({}, $scope.referidoTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.referidoTercero.current = {
              id: 0,
              identificacion: '',
              terceroId: 0,
              tercero: '',
              movil: '',
              direccion: '',
              email: '',
              porcentaje: 0,
              intermediarioId: $scope.tercero.model.id
            };
            $scope.referidoTercero.getData();
            message.show("success", "Referido actualizado satisfactoriamente");

          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }

      },
      delete: function (item) {

        $rootScope.loadingVisible = true;
        terceroHttp.deleteReferidoTercero({}, { referidoTerceroId: item.id }, function (response) {
          $scope.referidoTercero.getData();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", faild.Message);
        });
      },
      edit: function (item) {
        $scope.referidoTercero.current = item;
      },
      cancel: function (item) {
        $scope.referidoTercero.current = {

          id: 0,
          identificacion: '',
          terceroId: 0,
          tercero: '',
          movil: '',
          direccion: '',
          email: '',
          porcentaje: 0,
          intermediarioId: $scope.tercero.model.id

        };
      }
    }

    //ASEGURADORA
    $scope.aseguradora = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        terceroHttp.getAseguradora({}, {}, function(response) {
            $scope.aseguradora.data = $filter('orderBy')(response, 'idRow');
            $scope.aseguradora.current = $scope.aseguradora.data[0];
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      set: function(){
        
      }
    }

    $scope.comisionTercero = {

      current: {
        id: '',
        nombreAseguradora: $scope.aseguradora.current.tercero,
        comision: 0,
        idAseguradora:$scope.aseguradora.current.idRow,
        idEmpresa: $scope.tercero.model.id
      },
      data: [],
      getData: function () {
        if (tercero) {

          if ($scope.tercero.model.id != 0) {
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
            debugger;
            terceroHttp.getComisionesTercero({}, { terceroId: $scope.tercero.model.id }, function (response) {
              $scope.comisionTercero.data = $filter('orderBy')(response, 'nombreAseguradora');
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });
          }
        }
      },
      save: function () {

        //INSERTAR                    
         if ($scope.comisionTercero.current.comision == 0) { message.show("warning", "Debe especificar la comision"); return; }

        $rootScope.loadingVisible = true;
    
        $scope.comisionTercero.current.idEmpresa = $scope.tercero.model.id;
        $scope.comisionTercero.current.idAseguradora= $scope.aseguradora.current.idRow;
        $scope.comisionTercero.current.nombreAseguradora= $scope.aseguradora.current.tercero;

        if ($scope.comisionTercero.current.id == '') {

          terceroHttp.addComisionTercero({}, $scope.comisionTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.comisionTercero.current= {
              id: '',
              nombreAseguradora: $scope.aseguradora.current.tercero,
              comision: 0,
              idAseguradora:$scope.aseguradora.current.idRow,
              idEmpresa: $scope.tercero.model.id
            };
            $scope.comisionTercero.getData();
            message.show("success", "Comision actualizada satisfactoriamente");

          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
        else {

          terceroHttp.editComisionTercero({}, $scope.comisionTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            
            $scope.comisionTercero.current = {
              id: '',
              nombreAseguradora: $scope.aseguradora.current.tercero,
              comision: 0,
              idAseguradora:$scope.aseguradora.current.idRow,
              idEmpresa: $scope.tercero.model.id
            };

            $scope.comisionTercero.getData();
            message.show("success", "Comision actualizado satisfactoriamente");

          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }

      },
      delete: function (item) {

        $rootScope.loadingVisible = true;
        terceroHttp.deleteComisionTercero({}, { comisionTerceroId: item.id }, function (response) {
          $scope.comisionTercero.getData();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", faild.Message);
        });
      },
      edit: function (item) {
        $scope.comisionTercero.current = item;
        $scope.aseguradora.current = $filter('filter')($scope.aseguradora.data, { idRow : $scope.comisionTercero.current.idAseguradora })[0];
      },
      cancel: function (item) {

        $scope.aseguradora.current = $scope.aseguradora.data[0];

        $scope.comisionTercero.current = {
          id: '',
          nombreAseguradora: $scope.aseguradora.current.tercero,
          comision: 0,
          idAseguradora:$scope.aseguradora.current.idRow,
          idEmpresa: $scope.tercero.model.id
        };
        
      }
    }
    //TAB
    $scope.showTabs = function () {
      if ($scope.tercero.id == 0)
        return false;
      else
        return true;
    }
    //ESTADOS
    $scope.estados = {
      current: {}, data: [],
      getData: function () {
        $scope.estados.data.push({ value: '0', descripcion: 'Inactivo' });
        $scope.estados.data.push({ value: '1', descripcion: 'Activo' });
        $scope.estados.data.push({ value: '2', descripcion: 'Eliminado' });
      }
    }
    //SEXOS
    $scope.sexos = {
      current: {}, data: [],
      getData: function () {
        $scope.sexos.data.push({ value: 'M', descripcion: 'Masculino' });
        $scope.sexos.data.push({ value: 'F', descripcion: 'Femenino' });
        $scope.sexos.data.push({ value: 'E', descripcion: 'Empresa' });
      }
    }
    //CONTRIBUYENTES
    $scope.clasificaciones = {
      current: {}, data: [],
      getData: function () {
        $scope.clasificaciones.data.push({ value: '1', descripcion: 'Gran Contribuyente' });
        $scope.clasificaciones.data.push({ value: '2', descripcion: 'Empresa del Estado' });
        $scope.clasificaciones.data.push({ value: '3', descripcion: 'Régimen Común' });
        $scope.clasificaciones.data.push({ value: '4', descripcion: 'Régimen Simplificado' });
        $scope.clasificaciones.data.push({ value: '5', descripcion: 'Reg/Sim no residente' });
        $scope.clasificaciones.data.push({ value: '6', descripcion: 'No residente en país' });
        $scope.clasificaciones.data.push({ value: '7', descripcion: 'No responsable IVA' });

      }
    }
    //MONEDAS
    $scope.monedas = {
      current: {}, data: [],
      getData: function () {
        $scope.monedas.data.push({ value: '2', descripcion: 'Dolar' });
        $scope.monedas.data.push({ value: '4', descripcion: 'Pesos Colombianos' });
      }
    }
    //TIPO EMPRESAS
    $scope.tipoEmpresas = {
      current: {}, data: [],
      getData: function () {
        $scope.tipoEmpresas.data.push({ value: '3', descripcion: 'Venta Directa' });
        $scope.tipoEmpresas.data.push({ value: '5', descripcion: 'Corredores de Seguros' });
        $scope.tipoEmpresas.data.push({ value: '7', descripcion: 'Venta Web' });
        $scope.tipoEmpresas.data.push({ value: '8', descripcion: 'Intermediarios' });
        $scope.tipoEmpresas.data.push({ value: '9', descripcion: 'Cliente Intermediario' });
      }
    }
    //TIPO
    $scope.tipos = {
      current: {}, data: [],
      getData: function () {
        $scope.tipos.data.push({ value: 'N', descripcion: 'Natural' });
        $scope.tipos.data.push({ value: 'J', descripcion: 'Jurídica' });
      }
    }
    //BANCOS
    $scope.bancos = {
      current: {}, data: [],
      getData: function () {
        $scope.bancos.data.push({ value: '1', descripcion: 'BANCO DE BOGOTA' });
        $scope.bancos.data.push({ value: '2', descripcion: 'BANCOLOMBIA' });
        $scope.bancos.data.push({ value: '3', descripcion: 'CITI BANK' });
        $scope.bancos.data.push({ value: '4', descripcion: 'BANCO COLPATRIA' });
        $scope.bancos.data.push({ value: '5', descripcion: 'DAVIVIENDA' });
        $scope.bancos.data.push({ value: '6', descripcion: 'BANCO CAJA SOCIAL' });
        $scope.bancos.data.push({ value: '7', descripcion: 'BANCO AV VILLAS' });
        $scope.bancos.data.push({ value: '8', descripcion: 'BANCO HELM BANCO' });
        $scope.bancos.data.push({ value: '9', descripcion: 'HELM BANK' });
        $scope.bancos.data.push({ value: '10', descripcion: 'BANCO OCCIDENTE ' });
        $scope.bancos.data.push({ value: '11', descripcion: 'SUDAMERIS' });
        $scope.bancos.data.push({ value: '12', descripcion: 'BBVA' });
        $scope.bancos.data.push({ value: '13', descripcion: 'BANCOOMEVA' });
        $scope.bancos.data.push({ value: '14', descripcion: 'CORPBANCA' });
      }
    }
    //TIPO CUENTA
    $scope.tiposCuenta = {
      current: {}, data: [],
      getData: function () {
        $scope.tiposCuenta.data.push({ value: 'AHORROS', descripcion: 'AHORROS' });
        $scope.tiposCuenta.data.push({ value: 'CORRIENTE', descripcion: 'CORRIENTE' });
      }
    }

    

    //CARGAMOS LOS LISTADOS	
    $scope.estados.getData();
    $scope.sexos.getData();
    $scope.clasificaciones.getData();
    $scope.monedas.getData();
    $scope.tipoEmpresas.getData();
    $scope.tipos.getData();
    $scope.tiposCuenta.getData();
    $scope.bancos.getData();
    $scope.aseguradora.getData();

    //LIMPIAMOS EL TERCERO

    if (tercero.id == 0) {

      $scope.estados.current = $scope.estados.data[0];
      $scope.sexos.current = $scope.sexos.data[0];
      $scope.clasificaciones.current = $scope.clasificaciones.data[0];
      $scope.monedas.current = $scope.monedas.data[0];
      $scope.tipoEmpresas.current = $scope.tipoEmpresas.data[0];
      $scope.tipos.current = $scope.tipos.data[0];
      $scope.contactoTercero.data = [];
      $scope.referidoTercero.data = [];
      $scope.comisionTercero.data = [];

    }
    //CARGAMOS EL TERCERO
    else {
      $rootScope.loadingVisible = true;
      $scope.divContactoTercero = true;
      terceroHttp.read({}, { id: tercero.id }, function (data) {

        $scope.tercero.model = data;
        if ($scope.tercero.model.fechaReg) { $scope.tercero.fechaReg.value = new Date(parseFloat($scope.tercero.model.fechaReg)); }
        if ($scope.tercero.model.fechaAct) { $scope.tercero.fechaAct.value = new Date(parseFloat($scope.tercero.model.fechaAct)); }



        $scope.estados.current = $filter('filter')($scope.estados.data, { value: $scope.tercero.model.estado })[0];
        $scope.sexos.current = $filter('filter')($scope.sexos.data, { value: $scope.tercero.model.sexo })[0];
        $scope.clasificaciones.current = $filter('filter')($scope.clasificaciones.data, { value: $scope.tercero.model.clasificacion })[0];
        $scope.monedas.current = $filter('filter')($scope.monedas.data, { value: $scope.tercero.model.moneda })[0];
        $scope.tipoEmpresas.current = $filter('filter')($scope.tipoEmpresas.data, {
          value:
            $scope.tercero.model.tipoEmpresa
        })[0];
        $scope.tipos.current = $filter('filter')($scope.tipos.data, { value: $scope.tercero.model.tipo })[0];

        $scope.contactoTercero.getData();
        $scope.referidoTercero.getData();
        $scope.comisionTercero.getData();
        $scope.cuentaTercero.getData();

        $rootScope.loadingVisible = false;
      }, function (faild) {
        $rootScope.loadingVisible = false;
        message.show("error", faild.Message);
      });
    }

  }
})();
/**=========================================================
 * Module: app.tercero.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .service('terceroHttp', terceroHttp);

  terceroHttp.$inject = ['$resource', 'END_POINT'];


  function terceroHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {     
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/tercero/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/tercero/:id'
      },
      'getList' : {
        'method' : 'POST',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/busqueda'          
      },
      'getContactosTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
      'addContactoTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/contacto'
      },
      'editContactoTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/contacto'
      },
      'deleteContactoTercero': {
        'method':'DELETE',
        'params' : {
            contactoTerceroId : '@contactoTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/contacto/:contactoTerceroId'
      },
      
      'getCuentasTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/cuenta'
      },
      'addCuentaTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/cuenta'
      },
      'editCuentaTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/cuenta'
      },
      'deleteCuentaTercero': {
        'method':'DELETE',
        'params' : {
            cuentaTerceroId : '@cuentaTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/cuenta/:cuentaTerceroId'
      },

      'getReferidosTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/referido'
      },
      'addReferidoTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/referido'
      },
      'editReferidoTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/referido'
      },
      'deleteReferidoTercero': {
        'method':'DELETE',
        'params' : {
            referidoTerceroId : '@referidoTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/referido/:referidoTerceroId'
      },
      'getComisionesTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/comision'
      },
      'addComisionTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/comision'
      },
      'editComisionTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/comision'
      },
      'deleteComisionTercero': {
        'method':'DELETE',
        'params' : {
            comisionTerceroId : '@comisionTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/comision/:comisionTerceroId'
      },
      'getAseguradora': {
          'method' : 'GET',
          'isArray' : true,
          'url' : END_POINT + '/Comercial.svc/comision/aseguradora'
      },
      
      




    };
    return $resource( END_POINT + '/Comercial.svc/tercero', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.tercero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('terceroListController', terceroListController);

  terceroListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl','$modal', 'terceroHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function terceroListController($scope, $filter, $state, ngDialog, tpl,$modal, terceroHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	$scope.ESTADO_INACTIVO = '0';
    $scope.ESTADO_ACTIVO = '1';
    $scope.ESTADO_ELIMINADO = '2';
	//$rootScope.loadingVisible = true;


    $scope.terceros = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreterceros:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.terceros.selectedAll = !$scope.terceros.selectedAll; 
        for (var key in $scope.terceros.selectedItems) {
          $scope.terceros.selectedItems[key].check = $scope.terceros.selectedAll;
        }
      },
      add : function() {
          var terc = {
              codigo: 0,
              id: 0,
              identificacion: '', 
              nombre: '', 
              nombreComercial: '', 
			  telefono: '',
			  direccion: '',
			  correo: '',
              tipoId: 0,
			  estado: '',			  
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''			 
          };
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/tercero_form.html',
        controller: 'terceroController',
        size: 'lg',
        resolve: {
            parameters: { tercero: terc }
        }
        });
        modalInstance.result.then(function (parameters) {
        });      
      },
      edit : function(item) {
          
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/tercero_form.html',
        controller: 'terceroController',
        size: 'lg',
        resolve: {
            parameters: { tercero: item }
        }
        });
        modalInstance.result.then(function (parameters) {
        });          
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            terceroHttp.remove({}, { id: id }, function(response) {
                $scope.terceros.getData();
                message.show("success", "Tercero eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.terceros.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    terceroHttp.remove({}, { id: id }, function(response) {
                        $scope.terceros.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.terceros.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.terceros.filterText,
          "precision": false
        }];
        $scope.terceros.selectedItems = $filter('arrayFilter')($scope.terceros.dataSource, paramFilter);
        $scope.terceros.paginations.totalItems = $scope.terceros.selectedItems.length;
        $scope.terceros.paginations.currentPage = 1;
        $scope.terceros.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.terceros.paginations.currentPage == 1 ) ? 0 : ($scope.terceros.paginations.currentPage * $scope.terceros.paginations.itemsPerPage) - $scope.terceros.paginations.itemsPerPage;
        $scope.terceros.data = $scope.terceros.selectedItems.slice(firstItem , $scope.terceros.paginations.currentPage * $scope.terceros.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.terceros.data = [];
        $scope.terceros.loading = true;
        $scope.terceros.noData = false;
          
        var parametros= {
            "estado":$scope.estadoTercero.current.value,
            "tipo":$scope.tipoTercero.current.value,
            "codigo":$scope.busquedaTercero.model.codigo,   
            "razonSocial":$scope.busquedaTercero.model.razonSocial,
            "identificacion":$scope.busquedaTercero.model.identificacion,
            "email":$scope.busquedaTercero.model.email,
            "direccion":$scope.busquedaTercero.model.direccion,
            "telefono":$scope.busquedaTercero.model.telefono,
            "intermediario":$scope.busquedaTercero.model.intermediario            
        };
          
        terceroHttp.getList({}, parametros,function(response) {
          $scope.terceros.selectedItems = response;
          $scope.terceros.dataSource = response;
          for(var i=0; i<$scope.terceros.dataSource.length; i++){
            $scope.terceros.nombreterceros.push({id: i, nombre: $scope.terceros.dataSource[i]});
          }
          $scope.terceros.paginations.totalItems = $scope.terceros.selectedItems.length;
          $scope.terceros.paginations.currentPage = 1;
          $scope.terceros.changePage();
          $scope.terceros.loading = false;
          ($scope.terceros.dataSource.length < 1) ? $scope.terceros.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }       
    //CARGAMOS PROCESOS
    $scope.estadoTercero = {             
          current : {}, data:[],
        getData : function() {
            $scope.estadoTercero.data.push({value:'-1', descripcion: 'Todos'});
            $scope.estadoTercero.data.push({value:'1', descripcion: 'Activos'});
            $scope.estadoTercero.data.push({value:'0', descripcion: 'Inactivos'});
            $scope.estadoTercero.data.push({value:'2', descripcion: 'Eliminados'});
            $scope.estadoTercero.current =$scope.estadoTercero.data[0];
        }          
    }
    //CARGAMOS PROCESOS
    $scope.tipoTercero = {             
          current : {}, data:[],
        getData : function() {
            $scope.tipoTercero.data.push({value:'-1', descripcion: 'Todos'});
            $scope.tipoTercero.data.push({value:'3', descripcion: 'Venta Directa'});
            $scope.tipoTercero.data.push({value:'7', descripcion: 'Venta Web'});
            $scope.tipoTercero.data.push({value:'8', descripcion: 'Intermediarios'});
            $scope.tipoTercero.data.push({value:'9', descripcion: 'Cliente Intermediario'});
            $scope.tipoTercero.current =$scope.tipoTercero.data[0];
        }          
    }   
    
    $scope.busquedaTercero = {
    model : {
          codigo: '',
          razonSocial: '',
          identificacion: '',
          email: '',
          direccion: '',
          telefono: '',
          intermediario:''
            } 
    }
	//CARGAMOS LOS LISTADOS
    $scope.estadoTercero.getData();
    $scope.tipoTercero.getData();
    $scope.terceros.getData();
        
        
        
        
	}
  
  
  })();
(function() {
    'use strict';

    angular
        .module('app.translate')
        .config(translateConfig)
        ;
    translateConfig.$inject = ['$translateProvider'];
    function translateConfig($translateProvider){
  
      $translateProvider.useStaticFilesLoader({
          prefix : 'app/i18n/',
          suffix : '.json'
      });
      $translateProvider.preferredLanguage('es');
      $translateProvider.useLocalStorage();
      $translateProvider.usePostCompiling(true);

    }
})();
(function() {
    'use strict';

    angular
        .module('app.translate')
        .run(translateRun)
        ;
    translateRun.$inject = ['$rootScope', '$translate'];
    
    function translateRun($rootScope, $translate){

      // Internationalization
      // ----------------------

      $rootScope.language = {
        // Handles language dropdown
        listIsOpen: false,
        // list of available languages
        available: {
          'en':       'English',
          'es':    'Español'
        },
        // display always the current ui language
        init: function () {
          var proposedLanguage = $translate.proposedLanguage() || $translate.use();
          var preferredLanguage = $translate.preferredLanguage(); // we know we have set a preferred one in app.config
          $rootScope.language.selected = $rootScope.language.available[ (proposedLanguage || preferredLanguage) ];
        },
        set: function (localeId) {
          // Set the new idiom
          $translate.use(localeId);
          // save a reference for the current language
          $rootScope.language.selected = $rootScope.language.available[localeId];
          // finally toggle dropdown
          $rootScope.language.listIsOpen = ! $rootScope.language.listIsOpen;
        }
      };

      $rootScope.language.init();

    }
})();
/**=========================================================
 * Module: app.trm.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.trm')
    .service('trmHttp', trmHttp);

  trmHttp.$inject = ['$resource', 'END_POINT'];


  function trmHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,          
        'url' : END_POINT + '/General.svc/trm'          
      },     
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/trm'
      }        
    };
    return $resource( END_POINT + '/General.svc/trm', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.trm.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.trm')
    .controller('trmListController', trmListController);

  trmListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'trmHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function trmListController($scope, $filter,$state,$modal, ngDialog, tpl, trmHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        
		
    $scope.trms = {
      model : {        
        trm : 0    
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombretrms:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.trms.selectedAll = !$scope.trms.selectedAll; 
        for (var key in $scope.trms.selectedItems) {
          $scope.trms.selectedItems[key].check = $scope.trms.selectedAll;
        }
      },
      add : function() {
          
          debugger;
        
           if (confirm("Desea actualizar la Trm?")) {
               if($scope.trms.model.trm==""){message.show("warning", "Trm requerida");return;}

                  trmHttp.save({}, $scope.trms.model, function (data) { 	

                    message.show("success", "Trm actualizada satisfactoriamente!!");
                    $rootScope.loadingVisible = false;
                    $scope.trms.getData();

                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", faild.Message);
                });	
           }
          
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.trms.filterText,
          "precision": false
        }];
        $scope.trms.selectedItems = $filter('arrayFilter')($scope.trms.dataSource, paramFilter);
        $scope.trms.paginations.totalItems = $scope.trms.selectedItems.length;
        $scope.trms.paginations.currentPage = 1;
        $scope.trms.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.trms.paginations.currentPage == 1 ) ? 0 : ($scope.trms.paginations.currentPage * $scope.trms.paginations.itemsPerPage) - $scope.trms.paginations.itemsPerPage;
        $scope.trms.data = $scope.trms.selectedItems.slice(firstItem , $scope.trms.paginations.currentPage * $scope.trms.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.trms.data = [];
        $scope.trms.loading = true;
        $scope.trms.noData = false;        
          
        trmHttp.getList({}, {},function(response) {
          $scope.trms.selectedItems = response;
          $scope.trms.dataSource = response;
          for(var i=0; i<$scope.trms.dataSource.length; i++){
            $scope.trms.nombretrms.push({id: i, nombre: $scope.trms.dataSource[i]});
          }
          $scope.trms.paginations.totalItems = $scope.trms.selectedItems.length;
          $scope.trms.paginations.currentPage = 1;
          $scope.trms.changePage();
          $scope.trms.loading = false;
          ($scope.trms.dataSource.length < 1) ? $scope.trms.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }   
	//CARGAMOS DATA
    $scope.trms.getData();         
   
        
	}
  
  
  })();
/**=========================================================
 * Module: modals.js
 * Provides a simple way to implement bootstrap modals from templates
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.usuario')
        .controller('ModalGmapController', ModalGmapController);

    ModalGmapController.$inject =     
    ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'usuarioHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION','$timeout'];
      
    function ModalGmapController($scope, $filter, $state, $modalInstance, LDataSource, usuarioHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION,$timeout) { 
        
        var  objUbi = parameters.objUbicacion;
        var registroUbicacion = parameters.registroUbicacion;   
        var vm = this;
        
        
        
        function activate(longitude ,latitude) {     
            
                 
            var position = new google.maps.LatLng( latitude,longitude);
            
                $scope.mapOptionsModal = {
                    zoom: 16,
                    center:position,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                  };
            
              $timeout(function(){
                // 1. Add a marker at the position it was initialized
                new google.maps.Marker({
                  map: $scope.myMapModal,
                  position: position
                });
                // 2. Trigger a resize so the map is redrawed
                google.maps.event.trigger($scope.myMapModal, 'resize');
                // 3. Move to the center if it is misaligned
                $scope.myMapModal.panTo(position);
              });          
        }

        $scope.registroUbicacionModal={               
                     
            model : {
                id:0,
                contratoId:0,
                funcionarioId:0,
                fecha: '',
                tipo: '',
                comentario: '',
                longitude : 0,
                latitude : 0,
                accuracy : 0
		},             
            
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          save : function() {
                             
                if($scope.tipos.current.value==''){message.show("warning", "Tipo registro requerido");return;}
                else{$scope.registroUbicacionModal.model.tipo= $scope.tipos.current.value;}   
               
                    
                $rootScope.loadingVisible = true;
				usuarioHttp.addRegistroUbicacion({}, $scope.registroUbicacionModal.model, function (data) { 
					
						usuarioHttp.getRegistroUbicacion({},{ id : data.id}, function (data) {                           
							$scope.registroUbicacionModal.model=data;                         
								
							message.show("success", "Ubicación Registrada satisfactoriamente!!");
                             $modalInstance.close();                       
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});              
          } 
        }
        
    //TIPOS
    $scope.tipos = {
        current : {}, data:[],
        getData : function() {
            $scope.tipos.data.push({value:'', descripcion: 'Seleccione'});
            $scope.tipos.data.push({value:'E', descripcion: 'Entrada'});
            $scope.tipos.data.push({value:'S', descripcion: 'Salida'});
            $scope.tipos.current=$scope.tipos.data[0];
        }        
    } 
    
    
   //INICIALIZAMOS EL MAPA
 

   //activate(150.644,34.397);
    
    
    //CARGAMOS LISTADO
    $scope.tipos.getData();        
    if(registroUbicacion.id==0){   
       if(objUbi.load){ 
            $scope.registroUbicacionModal.model.longitude=objUbi.long;
            $scope.registroUbicacionModal.model.latitude=objUbi.lat;
            $scope.registroUbicacionModal.model.accuracy=objUbi.accu;
            $scope.registroUbicacionModal.model.contratoId=registroUbicacion.contratoId;
            $scope.registroUbicacionModal.model.funcionarioId=registroUbicacion.funcionarioId;           
        }
           else{               
               message.show("warning", "Debe cargar la ubicación!!!");
               $modalInstance.dismiss('cancel');               
           }
             //activate(objUbi.long,objUbi.lat);
             //activate2();
    }
    else{ 
         //activate(objUbi.long,objUbi.lat);
         usuarioHttp.getRegistroUbicacion({}, { id : registroUbicacion.id} , function (data) { 
         $scope.registroUbicacionModal.model = data;	
             
           objUbi.long=$scope.registroUbicacionModal.model.longitude;
           objUbi.lat=$scope.registroUbicacionModal.model.latitude;
           objUbi.accu=$scope.registroUbicacionModal.model.accuracy;
           objUbi.load=true;
             
            $scope.tipos.current = $filter('filter')($scope.tipos.data, {value : $scope.registroUbicacionModal.model.tipo})[0];
       
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });  
           //if(objUbi.load)
             //  activate(objUbi.long,objUbi.lat);
      
    }   
      
}
    
    
    


    })();

/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.usuario')
    .controller('registroUbicacionListController', registroUbicacionListController);

  registroUbicacionListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'usuarioHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION','$interval'];
    


  
    function registroUbicacionListController($scope, $filter, $state,$modal, ngDialog, usuarioHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION,$interval) {       
        
        
                
         
        var options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };        
        var ubicacion={
            long:0,
            lat:0,
            accu:0,            
            load:false            
        };       
               
      
       
        
            
    $scope.registroUbicacion={
        //listado           
     paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.registroUbicacion.fechaInicial.isOpen = true;
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.registroUbicacion.fechaFinal.isOpen = true;
        }
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreregistroUbicacion:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.registroUbicacion.selectedAll = !$scope.registroUbicacion.selectedAll; 
        for (var key in $scope.registroUbicacion.selectedItems) {
          $scope.registroUbicacion.selectedItems[key].check = $scope.registroUbicacion.selectedAll;
        }
      },
      add : function() {          
          
            if(!ubicacion.load){message.show("warning", "Debe cargar su ubicación");return;}
            if($scope.procesoAsociado.current.id==-1){message.show("warning", "Debe seleccionar un contrato");return;}     
            if($scope.responsableProceso.current.funcionarioId==-1){message.show("warning", "Debe seleccionar un responsable");return;}
              
         var  registroUbicacion = {
                id:0,
                contratoId:$scope.procesoAsociado.current.id,
                funcionarioId:$scope.responsableProceso.current.funcionarioId,
                fecha: '',
                tipo: '',
                comentario: '',
                longitude : 0,
                latitude : 0,
                accuracy : 0
		}
          
        var parameter= { objUbicacion : ubicacion,
                          registroUbicacion:registroUbicacion }
         
         var modalInstance = $modal.open({
          templateUrl: 'app/views/usuario/registroUbicacion_modal.html',
          controller: 'ModalGmapController',
          size: 'lg',
            resolve: {
                parameters: function () { return parameter; }
            }
        });
          
        modalInstance.result.then(function (parameters) {
            
            $scope.btnRegistroUbicacion=false; 
            $scope.registroUbicacion.getData();
            
              ubicacion.long=0;            
              ubicacion.lat=0,
              ubicacion.accu=0,            
              ubicacion.load=false;
            
             
            
            
          
        });
                      
      },
      edit : function(item) {
          
          message.show("warning", "Por el momento no se puede editar la ubicación");
          /*
        var modalInstance = $modal.open({
          templateUrl: 'app/views/usuario/registroUbicacion_modal.html',
          controller: 'ModalGmapController',
          size: 'lg',
          resolve: {            parameters: { objUbicacion : ubicacion,registroUbicacion: item}         }
        });
        modalInstance.result.then(function (parameters) {
            $scope.registroUbicacion.getData();
              
              ubicacion.long=0;            
              ubicacion.lat=0,
              ubicacion.accu=0,            
              ubicacion.load=false;
            
             $scope.registroUbicacion.obtenerUbicacion()
            
        });  */        
      
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.registroUbicacion.filterText,
          "precision": false
        }];
        $scope.registroUbicacion.selectedItems = $filter('arrayFilter')($scope.registroUbicacion.dataSource, paramFilter);
        $scope.registroUbicacion.paginations.totalItems = $scope.registroUbicacion.selectedItems.length;
        $scope.registroUbicacion.paginations.currentPage = 1;
        $scope.registroUbicacion.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.registroUbicacion.paginations.currentPage == 1 ) ? 0 : ($scope.registroUbicacion.paginations.currentPage * $scope.registroUbicacion.paginations.itemsPerPage) - $scope.registroUbicacion.paginations.itemsPerPage;
        $scope.registroUbicacion.data = $scope.registroUbicacion.selectedItems.slice(firstItem , $scope.registroUbicacion.paginations.currentPage * $scope.registroUbicacion.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.registroUbicacion.data = [];
        $scope.registroUbicacion.loading = true;
        $scope.registroUbicacion.noData = false;
          var objF={};
          
            if(!$scope.responsableProceso.current){message.show("warning", "Debe seleccionar un responsable");return;}
            if($scope.registroUbicacion.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{objF.fechaInicial=Date.parse(new Date($scope.registroUbicacion.fechaInicial.value));}	
          
            if($scope.registroUbicacion.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{objF.fechaFinal=Date.parse(new Date($scope.registroUbicacion.fechaFinal.value));}	  
              
              objF.idResponsable=$scope.responsableProceso.current.funcionarioId;              
              objF.idReferencia=$scope.procesoAsociado.current.id;    
          
        usuarioHttp.getRegistrosUbicacion({}, objF,function(response) {
          $scope.registroUbicacion.selectedItems = response;
          $scope.registroUbicacion.dataSource = response;
          for(var i=0; i<$scope.registroUbicacion.dataSource.length; i++){
            $scope.registroUbicacion.nombreregistroUbicacion.push({id: i, nombre: $scope.registroUbicacion.dataSource[i]});
          }
          $scope.registroUbicacion.paginations.totalItems = $scope.registroUbicacion.selectedItems.length;
          $scope.registroUbicacion.paginations.currentPage = 1;
          $scope.registroUbicacion.changePage();
          $scope.registroUbicacion.loading = false;
          ($scope.registroUbicacion.dataSource.length < 1) ? $scope.registroUbicacion.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }, 
        obtenerUbicacion : function() {
                //tryGeolocation();
            
            if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition( $scope.registroUbicacion.setUbicacion);
                } else {
                   message.show("error", "No se puede cargar la ubicación!");  
                    $scope.btnRegistroUbicacion=false; 
                }
                
        },
        setUbicacion : function(pos) {            
            
             var crd = pos.coords;  
            
              ubicacion.lat=crd.latitude;
              ubicacion.long=crd.longitude;
              ubicacion.accu= crd.accuracy;
              ubicacion.load= true;               
              message.show("success", "Ubicación cargada con exito!");        
              $scope.btnRegistroUbicacion=true; 
        }
        
        
        
        
        
        
        
    }
    
   
        
    //RESPONSABLE
    $scope.responsableProceso = {
      current : {}, data:[],
      getData : function() {        
          
          if($scope.procesoAsociado.current){              
              
                var obj= {
                    idProceso : $scope.procesoAsociado.current.id,
                    proceso : "CONTRATO"
                }  
              
                $rootScope.loadingVisible = true;
                usuarioHttp.getResponsablesProceso({}, obj, function(response) {                     
                    
                
                    
                    if(response.length==1){      
                       $scope.responsableProceso.data = response ;   
                       $scope.responsableProceso.current = $scope.responsableProceso.data[0];
                    }
                    else{
                        
                        $scope.responsableProceso.data = response ;   
                        $scope.responsableProceso.data.push({funcionarioId:(-1), nombre:"Todos"});
                        $scope.responsableProceso.data=$filter('orderBy')($scope.responsableProceso.data, 'id'); 
                        $scope.responsableProceso.current = $scope.responsableProceso.data[0];
                        }
                    
                    $rootScope.loadingVisible = false;
                    
                    
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      }
    } 
    
    $scope.procesoAsociado = {
      current : {}, data:[],
      getData : function() {
          
              
                $rootScope.loadingVisible = true;
                usuarioHttp.getProcesosAsociado({}, {proceso:"CONTRATO"}, function(response) {                       
                     
                    
                  if(response.length==1){     
                        $scope.procesoAsociado.data = response ;   
                        $scope.procesoAsociado.current = $scope.procesoAsociado.data[0];
                        $scope.responsableProceso.getData();
                      
                      
                    }
                    else{

                         $scope.procesoAsociado.data = response ;   
                         $scope.procesoAsociado.data.push({id:(-1), descripcion:"Todos",proceso:""});
                         $scope.procesoAsociado.data=$filter('orderBy')($scope.procesoAsociado.data, 'id');  
                         $scope.procesoAsociado.current=$scope.procesoAsociado.data[0];  
                         $scope.responsableProceso.getData();
                    }
                    
                    $rootScope.loadingVisible = false;
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });          
      },
        setProcesoAsociado: function() {             
           $scope.responsableProceso.getData();            
      }
     }
    
    //CARGAMOS LISTADO
    $scope.procesoAsociado.getData();
    $scope.registroUbicacion.obtenerUbicacion();
    // INICIALIZAMOS FECHAS
    var date = new Date();    
    $scope.registroUbicacion.fechaInicial.value=new Date(date.getFullYear(), date.getMonth(), 1);
    $scope.registroUbicacion.fechaFinal.value=new Date(date.getFullYear(), date.getMonth() + 1, 0);        
    
        
        
        
    }
  
  
  })();
/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.usuario')
    .service('usuarioHttp', usuarioHttp);

  usuarioHttp.$inject = ['$resource', 'END_POINT'];


  function usuarioHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getRegistrosUbicacion : {
        method : 'POST',
        isArray : true
      },
      getRegistroUbicacion : {
        method : 'GET',
        url : END_POINT + '/Cliente.svc/registroUbicacion/:id'
      },      
      addRegistroUbicacion: {
        method:'POST',
        url : END_POINT + '/Cliente.svc/addRegistroUbicacion'
      },
      editRegistroUbicacion: {
        method:'PUT',
        url : END_POINT + '/Cliente.svc/editRegistroUbicacion'
      },getProcesosAsociado: {
          method : 'GET',
          isArray : true,
          params : {
          proceso : '@proceso'           
          },
          'url' : END_POINT + '/General.svc/procesoAsociado/:proceso'
      }, 
        'getResponsablesProceso': {
          'method' : 'POST',
          'isArray' : true,          
          'url' : END_POINT + '/General.svc/procesoAsociado/responsable'
        }
      
        
    };

    return $resource(END_POINT + '/Cliente.svc/registroUbicacion', paramDefault, actions);
  }

})();

/**=========================================================
 * Module: animate-enabled.js
 * Enable or disables ngAnimate for element with directive
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('animateEnabled', animateEnabled);

    animateEnabled.$inject = ['$animate'];
    function animateEnabled ($animate) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
          scope.$watch(function () {
            return scope.$eval(attrs.animateEnabled, scope);
          }, function (newValue) {
            $animate.enabled(!!newValue, element);
          });
        }
    }

})();

/**=========================================================
 * Module: browser.js
 * Browser detection
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .service('Browser', Browser);

    Browser.$inject = ['$window'];
    function Browser($window) {
      return $window.jQBrowser;
    }

})();

/**=========================================================
 * Module: clear-storage.js
 * Removes a key from the browser storage via element click
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('resetKey', resetKey);

    resetKey.$inject = ['$state', '$localStorage'];
    function resetKey ($state, $localStorage) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
              resetKey: '@'
            }
        };
        return directive;

        function link(scope, element) {
          element.on('click', function (e) {
              e.preventDefault();

              if(scope.resetKey) {
                delete $localStorage[scope.resetKey];
                $state.go($state.current, {}, {reload: true});
              }
              else {
                $.error('No storage key specified for reset.');
              }
          });
        }
    }

})();

(function() {
    'use strict';
    angular.module('app.utils').
      directive('customOnChange', function() {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var onChangeFunc = scope.$eval(attrs.customOnChange);
          element.bind('change', onChangeFunc);
        }
      };
    });
  })();
/**=========================================================
 * Module: flot.js
 * Initializes the Flot chart plugin and handles data refresh
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('flot', flot);

    flot.$inject = ['$http', '$timeout'];
    function flot ($http, $timeout) {

        var directive = {
          restrict: 'EA',
          template: '<div></div>',
          scope: {
            dataset: '=?',
            options: '=',
            series: '=',
            callback: '=',
            src: '='
          },
          link: link
        };
        return directive;

        function link(scope, element, attrs) {
          var height, plot, plotArea, width;
          var heightDefault = 220;
          //debugger;
          plot = null;

          width = attrs.width || '100%';
          height = attrs.height || heightDefault;

          plotArea = $(element.children()[0]);
          plotArea.css({
            width: width,
            height: height
          });

          function init() {
            var plotObj;
            if(!scope.dataset || !scope.options) return;
            plotObj = $.plot(plotArea, scope.dataset, scope.options);
            scope.$emit('plotReady', plotObj);
            if (scope.callback) {
              scope.callback(plotObj, scope);
            }

            return plotObj;
          }

          function onDatasetChanged(dataset) {
            if (plot) {
              plot.setData(dataset);
              plot.setupGrid();
              return plot.draw();
            } else {
              plot = init();
              onSerieToggled(scope.series);
              return plot;
            }
          }
          scope.$watchCollection('dataset', onDatasetChanged, true);

          function onSerieToggled (series) {
            if( !plot || !series ) return;
            var someData = plot.getData();
            for(var sName in series) {
              angular.forEach(series[sName], toggleFor(sName));
            }
            
            plot.setData(someData);
            plot.draw();
            
            function toggleFor(sName) {
              return function (s, i){
                if(someData[i] && someData[i][sName])
                  someData[i][sName].show = s;
              };
            }
          }
          scope.$watch('series', onSerieToggled, true);
          
          function onSrcChanged(src) {

            if( src ) {

              $http.get(src)
                .success(function (data) {

                  $timeout(function(){
                    scope.dataset = data;
                  });

              }).error(function(){
                $.error('Flot chart: Bad request.');
              });
              
            }
          }
          scope.$watch('src', onSrcChanged);

        }
    }


})();
/**=========================================================
 * Module: fullscreen.js
 * Toggle the fullscreen mode on/off
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('toggleFullscreen', toggleFullscreen);

    toggleFullscreen.$inject = ['Browser'];
    function toggleFullscreen (Browser) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
          // Not supported under IE
          if( Browser.msie ) {
            element.addClass('hide');
          }
          else {
            element.on('click', function (e) {
                e.preventDefault();

                if (screenfull.enabled) {
                  
                  screenfull.toggle();
                  
                  // Switch icon indicator
                  if(screenfull.isFullscreen)
                    $(this).children('em').removeClass('fa-expand').addClass('fa-compress');
                  else
                    $(this).children('em').removeClass('fa-compress').addClass('fa-expand');

                } else {
                  $.error('Fullscreen not enabled');
                }

            });
          }
        }
    }


})();

/**=========================================================
 * Module: load-css.js
 * Request and load into the current page a css file
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('loadCss', loadCss);

    function loadCss () {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
          element.on('click', function (e) {
              if(element.is('a')) e.preventDefault();
              var uri = attrs.loadCss,
                  link;

              if(uri) {
                link = createLink(uri);
                if ( !link ) {
                  $.error('Error creating stylesheet link element.');
                }
              }
              else {
                $.error('No stylesheet location defined.');
              }

          });
        }
        
        function createLink(uri) {
          var linkId = 'autoloaded-stylesheet',
              oldLink = $('#'+linkId).attr('id', linkId + '-old');

          $('head').append($('<link/>').attr({
            'id':   linkId,
            'rel':  'stylesheet',
            'href': uri
          }));

          if( oldLink.length ) {
            oldLink.remove();
          }

          return $('#'+linkId);
        }
    }

})();

/**=========================================================
 * Module: now.js
 * Provides a simple way to display the current time formatted
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('now', now);

    now.$inject = ['dateFilter', '$interval'];
    function now (dateFilter, $interval) {
        var directive = {
            link: link,
            restrict: 'EA'
        };
        return directive;

        function link(scope, element, attrs) {
          var format = attrs.format;

          function updateTime() {
            var dt = dateFilter(new Date(), format);
            element.text(dt);
          }

          updateTime();
          var intervalPromise = $interval(updateTime, 1000);

          scope.$on('$destroy', function(){
            $interval.cancel(intervalPromise);
          });

        }
    }

})();

(function() {
  'use strict';

  angular
    .module('app.utils')
    .directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseInt(digits,10);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
  });    


})();




/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
  angular
    .module('app.utils')
    .filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});
/**=========================================================
 * Module: table-checkall.js
 * Tables check all checkbox
 =========================================================*/
(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('checkAll', checkAll);

    function checkAll () {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
          element.on('change', function() {
            var $this = $(this),
                index= $this.index() + 1,
                checkbox = $this.find('input[type="checkbox"]'),
                table = $this.parents('table');
            // Make sure to affect only the correct checkbox column
            table.find('tbody > tr > td:nth-child('+index+') input[type="checkbox"]')
              .prop('checked', checkbox[0].checked);

          });
        }
    }

})();

/**=========================================================
 * Module: trigger-resize.js
 * Triggers a window resize event from any element
 =========================================================*/
(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('triggerResize', triggerResize);

    triggerResize.$inject = ['$window', '$timeout'];
    function triggerResize ($window, $timeout) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
          element.on('click', function(){
            $timeout(function(){
              $window.dispatchEvent(new Event('resize'));
            });
          });
        }
    }

})();

/**=========================================================
 * Module: utils.js
 * Utility library to use across the theme
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .service('Utils', Utils);

    Utils.$inject = ['$window', 'APP_MEDIAQUERY'];
    function Utils($window, APP_MEDIAQUERY) {

        var $html = angular.element('html'),
            $win  = angular.element($window),
            $body = angular.element('body');

        return {
          // DETECTION
          support: {
            transition: (function() {
                    var transitionEnd = (function() {

                        var element = document.body || document.documentElement,
                            transEndEventNames = {
                                WebkitTransition: 'webkitTransitionEnd',
                                MozTransition: 'transitionend',
                                OTransition: 'oTransitionEnd otransitionend',
                                transition: 'transitionend'
                            }, name;

                        for (name in transEndEventNames) {
                            if (element.style[name] !== undefined) return transEndEventNames[name];
                        }
                    }());

                    return transitionEnd && { end: transitionEnd };
                })(),
            animation: (function() {

                var animationEnd = (function() {

                    var element = document.body || document.documentElement,
                        animEndEventNames = {
                            WebkitAnimation: 'webkitAnimationEnd',
                            MozAnimation: 'animationend',
                            OAnimation: 'oAnimationEnd oanimationend',
                            animation: 'animationend'
                        }, name;

                    for (name in animEndEventNames) {
                        if (element.style[name] !== undefined) return animEndEventNames[name];
                    }
                }());

                return animationEnd && { end: animationEnd };
            })(),
            requestAnimationFrame: window.requestAnimationFrame ||
                                   window.webkitRequestAnimationFrame ||
                                   window.mozRequestAnimationFrame ||
                                   window.msRequestAnimationFrame ||
                                   window.oRequestAnimationFrame ||
                                   function(callback){ window.setTimeout(callback, 1000/60); },
            /*jshint -W069*/
            touch: (
                ('ontouchstart' in window && navigator.userAgent.toLowerCase().match(/mobile|tablet/)) ||
                (window.DocumentTouch && document instanceof window.DocumentTouch)  ||
                (window.navigator['msPointerEnabled'] && window.navigator['msMaxTouchPoints'] > 0) || //IE 10
                (window.navigator['pointerEnabled'] && window.navigator['maxTouchPoints'] > 0) || //IE >=11
                false
            ),
            mutationobserver: (window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver || null)
          },
          // UTILITIES
          isInView: function(element, options) {
              /*jshint -W106*/
              var $element = $(element);

              if (!$element.is(':visible')) {
                  return false;
              }

              var window_left = $win.scrollLeft(),
                  window_top  = $win.scrollTop(),
                  offset      = $element.offset(),
                  left        = offset.left,
                  top         = offset.top;

              options = $.extend({topoffset:0, leftoffset:0}, options);

              if (top + $element.height() >= window_top && top - options.topoffset <= window_top + $win.height() &&
                  left + $element.width() >= window_left && left - options.leftoffset <= window_left + $win.width()) {
                return true;
              } else {
                return false;
              }
          },
          
          langdirection: $html.attr('dir') === 'rtl' ? 'right' : 'left',

          isTouch: function () {
            return $html.hasClass('touch');
          },

          isSidebarCollapsed: function () {
            return $body.hasClass('aside-collapsed');
          },

          isSidebarToggled: function () {
            return $body.hasClass('aside-toggled');
          },

          isMobile: function () {
            return $win.width() < APP_MEDIAQUERY.tablet;
          }

        };
    }
})();

/**=========================================================
 * Module: flot.js
 * Initializes the Flot chart plugin and handles data refresh
 =========================================================*/
(function() {
  'use strict';

  angular
    .module('app.utils')
    .directive('validation', validation);


  validation.$inject = ['$parse'];


  function validation ($parse) {

   return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attr, ctrl) {


        angular.element(element).on("keypress", function(e) {
          var pattern = new RegExp(scope.$eval(attr['validation']));
          var newInput = this.value + String.fromCharCode(e.charCode);
          if (pattern.test(newInput) == false) {
            e.preventDefault();
          };
        });


        angular.element(element).on("paste", function(e) {
          setTimeout(function () {
            var pattern = new RegExp(scope.$eval(attr['validation']));
            var newText = angular.element(element).val();
            if (pattern.test(newText) == false) {
              e.preventDefault();
                var model = $parse(attr.ngModel);
                console.log(model(scope));
                model.assign(scope,'');
                scope.$apply();
            };
          }, 5);
        });

      }
    };
  }
})();