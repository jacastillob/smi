'use strict';

angular.module('al-ui',[])
  .filter('arrayFilter', function () {

  return function (array, expression) {
    var result = [];
    var isValid = true;

    function isValidItem(e, itemArray) {
      var result = false;
      if (e.key.charAt(0) == '$') {
        for (var objKey in itemArray) {
          if (e.precision) {
            if (itemArray[objKey] == e.value) {
              result = true;
              break;
            }
          } else {
            var text = ('' + itemArray[objKey]).toLowerCase();
            if (('' + text).toLowerCase().indexOf(('' + e.value).toLowerCase()) !== -1) {
              result = true;
              break;
            }
          }
        }
      } else if (angular.isArray(e.value)) {
        var text = ('' + itemArray[e.key]).toLowerCase();
        for (var i = 0; i < e.value.length; i++) {
          if (e.precision) {
            if (itemArray[e.key] == e.value[i]) {
              result = true;
              break;
            }
          } else {
            if (('' + text).toLowerCase().indexOf(('' + e.value[i]).toLowerCase()) !== -1) {
              result = true;
              break;
            }
          }
        }
      } else {
        if (e.precision) {
          if (itemArray[e.key] == e.value) {
            result = true;
          } 
        }else {
          var text = ('' + itemArray[e.key]).toLowerCase();
          if (('' + text).toLowerCase().indexOf(('' + e.value).toLowerCase()) !== -1) {
            result = true;
          }
        }
      }
      return result;
    }

    if (array == undefined) {
      return result;
    } else {
      for (var i = 0; i < array.length; i++) {
        isValid = true;
        for (var e in expression) {
          (expression[e].value !== undefined && expression[e].value !== '' && isValid) ? 
            isValid = isValidItem(expression[e], array[i]): null;
        }
        (isValid) ? result.push(array[i]): null;
      }
      return result;
    }

  };
})
  .factory('LDataSource', function($http) {

  var DataSource = function(_params) {

    var serviceAttr = {
      method: "put",
      url : "",
      data: {},
      root: null
    };
    $.extend(serviceAttr, _params);

    var data = null;

    var callbackOnBeforeLoad = $.Callbacks();
    var callbackOnLoad = $.Callbacks();
    var callbackOnError = $.Callbacks();

    function invokeService(_parameters, _callbackDone, _callbackFail) {
      (_parameters) ? $.extend( serviceAttr.data, _parameters ): null;

      $http({
        method: serviceAttr.method,
        url: serviceAttr.url,
        data: serviceAttr.data
      }).then( _callbackDone, _callbackFail);

    };

    return {

      load: function(_parameters ) {
        if( callbackOnBeforeLoad.fire().fired() ) {
          invokeService(
            _parameters,
            // ok
            function(response) {
              data = response.data;
              if(serviceAttr.root && data && data.hasOwnProperty(serviceAttr.root)) {
                data = data[serviceAttr.root];
              }
              callbackOnLoad.fire(data,response.data);
            },
            // failure
            function(_jqXHR) {  
              var errorObject = null;
              try {
                errorObject = $.parseJSON(_jqXHR.responseText);
              }
              catch(e) {}

              var simpleErrorMessage = "";
              if(errorObject) {
                simpleErrorMessage = errorObject.errorMessage + " [" + errorObject.errorCode + "]";
              }
              else {
                simpleErrorMessage = "Http Code: " + _jqXHR.status + ", Http Error: " + _jqXHR.statusText;
              }

              callbackOnError.fire(errorObject, _jqXHR, simpleErrorMessage);
            }
          );

        }

      },

      tryAgain: function() {

        if( callbackOnBeforeLoad.fire().fired() ) {
          invokeService(
            null,
            // ok
            function(response) {
              data = response.data;
              if(serviceAttr.root && data && data.hasOwnProperty(serviceAttr.root)) {
                data = data[serviceAttr.root];
              }
              callbackOnLoad.fire(data, response.data);
            },
            // failure
            function(_jqXHR) { 

              var errorObject = null;
              try {
                errorObject = $.parseJSON(_jqXHR.responseText);
              }
              catch(e) {}

              var simpleErrorMessage = "";
              if(errorObject) {
                simpleErrorMessage = errorObject.errorMessage + " [" + errorObject.errorCode + "]";
              }
              else {
                simpleErrorMessage = "Http Code: " + _jqXHR.status + ", Http Error: " + _jqXHR.statusText;
              }

              callbackOnError.fire(errorObject, _jqXHR, simpleErrorMessage);
            }
          );
        }

      },

      onBeforeLoad: function(_callback) {
        callbackOnBeforeLoad.add( _callback );
      },

      onLoad: function(_callback) {
        callbackOnLoad.add( _callback );
      },

      onError: function(_callback) {
        callbackOnError.add( _callback );
      }
    };
  };

  return DataSource;

})
  .directive('ltable', ['$filter', function ($filter) {
    return {
      restrict: 'E',
      scope :  {
        options: '=',
        datasource : '=',
        onRowclick : '&'
      },
      link: function postLink(scope, element, attrs, ctrl) {

        var options = {
          tableClass: 'no-select no-margin table table-hover table-condensed',
          filterText: '',
          selectRow: true,
          multiSelect: false,
          loadingText: "<p class='text-center'><i class='fa fa-spinner fa-spin fa-2x text-muted'></i><p><p class='text-center lead text-primary'>Obteniendo Datos...</p>",
          columns: []
        };

        angular.extend(options, scope.options);

        var tableEL = element;
        var tableData = [];
        var enableSetScrollTop = false;
        var tableScrollTop = 0;
        var hasCheckbox = false;
        var shift_IsPress = false;
        var lastcheckId = '';


        if ( options.dataSource !== null ) {
          options.dataSource.onBeforeLoad(onBeforeLoadData);
          options.dataSource.onLoad(prepareData);
          options.dataSource.onError(onErrorData);
        }

        var tableId = new Date().getTime();

        function prepareData(_data) {
          scope.options.filterText = '';
          scope.options.data = [];
          scope.options.selectedRows = [];
          for (var i = 0; i < _data.length; i++) {
            var _tmp = _data[i];
            scope.options.data.push(angular.extend(_tmp, {
              _idRow: i
            }));
          }
          scope.options.visibleRows = scope.options.data.length;
          onLoadData(scope.options.data);
          angular.element("#" + tableId + " #checkAll").prop("checked", false);            
        }       

        /*** CALLBACKS **************************************************************/

        function onBeforeLoadData() {
          console.log("beforeLoadData");
          renderLoadingData();
        };

        function onLoadData(_data) {
          console.log("loadData");
          tableData = _data;
          renderBody();
        };


        function onErrorData(_errorObject, _jqXHR, _simpleErrorMessage) {
          console.log("ErrorData");
          renderErrorData(_jqXHR);
        };

        /*** EVENTS **************************************************************/

        scope.$watch('options.filterText', function() {

          var data = filterText(scope.options.filterText);
          angular.element("#"+ tableId + " tbody tr").attr("hidden", true);
          scope.options.visibleRows = data.length;
          for (var i = 0; i < data.length; i++) {
            angular.element("#" + tableId + " tbody tr#" + data[i]._idRow + "").attr("hidden", false);
          }
          getSelectedRows();
        });

        scope.$watch('options.columns', function() {
          angular.extend(options.columns, scope.options.columns)
          renderLTable();
        });

        angular.element(document).on('keydown', function(e) {
          shift_IsPress = e.shiftKey;
        });

        angular.element(document).on('keyup', function(e) {
          shift_IsPress = e.shiftKey;
          if (!shift_IsPress) {
            lastcheckId = '';
          }
        });

        tableEL.on("click", "#tryAgain", function() {
          scope.options.dataSource.tryAgain();
        });

        tableEL.on("click", "input[type=checkbox]", function(_event) {
          var cb = $(this);
          var status = cb.is(":checked");

          if (this.id == 'checkAll') {
            if (scope.options.data.length > 0 ) {
              if (status) {
                angular.element("#" + tableId + " tbody tr").each(function(index) {
                  if (!this.hidden) {
                    angular.element(this).addClass("active");
                  }
                });
                angular.element("#" + tableId+ " tbody .ltableCheckbox").prop("checked", true);
              } else {
                angular.element("#" + tableId + " tbody tr").each(function(index) {
                  if (!this.hidden) {
                    angular.element(this).removeClass("active");
                  }
                });
                angular.element("#" + tableId + " .ltableCheckbox").prop("checked", false);
              }                    
            }
          } else {
            if (shift_IsPress) {

              if (lastcheckId === '') {
                lastcheckId = Number(this.id);
              } else {

                if (lastcheckId < this.id) {
                  for (var i = Number(lastcheckId); i < this.id; i++) {
                    if (!angular.element("#" + tableId + " tr#" + i + "").attr("hidden")) {
                      if (status) {
                        angular.element("#" + tableId + " tr#" + i + "").addClass("active");
                        angular.element('#'+ tableId + ' input:checkbox[id="' + i + '"]').prop("checked", true);
                      } else {
                        angular.element("#" + tableId + " tr#" + i + "").removeClass("active");
                        angular.element('#' + tableId+ ' input:checkbox[id="' + i + '"]').prop("checked", false);
                      }
                    }                                  
                  }
                } else {
                  for (var i = Number(this.id); i < lastcheckId; i++) {
                    if (!angular.element("#" + tableId + " tr#" + i + "").attr("hidden")) {
                      if (status) {
                        angular.element("#" + tableId + " tr#" + i + "").addClass("active");
                        angular.element('#'+ tableId + ' input:checkbox[id="' + i + '"]').prop("checked", true);
                      } else {
                        angular.element("#" + tableId + " tr#" + i + "").removeClass("active");
                        angular.element('#' + tableId+ ' input:checkbox[id="' + i + '"]').prop("checked", false);
                      }
                    }                                  
                  }
                }

                _event.stopPropagation();

                lastcheckId = '';
              }
            }

            if (status) {
              angular.element('#' + tableId + " tbody tr#" + this.id + '').addClass("active");
            } else {
              angular.element('#' + tableId + " tbody tr#" + this.id + '').removeClass("active");
            }
            _event.stopPropagation();
          }
          getSelectedRows();
          scope.$apply();
        });


        tableEL.on("click", "tbody tr", function(_event) {
          if (_event.target.id !== 'no-select') {

            if (options.multiSelect) {
              if (_event.target.id !== "checkbox") {

                if (angular.element(this).hasClass("active") && 
                    !angular.element('#' + tableId+ ' input:checkbox[id="checkAll"]').is(":checked") ) {

                  angular.element('#' + tableId + ' input:checkbox[id="' + this.id + '"]').prop("checked", false);
                  angular.element(this).removeClass("active");     
                } else {
                  angular.element("#" + tableId + " tr").removeClass("active");
                  angular.element("#" + tableId + " .ltableCheckbox").prop("checked", false);
                  angular.element('#' + tableId + ' input:checkbox[id="' + this.id + '"]').prop("checked", true);
                  angular.element(this).addClass("active");     
                  angular.element('#' + tableId+ ' input:checkbox[id="checkAll"]').prop("checked", false);
                }
              }
            } else if (options.selectRow) {
              if (angular.element(this).hasClass("active")) {
                angular.element(this).removeClass("active");
              } else {
                (!options.multiSelect) ? angular.element("#" + tableId + " tbody tr").removeClass("active"): null;
                angular.element(this).addClass("active");
              }
            }
            getSelectedRows();
            if (options.hasOwnProperty('events') && options.events.hasOwnProperty('onRowClick')) {
              options.events.onRowClick(getDataById(this.id));
            }

            scope.$apply();

          }

        });

        tableEL.on("dblclick", "tbody tr", function(_event) {
          if (_event.target.id !== 'no-select') {
            if (options.hasOwnProperty('events') && options.events.hasOwnProperty('onRowDblClick')) {
              options.events.onRowDblClick(getDataById(this.id));
              scope.$apply();
            }
          }
        });



        /*** FUNCTIONS **************************************************************/

        function getSelectedRows() {
          scope.options.selectedRows = [];
          angular.element("#" + tableId + " tr.active").each(function(index) {
            (!this.hidden) ? scope.options.selectedRows.push(getDataById(this.id)): null;
          });
        }

        function filterText(text) {
          var paramFilter = [{
            "key": "$",
            "value": text,
            "precision": false
          }];
          return $filter('arrayFilter')(scope.options.data, paramFilter);
        }


        function getDataById(id) {
          for (var i = 0; i < scope.options.data.length; i++) {
            if (scope.options.data[i]._idRow == id) {
              return scope.options.data[i];
            }
          }
        }

        /*** RENDERS **************************************************************/

        function renderLTable() {
          renderTable();
          renderHeader();
          renderBody();
          renderFooter();        
        }

        function renderLoadingData() {

          var heightProgress = (options.columns.length > 0) ? options.columns.length : 1;
          (options.columns.length > 0 && options.multiSelect) ? heightProgress = options.columns.length + 1 : null;
          setTimeout(function() {
            tableEL.find("tbody").empty().append(
              "<tr><td class='height-sm' colspan=" + heightProgress + ">" + options.loadingText + "</td></tr>"
            );
          }, 50);
        };

        function renderErrorData(_errorMessage) {
          var message = '';
          if (_errorMessage.errorMessage) {
            message = _errorMessage.errorMessage;
          }

          var numCols = (options.columns.length > 0) ? options.columns.length : 1;
          (options.columns.length > 0 && options.multiSelect) ? numCols = options.columns.length + 1 : null;

          var templateMessage = "<p id='no-select' class='text-center lead'><i class='md md-cloud-off text-xl text-danger'></i></br>" + 
              "Ocurrio un error al intentar obtener los datos.</br>" +
              "<span id='no-select' class='text-xs'>" + message + '</span></p>' +
              "<a class='text-primary' id='tryAgain'>Reintentar</a>";

          setTimeout(function() {
            tableEL.find("tbody").empty().append(
              "<tr><td id='no-select' class='height-sm text-center' colspan=" + numCols + ">" + templateMessage + 
              "</td></tr>"
            );
          }, 50);

        };

        function renderBody() {
          var tbody = tableEL.find("tbody");
          //   tbody.empty();
          if (tableData.length == 0) {
            renderEmptyLine();
          } else {
            setTimeout(function() {
              tableEL.find("tbody").empty();
              angular.element("#" + tableId+ " #checkAll").prop("disabled", false);
              for (var i = 0; i < tableData.length; i++) {
                renderBodyLine(tableData[i]);
              };
            }, 50);
          }

        };


        function renderBodyLine(_item) {
          if (options.columns.length > 0) {
            var htmlBody = "<tr id=" + _item._idRow + ">";


            if (options.multiSelect) {
              htmlBody += '<td><div class="checkbox checkbox-styled">' +
                '<label class="cbRow"><input type="checkbox" class="ltableCheckbox" id=' + _item._idRow + ' >' +
                '<span id="checkbox"></span>' +
                '</label>' +
                '</div></td>';
            }

            for (var i = 0; i < options.columns.length; i++) {
              var column = options.columns[i];

              // get value
              var dataIndex = null;
              var value = null;
              if (column.hasOwnProperty("dataIndex") && _item.hasOwnProperty(column.dataIndex)) {
                dataIndex = column.dataIndex;
                value = _item[column.dataIndex];
              }

              htmlBody += "<td";

              // type: checkbox
              if (column.hasOwnProperty("type") && column.type === "checkbox") {
                htmlBody += ">";
                htmlBody += "<input type='checkbox' field='" + (dataIndex == null ? "" : dataIndex) + "' data='" + value + "'/>";
              }
              // type: text
              else {
                htmlBody += ">";

                // get render
                var render = function(_value) {
                  return _value;
                };
                if ($.isFunction(column.renderer)) {
                  render = column.renderer;
                }

                // render
                htmlBody += render(value, _item);
              }

              htmlBody += "</td>";
            }
            htmlBody += "</tr>";

            tableEL.find("tbody").append(htmlBody);
          }
        };

        function renderEmptyLine() {
          setTimeout(function() {
            var numCols = (options.columns.length > 0) ? options.columns.length : 1;
            (options.columns.length > 0 && options.multiSelect) ? numCols = options.columns.length + 1 : null;

            var _emptyText = "<p class='lead' id='no-select'>No se encuentran registros.</br></p>";

            var trEmptyLine = "<tr id='no-select'><td id='no-select' class='height-sm text-center' colspan=" + numCols + ">" + _emptyText + "</td></tr>";
            tableEL.find("tbody").empty().append(trEmptyLine);
            angular.element("#" + tableId+ " #checkAll").prop("disabled", true);
          }, 50);
        };

        function renderHeader() {
          if (options.columns.length > 0) {
            var htmlHead = "<tr class='style-default'>";

            if (options.multiSelect) {
              htmlHead += '<td><div class="checkbox checkbox-styled" >' +
                '<label><input type="checkbox" id="checkAll">' +
                '<span></span>' +
                '</label>' +
                '</div></td>';
            }

            for (var i = 0; i < options.columns.length; i++) {
              var column = options.columns[i];
              var displayName = column.displayName || "&nbsp;";
              htmlHead += "<th ";

              // type: checkbox
              if (column.hasOwnProperty("type") && column.type === "checkbox") {
                hasCheckbox = true;
                htmlHead += "class='la-ux-col-checkbox'";
              }

              htmlHead += ">" + displayName + "</th>";
            };
            htmlHead += "</tr>";
            tableEL.find("thead").html(htmlHead);
          }
        };

        function renderFooter() {
          if (tableData.length > 5) {
            var numCols = options.columns.length <= 0 ? 1 : options.columns.length;
            var htmlFoot = "<tr><td colspan=" + numCols + "><small class='text-muted'><em>" + tableData.length + " record(s).</em></small></td></tr>";
            tableEL.find("tfoot").empty().append(htmlFoot);
          }
        };

        function renderTable() {
          tableEL.html(
            '<div class="table-responsive border-default">' +
            '<table id="' + tableId + '" class="' + options.tableClass + '">' +
            '<thead></thead>' +
            '<tbody></tbody>' +
            '<tfoot></tfoot>' +
            '</table>' +
            '</div>'
          );
        };


      } // End Link
    };  

  }]);

