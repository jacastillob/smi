/**=========================================================
 * Module: app.mensajeria.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .controller('mensajeriaListController', mensajeriaListController);

  mensajeriaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'mensajeriaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function mensajeriaListController($scope, $filter, $state, ngDialog, tpl, mensajeriaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
		
	$scope.ESTADO_PROCESO = 'P';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    	

    $scope.mensajerias = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombremensajerias:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.mensajerias.selectedAll = !$scope.mensajerias.selectedAll; 
        for (var key in $scope.mensajerias.selectedItems) {
          $scope.mensajerias.selectedItems[key].check = $scope.mensajerias.selectedAll;
        }
      },
      add : function() {
        
       
            
              var mens = {
                id: 0,
                terceroId: 0,                      
                consecutivo: '',                     
                estado: '',
                descripcion: '',                  
                fechaRec: '',    
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.mensajeria_add', params : { mensajeria: mens } });
            $state.go('app.mensajeria_add');
      
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.mensajeria_edit', params : { mensajeria: item } });
        $state.go('app.mensajeria_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            mensajeriaHttp.remove({}, { id: id }, function(response) {
                $scope.mensajerias.getData();
                message.show("success", "mensajeria eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.mensajerias.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    mensajeriaHttp.remove({}, { id: id }, function(response) {
                        $scope.mensajerias.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.mensajerias.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.mensajerias.filterText,
          "precision": false
        }];
        $scope.mensajerias.selectedItems = $filter('arrayFilter')($scope.mensajerias.dataSource, paramFilter);
        $scope.mensajerias.paginations.totalItems = $scope.mensajerias.selectedItems.length;
        $scope.mensajerias.paginations.currentPage = 1;
        $scope.mensajerias.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.mensajerias.paginations.currentPage == 1 ) ? 0 : ($scope.mensajerias.paginations.currentPage * $scope.mensajerias.paginations.itemsPerPage) - $scope.mensajerias.paginations.itemsPerPage;
        $scope.mensajerias.data = $scope.mensajerias.selectedItems.slice(firstItem , $scope.mensajerias.paginations.currentPage * $scope.mensajerias.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.mensajerias.data = [];
        $scope.mensajerias.loading = true;
        $scope.mensajerias.noData = false;
       
        mensajeriaHttp.getList({},{} ,function(response) {
          $scope.mensajerias.selectedItems = response;
          $scope.mensajerias.dataSource = response;
          for(var i=0; i<$scope.mensajerias.dataSource.length; i++){
            $scope.mensajerias.nombremensajerias.push({id: i, nombre: $scope.mensajerias.dataSource[i]});
          }
          $scope.mensajerias.paginations.totalItems = $scope.mensajerias.selectedItems.length;
          $scope.mensajerias.paginations.currentPage = 1;
          $scope.mensajerias.changePage();
          $scope.mensajerias.loading = false;
          ($scope.mensajerias.dataSource.length < 1) ? $scope.mensajerias.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }

	//CARGAMOS LOS TIPOS DE PROCESO
    
    $scope.mensajerias.getData();  

        
	}
  
  
  })();