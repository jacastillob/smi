/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('buscarPolizaController', buscarPolizaController);

  buscarPolizaController.$inject = ['$scope','$rootScope', '$filter', '$state', '$modalInstance', 'LDataSource', 'cotizacionHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function buscarPolizaController($scope,$rootScope, $filter, $state, $modalInstance, LDataSource, cotizacionHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
            
      
      $scope.polizas = {   
              paginations : {
                maxSize : 3,
                itemsPerPage : 8,
                currentPage : 0,
                totalItems : 0
              },            
              selectedAll : false,
              filterText :'',
              dataSource : [],
              nombrepolizas:[],
              selectedItems : [],
              data : [],
              noData : false,
              loading : false,
              selectAll : function() {
                $scope.productos.selectedAll = !$scope.productos.selectedAll; 
                for (var key in $scope.productos.selectedItems) {
                  $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
                }
              },
          model:{
              origen:0,
              destino:0,
              tipo:0, 
              inicio:'', 
              fin:'',              
              email:'',
              estadia:1,
              e1:'',
              e2:'',
              e3:'',
              e4:'',
              e5:'',
              e6:'',
              e7:'',
              e8:'',
              aseguradora:'',
              ordenamiento:'',
              dias:0,
              pasajeros:0
          },   
           filter : function() {
            var paramFilter = [{
              "key": "$",
              "value": $scope.polizas.filterText,
              "precision": false
            }];
            $scope.polizas.selectedItems = $filter('arrayFilter')($scope.polizas.dataSource, paramFilter);
            $scope.polizas.paginations.totalItems = $scope.polizas.selectedItems.length;
            $scope.polizas.paginations.currentPage = 1;
            $scope.polizas.changePage();
          },
          changePage : function() {
            var firstItem = ($scope.polizas.paginations.currentPage == 1 ) ? 0 : ($scope.polizas.paginations.currentPage * $scope.polizas.paginations.itemsPerPage) - $scope.polizas.paginations.itemsPerPage;
            $scope.polizas.data = $scope.polizas.selectedItems.slice(firstItem , $scope.polizas.paginations.currentPage * $scope.polizas.paginations.itemsPerPage);
          }, 
          
      getData : function() {
        $scope.polizas.data = [];
        $scope.polizas.loading = true;
        $scope.polizas.noData = false;
          
        $scope.polizas.model.origen=parameters.cotizacion.origen;
        $scope.polizas.model.destino=parameters.cotizacion.destino;
        $scope.polizas.model.tipo=parameters.cotizacion.tipo;
        $scope.polizas.model.inicio= $scope.polizas.formatdate (parameters.cotizacion.fechaInicio);
        $scope.polizas.model.fin=    $scope.polizas.formatdate (parameters.cotizacion.fechaFin);             
        $scope.polizas.model.email='';
        $scope.polizas.model.estadia=1;
        $scope.polizas.model.e1=parameters.cotizacion.e1!=undefined?parameters.cotizacion.e1:"";
        $scope.polizas.model.e2=parameters.cotizacion.e2!=undefined?parameters.cotizacion.e2:"";
        $scope.polizas.model.e3=parameters.cotizacion.e3!=undefined?parameters.cotizacion.e3:"";
        $scope.polizas.model.e4=parameters.cotizacion.e4!=undefined?parameters.cotizacion.e4:"";
        $scope.polizas.model.e5=parameters.cotizacion.e5!=undefined?parameters.cotizacion.e5:"";
        $scope.polizas.model.e6=parameters.cotizacion.e6!=undefined?parameters.cotizacion.e6:"";
        $scope.polizas.model.e7=parameters.cotizacion.e7!=undefined?parameters.cotizacion.e7:"";
        $scope.polizas.model.e8=parameters.cotizacion.e8!=undefined?parameters.cotizacion.e8:"";
        $scope.polizas.model.aseguradora='';
        $scope.polizas.model.ordenamiento=''; 
          
        cotizacionHttp.getPolizas({}, $scope.polizas.model,function(response) {
          $scope.polizas.selectedItems = response.polizas;
          $scope.polizas.dataSource = response.polizas;
            
            $scope.polizas.model.dias=response.dias;
            $scope.polizas.model.pasajeros=response.pasajeros;
            
            
          for(var i=0; i<$scope.polizas.dataSource.length; i++){
            $scope.polizas.nombrepolizas.push({id: i, nombre: $scope.polizas.dataSource[i]});
          }
          $scope.polizas.paginations.totalItems = $scope.polizas.selectedItems.length;
          $scope.polizas.paginations.currentPage = 1;
          $scope.polizas.changePage();
          $scope.polizas.loading = false;
          ($scope.polizas.dataSource.length < 1) ? $scope.polizas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      },
               
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          },
           formatdate(t){    

            //var today = new Date();
            var dd = t.getDate();
            var mm = t.getMonth()+1; //January is 0!

            var yyyy = t.getFullYear();
            if(dd<10){
                dd='0'+dd;
            } 
            if(mm<10){
                mm='0'+mm;
            } 
            //2018-01-15
            return yyyy+'-'+mm+'-'+dd;         
       
        },
          save : function() {             
          
          
            var  lst= $scope.polizas.dataSource.filter( element => element.check==true  );
            parameters.cotizacion.polizas=lst;
            $modalInstance.close();
          }
          
      }
       	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      $scope.polizas.getData()
      
             
        
     
        
      
  }
})();