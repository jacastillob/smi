/**=========================================================
 * Module: app.cotizacion.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('cotizacionListController', cotizacionListController);

  cotizacionListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'cotizacionHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function cotizacionListController($scope, $filter,$state,$modal, ngDialog, tpl, cotizacionHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        
		
	  $scope.VERDE = 0;
    $scope.AMARILLO = 1;
    $scope.ROJO= 2;	    	

    $scope.cotizaciones = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrecotizaciones:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.cotizaciones.selectedAll = !$scope.cotizaciones.selectedAll; 
        for (var key in $scope.cotizaciones.selectedItems) {
          $scope.cotizaciones.selectedItems[key].check = $scope.cotizaciones.selectedAll;
        }
      },
      add : function() {
        
              var coti = {
                      id: 0,
                      terceroId: 0,                      
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
          
            var modalInstance = $modal.open({
            templateUrl: 'app/views/cotizacion/cotizacion_form.html',
            controller: 'cotizacionController',
            size: 'lg',
            resolve: {
                parameters: { cotizacion: coti,vcotizaciones:$scope.cotizaciones }
            }
            });
            modalInstance.result.then(function (parameters) {
            });      
        
          
      },
      edit : function(item) {
      
          
        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/cotizacion_form.html',
          controller: 'cotizacionController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion: item,vcotizaciones:$scope.cotizaciones }
          }
        });
        modalInstance.result.then(function (parameters) {
            $scope.cotizaciones.getData();
        }); 
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            cotizacionHttp.remove({}, { id: id }, function(response) {
                $scope.cotizaciones.getData();
                message.show("success", "cotizacion eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.cotizaciones.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    cotizacionHttp.remove({}, { id: id }, function(response) {
                        $scope.cotizaciones.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.cotizaciones.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.cotizaciones.filterText,
          "precision": false
        }];
        $scope.cotizaciones.selectedItems = $filter('arrayFilter')($scope.cotizaciones.dataSource, paramFilter);
        $scope.cotizaciones.paginations.totalItems = $scope.cotizaciones.selectedItems.length;
        $scope.cotizaciones.paginations.currentPage = 1;
        $scope.cotizaciones.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.cotizaciones.paginations.currentPage == 1 ) ? 0 : ($scope.cotizaciones.paginations.currentPage * $scope.cotizaciones.paginations.itemsPerPage) - $scope.cotizaciones.paginations.itemsPerPage;
        $scope.cotizaciones.data = $scope.cotizaciones.selectedItems.slice(firstItem , $scope.cotizaciones.paginations.currentPage * $scope.cotizaciones.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.cotizaciones.data = [];
        $scope.cotizaciones.loading = true;
        $scope.cotizaciones.noData = false;      
        
            var parametros= {
              "estado": $scope.estado.current.value,
              "fechainicial": $scope.cotizaciones.formatdate( $scope.busqueda.model.fechainicial),
              "fechafinal": $scope.cotizaciones.formatdate($scope.busqueda.model.fechafinal),   
                        
          };
          
        cotizacionHttp.getList({}, parametros,function(response) {
          $scope.cotizaciones.selectedItems = response;
          $scope.cotizaciones.dataSource = response;
          for(var i=0; i<$scope.cotizaciones.dataSource.length; i++){
            $scope.cotizaciones.nombrecotizaciones.push({id: i, nombre: $scope.cotizaciones.dataSource[i]});
          }
          $scope.cotizaciones.paginations.totalItems = $scope.cotizaciones.selectedItems.length;
          $scope.cotizaciones.paginations.currentPage = 1;
          $scope.cotizaciones.changePage();
          $scope.cotizaciones.loading = false;
          ($scope.cotizaciones.dataSource.length < 1) ? $scope.cotizaciones.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      },
      formatdate(t){    
        try{ 
                var dd = t.getDate();
                var mm = t.getMonth()+1; //January is 0!

                var yyyy = t.getFullYear();
                if(dd<10){
                    dd='0'+dd;
                } 
                if(mm<10){
                    mm='0'+mm;
                } 
                //2018-01-15
                return dd+'-'+mm+'-'+yyyy;   
            }
                catch(err) {
                    return t;
                }
   
    }
    }   

    
     //ESTADOS
     $scope.estado = {
      current : {}, data:[],
      getData : function() {
          $scope.estado.data.push({value:'1', descripcion: 'En Proceso'});
          $scope.estado.data.push({value:'2', descripcion: 'Cerrado'});
          $scope.estado.data.push({value:'3', descripcion: 'Anulado'}); 
          $scope.estado.current =$scope.estado.data[0];
      }    
  }

  $scope.busqueda = {
    model : {
          fechainicial: '',
          fechafinal: ''
          
            },
      init(){

        var dt = new Date();
        dt.setMonth( dt.getMonth() - 1 );
        $scope.busqueda.model.fechainicial=dt;
        $scope.busqueda.model.fechafinal=new Date();
        $scope.cotizaciones.getData();   
      } 
    }
	//CARGAMOS DATA
          
    $scope.estado.getData();  
    $scope.busqueda.init();  
    
        
	}
  
  
  })();