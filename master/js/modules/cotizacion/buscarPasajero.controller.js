/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('buscarPasajeroController', buscarPasajeroController);

  buscarPasajeroController.$inject = ['$scope','$rootScope', '$filter', '$state', '$modalInstance', 'LDataSource', 'cotizacionHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];

  function buscarPasajeroController($scope,$rootScope, $filter, $state, $modalInstance, LDataSource, cotizacionHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {            
      
      $scope.pasajeros = {   
            paginations : {
                maxSize : 3,
                itemsPerPage : 8,
                currentPage : 0,
                totalItems : 0
              },            
              selectedAll : false,
              filterText :'',
              dataSource : [],
              nombrepasajeros:[],
              selectedItems : [],
              data : [],
              noData : false,
              loading : false,
              selectAll : function() {
                $scope.productos.selectedAll = !$scope.productos.selectedAll; 
                for (var key in $scope.productos.selectedItems) {
                  $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
                }
              },
          model:{
             
              nombre:'', 
              apellido:'', 
              identificacion:'',              
              email:'',
              telefono:'',
              direccion:''                
          },   
           filter : function() {
            var paramFilter = [{
              "key": "$",
              "value": $scope.pasajeros.filterText,
              "precision": false
            }];
            $scope.pasajeros.selectedItems = $filter('arrayFilter')($scope.pasajeros.dataSource, paramFilter);
            $scope.pasajeros.paginations.totalItems = $scope.pasajeros.selectedItems.length;
            $scope.pasajeros.paginations.currentPage = 1;
            $scope.pasajeros.changePage();
          },
          changePage : function() {
            var firstItem = ($scope.pasajeros.paginations.currentPage == 1 ) ? 0 : ($scope.pasajeros.paginations.currentPage * $scope.pasajeros.paginations.itemsPerPage) - $scope.pasajeros.paginations.itemsPerPage;
            $scope.pasajeros.data = $scope.pasajeros.selectedItems.slice(firstItem , $scope.pasajeros.paginations.currentPage * $scope.pasajeros.paginations.itemsPerPage);
          }, 
          
      getData : function() {
          
        $scope.pasajeros.data = [];
        $scope.pasajeros.loading = true;
        $scope.pasajeros.noData = false;
          
        $scope.pasajeros.model.estado=$scope.estadoPasajero.current.value;
          
        cotizacionHttp.getPasajeros({}, $scope.pasajeros.model,function(response) {
            
          $scope.pasajeros.selectedItems = response;
          $scope.pasajeros.dataSource = response;
            
          for(var i=0; i<$scope.pasajeros.dataSource.length; i++){
            $scope.pasajeros.nombrepasajeros.push({id: i, nombre: $scope.pasajeros.dataSource[i]});
          }
          $scope.pasajeros.paginations.totalItems = $scope.pasajeros.selectedItems.length;
          $scope.pasajeros.paginations.currentPage = 1;
          $scope.pasajeros.changePage();
          $scope.pasajeros.loading = false;
          ($scope.pasajeros.dataSource.length < 1) ? $scope.pasajeros.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });          
          
      },               
      close : function() {
          $modalInstance.dismiss('cancel');
      },
      send : function() {
          $modalInstance.close();
      },
      setPasajero: function(item){  
            item.polizaId=parameters.poliza.idPoliza;
            parameters.poliza.pasajeros.push(item);
            $modalInstance.close();
        }
      }
            //CARGAMOS ESTADOS
    $scope.estadoPasajero = {             
          current : {}, data:[],
        getData : function() {            
            $scope.estadoPasajero.data.push({value:'1', descripcion: 'Activos'});
            $scope.estadoPasajero.data.push({value:'0', descripcion: 'Inactivos'});          
            $scope.estadoPasajero.current =$scope.estadoPasajero.data[0];
        }          
    }
       $scope.estadoPasajero.getData(); 	
  }
})();