/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('buscarClienteController', buscarClienteController);

  buscarClienteController.$inject = ['$scope','$rootScope', '$filter', '$state', '$modalInstance', 'LDataSource', 'cotizacionHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function buscarClienteController($scope,$rootScope, $filter, $state, $modalInstance, LDataSource, cotizacionHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
            
      
      $scope.clientes = {   
            paginations : {
                maxSize : 3,
                itemsPerPage : 8,
                currentPage : 0,
                totalItems : 0
              },            
              selectedAll : false,
              filterText :'',
              dataSource : [],
              nombreclientes:[],
              selectedItems : [],
              data : [],
              noData : false,
              loading : false,
              selectAll : function() {
                $scope.productos.selectedAll = !$scope.productos.selectedAll; 
                for (var key in $scope.productos.selectedItems) {
                  $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
                }
              },
          model:{
             
              razonSocial:'', 
              identificacion:'',              
              email:'',
              telefono:'',
              tipo:-1,
              estado:-1     
          },   
           filter : function() {
            var paramFilter = [{
              "key": "$",
              "value": $scope.clientes.filterText,
              "precision": false
            }];
            $scope.clientes.selectedItems = $filter('arrayFilter')($scope.clientes.dataSource, paramFilter);
            $scope.clientes.paginations.totalItems = $scope.clientes.selectedItems.length;
            $scope.clientes.paginations.currentPage = 1;
            $scope.clientes.changePage();
          },
          changePage : function() {
            var firstItem = ($scope.clientes.paginations.currentPage == 1 ) ? 0 : ($scope.clientes.paginations.currentPage * $scope.clientes.paginations.itemsPerPage) - $scope.clientes.paginations.itemsPerPage;
            $scope.clientes.data = $scope.clientes.selectedItems.slice(firstItem , $scope.clientes.paginations.currentPage * $scope.clientes.paginations.itemsPerPage);
          }, 
          
      getData : function() {
          
        $scope.clientes.data = [];
        $scope.clientes.loading = true;
        $scope.clientes.noData = false;
          
        cotizacionHttp.getTerceros({}, $scope.clientes.model,function(response) {
          $scope.clientes.selectedItems = response;
          $scope.clientes.dataSource = response;
            
          for(var i=0; i<$scope.clientes.dataSource.length; i++){
            $scope.clientes.nombreclientes.push({id: i, nombre: $scope.clientes.dataSource[i]});
          }
          $scope.clientes.paginations.totalItems = $scope.clientes.selectedItems.length;
          $scope.clientes.paginations.currentPage = 1;
          $scope.clientes.changePage();
          $scope.clientes.loading = false;
          ($scope.clientes.dataSource.length < 1) ? $scope.clientes.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
          
          
      },               
      close : function() {
          $modalInstance.dismiss('cancel');
      },
      send : function() {
          $modalInstance.close();
      },
        setClient: function(item){
            
            parameters.cotizacion.identificacion=item.identificacion;
            parameters.cotizacion.terceroId=item.codigo;
            parameters.cotizacion.tercero=item.nombre;
            parameters.cotizacion.movil=item.telefono;
            parameters.cotizacion.direccion=item.direccion;
            parameters.cotizacion.email=item.email;
            parameters.cotizacion.crearCliente=0;
            
            
            $modalInstance.close();
        }
      }
      
  }
})();