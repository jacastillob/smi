/**=========================================================
 * Module: app.cotizacion.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .service('cotizacionHttp', cotizacionHttp);

  cotizacionHttp.$inject = ['$resource', 'END_POINT'];


  function cotizacionHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'POST',
        'isArray' : true,          
        'url' : END_POINT + '/Comercial.svc/cotizacion/busqueda'       
      },
      'read' : {
        'method' : 'GET',
        'params' : {
            id : '@id'           
          },              
        'url' : END_POINT + '/Comercial.svc/cotizacion/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/cotizacion'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/cotizacion'          
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/cotizacion/:id'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
     
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercerocotizacion'
      },
         'getIntermediarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/intermediario'
      },
         'getUniversidades' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/universidad'
      },
        'getPolizas' : {
        'method' : 'POST',        
        'url' : END_POINT + '/Web.svc/poliza'
      },'getTerceros':   
        {
            'method':'POST',
            'isArray' : true,
            'url' : END_POINT + '/Comercial.svc/tercero/busqueda'
        },'getPasajeros':   
        {
            'method':'POST',
            'isArray' : true,
            'url' : END_POINT + '/Comercial.svc/pasajero/busqueda'
        },
        'enviarCotizacionCrm' : {
        'method' : 'GET', 
        'params' : {
            codigo : '@codigo',              
        },
        'url' : END_POINT + '/Web.svc/cotizacion/crm/:codigo'
      },
        'enviarCotizacionIntermediario' : {
        'method' : 'GET',
        'params' : {
            codigo : '@codigo',              
          },
        'url' : END_POINT + '/Web.svc/cotizacion/interm/:codigo'
      },
        
    };
    return $resource( END_POINT + '/Comercial.svc/cotizacion', {}, actions, {}); 
  }

})();