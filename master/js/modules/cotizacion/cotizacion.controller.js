/**=========================================================
 * Module: app.cotizacion.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.cotizacion')
    .controller('cotizacionController', cotizacionController);

 cotizacionController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'cotizacionHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal'];   

  function cotizacionController($scope, $rootScope, $state,$modalInstance,$filter,cotizacionHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window,$modal) {      
      
     
      var cotizacion =parameters.cotizacion;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.cotizacion = {
		model : {
              id: 0,
              idRow: "",
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,            
              padreId: 0,
              crearCliente:0,              
			  observaciones: '',
			  estado: 1,			  
              fechaVencimiento: '',
              fechaInicio: '',
              fechaFin: '' ,
              cotizacionId:0, 
              polizas:[]
		},deletePoliza: function(p) {
               $scope.cotizacion.model.polizas.splice($scope.cotizacion.model.polizas.indexOf(p), 1);     
          },
      back : function() {
          parametersOfState.set({ name : 'app.cotizacion', params : { filters : {procesoSeleccionado:cotizacion.tipo}, data : []} });
        $state.go('app.cotizacion');
          
      },
      save : function() {	
            
            //if($scope.cotizacion.model.identificacion==""){message.show("warning", "Identifiación del cliente requerido");return;}
          
            if($scope.cotizacion.model.tercero==""){message.show("warning", "Razón social del cliente requerido");return;}
            if($scope.cotizacion.model.email==""){message.show("warning", "Email del cliente requerido");return; }
            if( $scope.cotizacion.model.polizas.length ==0){message.show("warning", "Debe agregar polizas");return;}  
                
            try{
                if($scope.cotizacion.model.fechaVencimiento==""){message.show("warning", "Debe establecer una fecha de vencimiento");return;}
                else{$scope.cotizacion.model.fechaVencimiento=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaVencimiento);}
            }
            catch(err) {    
            }
            
            try{                
                $scope.cotizacion.model.fechaInicio=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaInicio);
            }
            catch(err) {
            }
            try{
                
                $scope.cotizacion.model.fechaFin=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaFin);
            }
            catch(err) {
            }
            
            $scope.cotizacion.model.grupoId= $scope.grupos.current.value;
            $scope.cotizacion.model.prioridadId= $scope.prioridades.current.value;
            $scope.cotizacion.model.viaContactoId= $scope.viasContacto.current.value;
            $scope.cotizacion.model.interes= $scope.interes.current.value;
            $scope.cotizacion.model.formaPagoId= $scope.formasPago.current.value;
            $scope.cotizacion.model.monedaId= $scope.monedas.current.value;
            $scope.cotizacion.model.destino= $scope.destinos.current.value;
            $scope.cotizacion.model.origen= $scope.origenes.current.value;
            $scope.cotizacion.model.tipoViaje= $scope.tiposViaje.current.value;
          
            if($scope.universidades.current){$scope.cotizacion.model.universidadId = $scope.universidades.current.codigo;
            }else{$scope.cotizacion.model.universidadId =0;}   
          
          
            if($scope.tipoCliente.current){$scope.cotizacion.model.tipoCliente = $scope.tipoCliente.current.value;
            }else{message.show("warning", "Debe especificar el tipo de cliente");return}   
       
            if($scope.intermediarios.current){$scope.cotizacion.model.intermediarioId = $scope.intermediarios.current.codigo;
            }else{$scope.cotizacion.model.intermediarioId ='00000000-0000-0000-0000-000000000000';} 
				
            $rootScope.loadingVisible = true;
            cotizacionHttp.save({}, $scope.cotizacion.model, function (data) { 	
                       
                       
                        if ($scope.cotizacion.model.id==0){
                            message.show("success", "Cotización creada satisfactoriamente!!");
                        }
                        else{message.show("success", "Cotización actualizada satisfactoriamente!!");}
                        // $scope.cotizacion.model=data;                        
                        $scope.cotizacion.model.id=data.id;		
                        $scope.cotizacion.model.intermediarioId=data.intermediarioId;
                
                        $scope.cotizacion.model.fechaVencimiento=new Date(data.fechaVencimiento);
                        $scope.cotizacion.model.fechaInicio=new Date(data.fechaInicio);
                        $scope.cotizacion.model.fechaFin=new Date(data.fechaFin);
                        	
                        $scope.btnEnviarCotizacion=true;
                        $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });				
		
		},          
          anular: function() {   
              
            if (confirm("Desea anular la cotización?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.cotizacion.model.estado=3;//estado anulado

                try{                
                    $scope.cotizacion.model.fechaInicio=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.cotizacion.model.fechaFin=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaFin);
                }
                catch(err) {
                }
                       cotizacionHttp.save({}, $scope.cotizacion.model, function (data) {

                            $rootScope.loadingVisible = false;
                            message.show("success", "Cotización anulada satisfactoriamente!!");	
                            $scope.cotizacion.close();               


                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
          generarPedido: function() {   
              
            if (confirm("Desea crear el pedido?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.cotizacion.model.estado=4;//estado anulado

                try{                
                    $scope.cotizacion.model.fechaInicio=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.cotizacion.model.fechaFin=$scope.cotizacion.formatdate($scope.cotizacion.model.fechaFin);
                }
                catch(err) {
                }
                       cotizacionHttp.save({}, $scope.cotizacion.model, function (data) {

                            $rootScope.loadingVisible = false;
                            message.show("success", "Pedido creado satisfactoriamente!!");	
                             var modalInstance = $modal.open({
                                  templateUrl: 'app/views/pedido/pedido_form.html',
                                  controller: 'pedidoController',
                                  size: 'lg',
                                  resolve: {
                                    parameters: { pedido: data,vpedidos:null}
                                  }
                                });
                                modalInstance.result.then(function (parameters) {
                                    $scope.pedidos.getData();
                                });      
                           
                            $scope.cotizacion.close();     

                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.id,
                         referencia : 'cotizacion' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
      
      ,agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.idRow,
                         referencia : 'cotizacion' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.cotizacion.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.padreId,
                         referencia : 'cotizacion' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },          
      buscarCliente : function() { 

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion : $scope.cotizacion.model}
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },   
    verSeguimientoHistorico : function() {            
    var modalInstance = $modal.open({
      templateUrl: 'app/views/seguimiento/seguimiento.html',
      controller: 'seguimientoController',
      size: 'lg',
      resolve: {
        parameters: { id : $scope.cotizacion.model.padreId,
                     referencia : 'cotizacion' }
      }
    });
    modalInstance.result.then(function (parameters) {
    });

    }
  ,buscarPolizas : function() {  
      
            if(!$scope.cotizacion.model.fechaInicio){message.show("warning", "Fecha inicial requerida");return;}
            if(!$scope.cotizacion.model.fechaFin){message.show("warning", "Fecha final requerida");return;}               
            $scope.cotizacion.model.origen= $scope.origenes.current.value;
            $scope.cotizacion.model.destino= $scope.destinos.current.value;
            $scope.cotizacion.model.tipo= $scope.tiposViaje.current.value;
          
            var modalInstance = $modal.open({
              templateUrl: 'app/views/cotizacion/buscarPolizas.html',
              controller: 'buscarPolizaController',
              size: 'lg',
              resolve: {
                parameters: { cotizacion : $scope.cotizacion.model}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.cotizacion.model.padreId,
                         referencia : 'cotizacion' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },  
        close : function() {
        
            
          if(parameters.vcotizaciones!=null && parameters.vcotizaciones!=undefined)
            parameters.vcotizaciones.getData();
            
        $modalInstance.dismiss('cancel');
      },
        formatdate(t){    
            try{ 
                    var dd = t.getDate();
                    var mm = t.getMonth()+1; //January is 0!

                    var yyyy = t.getFullYear();
                    if(dd<10){
                        dd='0'+dd;
                    } 
                    if(mm<10){
                        mm='0'+mm;
                    } 
                    //2018-01-15
                    return yyyy+'-'+mm+'-'+dd;   
                }
                    catch(err) {
                        return t;
                    }
       
        },
      enviarCotizacion(){

                $rootScope.loadingVisible = true;   
                    
                    if($scope.cotizacion.model.intermediarioId=="00000000-0000-0000-0000-000000000000"){
                      $rootScope.loadingVisible = true;
                       cotizacionHttp.enviarCotizacionCrm({}, {codigo: $scope.cotizacion.model.id }, function(response){
                          message.show("success", "Cotización enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
                    else{

                    $rootScope.loadingVisible = true;
                     cotizacionHttp.enviarCotizacionIntermediario({}, {codigo: $scope.cotizacion.model.id }, function(response){
                          message.show("success", "Cotización enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
      },
        agregarCliente: function(data){
            
            $scope.crearCliente=true;
            $scope.cotizacion.model.crearCliente=1;
        },
          calcularDescuento:function(data){            
            
               if( data.descuento>0 && data.precioOriginal){
                   data.precio=data.precioOriginal - (data.precioOriginal* (data.descuento/100));
                   data.precioCop=data.precio*data.trm;
               }
        }
          
    }
    //TERCEROS
    $scope.clientes = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            generalHttp.getTerceros({}, {}, function(response) {                
            $scope.clientes.data = response ;              
                
               if($scope.cotizacion.model){
                    
                    $scope.clientes.current = $filter('filter')($scope.clientes.data, { id : $scope.cotizacion.model.clienteId })[0];                   
               }
                else{
                    $scope.clientes.current=$scope.Clientes.data[0];                     
                }               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.cotizacion.model.terceroId=$scope.clientes.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.cotizacion.model.terceroId=$scope.clientes.current.id;
         // $scope.contactosOp.getData();
      }
    }
    
    //VIA CONTACTO
    $scope.viasContacto = {
      current : {},
      data:[],
      getData : function() {
                $scope.viasContacto.data.push({value:1, descripcion: 'Cliente'});
                $scope.viasContacto.data.push({value:2, descripcion: 'Facebook'});     
                $scope.viasContacto.data.push({value:3, descripcion: 'E-mail'}); 
                $scope.viasContacto.data.push({value:4, descripcion: 'Directorio'});  
                $scope.viasContacto.data.push({value:5, descripcion: 'Referido'});  
                $scope.viasContacto.data.push({value:6, descripcion: 'Compra online'});  
                $scope.viasContacto.data.push({value:7, descripcion: 'Intermediario'});  
                $scope.viasContacto.data.push({value:8, descripcion: 'Google'});
                $scope.viasContacto.data.push({value:9, descripcion: 'Whatsapp'});
                $scope.viasContacto.data.push({value:10, descripcion: 'Zopim'});  
                $scope.viasContacto.data.push({value:11, descripcion: 'Ori/Volante'}); 
                $scope.viasContacto.current=$scope.viasContacto.data[0];   
      },
      setViaContacto : function(){
          $scope.cotizacion.model.viaContactoId=$scope.viasContacto.current.codigo;
    }}
    //PRIORIDADES
    $scope.prioridades = {
      current : {},
      data:[],
      getData : function() {
                $scope.prioridades.data.push({value:1, descripcion: 'NORMAL(15DIAS)'});
                $scope.prioridades.data.push({value:2, descripcion: 'ALTA(48HORAS)'});     
                $scope.prioridades.data.push({value:3, descripcion: 'CRITICA(8HORAS)'}); 
                $scope.prioridades.data.push({value:4, descripcion: 'BAJA(1MES)'});  
                $scope.prioridades.data.push({value:5, descripcion: 'BAJA(2MESES)'});  
                $scope.prioridades.data.push({value:6, descripcion: 'BAJA(3MESES)'});  
                $scope.prioridades.data.push({value:7, descripcion: 'INTERM( 6MESES)'}); 
                $scope.prioridades.current=$scope.prioridades.data[0];   
      },
      setPrioridad : function(){
          $scope.cotizacion.model.prioridadId=$scope.prioridades.current.value;
    }}
    //PORCENTAJE DE INTERES
    $scope.interes = {
      current : {},
      data:[],
      getData : function() {
                $scope.interes.data.push({value:10, descripcion: '10'});
                $scope.interes.data.push({value:20, descripcion: '20'});     
                $scope.interes.data.push({value:30, descripcion: '30'});     
                $scope.interes.data.push({value:40, descripcion: '40'});     
                $scope.interes.data.push({value:50, descripcion: '50'});     
                $scope.interes.data.push({value:60, descripcion: '60'});     
                $scope.interes.data.push({value:70, descripcion: '70'});
                $scope.interes.data.push({value:80, descripcion: '80'});
                $scope.interes.data.push({value:90, descripcion: '90'});
                $scope.interes.data.push({value:100, descripcion: '100'});
                $scope.interes.current=$scope.interes.data[0];    
      },
      setInteres : function(){
          $scope.cotizacion.model.interes=$scope.interes.current.value;
    }}
    //TIPO DE CLIENTE
    $scope.tipoCliente = {
      current : {},
      data:[],
      getData : function() {
                $scope.tipoCliente.data.push({value:1, descripcion: 'CLIENTE CON FECHA DE VIAJE'});
                $scope.tipoCliente.data.push({value:2, descripcion: 'ESTUDIANTES'});     
                $scope.tipoCliente.data.push({value:3, descripcion: 'PROMOCION'});
                $scope.tipoCliente.data.push({value:4, descripcion: 'TRAMITE DE VISA'});
                $scope.tipoCliente.current=$scope.tipoCliente.data[0];    
      },
      settipoCliente : function(){
          $scope.cotizacion.model.tipoCliente=$scope.tipoCliente.current.value;
    }}
    //GRUPO
    $scope.grupos = {
      current : {},
      data:[],
      getData : function() {
                $scope.grupos.data.push({value:2, descripcion: 'Asesores'});    
                $scope.grupos.data.push({value:1, descripcion: 'Administrativo'});                 
                $scope.grupos.data.push({value:3, descripcion: 'Lista de pólizas'});     
                $scope.grupos.data.push({value:4, descripcion: 'Servicio al Cliente'});
                $scope.grupos.current=$scope.grupos.data[0];                   
          },
      setGrupo : function(){
          $scope.cotizacion.model.grupoId=$scope.grupos.current.value;
    }}
    //MONEDA
    $scope.monedas  = {
            current: {},
            data: [],
            getData: function() {
                $scope.monedas.data.push({value:2, descripcion: 'Dolar'});
                $scope.monedas.data.push({value:4, descripcion: 'Pesos Colombianos'});    
                $scope.monedas.current=$scope.monedas.data[0];    
            }
        }
    

   
      
    $scope.formasPago  = {
            current: {},
            data: [],
            getData: function() {
                $scope.formasPago.data.push({value:0, descripcion: 'Pesos en efectivo'});
                $scope.formasPago.data.push({value:1, descripcion: 'Tarjeta de credito'});     
                $scope.formasPago.data.push({value:2, descripcion: 'Pse'});     
                $scope.formasPago.data.push({value:3, descripcion: 'Consig Bogota'});     
                $scope.formasPago.data.push({value:4, descripcion: 'Consig Bancolombia'});     
                $scope.formasPago.data.push({value:5, descripcion: 'Consig Colpatria'});     
                $scope.formasPago.data.push({value:6, descripcion: 'Cheque'});
                $scope.formasPago.data.push({value:8, descripcion: 'Dolares'});  
                $scope.formasPago.data.push({value:9, descripcion: 'Tarjeta debito'});  
                $scope.formasPago.data.push({value:10, descripcion: 'Credito'});  
                $scope.formasPago.data.push({value:11, descripcion: 'Online'});  
                $scope.formasPago.data.push({value:12, descripcion: 'Giro'});  
                $scope.formasPago.current=$scope.formasPago.data[0];    
            }
        }
      
        $scope.origenes  = {
            current: {},
            data: [],
            getData: function() {
      
                        $scope.origenes.data.push({value: 48, descripcion: 'Colombia' });
                        $scope.origenes.data.push({value: 1, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 2, descripcion: 'Argentina' });
                        $scope.origenes.data.push({value: 6, descripcion: 'Alemania' });
                        $scope.origenes.data.push({value: 247, descripcion: 'Austria' });
                        $scope.origenes.data.push({value: 12, descripcion: 'Arabia Saudita' });
                        $scope.origenes.data.push({value: 15, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 16, descripcion: 'Australia' });
                        $scope.origenes.data.push({value: 19, descripcion: 'Bahamas' });
                        $scope.origenes.data.push({value: 21, descripcion: 'Barbados' });
                        $scope.origenes.data.push({value: 25, descripcion: 'Bemudas' });
                        $scope.origenes.data.push({value: 28, descripcion: 'Bolivia' });
                        $scope.origenes.data.push({value: 31, descripcion: 'Brasil' });
                        $scope.origenes.data.push({value: 33, descripcion: 'Bulgaria' });
                        $scope.origenes.data.push({value: 37, descripcion: 'Belgica' });
                        $scope.origenes.data.push({value: 38, descripcion: 'Cabo Verde' });
                        $scope.origenes.data.push({value: 39, descripcion: 'Islas Caiman' });
                        $scope.origenes.data.push({value: 40, descripcion: 'Camboya' });
                        $scope.origenes.data.push({value: 41, descripcion: 'Camerun' });
                        $scope.origenes.data.push({value: 42, descripcion: 'Canada' });
                        $scope.origenes.data.push({value: 45, descripcion: 'Chile' });
                        $scope.origenes.data.push({value: 52, descripcion: 'Corea del Norte' });
                        $scope.origenes.data.push({value: 53, descripcion: 'Corea del Sur' });
                        $scope.origenes.data.push({value: 54, descripcion: 'Costa de Marfil' });
                        $scope.origenes.data.push({value: 55, descripcion: 'Costa Rica' });
                        $scope.origenes.data.push({value: 56, descripcion: 'Croacia' });
                        $scope.origenes.data.push({value: 57, descripcion: 'Cuba' });
                        $scope.origenes.data.push({value: 59, descripcion: 'Dinamarca' });
                        $scope.origenes.data.push({value: 61, descripcion: 'Ecuador' });
                        $scope.origenes.data.push({value: 62, descripcion: 'Egipto' });
                        $scope.origenes.data.push({value: 63, descripcion: 'El Salvador' });
                        $scope.origenes.data.push({value: 64, descripcion: 'Emiratos Arabes Unvalueos' });
                        $scope.origenes.data.push({value: 66, descripcion: 'Eslovaquia' });
                        $scope.origenes.data.push({value: 67, descripcion: 'Eslovenia' });
                        $scope.origenes.data.push({value: 68, descripcion: 'España' });
                        $scope.origenes.data.push({value: 69, descripcion: 'Estados Unvalueos' });
                        $scope.origenes.data.push({value: 70, descripcion: 'Estonia' });
                        $scope.origenes.data.push({value: 71, descripcion: 'Etiopia' });
                        $scope.origenes.data.push({value: 72, descripcion: 'Islas Feroe' });
                        $scope.origenes.data.push({value: 73, descripcion: 'Filipinas' });
                        $scope.origenes.data.push({value: 74, descripcion: 'Finlandia' });
                        $scope.origenes.data.push({value: 75, descripcion: 'Fiji' });
                        $scope.origenes.data.push({value: 76, descripcion: 'Francia'});
                        $scope.origenes.data.push({value: 83, descripcion: 'Grecia' });
                        $scope.origenes.data.push({value: 84, descripcion: 'Groenlandia' });
                        $scope.origenes.data.push({value: 86, descripcion: 'Guatemala' });
                        $scope.origenes.data.push({value: 87, descripcion: 'Guernsey' });
                        $scope.origenes.data.push({value: 88, descripcion: 'Guinea' });
                        $scope.origenes.data.push({value: 89, descripcion: 'Guinea Ecuatorial' });
                        $scope.origenes.data.push({value: 90, descripcion: 'Guinea-Bissau' });
                        $scope.origenes.data.push({value: 91, descripcion: 'Guyana' });
                        $scope.origenes.data.push({value: 92, descripcion: 'Haiti' });
                        $scope.origenes.data.push({value: 93, descripcion: 'Honduras' });
                        $scope.origenes.data.push({value: 94, descripcion: 'Hong kong' });
                        $scope.origenes.data.push({value: 95, descripcion: 'Hungria' });
                        $scope.origenes.data.push({value: 96, descripcion: 'India' });
                        $scope.origenes.data.push({value: 97, descripcion: 'Indonesia' });
                        $scope.origenes.data.push({value: 98, descripcion: 'Iraq' });
                        $scope.origenes.data.push({value: 99, descripcion: 'Irlanda' });
                        $scope.origenes.data.push({value: 100, descripcion: 'Iran' });
                        $scope.origenes.data.push({value: 101, descripcion: 'Islandia' });
                        $scope.origenes.data.push({value: 102, descripcion: 'Israel' });
                        $scope.origenes.data.push({value: 103, descripcion: 'Italia' });
                        $scope.origenes.data.push({value: 104, descripcion: 'Jamaica' });
                        $scope.origenes.data.push({value: 105, descripcion: 'Japon' });
                        $scope.origenes.data.push({value: 117, descripcion: 'Liberia' });
                        $scope.origenes.data.push({value: 118, descripcion: 'Libia' });
                        $scope.origenes.data.push({value: 120, descripcion: 'Lituania' });
                        $scope.origenes.data.push({value: 121, descripcion: 'Luxemburgo' });
                        $scope.origenes.data.push({value: 122, descripcion: 'Libano' });
                        $scope.origenes.data.push({value: 128, descripcion: 'Maldivas' });
                        $scope.origenes.data.push({value: 129, descripcion: 'Malta' });
                        $scope.origenes.data.push({value: 130, descripcion: 'Islas Malvinas' });
                        $scope.origenes.data.push({value: 131, descripcion: 'Mali' });
                        $scope.origenes.data.push({value: 134, descripcion: 'Marruecos' });
                        $scope.origenes.data.push({value: 144, descripcion: 'Mexico' });
                        $scope.origenes.data.push({value: 145, descripcion: 'Monaco' });
                        $scope.origenes.data.push({value: 146, descripcion: 'Namibia' });
                        $scope.origenes.data.push({value: 147, descripcion: 'Nauru' });
                        $scope.origenes.data.push({value: 149, descripcion: 'Nepal' });
                        $scope.origenes.data.push({value: 150, descripcion: 'Nicaragua' });
                        $scope.origenes.data.push({value: 151, descripcion: 'Nigeria' });
                        $scope.origenes.data.push({value: 152, descripcion: 'Niue' });
                        $scope.origenes.data.push({value: 154, descripcion: 'Noruega' });
                        $scope.origenes.data.push({value: 155, descripcion: 'Nueva Caledonia' });
                        $scope.origenes.data.push({value: 156, descripcion: 'Nueva Zelanda' });
                        $scope.origenes.data.push({value: 157, descripcion: 'Niger' });
                        $scope.origenes.data.push({value: 158, descripcion: 'Oman' });
                        $scope.origenes.data.push({value: 160, descripcion: 'Pakistan' });
                        $scope.origenes.data.push({value: 161, descripcion: 'Palaos' });
                        $scope.origenes.data.push({value: 162, descripcion: 'Palestina' });
                        $scope.origenes.data.push({value: 163, descripcion: 'Panama' });
                        $scope.origenes.data.push({value: 165, descripcion: 'Paraguay' });
                        $scope.origenes.data.push({value: 167, descripcion: 'Peru' });
                        $scope.origenes.data.push({value: 170, descripcion: 'Polonia' });
                        $scope.origenes.data.push({value: 171, descripcion: 'Portugal' });
                        $scope.origenes.data.push({value: 172, descripcion: 'Puerto Rico' });
                        $scope.origenes.data.push({value: 173, descripcion: 'Reino Unvalueo' });
                        $scope.origenes.data.push({value: 175, descripcion: 'Republica Checa' });
                        $scope.origenes.data.push({value: 176, descripcion: 'Republica Dominicana' });
                        $scope.origenes.data.push({value: 178, descripcion: 'Rumania' });
                        $scope.origenes.data.push({value: 179, descripcion: 'Rusia' });
                        $scope.origenes.data.push({value: 197, descripcion: 'Singapur' });
                        $scope.origenes.data.push({value: 198, descripcion: 'Siria' });
                        $scope.origenes.data.push({value: 201, descripcion: 'Sri Lanka' });
                        $scope.origenes.data.push({value: 203, descripcion: 'Sudafrica' });
                        $scope.origenes.data.push({value: 206, descripcion: 'Suecia' });
                        $scope.origenes.data.push({value: 207, descripcion: 'Suiza' });
                        $scope.origenes.data.push({value: 210, descripcion: 'Tailandia' });
                        $scope.origenes.data.push({value: 211, descripcion: 'Taiwan' });
                        $scope.origenes.data.push({value: 219, descripcion: 'Trinvaluead y Tobajo' });
                        $scope.origenes.data.push({value: 222, descripcion: 'Turquia' });
                        $scope.origenes.data.push({value: 225, descripcion: 'Ucrania' });
                        $scope.origenes.data.push({value: 227, descripcion: 'Uruguay' });
                        $scope.origenes.data.push({value: 230, descripcion: 'Ciudad del Vaticano' });
                        $scope.origenes.data.push({value: 231, descripcion: 'Venezuela' });
                        $scope.origenes.data.push({value: 239, descripcion: 'Chipre' });
                        $scope.origenes.data.push({value: 240, descripcion: 'Guam' });
                        $scope.origenes.data.push({value: 241, descripcion: 'Hawai' });
                        $scope.origenes.data.push({value: 246, descripcion: 'Boniaire' });
                        $scope.origenes.current=$scope.origenes.data[0];    
            }
        }  
      
        $scope.destinos  = {
            current: {},
            data: [],
            getData: function() {
                
                $scope.destinos.data.push({value:1, descripcion: 'Sur América'});
                $scope.destinos.data.push({value:2, descripcion: 'Centro América'});     
                $scope.destinos.data.push({value:3, descripcion: 'Norte América'});                         
                $scope.destinos.data.push({value:5, descripcion: 'Europa'});     
                $scope.destinos.data.push({value:6, descripcion: 'Asia'});     
                $scope.destinos.data.push({value:7, descripcion: 'África'}); 
                $scope.destinos.data.push({value:8, descripcion: 'Oceanía'}); 
                $scope.destinos.data.push({value:9, descripcion: 'Antillas Holandesas'}); 
                $scope.destinos.data.push({value:10, descripcion: 'Internacional'}); 
                $scope.destinos.data.push({value:11, descripcion: 'Colombia'}); 
                $scope.destinos.data.push({value:13, descripcion: 'Parques Nacionales'}); 
                $scope.destinos.data.push({value:18, descripcion: 'Costa Rica'}); 
                $scope.destinos.data.push({value:20, descripcion: 'Holanda'}); 
                
                $scope.destinos.current=$scope.destinos.data[0]; 
             }
        }
      $scope.tiposViaje  = {
            current: {},
            data: [],
            getData: function() {  
                
                $scope.tiposViaje.data.push({value:11, descripcion: 'Estudio'});
                $scope.tiposViaje.data.push({value:12, descripcion: 'Larga Estadía (más de 60 días).'});     
                $scope.tiposViaje.data.push({value:13, descripcion: 'Corta Estadia (menos de 60 días)'});     
                $scope.tiposViaje.data.push({value:14, descripcion: 'MultiViajes'});     
                $scope.tiposViaje.data.push({value:15, descripcion: 'Estudio Con Responsabilidad Civil'});     
                $scope.tiposViaje.data.push({value:16, descripcion: 'Futura Mamá'});     
                $scope.tiposViaje.data.push({value:17, descripcion: 'Atención médica por Pre-existencia'});   
                $scope.tiposViaje.data.push({value:18, descripcion: 'Seguro obligatorio Parques Nacionales'});   
                $scope.tiposViaje.current=$scope.tiposViaje.data[0];
            }
      }
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            cotizacionHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.cotizacion.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.cotizacion.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.cotizacion.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.cotizacion.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }

    //INTERMEDIARIOS
    $scope.intermediarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            cotizacionHttp.getIntermediarios({}, {}, function(response) {                
            $scope.intermediarios.data = response ;              
                
               if(cotizacion.id!=0){
                $scope.intermediarios.current = $filter('filter')($scope.intermediarios.data, { codigo : $scope.cotizacion.model.intermediarioId })[0];
                
              }                           
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setIntermediario' : function() {
        
          $scope.cotizacion.model.intermediarioId=$scope.intermediarios.current.id;
      }
    }      
    //UNIVERSIDADES
    $scope.universidades = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            cotizacionHttp.getUniversidades({}, {}, function(response) {                
            $scope.universidades.data = response ;              
                
               if($scope.cotizacion.model){                    
                    //$scope.universidades.current = $filter('filter')($scope.universidades.data, { id : $scope.cotizacion.model.universidadId })[0];                   
               }                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setUniversidad' : function() {
          $scope.cotizacion.model.universidadId=$scope.universidades.current.id;
      }
    }    

 
	
	//CARGAMOS LOS LISTADOS	
      
      
  $scope.monedas.getData();
  $scope.formasPago.getData();      
  $scope.origenes.getData();
  $scope.destinos.getData();
  $scope.tiposViaje.getData();
  $scope.universidades.getData();
  $scope.prioridades.getData();
  $scope.grupos.getData();
  $scope.interes.getData();
  $scope.viasContacto.getData();
  $scope.tipoCliente.getData();
  
      
    $scope.btnHistorico=true;
    $scope.btnAdjunto=true;
      
   
	//CARGAMOS LOS DATOS DEL cotizacion	
	
	if(cotizacion.id==0){
        
          
          $scope.btnModificar=true;
          $scope.btnAnular=false;
          $scope.intermediarios.getData();
        
    }
    else{   
        $rootScope.loadingVisible = true;    
        
        
        $scope.btnEditar=false;
        $scope.btnEnviarCotizacion=true;       
        
		
            cotizacionHttp.read({},cotizacion, function (data) { 
                
                $scope.cotizacion.model = data;	
                
                $scope.cotizacion.model.fechaVencimiento=new Date(data.fechaVencimiento);
                $scope.cotizacion.model.fechaInicio=new Date(data.fechaInicio);
                $scope.cotizacion.model.fechaFin=new Date(data.fechaFin);
                $scope.monedas.current = $filter('filter')($scope.monedas.data, { value : $scope.cotizacion.model.monedaId })[0];
                $scope.formasPago.current = $filter('filter')($scope.formasPago.data, { value : $scope.cotizacion.model.formaPagoId })[0];
                $scope.destinos.current = $filter('filter')($scope.destinos.data, { value : $scope.cotizacion.model.destino })[0];               
                $scope.origenes.current = $filter('filter')($scope.origenes.data, { value : $scope.cotizacion.model.origen })[0];               
                
                $scope.tiposViaje.current = $filter('filter')($scope.tiposViaje.data, { value : $scope.cotizacion.model.tipoViaje })[0];
                
                $scope.interes.current = $filter('filter')($scope.interes.data, { value : $scope.cotizacion.model.interes })[0];

                $scope.tipoCliente.current = $filter('filter')($scope.tipoCliente.data, { value : $scope.cotizacion.model.tipoCliente })[0];

               
                $scope.prioridades.current = $filter('filter')($scope.prioridades.data, { value : $scope.cotizacion.model.prioridadId })[0];
                $scope.grupos.current = $filter('filter')($scope.grupos.data, { value : $scope.cotizacion.model.grupoId })[0];
                $scope.viasContacto.current = $filter('filter')($scope.viasContacto.data, { value : $scope.cotizacion.model.viaContactoId })[0];
                
                $scope.universidades.current = $filter('filter')($scope.universidades.data, { codigo : $scope.cotizacion.model.universidadId })[0];
                $scope.btnModificar=true;
                $scope.btnAnular=true;
                $scope.intermediarios.getData();
                
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
        
    }
    
      

      
        
	
    
    
  }
})();