/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.recibo')
    .service('reciboHttp', reciboHttp);

  reciboHttp.$inject = ['$resource', 'END_POINT'];


  function reciboHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };      
    var actions = {
      'getList' : {
       'method' : 'GET',
        'isArray' : true,
        'params' : {           
      },
          'url' : END_POINT + '/Comercial.svc/recibo'          
      },
      
     'getEmpleados' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/recibo'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/recibo'
      }
    };
    return $resource( END_POINT + '/Comercial.svc/recibo', {}, actions, {}); 
  }

})();