/**=========================================================
 * Module: app.recibo.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.recibo')
    .controller('reciboListController', reciboListController);

  reciboListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'reciboHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function reciboListController($scope, $filter, $state,$modal, ngDialog, tpl, reciboHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
    
  	
    $scope.recibos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrerecibos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.recibos.selectedAll = !$scope.recibos.selectedAll; 
        for (var key in $scope.recibos.selectedItems) {
          $scope.recibos.selectedItems[key].check = $scope.recibos.selectedAll;
        }
      },

      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.recibos.filterText,
          "precision": false
        }];
        $scope.recibos.selectedItems = $filter('arrayFilter')($scope.recibos.dataSource, paramFilter);
        $scope.recibos.paginations.totalItems = $scope.recibos.selectedItems.length;
        $scope.recibos.paginations.currentPage = 1;
        $scope.recibos.changePage();
      },  
      changePage : function() {
        var firstItem = ($scope.recibos.paginations.currentPage == 1 ) ? 0 : ($scope.recibos.paginations.currentPage * $scope.recibos.paginations.itemsPerPage) - $scope.recibos.paginations.itemsPerPage;
        $scope.recibos.data = $scope.recibos.selectedItems.slice(firstItem , $scope.recibos.paginations.currentPage * $scope.recibos.paginations.itemsPerPage);
      },      
      getData : function() {
      $scope.recibos.data = [];
      $scope.recibos.loading = true;
      $scope.recibos.noData = false;        
          
        reciboHttp.getList({}, {},function(response) {
          $scope.recibos.selectedItems = response;
          $scope.recibos.dataSource = response;
          for(var i=0; i<$scope.recibos.dataSource.length; i++){
            $scope.recibos.nombrerecibos.push({id: i, nombre: $scope.recibos.dataSource[i]});
          }
          $scope.recibos.paginations.totalItems = $scope.recibos.selectedItems.length;
          $scope.recibos.paginations.currentPage = 1;
          $scope.recibos.changePage();
          $scope.recibos.loading = false;
          ($scope.recibos.dataSource.length < 1) ? $scope.recibos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      },
        
      add : function() {
        
        var rec = {
                      id: 0,
                      terceroId: 0,                      
                      consecutivo: '',                     
                      estado: '',
                      facturaId: 0,    
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
          
            var modalInstance = $modal.open({
            templateUrl: 'app/views/recibo/recibo_form.html',
            controller: 'reciboController',
            size: 'lg',
            resolve: {
                parameters: { recibo: rec,vrecibos:$scope.recibos }
            }
            });
            modalInstance.result.then(function (parameters) {
            });  
          
      },
      edit : function(item) {
      
        var modalInstance = $modal.open({
          templateUrl: 'app/views/recibo/recibo_form.html',
          controller: 'reciboController',
          size: 'lg',
          resolve: {
            parameters: { recibo: item,vrecibos:$scope.recibos }
          }
        });
        modalInstance.result.then(function (parameters) {
            $scope.recibos.getData();
        });       
          
      }
        
        
        
        
        
        
        
    }

	//CARGAMOS LOS TIPOS DE PROCESO
    
    $scope.recibos.getData();  

        
	}
  
  
  })();