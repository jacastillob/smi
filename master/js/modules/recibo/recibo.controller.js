/**=========================================================
 * Module: app.recibo.js
 =========================================================*/
(function() {
  'use strict';

  angular
    .module('app.recibo')
    .controller('reciboController', reciboController);

 reciboController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'reciboHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal'];   

  function reciboController($scope, $rootScope, $state,$modalInstance,$filter,reciboHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window,$modal) {      
      
     
      var recibo =parameters.recibo;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.recibo = {
		model : {
              id: 0,
              terceroId: 0,                      
              consecutivo: '',                     
              estado: '',
              facturaId: 0,    
              fechaAct: '',
              usuarioAct: '',
              fechaReg: '',
              usuarioReg: ''
		},
      recibos:[],
      reciboSeleccionada:undefined,
      deleteRecibo: function(p) {
           $scope.recibo.model.recibos.splice($scope.recibo.model.recibos.indexOf(p), 1);     
      },
      back : function() {
          parametersOfState.set({ name : 'app.recibo', params : { filters : {procesoSeleccionado:recibo.tipo}, data : []} });
        $state.go('app.recibo');
          
      },        
      save : function() {		
          
            
            if($scope.recibo.model.identificacion==""){message.show("warning", "Identifiación del cliente requerido");return;}
            if($scope.recibo.model.tercero==""){message.show("warning", "Razón social del cliente requerido");return;}
            if($scope.recibo.model.email==""){message.show("warning", "Email del cliente requerido");return; }
            if($scope.recibo.model.polizas.length ==0){message.show("warning", "Debe agregar polizas");return;}  
                
         
            try{                
                $scope.recibo.model.fechaInicio=$scope.recibo.formatdate($scope.recibo.model.fecha);
            }
            catch(err) {
            }
          
           try{
                
                $scope.recibo.model.fechaEntrega=$scope.recibo.formatdate($scope.recibo.model.fechaEntrega);
            }
            catch(err) {
                $scope.recibo.model.fechaEntrega='';
            }
            
				
            $rootScope.loadingVisible = true;
          
            reciboHttp.save({}, $scope.recibo.model, function (data) { 	
                       
                
                        if ($scope.recibo.model.id==0){
                            message.show("success", "Recibo creado satisfactoriamente!!");
                        }
                        else{message.show("success", "Recibo actualizado satisfactoriamente!!");}
                        
                
                        $scope.pedido.model.id=data.id;		
                        $scope.pedido.model.intermediarioId=data.intermediarioId;
                        	
                        $scope.btnEnviarCotizacion=true;
                        $scope.crearCliente=false;
                        $scope.pedido.model.crearCliente=0;                        
                                     
                        $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });				
		
		},          
          anular: function() {   
              
            if (confirm("Desea anular el recibo?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.recibo.model.estado=3;//estado anulado

                try{                
                    $scope.recibo.model.fechaInicio=$scope.recibo.formatdate($scope.recibo.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.recibo.model.fechaFin=$scope.recibo.formatdate($scope.recibo.model.fechaFin);
                }
                catch(err) {
                }
                       reciboHttp.save({}, $scope.recibo.model, function (data) {

                            $rootScope.loadingVisible = false;
                            message.show("success", "Recibo anulado satisfactoriamente!!");	
                            $scope.recibo.close();               


                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
       
  
         
  
        close : function() {
        if(parameters.vrecibos!=null && parameters.vrecibos!=undefined)
            parameters.vrecibos.getData();
            
        $modalInstance.dismiss('cancel');
      },
        formatdate(t){    

            //var today = new Date();
            var dd = t.getDate();
            var mm = t.getMonth()+1; //January is 0!

            var yyyy = t.getFullYear();
            if(dd<10){
                dd='0'+dd;
            } 
            if(mm<10){
                mm='0'+mm;
            } 
            //2018-01-15
            return yyyy+'-'+mm+'-'+dd;         
       
        },
        
       buscarCliente : function() { 

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion : $scope.recibo.model}
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }, 
      
    }      
      
      
    $scope.tipoPagos  = {
            current: {},
            data: [],
            getData: function() {
                $scope.tipoPagos.data.push({value:0, descripcion: 'Tarjeta VISA'});
                $scope.tipoPagos.data.push({value:1, descripcion: 'Tarjeta MASTERCARD'});
                $scope.tipoPagos.data.push({value:2, descripcion: 'Tarjeta Diners'});
                $scope.tipoPagos.data.push({value:3, descripcion: 'Tarjeta AMERICAN'});
                $scope.tipoPagos.data.push({value:4, descripcion: 'Pse'});     
                $scope.tipoPagos.data.push({value:5, descripcion: 'Consig Bogota'});     
                $scope.tipoPagos.data.push({value:6, descripcion: 'Consig Bancolombia'});     
                $scope.tipoPagos.data.push({value:7, descripcion: 'Consig Colpatria'});     
                $scope.tipoPagos.data.push({value:8, descripcion: 'Cheque'});
                $scope.tipoPagos.current=$scope.tipoPagos.data[0];    
            }
        }
      
        $scope.ciudades  = {
            current: {},
            data: [],
            getData: function() {
                $scope.ciudades.data.push({value:0, descripcion: 'Bogotá D.C'});
                $scope.ciudades.data.push({value:1, descripcion: 'Medellín'});     
                $scope.ciudades.data.push({value:2, descripcion: 'Barranquilla'});     
                $scope.ciudades.data.push({value:3, descripcion: 'Calí'});     
                $scope.ciudades.data.push({value:4, descripcion: 'Bucaramanga'});      
                $scope.ciudades.data.push({value:5, descripcion: 'Santa Marta'});  
                $scope.ciudades.current=$scope.ciudades.data[0];    
            }
        }
      debugger
    //EMPLEADOS
    $scope.empleados = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            reciboHttp.getEmpleados({}, {}, function(response) {                
            $scope.empleados.data = response ;              
                
               if($scope.recibo.model){
                                     
               }                           
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setEmpleado' : function() {
          $scope.recibo.model.empleadoId=$scope.empleados.current.id;
      }
     } 
	
	//CARGAMOS LOS LISTADOS	
      
    $scope.tipoPagos.getData();
    $scope.ciudades.getData();
    $scope.empleados.getData();
   

	//CARGAMOS LOS DATOS DEL RECIBO	
	
	if(recibo.id==0){
          $scope.btnModificar=true;
          $scope.btnAnular=false;        
    }
    else{   
        $rootScope.loadingVisible = true;  
        $scope.btnEditar=false;
        $scope.btnEnviarCotizacion=true;
        
        
		
            reciboHttp.read({},pedido, function (data) { 
                
            $scope.recibo.model = data;	                

            $scope.recibo.model.fecha=new Date(data.fecha);

            $scope.tipoPagos.current = $filter('filter')($scope.tipoPagos.data, { value : $scope.pedido.model.tipoPagoId })[0];
            $scope.ciudades.current = $filter('filter')($scope.ciudades.data, { value : $scope.pedido.model.ciudadId })[0];



            $scope.btnModificar=true;
            $scope.btnAnular=true;
                
                
                
                
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
        
    }
      

        
    
  }
})();