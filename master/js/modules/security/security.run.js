(function() {
  'use strict';

  angular
    .module('app.security')
    .run(securityRun);

  securityRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$q', 'tokenManager', 'securityHttp', 'message'];

  function securityRun($rootScope, $state, $stateParams, $window, $q, tokenManager, securityHttp, message) {

    // Hook not found
    $rootScope.$on('$stateNotFound',
                   function(event, unfoundState/*, fromState, fromParams*/) {
      console.log(unfoundState.to); // "lazy.state"
      console.log(unfoundState.toParams); // {a:1, b:2}
      console.log(unfoundState.options); // {inherit:false} + default options
    });
    // Hook error
    $rootScope.$on('$stateChangeError',
                   function(event, toState, toParams, fromState, fromParams, error){
      console.log(error);
    });
    // Hook success

    var LAST_STATE = '';
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        
      if ((toState.name == 'page.recover' && fromState.name == 'page.login') || toState.name == 'page.resetpassword') {
      } else {
        if ((tokenManager.get() == undefined && toState.name != 'page.login') || (tokenManager.get() == '' && toState.name != 'page.login')) {
          if (LAST_STATE != toState.name) {
            event.preventDefault();  
            refreshToken().then(function() {
              LAST_STATE = 'app.home';
              $state.go('app.home');
            }, function() {
              $state.go('page.login');
            });
          }
        };        
      }
    });



    function refreshToken() {
      var defer = $q.defer();
      if (!tokenManager.get() && tokenManager.getRefreshToken()) {
        $rootScope.updatingToken = true;
        var param = { refreshToken : tokenManager.getRefreshToken() };
        tokenManager.resetToken();
        securityHttp.refreshToken({}, param, function(token) {
          tokenManager.set(token);
          $rootScope.updatingToken = false;
          defer.resolve(true);
        }, function(faild) {
          message.show("error", "No fue posible ingresar, con credenciales actuales");
          $rootScope.updatingToken = false;
          defer.reject();
        }); 
      } else {
        defer.reject();
      }
      return defer.promise;
    }
  }

})();