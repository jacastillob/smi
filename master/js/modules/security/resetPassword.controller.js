/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('ResetPasswordController', ResetPasswordController);

  ResetPasswordController.$inject = ['$scope', '$rootScope', '$state', '$location', 'securityHttp', 'tokenManager', 'message'];


  function ResetPasswordController($scope, $rootScope, $state, $location, securityHttp, tokenManager, message) {


    $scope.reset = {
        model : {
            token : '',
            password : '',
            repeatPassword: ''
        },
      resetPassword : function() {
        if ($scope.reset.model.password == $scope.reset.model.repeatPassword) {
        debugger;
        $rootScope.loadingVisible = true;
        tokenManager.set({ token : $scope.reset.model.token });
        securityHttp.confirmPassword({}, { password : $scope.reset.model.password }, function(response) {
          $state.go('page.login');
          $rootScope.loadingVisible = false;
          tokenManager.resetToken();
          message.show('info', 'Su contraseña ha sido actualizada exitosamente.');
        }, function(faild) {
          tokenManager.resetToken();
          $state.go('page.recover');
          message.show('error', faild.Message);
          $rootScope.loadingVisible = false;
        })
          
        } else {
          message.show("info", "Las claves no corresponden");
          $scope.reset.model.password = '';
          $scope.reset.model.repeatPassword = '';          
        }
      }
    }
    
    $scope.reset.model.token = $location.url().split("/")[3];

       
      
  }

})();