/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$scope', '$rootScope', '$state', 'securityHttp', 'tokenManager', 'message'];


  function LoginController($scope, $rootScope, $state, securityHttp, tokenManager, message) {

    (tokenManager.getRefreshToken()) ? $state.go('app.home') : null;
      
      

      
    $scope.login = {
      account : {
        user : '',
        password : ''
      },
      login : function() {
        var params = {
          user : $scope.login.account.user,
          pass : $scope.login.account.password
        };

        $rootScope.loadingVisible = true;

        securityHttp.login({}, params, function(response) {
          $rootScope.perfil = response.perfil;
          tokenManager.set(response);
          $state.go('app.home');
          $rootScope.loadingVisible = false;
        }, function(faild) {
          message.show('error', 'Usuario o Clave incorrecta');
          $rootScope.loadingVisible = false;
        })


        $rootScope.$on('refresh_token', function(event, param) {
          if ($rootScope.updatingToken == false) {
            $rootScope.updatingToken = true;
            securityHttp.refreshToken({}, param, function(token) {
              tokenManager.set(token);
              $rootScope.updatingToken = false;
            }, function(faild) {
              message.show('error', "No fue posible ingresar, con credenciales actuales");
              $rootScope.updatingToken = false;
            });
          }
        })

      }
    }

  }

})();