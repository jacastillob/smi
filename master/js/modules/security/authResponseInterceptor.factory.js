(function() {
  'use strict';
  angular
    .module('app.security')
    .service('authResponseInterceptor', authResponseInterceptor);

  authResponseInterceptor.$inject = ['$q', '$timeout', '$location',  '$rootScope', '$window', 'tokenManager', 'message'];


  function authResponseInterceptor($q, $timeout, $location,  $rootScope, $window, tokenManager, message) {
    return {
      response: function(response) {
        return response;
      },
      responseError: function (response) {
        var INVALID_TOKEN =  1;
        if(response.status == 0) {
          if ($rootScope.updatingToken == false) {
            message.show("warning", "No fue posible conectarse al servidor");
          }
          return $q.reject(response.status);
        } else if (response.data.ErrorCode !== undefined) {
          if (response.data.ErrorCode === INVALID_TOKEN && !$rootScope.updatingToken){  
            tokenManager.resetToken();
            if ($rootScope.updatingToken == false) {
              message.show("error", "No fue posible conectarse al servidor");
            }
            $location.path('page/login');
            return $q.reject(null);
          } else {
            return $q.reject(response.data);
          }
        } else if (response.data !== undefined) {
          return $q.resolve(response.data);
        } else {
          return $q.resolve(response);
        }
      }
    }
  }
  
})();