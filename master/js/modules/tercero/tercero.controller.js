/**=========================================================
 * Module: app.tercero.js
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.tercero')
    .controller('terceroController', terceroController);

  terceroController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', '$filter', 'terceroHttp', 'parameters', 'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window', '$modal'];

  function terceroController($scope, $rootScope, $state, $modalInstance, $filter, terceroHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window, $modal) {

    var tercero = parameters.tercero;
    var currentDate = new Date();

    $scope.tercero = {
      model: {
        id: 0,
        identificacion: '',
        nombre: '',
        telefono: '',
        direccion: '',
        correo: '',
        tipo: 0,
        moneda: '',
        sexo: '',
        tipoEmpresa: '',
        clasificacion: '',
        estado: '',
        fechaAct: '',
        usuarioAct: '',
        fechaReg: '',
        usuarioReg: '',
        comision: ''
      },
      fechaReg: {
        isOpen: false,
        value: '',
        dateOptions: {
          formatYear: 'yy',
          startingDay: 1
        },
        open: function ($event) {
          $scope.tercero.fechaReg.isOpen = false;
        }
      },
      fechaAct: {
        isOpen: false,
        value: '',
        dateOptions: {
          formatYear: 'yy',
          startingDay: 1
        },
        open: function ($event) {
          $scope.tercero.fechaAct.isOpen = false;
        }
      },
      back: function () {
        parametersOfState.set({ name: 'app.tercero', params: { filters: { estadoSeleccionado: tercero.estado }, data: [] } });
        $state.go('app.tercero');
      },
      save: function () {

        if ($scope.tercero.model.nombre == "") { message.show("warning", "Nombre requerido"); return; }
        if ($scope.tercero.model.identificacion == "") { message.show("warning", "Identificación requerido"); return; }
        if ($scope.tercero.model.direccion == "") { message.show("warning", "Dirección requerido"); return; }

        try {
          $scope.tercero.model.estado = $scope.estados.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.sexo = $scope.sexos.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.clasificacion = $scope.clasificaciones.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.moneda = $scope.monedas.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.tipoEmpresa = $scope.tipoEmpresas.current.value;
        }
        catch (err) {
        }
        try {
          $scope.tercero.model.tipo = $scope.tipos.current.value;
        }
        catch (err) {
        }
        //INSERTAR
        if ($scope.tercero.model.id == 0) {

          $rootScope.loadingVisible = true;
          terceroHttp.save({}, $scope.tercero.model, function (data) {

            terceroHttp.read({}, { id: data.id }, function (data) {
              $scope.tercero.model = data;
              message.show("success", "Cliente creado satisfactoriamente!!");
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });

          }, function (faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });

        }
        //ACTUALIZAR
        else {
          if (!$scope.tercero.model.id) {
            $state.go('app.tercero');
          } else {

            $rootScope.loadingVisible = true;
            terceroHttp.update({}, $scope.tercero.model, function (data) {
              $rootScope.loadingVisible = false;
              $scope.tercero.model = data;

              message.show("success", "Cliente actualizado satisfactoriamente");


            }, function (faild) {
              message.show("error", faild.Message);
              $rootScope.loadingVisible = false;
            });

          }
        }
      },
      uploadFile: function () {
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: {
              id: $scope.tercero.model.id,
              referencia: 'TERCERO'
            }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
      seguimiento: function () {
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: {
              id: $scope.tercero.model.id,
              referencia: 'TERCERO'
            }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
      close: function () {
        $modalInstance.dismiss('cancel');
      }
    }

    $scope.contactoTercero = {

      model: {
        id: 0,
        nombre: '',
        telefono: '',
        ext: '',
        email: '',
        terceroId: $scope.tercero.model.id
      },
      current: {

        id: 0,
        nombre: '',
        telefono: '',
        ext: '',
        email: '',
        comision: false,
        cotizacion: false,
        activo: false,
        terceroId: $scope.tercero.model.id
      },
      data: [],
      getData: function () {
        if (tercero) {

          if ($scope.tercero.model.id != 0) {
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
            terceroHttp.getContactosTercero({}, { terceroId: $scope.tercero.model.id }, function (response) {
              $scope.contactoTercero.data = $filter('orderBy')(response, 'nombre');
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });

          }
        }
      },
      save: function () {

        //INSERTAR
        if ($scope.tercero.model.id == 0) { message.show("warning", "Debe guardar primero el cliente"); return; }
        if ($scope.contactoTercero.current.nombre == "") { message.show("warning", "Debe especificar el nombre"); return; }
        if ($scope.contactoTercero.current.email == "") { message.show("warning", "Debe especificar el email"); return; }

        $rootScope.loadingVisible = true;
        $scope.contactoTercero.current.terceroId = $scope.tercero.model.id;
        if ($scope.contactoTercero.current.id == 0) {

          terceroHttp.addContactoTercero({}, $scope.contactoTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.contactoTercero.current = {
              id: 0,
              nombre: '',
              telefono: '',
              ext: '',
              email: '',
              comision: false,
              cotizacion: false,
              activo: false,
              terceroId: $scope.tercero.model.id
            };

            $scope.contactoTercero.getData();
            message.show("success", "Contacto actualizado satisfactoriamente");


          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
        else if ($scope.contactoTercero.current.id > 0) {

          terceroHttp.editContactoTercero({}, $scope.contactoTercero.current, function (data) {
            $rootScope.loadingVisible = false;
            $scope.contactoTercero.current = {
              id: 0,
              nombre: '',
              telefono: '',
              ext: '',
              email: '',
              comision: false,
              cotizacion: false,
              activo: false,
              terceroId: $scope.tercero.model.id
            };
            $scope.contactoTercero.getData();
            message.show("success", "Contacto actualizado satisfactoriamente");
          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
      },
      delete: function (item) {

        $rootScope.loadingVisible = true;
        terceroHttp.deleteContactoTercero({}, { contactoTerceroId: item.id }, function (response) {
          $scope.contactoTercero.getData();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", faild.Message);
        });
      },
      edit: function (item) {
        $scope.contactoTercero.current = item;
      },
      cancel: function (item) {
        $scope.contactoTercero.current = {};
      }
    }

    $scope.cuentaTercero = {

      model: {
        id: 0,
        banco: 1,
        tipoCuenta: 'AHORROS',
        numero: '',
        cedula: '',
        descripcion: '',
        activo: false,
        terceroId: $scope.tercero.model.id
      },
      current: {

        id: 0,
        banco: 1,
        tipoCuenta: 'AHORROS',
        numero: '',
        cedula: '',
        descripcion: '',
        activo: false,
        terceroId: $scope.tercero.model.id
      },
      data: [],
      getData: function () {
        if (tercero) {

          if ($scope.tercero.model.id != 0) {
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
            terceroHttp.getCuentasTercero({}, { terceroId: $scope.tercero.model.id }, function (response) {
              $scope.cuentaTercero.data = $filter('orderBy')(response, 'descripcion');
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });

          }
        }
      },
      save: function () {

        //INSERTAR
        if ($scope.tercero.model.id == 0) { message.show("warning", "Debe guardar primero el cliente"); return; }
        if ($scope.cuentaTercero.current.numero == "") { message.show("warning", "Debe especificar el número"); return; }
        if ($scope.cuentaTercero.current.descripcion == "") { message.show("warning", "Debe especificar la descripción"); return; }

        $scope.cuentaTercero.current.banco = $scope.bancos.current.value;
        $scope.cuentaTercero.current.tipoCuenta = $scope.tiposCuenta.current.value;

        $rootScope.loadingVisible = true;
        $scope.cuentaTercero.current.terceroId = $scope.tercero.model.id;
        if ($scope.cuentaTercero.current.id == 0) {

          terceroHttp.addCuentaTercero({}, $scope.cuentaTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.cuentaTercero.current = {
              id: 0,
              banco: 1,
              tipoCuenta: 'AHORROS',
              numero: '',
              cedula: '',
              descripcion: '',
              activo: false,
              terceroId: $scope.tercero.model.id
            };

            $scope.cuentaTercero.getData();

            message.show("success", "Cuenta actualizada satisfactoriamente");


          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });

        }
        else if ($scope.cuentaTercero.current.id > 0) {

          terceroHttp.editCuentaTercero({}, $scope.cuentaTercero.current, function (data) {
            $rootScope.loadingVisible = false;
            $scope.cuentaTercero.current = {
              id: 0,
              banco: 1,
              tipoCuenta: 'AHORROS',
              numero: '',
              cedula: '',
              descripcion: '',
              activo: false,
              terceroId: $scope.tercero.model.id
            };

            $scope.cuentaTercero.getData();

            message.show("success", "Cuenta actualizada satisfactoriamente");
          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
      },
      delete: function (item) {

        $rootScope.loadingVisible = true;
        terceroHttp.deleteCuentaTercero({}, { cuentaTerceroId: item.id }, function (response) {

          $scope.cuentaTercero.getData();
          $rootScope.loadingVisible = false;

        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", faild.Message);
        });
      },
      edit: function (item) {
        $scope.cuentaTercero.current = item;
        $scope.bancos.current = $filter('filter')($scope.bancos.data, { value: $scope.cuentaTercero.current.banco })[0];
        $scope.tiposCuenta.current = $filter('filter')($scope.tiposCuenta.data, { value: $scope.cuentaTercero.current.tipoCuenta })[0];

      },
      cancel: function (item) {
        $scope.cuentaTercero.current = {};
      }
    }
    //REFERIDOS
    $scope.referidoTercero = {

      current: {
        id: 0,
        identificacion: '',
        terceroId: 0,
        tercero: '',
        movil: '',
        direccion: '',
        email: '',
        porcentaje: 0,
        intermediarioId: $scope.tercero.model.id
      },
      data: [],
      getData: function () {
        if (tercero) {

          if ($scope.tercero.model.id != 0) {
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
            terceroHttp.getReferidosTercero({}, { terceroId: $scope.tercero.model.id }, function (response) {
              $scope.referidoTercero.data = $filter('orderBy')(response, 'nombre');
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });
          }
        }
      }, buscar: function () {

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion: $scope.referidoTercero.current }
          }
        });
        modalInstance.result.then(function (parameters) {
        });

      },
      save: function () {

        //INSERTAR                    
        if ($scope.referidoTercero.current.tercero == "") { message.show("warning", "Debe especificar el intermediario"); return; }
        if ($scope.referidoTercero.current.porcentaje == 0) { message.show("warning", "Debe especificar el porcentaje"); return; }

        $rootScope.loadingVisible = true;
        $scope.referidoTercero.current.intermediarioId = $scope.tercero.model.id;
        if ($scope.referidoTercero.current.id == 0) {

          terceroHttp.addReferidoTercero({}, $scope.referidoTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.referidoTercero.current = {

              id: 0,
              identificacion: '',
              terceroId: 0,
              tercero: '',
              movil: '',
              direccion: '',
              email: '',
              porcentaje: 0,
              intermediarioId: $scope.tercero.model.id

            };
            $scope.referidoTercero.getData();
            message.show("success", "Referido actualizado satisfactoriamente");

          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
        else if ($scope.referidoTercero.current.id > 0) {

          terceroHttp.editReferidoTercero({}, $scope.referidoTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.referidoTercero.current = {
              id: 0,
              identificacion: '',
              terceroId: 0,
              tercero: '',
              movil: '',
              direccion: '',
              email: '',
              porcentaje: 0,
              intermediarioId: $scope.tercero.model.id
            };
            $scope.referidoTercero.getData();
            message.show("success", "Referido actualizado satisfactoriamente");

          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }

      },
      delete: function (item) {

        $rootScope.loadingVisible = true;
        terceroHttp.deleteReferidoTercero({}, { referidoTerceroId: item.id }, function (response) {
          $scope.referidoTercero.getData();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", faild.Message);
        });
      },
      edit: function (item) {
        $scope.referidoTercero.current = item;
      },
      cancel: function (item) {
        $scope.referidoTercero.current = {

          id: 0,
          identificacion: '',
          terceroId: 0,
          tercero: '',
          movil: '',
          direccion: '',
          email: '',
          porcentaje: 0,
          intermediarioId: $scope.tercero.model.id

        };
      }
    }

    //ASEGURADORA
    $scope.aseguradora = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        terceroHttp.getAseguradora({}, {}, function(response) {
            $scope.aseguradora.data = $filter('orderBy')(response, 'idRow');
            $scope.aseguradora.current = $scope.aseguradora.data[0];
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      set: function(){
        
      }
    }

    $scope.comisionTercero = {

      current: {
        id: '',
        nombreAseguradora: $scope.aseguradora.current.tercero,
        comision: 0,
        idAseguradora:$scope.aseguradora.current.idRow,
        idEmpresa: $scope.tercero.model.id
      },
      data: [],
      getData: function () {
        if (tercero) {

          if ($scope.tercero.model.id != 0) {
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
            debugger;
            terceroHttp.getComisionesTercero({}, { terceroId: $scope.tercero.model.id }, function (response) {
              $scope.comisionTercero.data = $filter('orderBy')(response, 'nombreAseguradora');
              $rootScope.loadingVisible = false;
            }, function (faild) {
              $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
            });
          }
        }
      },
      save: function () {

        //INSERTAR                    
         if ($scope.comisionTercero.current.comision == 0) { message.show("warning", "Debe especificar la comision"); return; }

        $rootScope.loadingVisible = true;
    
        $scope.comisionTercero.current.idEmpresa = $scope.tercero.model.id;
        $scope.comisionTercero.current.idAseguradora= $scope.aseguradora.current.idRow;
        $scope.comisionTercero.current.nombreAseguradora= $scope.aseguradora.current.tercero;

        if ($scope.comisionTercero.current.id == '') {

          terceroHttp.addComisionTercero({}, $scope.comisionTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            $scope.comisionTercero.current= {
              id: '',
              nombreAseguradora: $scope.aseguradora.current.tercero,
              comision: 0,
              idAseguradora:$scope.aseguradora.current.idRow,
              idEmpresa: $scope.tercero.model.id
            };
            $scope.comisionTercero.getData();
            message.show("success", "Comision actualizada satisfactoriamente");

          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }
        else {

          terceroHttp.editComisionTercero({}, $scope.comisionTercero.current, function (data) {
            $rootScope.loadingVisible = false;

            
            $scope.comisionTercero.current = {
              id: '',
              nombreAseguradora: $scope.aseguradora.current.tercero,
              comision: 0,
              idAseguradora:$scope.aseguradora.current.idRow,
              idEmpresa: $scope.tercero.model.id
            };

            $scope.comisionTercero.getData();
            message.show("success", "Comision actualizado satisfactoriamente");

          }, function (faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
          });
        }

      },
      delete: function (item) {

        $rootScope.loadingVisible = true;
        terceroHttp.deleteComisionTercero({}, { comisionTerceroId: item.id }, function (response) {
          $scope.comisionTercero.getData();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", faild.Message);
        });
      },
      edit: function (item) {
        $scope.comisionTercero.current = item;
        $scope.aseguradora.current = $filter('filter')($scope.aseguradora.data, { idRow : $scope.comisionTercero.current.idAseguradora })[0];
      },
      cancel: function (item) {

        $scope.aseguradora.current = $scope.aseguradora.data[0];

        $scope.comisionTercero.current = {
          id: '',
          nombreAseguradora: $scope.aseguradora.current.tercero,
          comision: 0,
          idAseguradora:$scope.aseguradora.current.idRow,
          idEmpresa: $scope.tercero.model.id
        };
        
      }
    }
    //TAB
    $scope.showTabs = function () {
      if ($scope.tercero.id == 0)
        return false;
      else
        return true;
    }
    //ESTADOS
    $scope.estados = {
      current: {}, data: [],
      getData: function () {
        $scope.estados.data.push({ value: '0', descripcion: 'Inactivo' });
        $scope.estados.data.push({ value: '1', descripcion: 'Activo' });
        $scope.estados.data.push({ value: '2', descripcion: 'Eliminado' });
      }
    }
    //SEXOS
    $scope.sexos = {
      current: {}, data: [],
      getData: function () {
        $scope.sexos.data.push({ value: 'M', descripcion: 'Masculino' });
        $scope.sexos.data.push({ value: 'F', descripcion: 'Femenino' });
        $scope.sexos.data.push({ value: 'E', descripcion: 'Empresa' });
      }
    }
    //CONTRIBUYENTES
    $scope.clasificaciones = {
      current: {}, data: [],
      getData: function () {
        $scope.clasificaciones.data.push({ value: '1', descripcion: 'Gran Contribuyente' });
        $scope.clasificaciones.data.push({ value: '2', descripcion: 'Empresa del Estado' });
        $scope.clasificaciones.data.push({ value: '3', descripcion: 'Régimen Común' });
        $scope.clasificaciones.data.push({ value: '4', descripcion: 'Régimen Simplificado' });
        $scope.clasificaciones.data.push({ value: '5', descripcion: 'Reg/Sim no residente' });
        $scope.clasificaciones.data.push({ value: '6', descripcion: 'No residente en país' });
        $scope.clasificaciones.data.push({ value: '7', descripcion: 'No responsable IVA' });

      }
    }
    //MONEDAS
    $scope.monedas = {
      current: {}, data: [],
      getData: function () {
        $scope.monedas.data.push({ value: '2', descripcion: 'Dolar' });
        $scope.monedas.data.push({ value: '4', descripcion: 'Pesos Colombianos' });
      }
    }
    //TIPO EMPRESAS
    $scope.tipoEmpresas = {
      current: {}, data: [],
      getData: function () {
        $scope.tipoEmpresas.data.push({ value: '3', descripcion: 'Venta Directa' });
        $scope.tipoEmpresas.data.push({ value: '5', descripcion: 'Corredores de Seguros' });
        $scope.tipoEmpresas.data.push({ value: '7', descripcion: 'Venta Web' });
        $scope.tipoEmpresas.data.push({ value: '8', descripcion: 'Intermediarios' });
        $scope.tipoEmpresas.data.push({ value: '9', descripcion: 'Cliente Intermediario' });
      }
    }
    //TIPO
    $scope.tipos = {
      current: {}, data: [],
      getData: function () {
        $scope.tipos.data.push({ value: 'N', descripcion: 'Natural' });
        $scope.tipos.data.push({ value: 'J', descripcion: 'Jurídica' });
      }
    }
    //BANCOS
    $scope.bancos = {
      current: {}, data: [],
      getData: function () {
        $scope.bancos.data.push({ value: '1', descripcion: 'BANCO DE BOGOTA' });
        $scope.bancos.data.push({ value: '2', descripcion: 'BANCOLOMBIA' });
        $scope.bancos.data.push({ value: '3', descripcion: 'CITI BANK' });
        $scope.bancos.data.push({ value: '4', descripcion: 'BANCO COLPATRIA' });
        $scope.bancos.data.push({ value: '5', descripcion: 'DAVIVIENDA' });
        $scope.bancos.data.push({ value: '6', descripcion: 'BANCO CAJA SOCIAL' });
        $scope.bancos.data.push({ value: '7', descripcion: 'BANCO AV VILLAS' });
        $scope.bancos.data.push({ value: '8', descripcion: 'BANCO HELM BANCO' });
        $scope.bancos.data.push({ value: '9', descripcion: 'HELM BANK' });
        $scope.bancos.data.push({ value: '10', descripcion: 'BANCO OCCIDENTE ' });
        $scope.bancos.data.push({ value: '11', descripcion: 'SUDAMERIS' });
        $scope.bancos.data.push({ value: '12', descripcion: 'BBVA' });
        $scope.bancos.data.push({ value: '13', descripcion: 'BANCOOMEVA' });
        $scope.bancos.data.push({ value: '14', descripcion: 'CORPBANCA' });
      }
    }
    //TIPO CUENTA
    $scope.tiposCuenta = {
      current: {}, data: [],
      getData: function () {
        $scope.tiposCuenta.data.push({ value: 'AHORROS', descripcion: 'AHORROS' });
        $scope.tiposCuenta.data.push({ value: 'CORRIENTE', descripcion: 'CORRIENTE' });
      }
    }

    

    //CARGAMOS LOS LISTADOS	
    $scope.estados.getData();
    $scope.sexos.getData();
    $scope.clasificaciones.getData();
    $scope.monedas.getData();
    $scope.tipoEmpresas.getData();
    $scope.tipos.getData();
    $scope.tiposCuenta.getData();
    $scope.bancos.getData();
    $scope.aseguradora.getData();

    //LIMPIAMOS EL TERCERO

    if (tercero.id == 0) {

      $scope.estados.current = $scope.estados.data[0];
      $scope.sexos.current = $scope.sexos.data[0];
      $scope.clasificaciones.current = $scope.clasificaciones.data[0];
      $scope.monedas.current = $scope.monedas.data[0];
      $scope.tipoEmpresas.current = $scope.tipoEmpresas.data[0];
      $scope.tipos.current = $scope.tipos.data[0];
      $scope.contactoTercero.data = [];
      $scope.referidoTercero.data = [];
      $scope.comisionTercero.data = [];

    }
    //CARGAMOS EL TERCERO
    else {
      $rootScope.loadingVisible = true;
      $scope.divContactoTercero = true;
      terceroHttp.read({}, { id: tercero.id }, function (data) {

        $scope.tercero.model = data;
        if ($scope.tercero.model.fechaReg) { $scope.tercero.fechaReg.value = new Date(parseFloat($scope.tercero.model.fechaReg)); }
        if ($scope.tercero.model.fechaAct) { $scope.tercero.fechaAct.value = new Date(parseFloat($scope.tercero.model.fechaAct)); }



        $scope.estados.current = $filter('filter')($scope.estados.data, { value: $scope.tercero.model.estado })[0];
        $scope.sexos.current = $filter('filter')($scope.sexos.data, { value: $scope.tercero.model.sexo })[0];
        $scope.clasificaciones.current = $filter('filter')($scope.clasificaciones.data, { value: $scope.tercero.model.clasificacion })[0];
        $scope.monedas.current = $filter('filter')($scope.monedas.data, { value: $scope.tercero.model.moneda })[0];
        $scope.tipoEmpresas.current = $filter('filter')($scope.tipoEmpresas.data, {
          value:
            $scope.tercero.model.tipoEmpresa
        })[0];
        $scope.tipos.current = $filter('filter')($scope.tipos.data, { value: $scope.tercero.model.tipo })[0];

        $scope.contactoTercero.getData();
        $scope.referidoTercero.getData();
        $scope.comisionTercero.getData();
        $scope.cuentaTercero.getData();

        $rootScope.loadingVisible = false;
      }, function (faild) {
        $rootScope.loadingVisible = false;
        message.show("error", faild.Message);
      });
    }

  }
})();