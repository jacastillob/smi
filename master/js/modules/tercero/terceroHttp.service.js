/**=========================================================
 * Module: app.tercero.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .service('terceroHttp', terceroHttp);

  terceroHttp.$inject = ['$resource', 'END_POINT'];


  function terceroHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {     
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/tercero/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/tercero/:id'
      },
      'getList' : {
        'method' : 'POST',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/busqueda'          
      },
      'getContactosTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
      'addContactoTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/contacto'
      },
      'editContactoTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/contacto'
      },
      'deleteContactoTercero': {
        'method':'DELETE',
        'params' : {
            contactoTerceroId : '@contactoTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/contacto/:contactoTerceroId'
      },
      
      'getCuentasTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/cuenta'
      },
      'addCuentaTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/cuenta'
      },
      'editCuentaTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/cuenta'
      },
      'deleteCuentaTercero': {
        'method':'DELETE',
        'params' : {
            cuentaTerceroId : '@cuentaTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/cuenta/:cuentaTerceroId'
      },

      'getReferidosTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/referido'
      },
      'addReferidoTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/referido'
      },
      'editReferidoTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/referido'
      },
      'deleteReferidoTercero': {
        'method':'DELETE',
        'params' : {
            referidoTerceroId : '@referidoTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/referido/:referidoTerceroId'
      },
      'getComisionesTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/comision'
      },
      'addComisionTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/comision'
      },
      'editComisionTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/comision'
      },
      'deleteComisionTercero': {
        'method':'DELETE',
        'params' : {
            comisionTerceroId : '@comisionTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/comision/:comisionTerceroId'
      },
      'getAseguradora': {
          'method' : 'GET',
          'isArray' : true,
          'url' : END_POINT + '/Comercial.svc/comision/aseguradora'
      },
      
      




    };
    return $resource( END_POINT + '/Comercial.svc/tercero', {}, actions, {}); 
  }

})();