/**=========================================================
 * Module: app.tercero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('terceroListController', terceroListController);

  terceroListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl','$modal', 'terceroHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function terceroListController($scope, $filter, $state, ngDialog, tpl,$modal, terceroHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	$scope.ESTADO_INACTIVO = '0';
    $scope.ESTADO_ACTIVO = '1';
    $scope.ESTADO_ELIMINADO = '2';
	//$rootScope.loadingVisible = true;


    $scope.terceros = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreterceros:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.terceros.selectedAll = !$scope.terceros.selectedAll; 
        for (var key in $scope.terceros.selectedItems) {
          $scope.terceros.selectedItems[key].check = $scope.terceros.selectedAll;
        }
      },
      add : function() {
          var terc = {
              codigo: 0,
              id: 0,
              identificacion: '', 
              nombre: '', 
              nombreComercial: '', 
			  telefono: '',
			  direccion: '',
			  correo: '',
              tipoId: 0,
			  estado: '',			  
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''			 
          };
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/tercero_form.html',
        controller: 'terceroController',
        size: 'lg',
        resolve: {
            parameters: { tercero: terc }
        }
        });
        modalInstance.result.then(function (parameters) {
        });      
      },
      edit : function(item) {
          
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/tercero_form.html',
        controller: 'terceroController',
        size: 'lg',
        resolve: {
            parameters: { tercero: item }
        }
        });
        modalInstance.result.then(function (parameters) {
        });          
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            terceroHttp.remove({}, { id: id }, function(response) {
                $scope.terceros.getData();
                message.show("success", "Tercero eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.terceros.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    terceroHttp.remove({}, { id: id }, function(response) {
                        $scope.terceros.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.terceros.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.terceros.filterText,
          "precision": false
        }];
        $scope.terceros.selectedItems = $filter('arrayFilter')($scope.terceros.dataSource, paramFilter);
        $scope.terceros.paginations.totalItems = $scope.terceros.selectedItems.length;
        $scope.terceros.paginations.currentPage = 1;
        $scope.terceros.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.terceros.paginations.currentPage == 1 ) ? 0 : ($scope.terceros.paginations.currentPage * $scope.terceros.paginations.itemsPerPage) - $scope.terceros.paginations.itemsPerPage;
        $scope.terceros.data = $scope.terceros.selectedItems.slice(firstItem , $scope.terceros.paginations.currentPage * $scope.terceros.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.terceros.data = [];
        $scope.terceros.loading = true;
        $scope.terceros.noData = false;
          
        var parametros= {
            "estado":$scope.estadoTercero.current.value,
            "tipo":$scope.tipoTercero.current.value,
            "codigo":$scope.busquedaTercero.model.codigo,   
            "razonSocial":$scope.busquedaTercero.model.razonSocial,
            "identificacion":$scope.busquedaTercero.model.identificacion,
            "email":$scope.busquedaTercero.model.email,
            "direccion":$scope.busquedaTercero.model.direccion,
            "telefono":$scope.busquedaTercero.model.telefono,
            "intermediario":$scope.busquedaTercero.model.intermediario            
        };
          
        terceroHttp.getList({}, parametros,function(response) {
          $scope.terceros.selectedItems = response;
          $scope.terceros.dataSource = response;
          for(var i=0; i<$scope.terceros.dataSource.length; i++){
            $scope.terceros.nombreterceros.push({id: i, nombre: $scope.terceros.dataSource[i]});
          }
          $scope.terceros.paginations.totalItems = $scope.terceros.selectedItems.length;
          $scope.terceros.paginations.currentPage = 1;
          $scope.terceros.changePage();
          $scope.terceros.loading = false;
          ($scope.terceros.dataSource.length < 1) ? $scope.terceros.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }       
    //CARGAMOS PROCESOS
    $scope.estadoTercero = {             
          current : {}, data:[],
        getData : function() {
            $scope.estadoTercero.data.push({value:'-1', descripcion: 'Todos'});
            $scope.estadoTercero.data.push({value:'1', descripcion: 'Activos'});
            $scope.estadoTercero.data.push({value:'0', descripcion: 'Inactivos'});
            $scope.estadoTercero.data.push({value:'2', descripcion: 'Eliminados'});
            $scope.estadoTercero.current =$scope.estadoTercero.data[0];
        }          
    }
    //CARGAMOS PROCESOS
    $scope.tipoTercero = {             
          current : {}, data:[],
        getData : function() {
            $scope.tipoTercero.data.push({value:'-1', descripcion: 'Todos'});
            $scope.tipoTercero.data.push({value:'3', descripcion: 'Venta Directa'});
            $scope.tipoTercero.data.push({value:'7', descripcion: 'Venta Web'});
            $scope.tipoTercero.data.push({value:'8', descripcion: 'Intermediarios'});
            $scope.tipoTercero.data.push({value:'9', descripcion: 'Cliente Intermediario'});
            $scope.tipoTercero.current =$scope.tipoTercero.data[0];
        }          
    }   
    
    $scope.busquedaTercero = {
    model : {
          codigo: '',
          razonSocial: '',
          identificacion: '',
          email: '',
          direccion: '',
          telefono: '',
          intermediario:''
            } 
    }
	//CARGAMOS LOS LISTADOS
    $scope.estadoTercero.getData();
    $scope.tipoTercero.getData();
    $scope.terceros.getData();
        
        
        
        
	}
  
  
  })();