/**=========================================================
 * Module: app.trm.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.trm')
    .controller('trmListController', trmListController);

  trmListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'trmHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function trmListController($scope, $filter,$state,$modal, ngDialog, tpl, trmHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        
		
    $scope.trms = {
      model : {        
        trm : 0    
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombretrms:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.trms.selectedAll = !$scope.trms.selectedAll; 
        for (var key in $scope.trms.selectedItems) {
          $scope.trms.selectedItems[key].check = $scope.trms.selectedAll;
        }
      },
      add : function() {
          
          debugger;
        
           if (confirm("Desea actualizar la Trm?")) {
               if($scope.trms.model.trm==""){message.show("warning", "Trm requerida");return;}

                  trmHttp.save({}, $scope.trms.model, function (data) { 	

                    message.show("success", "Trm actualizada satisfactoriamente!!");
                    $rootScope.loadingVisible = false;
                    $scope.trms.getData();

                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", faild.Message);
                });	
           }
          
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.trms.filterText,
          "precision": false
        }];
        $scope.trms.selectedItems = $filter('arrayFilter')($scope.trms.dataSource, paramFilter);
        $scope.trms.paginations.totalItems = $scope.trms.selectedItems.length;
        $scope.trms.paginations.currentPage = 1;
        $scope.trms.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.trms.paginations.currentPage == 1 ) ? 0 : ($scope.trms.paginations.currentPage * $scope.trms.paginations.itemsPerPage) - $scope.trms.paginations.itemsPerPage;
        $scope.trms.data = $scope.trms.selectedItems.slice(firstItem , $scope.trms.paginations.currentPage * $scope.trms.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.trms.data = [];
        $scope.trms.loading = true;
        $scope.trms.noData = false;        
          
        trmHttp.getList({}, {},function(response) {
          $scope.trms.selectedItems = response;
          $scope.trms.dataSource = response;
          for(var i=0; i<$scope.trms.dataSource.length; i++){
            $scope.trms.nombretrms.push({id: i, nombre: $scope.trms.dataSource[i]});
          }
          $scope.trms.paginations.totalItems = $scope.trms.selectedItems.length;
          $scope.trms.paginations.currentPage = 1;
          $scope.trms.changePage();
          $scope.trms.loading = false;
          ($scope.trms.dataSource.length < 1) ? $scope.trms.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }   
	//CARGAMOS DATA
    $scope.trms.getData();         
   
        
	}
  
  
  })();