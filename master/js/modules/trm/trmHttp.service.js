/**=========================================================
 * Module: app.trm.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.trm')
    .service('trmHttp', trmHttp);

  trmHttp.$inject = ['$resource', 'END_POINT'];


  function trmHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,          
        'url' : END_POINT + '/General.svc/trm'          
      },     
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/trm'
      }        
    };
    return $resource( END_POINT + '/General.svc/trm', {}, actions, {}); 
  }

})();