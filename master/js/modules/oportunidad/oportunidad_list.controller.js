/**=========================================================
 * Module: app.oportunidad.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('oportunidadListController', oportunidadListController);

  oportunidadListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'oportunidadHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function oportunidadListController($scope, $filter,$state,$modal, ngDialog, tpl, oportunidadHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        
		
	$scope.VERDE = 0;
    $scope.AMARILLO = 1;
    $scope.ROJO= 2;	    	

    $scope.oportunidades = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreoportunidades:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.oportunidades.selectedAll = !$scope.oportunidades.selectedAll; 
        for (var key in $scope.oportunidades.selectedItems) {
          $scope.oportunidades.selectedItems[key].check = $scope.oportunidades.selectedAll;
        }
      },
      add : function() {
        
        if($scope.tipoProceso.current.value){        
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: $scope.tipoProceso.current.value,
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.oportunidad_add', params : { oportunidad: oport } });
            $state.go('app.oportunidad_add');
          
          }
          else{
              
              message.show("warning", "Debe seleccionar el tipo de proceso!!")
          }
          
      },
      edit : function(item) {
      
          
        var modalInstance = $modal.open({
          templateUrl: 'app/views/oportunidad/oportunidad_form.html',
          controller: 'oportunidadController',
          size: 'lg',
          resolve: {
            parameters: { oportunidad: item }
          }
        });
        modalInstance.result.then(function (parameters) {
        }); 
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            oportunidadHttp.remove({}, { id: id }, function(response) {
                $scope.oportunidades.getData();
                message.show("success", "Oportunidad eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.oportunidades.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    oportunidadHttp.remove({}, { id: id }, function(response) {
                        $scope.oportunidades.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.oportunidades.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.oportunidades.filterText,
          "precision": false
        }];
        $scope.oportunidades.selectedItems = $filter('arrayFilter')($scope.oportunidades.dataSource, paramFilter);
        $scope.oportunidades.paginations.totalItems = $scope.oportunidades.selectedItems.length;
        $scope.oportunidades.paginations.currentPage = 1;
        $scope.oportunidades.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.oportunidades.paginations.currentPage == 1 ) ? 0 : ($scope.oportunidades.paginations.currentPage * $scope.oportunidades.paginations.itemsPerPage) - $scope.oportunidades.paginations.itemsPerPage;
        $scope.oportunidades.data = $scope.oportunidades.selectedItems.slice(firstItem , $scope.oportunidades.paginations.currentPage * $scope.oportunidades.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.oportunidades.data = [];
        $scope.oportunidades.loading = true;
        $scope.oportunidades.noData = false;        
          
        oportunidadHttp.getList({}, {},function(response) {
          $scope.oportunidades.selectedItems = response;
          $scope.oportunidades.dataSource = response;
          for(var i=0; i<$scope.oportunidades.dataSource.length; i++){
            $scope.oportunidades.nombreoportunidades.push({id: i, nombre: $scope.oportunidades.dataSource[i]});
          }
          $scope.oportunidades.paginations.totalItems = $scope.oportunidades.selectedItems.length;
          $scope.oportunidades.paginations.currentPage = 1;
          $scope.oportunidades.changePage();
          $scope.oportunidades.loading = false;
          ($scope.oportunidades.dataSource.length < 1) ? $scope.oportunidades.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }   
	//CARGAMOS DATA
    $scope.oportunidades.getData();         
   
        
	}
  
  
  })();