/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .service('oportunidadHttp', oportunidadHttp);

  oportunidadHttp.$inject = ['$resource', 'END_POINT'];


  function oportunidadHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            proceso : '@proceso'           
          },
          'url' : END_POINT + '/Comercial.svc/oportunidad'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      },
       'getProductos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/productoBusqueda'
      },
       'getProductosOportunidad' : {
       'params' : {
            oportunidadId : '@oportunidadId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:oportunidadId/producto'
      },
        'addProductoOportunidad': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
        'editProductoOportunidad': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
      'removeProductoOportunidad':  {
        'params' : {
            oportunidadId : '@oportunidadId',
            productoId : '@productoId'   
          },
        'method':'DELETE',        
        'url' : END_POINT + '/Comercial.svc/oportunidad/:oportunidadId/producto/:productoId'
      }
     
    };
    return $resource( END_POINT + '/Comercial.svc/oportunidad', {}, actions, {}); 
  }

})();