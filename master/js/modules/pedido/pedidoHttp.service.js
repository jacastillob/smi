/**=========================================================
 * Module: app.pedido.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pedido')
    .service('pedidoHttp', pedidoHttp);

  pedidoHttp.$inject = ['$resource', 'END_POINT'];


  function pedidoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'POST',
        'isArray' : true,
          'params' : {
            estado : '@estado'           
          },
          'url' : END_POINT + '/Comercial.svc/pedidos'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/pedido/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/pedido'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/pedido'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },      
      'deletepasajero':  {
        'method':'DELETE',
        'params' : {
          id : '@id'           
        },
        'url' : END_POINT + '/Comercial.svc/pedido/pasajero/:id'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceropedido'
      },
         'getIntermediarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/intermediario'
      },
         'getUniversidades' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/universidad'
      },
     
    };
    return $resource( END_POINT + '/Comercial.svc/pedido', {}, actions, {}); 
  }

})();