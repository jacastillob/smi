/**=========================================================
 * Module: app.pedido.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pedido')
    .controller('pedidoListController', pedidoListController);

  pedidoListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'pedidoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function pedidoListController($scope, $filter,$state,$modal, ngDialog, tpl, pedidoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        
		
	  $scope.VERDE = 0;
    $scope.AMARILLO = 1;
    $scope.ROJO= 2;	    	

    $scope.pedidos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrepedidos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.pedidos.selectedAll = !$scope.pedidos.selectedAll; 
        for (var key in $scope.pedidos.selectedItems) {
          $scope.pedidos.selectedItems[key].check = $scope.pedidos.selectedAll;
        }
      },
      add : function() {
        
        var ped = {
                      id: 0,
                      terceroId: 0,                      
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
          
            var modalInstance = $modal.open({
            templateUrl: 'app/views/pedido/pedido_form.html',
            controller: 'pedidoController',
            size: 'lg',
            resolve: {
                parameters: { pedido: ped,vpedidos:$scope.pedidos }
            }
            });
            modalInstance.result.then(function (parameters) {
            });  
          
      },
      edit : function(item) {
      
        var modalInstance = $modal.open({
          templateUrl: 'app/views/pedido/pedido_form.html',
          controller: 'pedidoController',
          size: 'lg',
          resolve: {
            parameters: { pedido: item,vpedidos:$scope.pedidos }
          }
        });
        modalInstance.result.then(function (parameters) {
            $scope.pedidos.getData();
        });       
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            pedidoHttp.remove({}, { id: id }, function(response) {
                $scope.pedidos.getData();
                message.show("success", "pedido eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.pedidos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    pedidoHttp.remove({}, { id: id }, function(response) {
                        $scope.pedidos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.pedidos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.pedidos.filterText,
          "precision": false
        }];
        $scope.pedidos.selectedItems = $filter('arrayFilter')($scope.pedidos.dataSource, paramFilter);
        $scope.pedidos.paginations.totalItems = $scope.pedidos.selectedItems.length;
        $scope.pedidos.paginations.currentPage = 1;
        $scope.pedidos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.pedidos.paginations.currentPage == 1 ) ? 0 : ($scope.pedidos.paginations.currentPage * $scope.pedidos.paginations.itemsPerPage) - $scope.pedidos.paginations.itemsPerPage;
        $scope.pedidos.data = $scope.pedidos.selectedItems.slice(firstItem , $scope.pedidos.paginations.currentPage * $scope.pedidos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.pedidos.data = [];
        $scope.pedidos.loading = true;
        $scope.pedidos.noData = false;        
        
        pedidoHttp.getList({}, {estado:$scope.estados.current.value},function(response) {
          $scope.pedidos.selectedItems = response;
          $scope.pedidos.dataSource = response;
          for(var i=0; i<$scope.pedidos.dataSource.length; i++){
            $scope.pedidos.nombrepedidos.push({id: i, nombre: $scope.pedidos.dataSource[i]});
          }
          $scope.pedidos.paginations.totalItems = $scope.pedidos.selectedItems.length;
          $scope.pedidos.paginations.currentPage = 1;
          $scope.pedidos.changePage();
          $scope.pedidos.loading = false;
          ($scope.pedidos.dataSource.length < 1) ? $scope.pedidos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
    
    
 //VIA CONTACTO
 $scope.estados = {
  current : {},
  data:[],
  getData : function() {
            $scope.estados.data.push({value:1, descripcion: 'En Proceso'});
            $scope.estados.data.push({value:2, descripcion: 'Cerrada'});     
            $scope.estados.data.push({value:3, descripcion: 'Anulada'}); 
            
            $scope.estados.current=$scope.estados.data[0];   
        
  }
 }



  //CARGAMOS DATA
    $scope.estados.getData();     
    $scope.pedidos.getData();
        
   
        
	}
  
  
  })();