/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .service('agendaHttp', agendaHttp);

  agendaHttp.$inject = ['$resource', 'END_POINT'];


  function agendaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getAgendas : {
        method : 'POST',
        isArray : true
      },
      getAgenda : {
        method : 'GET',
        url : END_POINT + '/General.svc/agenda/:id'
      },
      removeAgenda : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/agenda/:id'
      },
      addAgenda: {
        method:'POST',
        url : END_POINT + '/General.svc/addAgenda'
      },
      editAgenda: {
        method:'PUT',
        url : END_POINT + '/General.svc/editAgenda'
      },
      removeAgenda: {
        method:'DELETE',
        params : {
            agendaId : '@agendaId'           
          },
        url : END_POINT + '/General.svc/agenda/:agendaId'
      },
     getFuncionarios : {
        method : 'GET',
        isArray : true,
        url : END_POINT + '/General.svc/funcionarioBusqueda'
      },        
        'getTipoAgenda' : {
        'method' : 'GET',
        'isArray' : true,
         'params' : {
            tipoAgenda : '@tipoAgenda'           
          },        
        'url' : END_POINT + '/General.svc/tipoAgenda/:tipoAgenda'
      },
        'getListaAgendas' : {
        'method': 'POST',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/listaAgenda'
      } 
        
        
    };

    return $resource(END_POINT + '/General.svc/agenda', paramDefault, actions);
  }

})();
