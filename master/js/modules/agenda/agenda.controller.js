/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaController', agendaController);

  agendaController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'agendaHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal','$filter'];


  function agendaController($scope, $rootScope, $state, $modalInstance, agendaHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window, $modal,$filter) {    
      
    $scope.agenda = {      
      model : {        
        id : "",        
        referenciaId : "",        
        asunto : '',
        descripcion : '',
        fechaInicial: '',
        fechaFinal: '',
        estado: '',          
        fechaAct: '',
        usuarioAct: '',
        fechaReg: '',
        usuarioReg: '',
        responsable:''
        
      },        
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1          
        },
        open : function($event) {
          $scope.agenda.fechaInicial.isOpen = true;
        }
      },
        horaInicial : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        horaFinal : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agenda.fechaFinal.isOpen = true;
        }
      },        
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.agenda.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.agenda.dataSource, paramFilter);
        $scope.agenda.paginations.totalItems = $scope.agenda.selectedItems.length;
        $scope.agenda.paginations.currentPage = 1;
        $scope.agenda.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.agenda.paginations.currentPage == 1 ) ? 0 : ($scope.agenda.paginations.currentPage * $scope.agenda.paginations.itemsPerPage) - $scope.agenda.paginations.itemsPerPage;
        $scope.agenda.data = $scope.agenda.selectedItems.slice(firstItem , $scope.agenda.paginations.currentPage * $scope.agenda.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.agenda.model.referenciaId,
          tipoReferencia : $scope.agenda.model.tipoReferencia
        };
        $scope.agenda.getData(params);
      },
      setData : function(data) {
        $scope.agenda.selectedItems = data;
        $scope.agenda.dataSource = data;
        $scope.agenda.paginations.totalItems = $scope.agenda.selectedItems.length;
        $scope.agenda.paginations.currentPage = 1;
        $scope.agenda.changePage();
        $scope.agenda.noData = false;
        $scope.agenda.loading = false;
        ($scope.agenda.dataSource.length < 1) ? $scope.agenda.noData = true : null;
      },
      getData : function(params) {
        $scope.agenda.data = [];
        $scope.agenda.loading = true;
        $scope.agenda.noData = false;

        agendaHttp.getAgendas({}, params, function(response) {
          $scope.agenda.setData(response);
        })
      },
      editar : function(id) {
        var params = {
          id : id 
        };
          
        agendaHttp.getAgenda({}, params, function(response) {
            $scope.agenda.model=response;
            
            
            $scope.modoEdicion=true;
               
			if($scope.agenda.model.fechaInicial){
                
                $scope.agenda.fechaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                $scope.agenda.horaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
            }    
			if($scope.agenda.model.fechaFinal){
                
                $scope.agenda.fechaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                $scope.agenda.horaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
            }  
            
            $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
            $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.agenda.model.responsable })[0];
            
        }, function() {
          message.show("error", "Ocurrio un error al intentar cargar la agenda");
          $rootScope.loadingVisible = false;
        }) 
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;

        agendaHttp.removeDocumento(params, function(response) {
          $scope.agenda.refresh();
          message.show("info", "Adjunto eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },    
      close : function() {
        $modalInstance.dismiss('cancel');
      }, seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        save : function() {
       
         
            if($scope.agenda.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
            else{$scope.agenda.model.fechaInicial=Date.parse(new Date($scope.agenda.fechaInicial.value));}	

            if($scope.agenda.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
            else{$scope.agenda.model.fechaFinal=Date.parse(new Date($scope.agenda.fechaFinal.value));}	
            
            if($scope.agenda.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
            if($scope.agenda.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
            
            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.agenda.model.estado= $scope.estados.current.value;}
            
            if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;}
            else{$scope.agenda.model.responsable= $scope.funcionarios.current.id;}
            
            $scope.agenda.model.referenciaId = parameters.id;
            
            var _http = new agendaHttp($scope.agenda.model);
             if(  $scope.agenda.model.id==0){
                 
                  _http.$addAgenda(function(response){                  
                      message.show("success", "Agenda creada satisfactoriamente!!");           
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editAgenda(function(response){                  
                      message.show("success", "Agenda actualizada satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }
            
            $scope.agenda.cancel();
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.funcionarios.current=$scope.funcionarios.data[0]; 
            
      },
        cancel : function(){
            
             var   model = {        
                id : 0,              
                asunto : '',
                tipoId : 0,
                descripcion : '',
                fechaInicial: '',
                referenciaId : parameters.id,
                fechaFinal: '',
                estado: '',          
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''
            };
            
           $scope.agenda.fechaInicial.value='';
           $scope.agenda.horaInicial.value='';
            
            $scope.agenda.model=model;
            $scope.modoEdicion= false;
            $scope.agenda.refresh();
            
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.funcionarios.current=$scope.funcionarios.data[0]; 
            
        }
    }    
      //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            
            $scope.estados.data.push({value:1, descripcion: 'Abierta'});
            $scope.estados.data.push({value:0, descripcion: 'Cerrada'});           
            
            if(!$scope.agenda.model){$scope.estados.current = $scope.estados.data[0];}
            else{$scope.estados.current = $filter('filter')($scope.estados.data, { id : $scope.agenda.model.estado })[0];}
                        
        }        
    }
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;             
            agendaHttp.getFuncionarios({}, {}, function(response) {
            
                $scope.funcionarios.data = response;                    
                $scope.funcionarios.current=$scope.funcionarios.data[0];

            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
        setFuncionario : function(){
            
        }
    }        
    $scope.funcionarios.getData();
    $scope.estados.getData();
      
    $scope.modoEdicion= false;
    $scope.agenda.model.referenciaId = parameters.id;
    $scope.agenda.model.tipoReferencia = parameters.referencia;
      
      
    $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
       
    //CARGAMOS LOS DATOS
     $scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;

    $scope.agenda.refresh();

  }

})();