/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .service('contratoHttp', contratoHttp);

  contratoHttp.$inject = ['$resource', 'END_POINT'];


  function contratoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getList' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/contrato/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/contrato'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/contrato'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/contrato/:id'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'getTipoContrato' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/TIPOCONTRATO'
      },
        'getObjetoContrato' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/OBJETOCONTRATO'
      },
        'getCargos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/CARGO'
      },
       'getFuncionarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/funcionarioBusqueda'
      },
        'getParticipantesContrato': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            contratoId : '@contratoId'           
          },
          'url' : END_POINT + '/Cliente.svc/contrato/:contratoId/participante'
      },
      'addParticipanteContrato': {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/contrato/participante'
      },
      'editParticipanteContrato': {
        'method':'PUT',
        'url' : END_POINT + '/Cliente.svc/contrato/participante'
      },
      'deleteParticipanteContrato': {
        'method':'DELETE',
        'params' : {
            participanteContratoId : '@participanteContratoId'           
          },
        'url' : END_POINT + '/Cliente.svc/contrato/participante/:participanteContratoId'
      }
        
     
    };
    return $resource( END_POINT + '/Cliente.svc/contrato', {}, actions, {}); 
  }

})();