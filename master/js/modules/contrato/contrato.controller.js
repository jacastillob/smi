/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoController', contratoController);

  contratoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'contratoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function contratoController($scope, $filter, $state, LDataSource, contratoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var contrato = $state.params.contrato;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.contrato = {
		model : {
              id: 0,
              terceroId: 0,
              tipoContratoId: 0,            
              objetoId: 0,
              descripcion: '',
              fechaInicial: '',
              horaInicial: '',
              fechaFinal: '',
              estado: '',
              consecutivo: '',
              fechaAct: '',
              usuarioAct: '',
              fechaReg: '',
              usuarioReg: '',                        
              longitude: 0,
              latitude: 0
		}, 
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaInicial.isOpen = true;
        }
      },  
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaFinal.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.contrato', params : { filters : {procesoSeleccionado:contrato.tipo}, data : []} });
        $state.go('app.contrato');
          
      },
      save : function() {		  
       
		  
          
          if($scope.contrato.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{$scope.contrato.model.fechaInicial=Date.parse(new Date($scope.contrato.fechaInicial.value));}	
          
             if($scope.contrato.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{$scope.contrato.model.fechaFinal=Date.parse(new Date($scope.contrato.fechaFinal.value));}	
          	
          
                if(!$scope.tercerosCon.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.contrato.model.terceroId = $scope.tercerosCon.current.id;}
          
          		if($scope.contrato.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				if($scope.contrato.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.contrato.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoContrato.current){message.show("warning", "Tipo contrato requerido");return;}
				else{$scope.contrato.model.tipoContratoId= $scope.tipoContrato.current.codigo;}	
          
                if(!$scope.objetoContrato.current){message.show("warning", "Objeto contrato requerido");return;}
				else{$scope.contrato.model.objetoId= $scope.objetoContrato.current.codigo;}	
          
          
			//INSERTAR
            if($scope.contrato.model.id==0){
				
				$rootScope.loadingVisible = true;
				contratoHttp.save({}, $scope.contrato.model, function (data) { 
					
						contratoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.contrato.model=data;
							contrato.id=$scope.contrato.model.id; 
							if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}    
							if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));} 
                            
                            if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
							if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));} 	
                            
								
							message.show("success", "contrato creada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.contrato.model.id){
					$state.go('app.contrato');
				}else{
					
					contratoHttp.update({}, $scope.contrato.model, function (data) {                           
					  $rootScope.loadingVisible = false;
					       $scope.contrato.model=data;
						if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}    
						if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));}   		
                        
                        if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
                        if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));} 	
                        
                            
					   message.show("success", "contrato actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
          seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
          
        
      }
    //TIPO CONTRATO
    $scope.tipoContrato = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getTipoContrato({}, {}, function(response) {
            $scope.tipoContrato.data = $filter('orderBy')(response, 'codigo');
            if(!$scope.contrato.model){$scope.tipoContrato.current = $scope.tipoContrato.data[0];}
            else{$scope.tipoContrato.current = $filter('filter')($scope.tipoContrato.data, { codigo : $scope.contrato.model.tipoContratoId })[0];}
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setTipo : function(){
          $scope.contrato.model.tipoContratoId=$scope.tipoContrato.current.codigo;
    }}
    
    
    //OBJETO CONTRATO
    $scope.objetoContrato = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getObjetoContrato({}, {}, function(response) {
            $scope.objetoContrato.data = $filter('orderBy')(response, 'codigo');
            if(!$scope.contrato.model){$scope.objetoContrato.current = $scope.objetoContrato.data[0];}
            else{$scope.objetoContrato.current = $filter('filter')($scope.objetoContrato.data, { codigo : $scope.contrato.model.objetoId })[0];}
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setObjeto : function(){
          $scope.contrato.model.objetoId=$scope.objetoContrato.current.codigo;
    }}
    
    
$scope.participanteContrato = {
    model : {  
            id: 0,
            funcionarioId: 0,
            contratoId: 0,
            cargoId: 0,
            funcionario: '',
            cargo: ''		
		},
      current : {},
      data:[],
      getData : function() {
          if(contrato){
              
            if($scope.contrato.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              contratoHttp.getParticipantesContrato({}, { contratoId: $scope.contrato.model.id }, function(response) {
                  $scope.participanteContrato.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },
       add : function() {    
           
        var parameter = {
              participanteContrato : {              
                        id: 0,
                        funcionarioId: 0,
                        cargoId: 0,
                        contratoId: $scope.contrato.model.id,
                        funcionario: '',
                        cargo: ''
              }            
        };

        var modalInstance = $modal.open({
          templateUrl: 'app/views/contrato/participanteContrato_edit.html',
          controller: 'participanteContratoEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {
             $scope.participanteContrato.getData();
        });
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          contratoHttp.deleteParticipanteContrato({}, {participanteContratoId: item.id}, function(response){
            $scope.participanteContrato.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
          
        var parameter = {
          participanteContrato : item
        };
        var modalInstance = $modal.open({
        templateUrl: 'app/views/contrato/participanteContrato_edit.html',
          controller: 'participanteContratoEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {
            
             $scope.participanteContrato.getData();
        });
      }       
    }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'E', descripcion: 'Ejecución'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'});             
   
        }        
    } 
        //TERCEROS
    $scope.tercerosCon = {
      current : {}, data:[],
      getData : function() {
        $rootScope.loadingVisible = true;
            contratoHttp.getTerceros({}, {}, function(response) {  
            $scope.tercerosCon.data = response ;              
                
           if($scope.contrato.model.terceroId ){

                $scope.tercerosCon.current = $filter('filter')($scope.tercerosCon.data, { id : $scope.contrato.model.terceroId })[0];                   
           }
            else{
                $scope.tercerosCon.current=$scope.tercerosCon.data[0];                     
            }     
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
	//CARGAMOS LOS LISTADOS	
    
	$scope.tipoContrato.getData();
	$scope.objetoContrato.getData();    
    $scope.estados.getData();	
    $scope.tercerosCon.getData();      
	
	//CARGAMOS LOS DATOS DEL contrato	
	
	if(contrato.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.tipoContrato.current = $scope.tipoContrato.data[0];
      $scope.objetoContrato.current = $scope.objetoContrato.data[0];
      //$scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : contrato.tipo})[0]; 
      //$scope.contrato.model.tipo=contrato.tipo;    
        
        
    }
    else{   
        $rootScope.loadingVisible = true;        
        $scope.divParticipanteContrato=true;
		
        contratoHttp.read({},$state.params.contrato, function (data) { 
        $scope.contrato.model = data;		

        if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}
        if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));}  
        
        		
        if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
        if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));}   
            
        $scope.tercerosCon.current = $filter('filter')($scope.tercerosCon.data, { id : $scope.contrato.model.terceroId })[0];  
     
        //$scope.estados.current=$scope.contrato.model.estado;	            
        $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.contrato.model.estado})[0];
        $scope.tipoContrato.current = $filter('filter')($scope.tipoContrato.data, {codigo : $scope.contrato.model.tipoContratoId})[0];
        $scope.objetoContrato.current = $filter('filter')($scope.objetoContrato.data, {codigo : $scope.contrato.model.objetoId})[0];            
        
        $scope.participanteContrato.getData();
        
       
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      
    
  }
})();