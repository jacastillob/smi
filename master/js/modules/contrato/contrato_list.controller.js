/**=========================================================
 * Module: app.CONTRATO.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoListController', contratoListController);

  contratoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'contratoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function contratoListController($scope, $filter, $state, ngDialog, tpl, contratoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
		
	$scope.ESTADO_EJECUCION = 'E';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.ESTADO_CANCELADO= 'CA';		

    $scope.contratos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrecontratos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.contratos.selectedAll = !$scope.contratos.selectedAll; 
        for (var key in $scope.contratos.selectedItems) {
          $scope.contratos.selectedItems[key].check = $scope.contratos.selectedAll;
        }
      },
      add : function() {  
              var cont = {
                      id: 0,
                      terceroId: 0,
                      tipoContratoId: 0,
                      objetoId: 0,
                      descripcion: '',
                      fechaInicial: '',
                      fechaFinal: '',
                      estado: '',
                      consecutivo: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.contrato_add', params : { contrato: cont } });
            $state.go('app.contrato_add');  
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.contrato_edit', params : { contrato: item } });
        $state.go('app.contrato_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            contratoHttp.remove({}, { id: id }, function(response) {
                $scope.contratos.getData();
                message.show("success", "Contrato eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.contratos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    contratoHttp.remove({}, { id: id }, function(response) {
                        $scope.contratos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.contratos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.contratos.filterText,
          "precision": false
        }];
        $scope.contratos.selectedItems = $filter('arrayFilter')($scope.contratos.dataSource, paramFilter);
        $scope.contratos.paginations.totalItems = $scope.contratos.selectedItems.length;
        $scope.contratos.paginations.currentPage = 1;
        $scope.contratos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.contratos.paginations.currentPage == 1 ) ? 0 : ($scope.contratos.paginations.currentPage * $scope.contratos.paginations.itemsPerPage) - $scope.contratos.paginations.itemsPerPage;
        $scope.contratos.data = $scope.contratos.selectedItems.slice(firstItem , $scope.contratos.paginations.currentPage * $scope.contratos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.contratos.data = [];
        $scope.contratos.loading = true;
        $scope.contratos.noData = false;      
         contratoHttp.getList(function(response) {
             
          $scope.contratos.selectedItems = response;
          $scope.contratos.dataSource = response;
          for(var i=0; i<$scope.contratos.dataSource.length; i++){
            $scope.contratos.nombrecontratos.push({id: i, nombre: $scope.contratos.dataSource[i]});
          }
          $scope.contratos.paginations.totalItems = $scope.contratos.selectedItems.length;
          $scope.contratos.paginations.currentPage = 1;
          $scope.contratos.changePage();
          $scope.contratos.loading = false;
          ($scope.contratos.dataSource.length < 1) ? $scope.contratos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }  
    
	//CARGAMOS DATA    
    $scope.contratos.getData();  
        
 
	}
  
  
  })();