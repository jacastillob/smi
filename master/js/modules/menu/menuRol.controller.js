/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('menuRolController', menuRolController);

  menuRolController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'sidebarHttp', 'UsuarioHttp'];


  function menuRolController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, sidebarHttp, UsuarioHttp){
    
    $scope.menu= {
        current : {},
        data:[],
        getData: function(){
            sidebarHttp.getMenu({},{},function(response) {
                $scope.menu.data = $filter('orderBy')(response, 'menuId');
                for(var i = 0; i < $scope.menu.data.length; i++){
                    $scope.menu.data[i].submenu = $filter('orderBy')($scope.menu.data[i].submenu, 'menuId');
                }
            }, function(faild) {
                $scope.menu.data = [];
                message.show("error", "No se cargaron los Menus: " + faild.Message);
            })
        }, edit: function (item){
            var parameter = {id:0, nombreOperadora:$scope.operadora.model.nombre, empresaId:$scope.operadora.model.id};
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menuRol_edit.html',
                controller: 'menuRolController',
                size: 'lg',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {$scope.menu.getData();});
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        getData: function(){
            UsuarioHttp.consultarPerfilesAll({}, {}, function(response) {
                $scope.roles.data = response;
            }, function(faild) {
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        }
    }
    
    $scope.menu.getData();
    $scope.roles.getData();
  }
})();