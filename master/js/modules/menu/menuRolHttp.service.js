/**=========================================================
 * Module: app.setMenu.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .service('menuRolHttp', menuRolHttp);

  menuRolHttp.$inject = ['$resource', 'END_POINT'];


  function menuRolHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getListMenu' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/menu'
      },
      'getMenuRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/menu/:id/rol'
      },
      'getRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/rol'
      },
      'updateMenu' : {///
        'method' : 'PUT',
        'url' : END_POINT + '/General.svc/menu'
      },
      'removeMenu':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/menu/:id'
      },
      'newMenu':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/menu/'
      },
      'deleteMenu': {
          'method':'DELETE',
          'isArray' : true,
          'params' : {
            menuId : '@menuId'
          },
          'url' : END_POINT + '/General.svc/menu/:menuId'
      },
      'deleteRol': {
          'method':'DELETE',
          'params' : {
            rolId : '@rolId'
          },
          'url' : END_POINT + '/General.svc/rol/:rolId'
      },
      'assignRol':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/menu/rol'
      },
      'unassignRol':   {
        'method':'PUT',
        'url' : END_POINT + '/General.svc/menu/rol'
      }
    };
    return $resource( END_POINT + '/General.svc/menu', {}, actions, {}); 
  }

})();