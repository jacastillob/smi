/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('menuAddController', menuAddController);

  menuAddController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'menuRolHttp', 'message', '$rootScope', 'parameters', '$modalInstance'];


  function menuAddController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, menuRolHttp, message, $rootScope, parameters, $modalInstance){
    $scope.datosParams= parameters;
    var datos=$scope.datosParams.detalle;
    
    $scope.menu= {
        model : {
            alert: '',
            icon: '',
            index: 0,
            label: '',
            orden: 0,
            padreId: 0,
            param: '',
            sref: '',
            submenu: null,
            text: '',
            urlParam: '',
            menuName:''
        },
        add: function(){
          if(!$scope.menu.model.label){message.show("error", "Label Requerido..");return;}
          if($scope.menu.model.label==""){message.show("error", "Label Requerido");return;}
          if(!$scope.menu.model.text){message.show("error", "Texto Requerido");return;}
          if($scope.menu.model.text==""){message.show("error", "Texto Requerido");return;}
          
          var paramatros = {
            alert: '',
            icon: $scope.menu.model.name,
            index: $scope.menu.model.index,
            label: $scope.menu.model.label,
            orden: $scope.menu.model.index,
            padreId: $scope.menu.model.padreId,
            param: $scope.menu.model.param,
            sref: $scope.menu.model.sref,
            submenu: null,
            text: $scope.menu.model.text,
            urlParam: $scope.menu.model.urlParam
          };
          
          $rootScope.loadingVisible = true;
          
          var _Http = new menuRolHttp($scope.menu.model);
           _Http.$newMenu (function(response) {
             var parametros = { menuId:response.menuId, rolId:1, seleccionado:true};
             var _HttpRol = new menuRolHttp(parametros);
             $rootScope.loadingVisible = true;  
             _HttpRol.$assignRol(function(response) {}, function(faild) {});
            $rootScope.loadingVisible = false;
            if(datos.padreId!=0){
              message.show("success", "Submenú Agregado");
            }else{
              message.show("success", "Menú Agregado");
            }
            $scope.menu.close();

          }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", "No se creó el Menu: " + faild.Message);
          })//*/
        },
        edit: function(){
          
          $rootScope.loadingVisible = true;
            
          var _Http = new menuRolHttp($scope.menu.model);
          
          _Http.$updateMenu(function(response) {
            $rootScope.loadingVisible = false;
            if(datos.padreId!=0){
              message.show("success", "Submenú Editado");
            }else{
              message.show("success", "Menú Editado");
            }
            $scope.menu.close();

          }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", "No se cargaron los Menus: " + faild.Message);
          })
        },
        dismiss : function() {
            $modalInstance.dismiss('cancel');
        },
        close : function() {
            $modalInstance.close('close');
        }
    }
    $scope.menu.model=datos;
  }
})();