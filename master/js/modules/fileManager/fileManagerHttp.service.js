/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.fileManager')
    .service('fileManagerHttp', fileManagerHttp);

  fileManagerHttp.$inject = ['$resource', 'END_POINT'];


  function fileManagerHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getDocumentos : {
        method : 'POST',
        isArray : true
      },
      getDocumento : {
        method : 'GET',
        url : END_POINT + '/General.svc/document/:id'
      },
      removeDocumento : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/document/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/document', paramDefault, actions);
  }

})();
