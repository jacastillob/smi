/**=========================================================
 * Module: app.forma30.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.fileManager')
    .controller('fileManagerController', fileManagerController);

  fileManagerController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'fileManagerHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];


  function fileManagerController($scope, $rootScope, $state, $modalInstance, fileManagerHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {


    $scope.fileManager = {
      model : {
        file : {},
        referenciaId : '',
        tipoReferencia : '',
        comentario : ''
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.fileManager.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.fileManager.dataSource, paramFilter);
        $scope.fileManager.paginations.totalItems = $scope.fileManager.selectedItems.length;
        $scope.fileManager.paginations.currentPage = 1;
        $scope.fileManager.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.fileManager.paginations.currentPage == 1 ) ? 0 : ($scope.fileManager.paginations.currentPage * $scope.fileManager.paginations.itemsPerPage) - $scope.fileManager.paginations.itemsPerPage;
        $scope.fileManager.data = $scope.fileManager.selectedItems.slice(firstItem , $scope.fileManager.paginations.currentPage * $scope.fileManager.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.fileManager.model.referenciaId,
          tipoReferencia : $scope.fileManager.model.tipoReferencia
        };
        $scope.fileManager.getData(params);
      },
      setData : function(data) {
        $scope.fileManager.selectedItems = data;
        $scope.fileManager.dataSource = data;
        $scope.fileManager.paginations.totalItems = $scope.fileManager.selectedItems.length;
        $scope.fileManager.paginations.currentPage = 1;
        $scope.fileManager.changePage();
        $scope.fileManager.noData = false;
        $scope.fileManager.loading = false;
        ($scope.fileManager.dataSource.length < 1) ? $scope.fileManager.noData = true : null;
      },
      getData : function(params) {
        $scope.fileManager.data = [];
        $scope.fileManager.loading = true;
        $scope.fileManager.noData = false;

        fileManagerHttp.getDocumentos({}, params, function(response) {
          $scope.fileManager.setData(response);
        })
      },
      visualizar : function(id) {
        
        
        var params = {
          id : id 
        };

        fileManagerHttp.getDocumento({}, params, function(response) {
          if (response.url != '') {
            var urlDownload = SERVER_URL + base64.decode(response.url);
            $window.open(urlDownload);
          }
        })
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;

        fileManagerHttp.removeDocumento(params, function(response) {
          $scope.fileManager.refresh();
          message.show("info", "Adjunto eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },
      uploader : function(file) {

        $rootScope.loadingVisible = true;

        file.upload = Upload.upload({
          url: END_POINT + '/General.svc/uploadDocument',
          headers: {
            Authorization : tokenManager.get()
          },
          data: { file: file, 
                 referenciaId: base64.encode($scope.fileManager.model.referenciaId),
                 tipoReferencia : base64.encode($scope.fileManager.model.tipoReferencia),
                 comentario : base64.encode($scope.fileManager.model.comentario)
                }
        });

        file.upload.then(function (response) {
          message.show("success", "Archivo almacenado correctamente");
          $scope.fileManager.model.file = {};
          $scope.fileManager.model.comentario = '';
          $scope.fileManager.refresh();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", "Ocurrio un error al intentar guardar el archivo");
        });

      },
      close : function() {
        $modalInstance.dismiss('cancel');
      }
    }

    $scope.fileManager.model.referenciaId = parameters.id.toString();
    $scope.fileManager.model.tipoReferencia = parameters.referencia.toString();
    $scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;

    $scope.fileManager.refresh();

  }

})();