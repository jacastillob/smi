/**=========================================================
 * Module: app.fileManager.js
 =========================================================*/
(function() {
    'use strict';
    angular.module('app.fileManager').controller('fileManagerDynamicController', fileManagerDynamicController);
    fileManagerDynamicController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'fileManagerHttp', 'parameters', 'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];

    function fileManagerDynamicController($scope, $rootScope, $state, $modalInstance, fileManagerHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {
        $scope.motrarTabla = false;
        $scope.fileManager = {
                model: {
                    file: {},
                    tipoReferencia: null,
                    anio: null,
                    mes: null,
                    liquidacion: null,
                    tipoPrecio: null,
                    curvaId: null,
                    tipoHdr: null
                },
                error: [],
                uploader: function(file) {
                    $rootScope.loadingVisible = true;
                    cargueArchivo(file);
                },
                close: function() {
                    $modalInstance.dismiss('cancel');
                }
            }
            /**
             * Objeto de la tabla
             */
        $scope.dataPage = {
            paginations: {
                maxSize: 3,
                itemsPerPage: 10,
                currentPage: 0,
                totalItems: 0
            },
            selectedAll: false,
            filterText: '',
            dataSource: [],
            selectedItems: [],
            data: [],
            noData: false,
            loading: false,
            selectAll: function() {
                $scope.dataPage.selectedAll = !$scope.dataPage.selectedAll;
                for (var key in $scope.dataPage.selectedItems) {
                    $scope.dataPage.selectedItems[key].check = $scope.dataPage.selectedAll;
                }
            },
            filter: function() {
                var paramFilter = [{
                    "key": "$",
                    "value": $scope.dataPage.filterText,
                    "precision": false
                }];
                $scope.dataPage.selectedItems = $filter('arrayFilter')($scope.dataPage.dataSource, paramFilter);
                $scope.dataPage.paginations.totalItems = $scope.dataPage.selectedItems.length;
                $scope.dataPage.paginations.currentPage = 1;
                $scope.dataPage.changePage();
            },
            changePage: function() {
                var firstItem = ($scope.dataPage.paginations.currentPage == 1) ? 0 : ($scope.dataPage.paginations.currentPage * $scope.dataPage.paginations.itemsPerPage) - $scope.dataPage.paginations.itemsPerPage;
                $scope.dataPage.data = $scope.dataPage.selectedItems.slice(firstItem, $scope.dataPage.paginations.currentPage * $scope.dataPage.paginations.itemsPerPage);
            },
            getData: function() {
                $scope.dataPage.data = [];
                $scope.dataPage.loading = true;
                $scope.dataPage.noData = false;
            }
        }
        $scope.fileManager.model.tipoReferencia = parameters.item.tipoReferencia;
        if ($scope.fileManager.model.tipoReferencia == "TRM") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
        }
        if ($scope.fileManager.model.tipoReferencia == "OIL") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
            $scope.fileManager.model.liquidacion = parameters.item.liquidacion;
            $scope.fileManager.model.tipoPrecio = parameters.item.tipoPrecio;
        }
        if ($scope.fileManager.model.tipoReferencia == "GAS") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
            $scope.fileManager.model.liquidacion = parameters.item.liquidacion;
        }
        if ($scope.fileManager.model.tipoReferencia == "CURVA") {
            $scope.motrarTabla = false;
            $scope.fileManager.model.curvaId = parameters.item.curvaId;
            $scope.fileManager.model.tipoHdr = parameters.item.tipoHdr;
        }
        /**
         * Metodo que carga un archivo dependiendo del tipo de cargue
         *
         * @method     cargueArchivo
         * @param      {<Object>}  file    { Contiene el archivo a cargar }
         */
        function cargueArchivo(file) {
            if ($scope.fileManager.model.tipoReferencia == "CURVA") {
                $rootScope.loadingVisible = true;
                console.log("Entro CURVA");
                file.upload = Upload.upload({
                    url: END_POINT + '/General.svc/uploadCurvaBase',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        curvaId: base64.encode('' + $scope.fileManager.model.curvaId),
                        tipoHdr: base64.encode($scope.fileManager.model.tipoHdr)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "TRM") {
                $rootScope.loadingVisible = true;
                console.log("Entro TRM");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadTrm',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "OIL") {
                $rootScope.loadingVisible = true;
                console.log("Entro OIL");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadPrecioOil',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes),
                        liquidacion: base64.encode($scope.fileManager.model.liquidacion),
                        tipoPrecio: base64.encode($scope.fileManager.model.tipoPrecio)
                    }
                });
                file.upload.then(function(response) {
                   if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "GAS") {
                $rootScope.loadingVisible = true;
                console.log("Entro GAS");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadPrecioGas',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes),
                        liquidacion: base64.encode($scope.fileManager.model.liquidacion)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            $rootScope.loadingVisible = false;
        }
    }
})();