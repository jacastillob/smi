/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.calendar')
    .service('calendarHttp', calendarHttp);

  calendarHttp.$inject = ['$resource', 'END_POINT'];


  function calendarHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getAgendas' : {
        'method' : 'GET',
        'isArray' : true,          
        'url' : END_POINT + '/General.svc/agenda/calendario'
      }
    };
    return $resource( END_POINT + '/General.svc/agenda/calendario', {}, actions, {}); 
  }

})();