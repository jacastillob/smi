/**=========================================================
 * Module: app.pasajero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pasajero')
    .controller('pasajeroListController', pasajeroListController);

  pasajeroListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl','$modal', 'pasajeroHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function pasajeroListController($scope, $filter, $state, ngDialog, tpl,$modal, pasajeroHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	
    $scope.ESTADO_ACTIVO = '1';
    $scope.ESTADO_INACTIVO = '0';
        
	//$rootScope.loadingVisible = true;

 
    $scope.pasajeros = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrepasajeros:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.pasajeros.selectedAll = !$scope.pasajeros.selectedAll; 
        for (var key in $scope.pasajeros.selectedItems) {
          $scope.pasajeros.selectedItems[key].check = $scope.pasajeros.selectedAll;
        }
      },
      add : function() {
          var item = {
              codigo: 0,
              id: 0,
              identificacion: '', 
              nombre: '', 
              apellido: '', 
              fechaNacimiento:'',
			  telefono: '',
			  direccion: '',
			  correo: '',
              tipoId: 0,
			  estado: '',			  
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''			 
          };
         var modalInstance = $modal.open({
            templateUrl: 'app/views/pasajero/pasajero_form.html',
            controller: 'pasajeroController',
            size: 'lg',
            resolve: {
                
                        parameters: { pasajero: item }
                
            }
            });
            modalInstance.result.then(function (parameters) {
            });  
      },
      edit : function(item) {
          
        var modalInstance = $modal.open({
        templateUrl: 'app/views/pasajero/pasajero_form.html',
        controller: 'pasajeroController',
        size: 'lg',
        resolve: {
            parameters: { pasajero: item }
        }
        });
        modalInstance.result.then(function (parameters) {
        });          
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            pasajeroHttp.remove({}, { id: id }, function(response) {
                $scope.pasajeros.getData();
                message.show("success", "pasajero eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.pasajeros.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    pasajeroHttp.remove({}, { id: id }, function(response) {
                        $scope.pasajeros.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.pasajeros.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.pasajeros.filterText,
          "precision": false
        }];
        $scope.pasajeros.selectedItems = $filter('arrayFilter')($scope.pasajeros.dataSource, paramFilter);
        $scope.pasajeros.paginations.totalItems = $scope.pasajeros.selectedItems.length;
        $scope.pasajeros.paginations.currentPage = 1;
        $scope.pasajeros.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.pasajeros.paginations.currentPage == 1 ) ? 0 : ($scope.pasajeros.paginations.currentPage * $scope.pasajeros.paginations.itemsPerPage) - $scope.pasajeros.paginations.itemsPerPage;
        $scope.pasajeros.data = $scope.pasajeros.selectedItems.slice(firstItem , $scope.pasajeros.paginations.currentPage * $scope.pasajeros.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.pasajeros.data = [];
        $scope.pasajeros.loading = true;
        $scope.pasajeros.noData = false;
          
        var parametros= {
            "estado":$scope.estadoPasajero.current.value,            
            "nombre":$scope.busquedaPasajero.model.nombre,
            "apellido":$scope.busquedaPasajero.model.apellido,
            "identificacion":$scope.busquedaPasajero.model.identificacion,
            "email":$scope.busquedaPasajero.model.email,
            "direccion":$scope.busquedaPasajero.model.direccion,
            "telefono":$scope.busquedaPasajero.model.telefono,
            "movil":$scope.busquedaPasajero.model.movil,
            "intermediario":$scope.busquedaPasajero.model.intermediario            
        };
          
        pasajeroHttp.getList({}, parametros,function(response) {
          $scope.pasajeros.selectedItems = response;
          $scope.pasajeros.dataSource = response;
          for(var i=0; i<$scope.pasajeros.dataSource.length; i++){
            $scope.pasajeros.nombrepasajeros.push({id: i, nombre: $scope.pasajeros.dataSource[i]});
          }
          $scope.pasajeros.paginations.totalItems = $scope.pasajeros.selectedItems.length;
          $scope.pasajeros.paginations.currentPage = 1;
          $scope.pasajeros.changePage();
          $scope.pasajeros.loading = false;
          ($scope.pasajeros.dataSource.length < 1) ? $scope.pasajeros.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }       
    //CARGAMOS PROCESOS
    $scope.estadoPasajero = {             
          current : {}, data:[],
        getData : function() {            
            $scope.estadoPasajero.data.push({value:'1', descripcion: 'Activos'});
            $scope.estadoPasajero.data.push({value:'0', descripcion: 'Inactivos'});          
            $scope.estadoPasajero.current =$scope.estadoPasajero.data[0];
        }          
    }
    
    
    $scope.busquedaPasajero = {
    model : {
          nombre: '',
          apellido: '',
          identificacion: '',
          email: '',
          direccion: '',
          telefono: '',
          movil: '',
          estado: '',
          intermediario:''
            } 
    }
	//CARGAMOS LOS LISTADOS
    $scope.estadoPasajero.getData();    
    $scope.pasajeros.getData();
        
        
        
	}
  
  
  })();