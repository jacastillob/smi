/**=========================================================
 * Module: app.pasajero.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pasajero')
    .service('pasajeroHttp', pasajeroHttp);

  pasajeroHttp.$inject = ['$resource', 'END_POINT'];


  function pasajeroHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {     
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/pasajero/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/pasajero/:id'
      },
      'getList' : {
        'method' : 'POST',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/pasajero/busqueda'          
        },        
      'getContactosPasajero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            pasajeroId : '@pasajeroId'           
          },
          'url' : END_POINT + '/Comercial.svc/pasajero/:pasajeroId/contacto'
      },
      'addContactoPasajero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/pasajero/contacto'
      },
      'editContactoPasajero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/pasajero/contacto'
      },
      'deleteContactoPasajero': {
        'method':'DELETE',
        'params' : {
            contactoPasajeroId : '@contactoPasajeroId'           
          },
        'url' : END_POINT + '/Comercial.svc/pasajero/contacto/:contactoPasajeroId'
      } 
    };
    return $resource( END_POINT + '/Comercial.svc/pasajero', {}, actions, {}); 
  }

})();