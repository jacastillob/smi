/**=========================================================
 * Module: app.pasajero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.pasajero')
    .controller('pasajeroController', pasajeroController);

  pasajeroController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'pasajeroHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];   

  function pasajeroController($scope, $rootScope, $state,$modalInstance,$filter,pasajeroHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {
      
      
      var pasajero = parameters.pasajero;
      var currentDate = new Date();
      
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?.¿ ]+$/;
      $scope.pasajero = {
		model : {
				  id: 0,
                  identificacion: '', 
                  nombre: '', 
                  telefono: '',
                  direccion: '',
                  correo: '',
                  tipoId: 0,
                  estado: '',			  
                  fechaAct: '',
                  usuarioAct: '',
                  fechaReg: '',
                  usuarioReg: '',
                  fechaNacimiento: ''
		},  
        fechaNacimiento : {
        isOpen : false,
        value:'',
        dateOptions : {          
          startingDay: 1         
          
        },
        open : function($event) {
          $scope.pasajero.fechaNacimiento.isOpen = true;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.pasajero', params : { filters : {estadoSeleccionado:pasajero.estado}, data : []} });
        $state.go('app.pasajero');
      },
      save : function() {		  
       
		        if($scope.pasajero.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.pasajero.model.identificacion ==""){message.show("warning", "Identificación requerido");return;}
                
                if($scope.pasajero.fechaNacimiento.value==""){message.show("warning", "Fecha de nacimiento requerida");return;}
                else{
                    
                    $scope.pasajero.model.fechaNacimiento=Date.parse(new Date($scope.pasajero.fechaNacimiento.value));
                }	

                	
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.pasajero.model.estado= $scope.estados.current.value;}			
				
			
          
			//INSERTAR
            if($scope.pasajero.model.id==0){
				
				$rootScope.loadingVisible = true;
				pasajeroHttp.save({}, $scope.pasajero.model, function (data) { 
					
						pasajeroHttp.read({},{ id : data.id}, function (data) {
							$scope.pasajero.model=data;
							pasajero.id=$scope.pasajero.model.id; 
							if($scope.pasajero.model.fechaReg){$scope.pasajero.fechaReg.value=new Date(parseFloat($scope.pasajero.model.fechaReg));}    
							if($scope.pasajero.model.fechaAct){$scope.pasajero.fechaAct.value=new Date(parseFloat($scope.pasajero.model.fechaAct));} 		
								
							message.show("success", "pasajero creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.pasajero.model.id){
					$state.go('app.pasajero');
				}else{
					 
					pasajeroHttp.update({}, $scope.pasajero.model, function (data) {
					  $rootScope.loadingVisible = false;
					  $scope.pasajero.model=data;
						if($scope.pasajero.model.fechaReg){$scope.pasajero.fechaReg.value=new Date(parseFloat($scope.pasajero.model.fechaReg));}    
						if($scope.pasajero.model.fechaAct){$scope.pasajero.fechaAct.value=new Date(parseFloat($scope.pasajero.model.fechaAct));}   					  
					   message.show("success", "pasajero actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pasajero.model.id,
                         referencia : 'pasajero' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.pasajero.model.id,
                         referencia : 'pasajero' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        close : function() {
        $modalInstance.dismiss('cancel');
      }
    }
      
    $scope.contactoPasajero = {
        
    model : {              
            id: 0,
            nombre: '',
            telefono: '',
			ext: '',
			email: ''
		},
      current : {
          id: 0,
          comision:false,
          cotizacion:false,
          activo:false
      },
      data:[],
      getData : function() {
          if(pasajero){
              
            if($scope.pasajero.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              pasajeroHttp.getContactosPasajero({}, { pasajeroId: $scope.pasajero.model.id }, function(response) {
                  $scope.contactoPasajero.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },
       save : function() {   
           
        
           if($scope.contactoPasajero.current.nombre==""){message.show("warning", "Debe especificar el nombre");return;}
           if($scope.contactoPasajero.current.email==""){message.show("warning", "Debe especificar el email");return;} 
           
        $rootScope.loadingVisible = true;
           
           if($scope.contactoPasajero.current.id==0){
               $scope.contactoPasajero.current.terceroId= $scope.pasajero.model.id;
               	pasajeroHttp.addContactoPasajero({}, $scope.contactoPasajero.current, function (data) {
					  $rootScope.loadingVisible = false;
					  
                      $scope.contactoPasajero.current={  
                            id: 0,
                            nombre: '',
                            telefono: '',
                            ext: '',
                            email: '',
                            comision:false,
                            cotizacion:false,
                            activo:false
                      };
                        
                       $scope.contactoPasajero.getData();
               
					   message.show("success", "Contacto actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
           
               
           }
           else if ($scope.contactoPasajero.current.id > 0){
               
               	pasajeroHttp.editContactoPasajero({}, $scope.contactoPasajero.current, function (data) {
					  $rootScope.loadingVisible = false;
					  
                      $scope.contactoPasajero.current={  
                            id: 0,
                            nombre: '',
                            telefono: '',
                            ext: '',
                            email: '',
                            comision:false,
                            cotizacion:false,
                            activo:false
                      };
                        
                       $scope.contactoPasajero.getData();
               
					   message.show("success", "Contacto actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
           
           }
				
         
           
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          pasajeroHttp.deleteContactoPasajero({}, {contactoPasajeroId: item.id}, function(response){
            //$scope.contactoPasajero.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
         $scope.contactoPasajero.current=item;
      },
        cancel : function(item) {
         $scope.contactoPasajero.current={};
      }
    }
    //TAB
     $scope.showTabs= function(){
        if ($scope.pasajero.id==0)
          return false;
        else
          return true;
    }
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:0 , descripcion: 'Inactivo'});
            $scope.estados.data.push({value:1 , descripcion: 'Activo'});
              
        }        
    }	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	
	
	//CARGAMOS LOS DATOS DEL pasajero
	
	if(pasajero.id==0){
        $scope.estados.current=$scope.estados.data[1];     
        $scope.pasajero.model.direccion=pasajero.direccion;
        $scope.pasajero.model.correo=pasajero.correo;
        $scope.pasajero.model.telefono=pasajero.telefono;
    }
    else{        
        $rootScope.loadingVisible = true;
		$scope.divContactopasajero=true;
        pasajeroHttp.read({}, { id : pasajero.id} , function (data) { 
            
            $scope.pasajero.model = data;	

                  
           
            if($scope.pasajero.model.fechaNacimiento){
              var d = new Date(parseFloat($scope.pasajero.model.fechaNacimiento));
              d.setMinutes( d.getMinutes() + d.getTimezoneOffset() );   
              $scope.pasajero.fechaNacimiento.value=d;
            }   
			       
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.pasajero.model.estado})[0];            
			$scope.contactoPasajero.getData();
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
	
    
    
  }
})();