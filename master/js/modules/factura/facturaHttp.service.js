/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .service('facturaHttp', facturaHttp);

  facturaHttp.$inject = ['$resource', 'END_POINT'];


  function facturaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            proceso : '@proceso'           
          },
          'url' : END_POINT + '/Comercial.svc/factura'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/factura/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/factura'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/factura'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/factura'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/factura'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      },
        'getIntermediarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/intermediario'
      },    
    };
    return $resource( END_POINT + '/Comercial.svc/factura', {}, actions, {}); 
  }

})();