/**=========================================================
 * Module: app.factura.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .controller('facturaController', facturaController);

 facturaController.$inject =  ['$scope', '$rootScope', '$state', '$modalInstance','$filter', 'facturaHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal','REPORT_URL'];   

  function facturaController($scope, $rootScope, $state,$modalInstance,$filter,facturaHttp,parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window,$modal,REPORT_URL) {      
      
     
      var factura =parameters.factura;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.factura = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              entregaId: 0,
              viaContactoId: 0,            
              padreId: 0,
              crearCliente:0,              
			  observaciones: '',
			  estado: 1,	
              fechaInicio: '',
              fechaFin: '' ,              
              polizas:[],
              ciudadEntregaId:0,
              fechaEntrega: '' ,
              direccionEntrega: '' ,
              observacionEntrega: '',
              facturaId:0
		},
      pasajeros:[],
      polizaSeleccionada:undefined,
      deletePoliza: function(p) {
           $scope.factura.model.polizas.splice($scope.factura.model.polizas.indexOf(p), 1);     
      },
      back : function() {
          parametersOfState.set({ name : 'app.factura', params : { filters : {procesoSeleccionado:factura.tipo}, data : []} });
        $state.go('app.factura');
          
      },
      save : function() {	
        
            if($scope.factura.model.identificacion==""){message.show("warning", "Identifiación del cliente requerido");return;}
            if($scope.factura.model.tercero==""){message.show("warning", "Razón social del cliente requerido");return;}
            if($scope.factura.model.email==""){message.show("warning", "Email del cliente requerido");return; }
            if( $scope.factura.model.polizas.length ==0){message.show("warning", "Debe agregar polizas");return;}  
        
            try{                
              $scope.factura.model.fechaInicio=$scope.factura.formatdate($scope.factura.model.fechaInicio);
              //$scope.factura.model.fechaInicio=$scope.factura.formatdate(new Date($scope.factura.model.fechaInicio));
            }
            catch(err) {
            }
            try{
                
              $scope.factura.model.fechaFin=$scope.factura.formatdate($scope.factura.model.fechaFin);
              //$scope.factura.model.fechaFin=$scope.factura.formatdate(new Date($scope.factura.model.fechaFin));
            }
            catch(err) {
            }
          
           try{                
                $scope.factura.model.fechaEntrega=$scope.factura.formatdate($scope.factura.model.fechaEntrega);
                //$scope.factura.model.fechaEntrega=$scope.factura.formatdate(new Date($scope.factura.model.fechaEntrega));
            }
            catch(err) {
                $scope.factura.model.fechaEntrega='';
            }
            
            $scope.factura.model.grupoId= $scope.grupos.current.value;
            $scope.factura.model.prioridadId= $scope.prioridades.current.value;
            $scope.factura.model.viaContactoId= $scope.viasContacto.current.value;
            $scope.factura.model.entregaId= $scope.entregas.current.value;
            $scope.factura.model.formaPagoId= $scope.formasPago.current.value;
            $scope.factura.model.monedaId= $scope.monedas.current.value;
            $scope.factura.model.destino= $scope.destinos.current.value;
            $scope.factura.model.tipoViaje= $scope.tiposViaje.current.value;
          
            $scope.factura.model.entregaId= $scope.entregas.current.value;
            $scope.factura.model.ciudadEntregaId= $scope.ciudades.current.value;          
          
          
            
            if($scope.intermediarios.current){$scope.factura.model.intermediarioId = $scope.intermediarios.current.codigo;
            }else{$scope.factura.model.intermediarioId ='00000000-0000-0000-0000-000000000000';} 
				
            $rootScope.loadingVisible = true;
          
            facturaHttp.save({}, $scope.factura.model, function (data) { 	
                       
                
                        if ($scope.factura.model.id==0){
                            message.show("success", "Factura creada satisfactoriamente!!");
                        }
                        else{message.show("success", "Factura actualizada satisfactoriamente!!");}
                        
                
                        $scope.factura.model.id=data.id;		
                        $scope.factura.model.intermediarioId=data.intermediarioId;
                        	
                        $scope.btnEnviarCotizacion=true;
                        $scope.crearCliente=false;
                        $scope.factura.model.crearCliente=0;                        
                      
                
                        $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });				
		
		},          
          anular: function() {   
              
            if (confirm("Desea anular el factura?")) {
                    $rootScope.loadingVisible = true;              
                    $scope.factura.model.estado=3;//estado anulado

                try{                
                    $scope.factura.model.fechaInicio=$scope.factura.formatdate($scope.factura.model.fechaInicio);
                }
                catch(err) {
                }
                try{
                    $scope.factura.model.fechaFin=$scope.factura.formatdate($scope.factura.model.fechaFin);
                }
                catch(err) {
                }
                       facturaHttp.save({}, $scope.factura.model, function (data) {
                            $rootScope.loadingVisible = false;
                            message.show("success", "Factura anulada satisfactoriamente!!");	
                            $scope.factura.close(); 
                    }, function(faild) {
                        $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                    });		
            } 
          },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.idRow,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.id,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }      
      ,agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.idRow,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.factura.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.padreId,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },          
      buscarCliente : function() { 

        var modalInstance = $modal.open({
          templateUrl: 'app/views/cotizacion/buscarCliente.html',
          controller: 'buscarClienteController',
          size: 'lg',
          resolve: {
            parameters: { cotizacion : $scope.factura.model}
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },   
    verSeguimientoHistorico : function() {            
    var modalInstance = $modal.open({
      templateUrl: 'app/views/seguimiento/seguimiento.html',
      controller: 'seguimientoController',
      size: 'lg',
      resolve: {
        parameters: { id : $scope.factura.model.padreId,
                     referencia : 'factura' }
      }
    });
    modalInstance.result.then(function (parameters) {
    });

    }
  ,buscarPolizas : function() {  
      
            if(!$scope.factura.model.fechaInicio){message.show("warning", "Fecha inicial requerida");return;}
            if(!$scope.factura.model.fechaFin){message.show("warning", "Fecha final requerida");return;}               
            $scope.factura.model.origen= $scope.origenes.current.value;
            $scope.factura.model.destino= $scope.destinos.current.value;
            $scope.factura.model.tipo= $scope.tiposViaje.current.value;
          
            var modalInstance = $modal.open({
              templateUrl: 'app/views/cotizacion/buscarPolizas.html',
              controller: 'buscarPolizaController',
              size: 'lg',
              resolve: {
                parameters: { cotizacion : $scope.factura.model}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.padreId,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      },  
        close : function() {
        if(parameters.vfacturas!=null && parameters.vfacturas!=undefined)
            parameters.vfacturas.getData();
            
        $modalInstance.dismiss('cancel');
      },
        formatdate(t){    

            //var today = new Date();
            var dd = t.getDate();
            var mm = t.getMonth()+1; //January is 0!

            var yyyy = t.getFullYear();
            if(dd<10){
                dd='0'+dd;
            } 
            if(mm<10){
                mm='0'+mm;
            } 
            //2018-01-15
            return yyyy+'-'+mm+'-'+dd;         
       
        },
      enviarCotizacion(){

                $rootScope.loadingVisible = true;   
                    
                    if($scope.factura.model.intermediarioId=="00000000-0000-0000-0000-000000000000"){
                      $rootScope.loadingVisible = true;
                       facturaHttp.enviarCotizacionCrm({}, {codigo: $scope.factura.model.id }, function(response){
                          message.show("success", "Pedido enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
                    else{

                    $rootScope.loadingVisible = true;
                     facturaHttp.enviarCotizacionIntermediario({}, {codigo: $scope.factura.model.id }, function(response){
                          message.show("success", "Pedido enviada satisfactoriamente!!");	
                          $rootScope.loadingVisible = false;
                      },function(faild) {
                          $rootScope.loadingVisible = false;
                        message.show("error", faild.Message);
                      }); 

                    }
      },
        agregarCliente: function(data){
            
            $scope.crearCliente=true;
            $scope.factura.model.crearCliente=1;
        },
          selectPoliza: function(polizaSeleccionada){            
              
              $scope.factura.pasajeros=polizaSeleccionada.pasajeros; 
              $scope.polizaSeleccionada=polizaSeleccionada;
              
        },buscarPasajero: function(data){
            
            if($scope.polizaSeleccionada!=null && $scope.polizaSeleccionada!=undefined){

                      var modalInstance = $modal.open({
                      templateUrl: 'app/views/cotizacion/buscarPasajero.html',
                      controller: 'buscarPasajeroController',
                      size: 'lg',
                      resolve: {
                        parameters: { cotizacion : $scope.factura.model,poliza:$scope.polizaSeleccionada}
                      }
                    });
                    modalInstance.result.then(function (parameters) {
                    });
                }
            else{                
                message.show("warning", "Debe seleccionar una poliza!!");
            }
            
        },imprimir: function(data){
            
             var ancho=800;
             var alto=500;
             var posicion_x;
             var posicion_y;
             posicion_x=(screen.width/2)-(ancho/2);
             posicion_y=(screen.height/2)-(alto/2);
             var targeturl = REPORT_URL + "Factura&NumCotizacion=" + $scope.factura.model.id;
             window.open(targeturl, '',"width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top="+posicion_y+"");
     
        } 
    }
      
      
      
    //TERCEROS
    $scope.clientes = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            generalHttp.getTerceros({}, {}, function(response) {                
            $scope.clientes.data = response ;              
                
               if($scope.factura.model){
                    
                    $scope.clientes.current = $filter('filter')($scope.clientes.data, { id : $scope.factura.model.clienteId })[0];                   
               }
                else{
                    $scope.clientes.current=$scope.Clientes.data[0];                     
                }               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.factura.model.terceroId=$scope.clientes.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.factura.model.terceroId=$scope.clientes.current.id;
         // $scope.contactosOp.getData();
      }
    }

    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'}); 
        }        
    }

    //VIA CONTACTO
    $scope.viasContacto = {
      current : {},
      data:[],
      getData : function() {
                $scope.viasContacto.data.push({value:1, descripcion: 'Cliente'});
                $scope.viasContacto.data.push({value:2, descripcion: 'Facebook'});     
                $scope.viasContacto.data.push({value:3, descripcion: 'E-mail'}); 
                $scope.viasContacto.data.push({value:4, descripcion: 'Directorio'});  
                $scope.viasContacto.data.push({value:5, descripcion: 'Referido'});  
                $scope.viasContacto.data.push({value:6, descripcion: 'Compra online'});  
                $scope.viasContacto.data.push({value:7, descripcion: 'Intermediario'});  
                $scope.viasContacto.data.push({value:8, descripcion: 'Google'});
                $scope.viasContacto.data.push({value:9, descripcion: 'Whatsapp'});
                $scope.viasContacto.data.push({value:10, descripcion: 'Zopim'});  
                $scope.viasContacto.data.push({value:11, descripcion: 'Ori/Volante'}); 
                $scope.viasContacto.current=$scope.viasContacto.data[0];   
      },
      setViaContacto : function(){
          $scope.factura.model.viaContactoId=$scope.viasContacto.current.codigo;
    }}
    //PRIORIDADES
    $scope.prioridades = {
      current : {},
      data:[],
      getData : function() {
                $scope.prioridades.data.push({value:1, descripcion: 'NORMAL(15DIAS)'});
                $scope.prioridades.data.push({value:2, descripcion: 'ALTA(48HORAS)'});     
                $scope.prioridades.data.push({value:3, descripcion: 'CRITICA(8HORAS)'}); 
                $scope.prioridades.data.push({value:4, descripcion: 'BAJA(1MES)'});  
                $scope.prioridades.data.push({value:5, descripcion: 'BAJA(2MESES)'});  
                $scope.prioridades.data.push({value:6, descripcion: 'BAJA(3MESES)'});  
                $scope.prioridades.current=$scope.prioridades.data[0];   
      },
      setPrioridad : function(){
          $scope.factura.model.prioridadId=$scope.prioridades.current.value;
    }}
   
    //GRUPO
    $scope.grupos = {
      current : {},
      data:[],
      getData : function() {
                $scope.grupos.data.push({value:2, descripcion: 'Asesores'});    
                $scope.grupos.data.push({value:1, descripcion: 'Administrativo'});                 
                $scope.grupos.data.push({value:3, descripcion: 'Lista de pólizas'});     
                $scope.grupos.data.push({value:4, descripcion: 'Servicio al Cliente'});
                $scope.grupos.current=$scope.grupos.data[0];                   
          },
      setGrupo : function(){
          $scope.factura.model.grupoId=$scope.grupos.current.value;
    }}
    //ENTREGAS
    $scope.entregas = {
      current : {},
      data:[],
      getData : function() {
                $scope.entregas.data.push({value:1, descripcion: 'Entrega Oficina'});    
                $scope.entregas.data.push({value:2, descripcion: 'Entrega Domicilio'});                 
                $scope.entregas.data.push({value:3, descripcion: 'Pago Contra Entrega'});     
                $scope.entregas.data.push({value:4, descripcion: 'Consignación Bancaria'});
                $scope.entregas.current=$scope.entregas.data[0];                   
          },
      setEntrega : function(){
          $scope.factura.model.entregaId=$scope.entregas.current.value;
    }}
      
      
    //MONEDA
    $scope.monedas  = {
            current: {},
            data: [],
            getData: function() {
                $scope.monedas.data.push({value:2, descripcion: 'Dolar'});
                $scope.monedas.data.push({value:4, descripcion: 'Pesos Colombianos'});    
                $scope.monedas.current=$scope.monedas.data[0];    
            }
        }
      
    $scope.formasPago  = {
            current: {},
            data: [],
            getData: function() {
                $scope.formasPago.data.push({value:0, descripcion: 'Pesos'});
                $scope.formasPago.data.push({value:1, descripcion: 'Visa Tarjeta de credito'});     
                $scope.formasPago.data.push({value:2, descripcion: 'Pse'});     
                $scope.formasPago.data.push({value:3, descripcion: 'Consig  Bogota'});     
                $scope.formasPago.data.push({value:4, descripcion: 'Consig  Bancolombia'});     
                $scope.formasPago.data.push({value:5, descripcion: 'Consig  Colpatria'});     
                $scope.formasPago.data.push({value:6, descripcion: 'Consig  Davivienda'});
                $scope.formasPago.data.push({value:8, descripcion: 'Dolares'});  
                $scope.formasPago.data.push({value:10, descripcion: 'Mastercard Tarjeta'});  
                $scope.formasPago.data.push({value:11, descripcion: 'Diners Tarjeta'});  
                $scope.formasPago.data.push({value:12, descripcion: 'Amex Tarjeta'});  
                $scope.formasPago.data.push({value:13, descripcion: 'CREDITO/SIN RECIBO'});  
                
                $scope.formasPago.current=$scope.formasPago.data[0]; 
                   
            }
        }
      
        $scope.ciudades  = {
            current: {},
            data: [],
            getData: function() {
                $scope.ciudades.data.push({value:0, descripcion: 'Bogotá D.C'});
                $scope.ciudades.data.push({value:1, descripcion: 'Medellín'});     
                $scope.ciudades.data.push({value:2, descripcion: 'Barranquilla'});     
                $scope.ciudades.data.push({value:3, descripcion: 'Calí'});     
                $scope.ciudades.data.push({value:4, descripcion: 'Bucaramanga'});     
                $scope.ciudades.data.push({value:5, descripcion: 'Consig Colpatria'});     
                $scope.ciudades.data.push({value:6, descripcion: 'Santa Marta'});
                
                $scope.ciudades.current=$scope.ciudades.data[0];    
            }
        }
      
      
      
        $scope.origenes  = {
            current: {},
            data: [],
            getData: function() {
      
                        $scope.origenes.data.push({value: 48, descripcion: 'Colombia' });
                        $scope.origenes.data.push({value: 1, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 2, descripcion: 'Argentina' });
                        $scope.origenes.data.push({value: 6, descripcion: 'Alemania' });
                        $scope.origenes.data.push({value: 12, descripcion: 'Arabia Saudita' });
                        $scope.origenes.data.push({value: 15, descripcion: 'Aruba' });
                        $scope.origenes.data.push({value: 16, descripcion: 'Australia' });
                        $scope.origenes.data.push({value: 19, descripcion: 'Bahamas' });
                        $scope.origenes.data.push({value: 21, descripcion: 'Barbados' });
                        $scope.origenes.data.push({value: 25, descripcion: 'Bemudas' });
                        $scope.origenes.data.push({value: 28, descripcion: 'Bolivia' });
                        $scope.origenes.data.push({value: 31, descripcion: 'Brasil' });
                        $scope.origenes.data.push({value: 33, descripcion: 'Bulgaria' });
                        $scope.origenes.data.push({value: 37, descripcion: 'Belgica' });
                        $scope.origenes.data.push({value: 38, descripcion: 'Cabo Verde' });
                        $scope.origenes.data.push({value: 39, descripcion: 'Islas Caiman' });
                        $scope.origenes.data.push({value: 40, descripcion: 'Camboya' });
                        $scope.origenes.data.push({value: 41, descripcion: 'Camerun' });
                        $scope.origenes.data.push({value: 42, descripcion: 'Canada' });
                        $scope.origenes.data.push({value: 45, descripcion: 'Chile' });
                        $scope.origenes.data.push({value: 52, descripcion: 'Corea del Norte' });
                        $scope.origenes.data.push({value: 53, descripcion: 'Corea del Sur' });
                        $scope.origenes.data.push({value: 54, descripcion: 'Costa de Marfil' });
                        $scope.origenes.data.push({value: 55, descripcion: 'Costa Rica' });
                        $scope.origenes.data.push({value: 56, descripcion: 'Croacia' });
                        $scope.origenes.data.push({value: 57, descripcion: 'Cuba' });
                        $scope.origenes.data.push({value: 59, descripcion: 'Dinamarca' });
                        $scope.origenes.data.push({value: 61, descripcion: 'Ecuador' });
                        $scope.origenes.data.push({value: 62, descripcion: 'Egipto' });
                        $scope.origenes.data.push({value: 63, descripcion: 'El Salvador' });
                        $scope.origenes.data.push({value: 64, descripcion: 'Emiratos Arabes Unvalueos' });
                        $scope.origenes.data.push({value: 66, descripcion: 'Eslovaquia' });
                        $scope.origenes.data.push({value: 67, descripcion: 'Eslovenia' });
                        $scope.origenes.data.push({value: 68, descripcion: 'España' });
                        $scope.origenes.data.push({value: 69, descripcion: 'Estados Unvalueos' });
                        $scope.origenes.data.push({value: 70, descripcion: 'Estonia' });
                        $scope.origenes.data.push({value: 71, descripcion: 'Etiopia' });
                        $scope.origenes.data.push({value: 72, descripcion: 'Islas Feroe' });
                        $scope.origenes.data.push({value: 73, descripcion: 'Filipinas' });
                        $scope.origenes.data.push({value: 74, descripcion: 'Finlandia' });
                        $scope.origenes.data.push({value: 75, descripcion: 'Fiji' });
                        $scope.origenes.data.push({value: 76, descripcion: 'Francia'});
                        $scope.origenes.data.push({value: 83, descripcion: 'Grecia' });
                        $scope.origenes.data.push({value: 84, descripcion: 'Groenlandia' });
                        $scope.origenes.data.push({value: 86, descripcion: 'Guatemala' });
                        $scope.origenes.data.push({value: 87, descripcion: 'Guernsey' });
                        $scope.origenes.data.push({value: 88, descripcion: 'Guinea' });
                        $scope.origenes.data.push({value: 89, descripcion: 'Guinea Ecuatorial' });
                        $scope.origenes.data.push({value: 90, descripcion: 'Guinea-Bissau' });
                        $scope.origenes.data.push({value: 91, descripcion: 'Guyana' });
                        $scope.origenes.data.push({value: 92, descripcion: 'Haiti' });
                        $scope.origenes.data.push({value: 93, descripcion: 'Honduras' });
                        $scope.origenes.data.push({value: 94, descripcion: 'Hong kong' });
                        $scope.origenes.data.push({value: 95, descripcion: 'Hungria' });
                        $scope.origenes.data.push({value: 96, descripcion: 'India' });
                        $scope.origenes.data.push({value: 97, descripcion: 'Indonesia' });
                        $scope.origenes.data.push({value: 98, descripcion: 'Iraq' });
                        $scope.origenes.data.push({value: 99, descripcion: 'Irlanda' });
                        $scope.origenes.data.push({value: 100, descripcion: 'Iran' });
                        $scope.origenes.data.push({value: 101, descripcion: 'Islandia' });
                        $scope.origenes.data.push({value: 102, descripcion: 'Israel' });
                        $scope.origenes.data.push({value: 103, descripcion: 'Italia' });
                        $scope.origenes.data.push({value: 104, descripcion: 'Jamaica' });
                        $scope.origenes.data.push({value: 105, descripcion: 'Japon' });
                        $scope.origenes.data.push({value: 117, descripcion: 'Liberia' });
                        $scope.origenes.data.push({value: 118, descripcion: 'Libia' });
                        $scope.origenes.data.push({value: 120, descripcion: 'Lituania' });
                        $scope.origenes.data.push({value: 121, descripcion: 'Luxemburgo' });
                        $scope.origenes.data.push({value: 122, descripcion: 'Libano' });
                        $scope.origenes.data.push({value: 128, descripcion: 'Maldivas' });
                        $scope.origenes.data.push({value: 129, descripcion: 'Malta' });
                        $scope.origenes.data.push({value: 130, descripcion: 'Islas Malvinas' });
                        $scope.origenes.data.push({value: 131, descripcion: 'Mali' });
                        $scope.origenes.data.push({value: 134, descripcion: 'Marruecos' });
                        $scope.origenes.data.push({value: 144, descripcion: 'Mexico' });
                        $scope.origenes.data.push({value: 145, descripcion: 'Monaco' });
                        $scope.origenes.data.push({value: 146, descripcion: 'Namibia' });
                        $scope.origenes.data.push({value: 147, descripcion: 'Nauru' });
                        $scope.origenes.data.push({value: 149, descripcion: 'Nepal' });
                        $scope.origenes.data.push({value: 150, descripcion: 'Nicaragua' });
                        $scope.origenes.data.push({value: 151, descripcion: 'Nigeria' });
                        $scope.origenes.data.push({value: 152, descripcion: 'Niue' });
                        $scope.origenes.data.push({value: 154, descripcion: 'Noruega' });
                        $scope.origenes.data.push({value: 155, descripcion: 'Nueva Caledonia' });
                        $scope.origenes.data.push({value: 156, descripcion: 'Nueva Zelanda' });
                        $scope.origenes.data.push({value: 157, descripcion: 'Niger' });
                        $scope.origenes.data.push({value: 158, descripcion: 'Oman' });
                        $scope.origenes.data.push({value: 160, descripcion: 'Pakistan' });
                        $scope.origenes.data.push({value: 161, descripcion: 'Palaos' });
                        $scope.origenes.data.push({value: 162, descripcion: 'Palestina' });
                        $scope.origenes.data.push({value: 163, descripcion: 'Panama' });
                        $scope.origenes.data.push({value: 165, descripcion: 'Paraguay' });
                        $scope.origenes.data.push({value: 167, descripcion: 'Peru' });
                        $scope.origenes.data.push({value: 170, descripcion: 'Polonia' });
                        $scope.origenes.data.push({value: 171, descripcion: 'Portugal' });
                        $scope.origenes.data.push({value: 172, descripcion: 'Puerto Rico' });
                        $scope.origenes.data.push({value: 173, descripcion: 'Reino Unvalueo' });
                        $scope.origenes.data.push({value: 175, descripcion: 'Republica Checa' });
                        $scope.origenes.data.push({value: 176, descripcion: 'Republica Dominicana' });
                        $scope.origenes.data.push({value: 178, descripcion: 'Rumania' });
                        $scope.origenes.data.push({value: 179, descripcion: 'Rusia' });
                        $scope.origenes.data.push({value: 197, descripcion: 'Singapur' });
                        $scope.origenes.data.push({value: 198, descripcion: 'Siria' });
                        $scope.origenes.data.push({value: 201, descripcion: 'Sri Lanka' });
                        $scope.origenes.data.push({value: 203, descripcion: 'Sudafrica' });
                        $scope.origenes.data.push({value: 206, descripcion: 'Suecia' });
                        $scope.origenes.data.push({value: 207, descripcion: 'Suiza' });
                        $scope.origenes.data.push({value: 210, descripcion: 'Tailandia' });
                        $scope.origenes.data.push({value: 211, descripcion: 'Taiwan' });
                        $scope.origenes.data.push({value: 219, descripcion: 'Trinvaluead y Tobajo' });
                        $scope.origenes.data.push({value: 222, descripcion: 'Turquia' });
                        $scope.origenes.data.push({value: 225, descripcion: 'Ucrania' });
                        $scope.origenes.data.push({value: 227, descripcion: 'Uruguay' });
                        $scope.origenes.data.push({value: 230, descripcion: 'Ciudad del Vaticano' });
                        $scope.origenes.data.push({value: 231, descripcion: 'Venezuela' });
                        $scope.origenes.data.push({value: 239, descripcion: 'Chipre' });
                        $scope.origenes.data.push({value: 240, descripcion: 'Guam' });
                        $scope.origenes.data.push({value: 241, descripcion: 'Hawai' });
                        $scope.origenes.data.push({value: 246, descripcion: 'Boniaire' });
                        $scope.origenes.current=$scope.origenes.data[0];    
            }
        }  
      
        $scope.destinos  = {
            current: {},
            data: [],
            getData: function() {
                
                $scope.destinos.data.push({value:1, descripcion: 'Sur América'});
                $scope.destinos.data.push({value:2, descripcion: 'Centro América'});     
                $scope.destinos.data.push({value:3, descripcion: 'Norte América'});                         
                $scope.destinos.data.push({value:5, descripcion: 'Europa'});     
                $scope.destinos.data.push({value:6, descripcion: 'Asia'});     
                $scope.destinos.data.push({value:7, descripcion: 'África'}); 
                $scope.destinos.data.push({value:8, descripcion: 'Oceanía'}); 
                $scope.destinos.data.push({value:9, descripcion: 'Antillas Holandesas'}); 
                $scope.destinos.data.push({value:10, descripcion: 'Internacional'}); 
                $scope.destinos.data.push({value:11, descripcion: 'Colombia'}); 
                $scope.destinos.current=$scope.destinos.data[0];    
                
             }
        }
      
      $scope.tiposViaje  = {
            current: {},
            data: [],
            getData: function() {   
                
                $scope.tiposViaje.data.push({value:11, descripcion: 'Estudio'});
                $scope.tiposViaje.data.push({value:12, descripcion: 'Larga Estadía (más de 60 días).'});     
                $scope.tiposViaje.data.push({value:13, descripcion: 'Corta Estadia (menos de 60 días)'});     
                $scope.tiposViaje.data.push({value:14, descripcion: 'MultiViajes'});     
                $scope.tiposViaje.data.push({value:15, descripcion: 'Estudio Con Responsabilidad Civil'});     
                $scope.tiposViaje.data.push({value:16, descripcion: 'Futura Mamá'});     
                $scope.tiposViaje.data.push({value:17, descripcion: 'Atención médica por Pre-existencia'});   
                $scope.tiposViaje.current=$scope.tiposViaje.data[0];
          
            }
      }
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            facturaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.factura.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.factura.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.factura.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.factura.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }

    //INTERMEDIARIOS
    $scope.intermediarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            facturaHttp.getIntermediarios({}, {}, function(response) {                
            $scope.intermediarios.data = response ;              
                
               if($scope.factura.model){
                                     
               }                           
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setIntermediario' : function() {
          $scope.factura.model.intermediarioId=$scope.intermediarios.current.id;
      }
    }      
 

 
	
	//CARGAMOS LOS LISTADOS	
      
      
	$scope.monedas.getData();
    $scope.formasPago.getData();      
    $scope.origenes.getData();
    $scope.destinos.getData();
    $scope.tiposViaje.getData();    
    $scope.intermediarios.getData();
    $scope.prioridades.getData();
    $scope.grupos.getData();
    $scope.entregas.getData();
    $scope.viasContacto.getData();
    $scope.ciudades.getData();
      
    $scope.btnHistorico=true;
    $scope.btnAdjunto=true;
      
   
	//CARGAMOS LOS DATOS DEL factura	
	
	if(factura.id==0){
          $scope.btnModificar=true;
          $scope.btnAnular=false;        
    }
    else{   
        $rootScope.loadingVisible = true;  
        $scope.btnEditar=false;
        $scope.btnEnviarCotizacion=true;
        
        
		
            facturaHttp.read({},factura, function (data) { 
                
            $scope.factura.model = data;	
            $scope.factura.model.fechaInicio=new Date(data.fechaInicio);
            $scope.factura.model.fechaFin=new Date(data.fechaFin);  
           
            try{                
                 $scope.factura.model.fechaEntrega=new Date(data.fechaEntrega);  
            }
            catch(err) {
                $scope.factura.model.fechaEntrega='';
            }

            $scope.monedas.current = $filter('filter')($scope.monedas.data, { value : $scope.factura.model.monedaId })[0];
            debugger;
            $scope.formasPago.current = $filter('filter')($scope.formasPago.data, { value : $scope.factura.model.formaPagoId })[0];

            $scope.destinos.current = $filter('filter')($scope.destinos.data, { value : $scope.factura.model.destino })[0];
            $scope.entregas.current = $filter('filter')($scope.entregas.data, { value : $scope.factura.model.entregaId })[0];


            $scope.entregas.current = $filter('filter')($scope.entregas.data, { value : $scope.factura.model.entregaId })[0];
            $scope.ciudades.current = $filter('filter')($scope.ciudades.data, { value : $scope.factura.model.ciudadEntregaId })[0];

            $scope.prioridades.current = $filter('filter')($scope.prioridades.data, { value : $scope.factura.model.prioridadId })[0];

            $scope.grupos.current = $filter('filter')($scope.grupos.data, { value : $scope.factura.model.grupoId })[0];
            $scope.viasContacto.current = $filter('filter')($scope.viasContacto.data, { value : $scope.factura.model.viaContactoId })[0];        

            $scope.intermediarios.current = $filter('filter')($scope.intermediarios.data, { codigo : $scope.factura.model.intermediarioId })[0];

            $scope.btnModificar=true;
            $scope.btnAnular=true;
                
            if($scope.factura.model.polizas.length>0)
            $scope.factura.selectPoliza($scope.factura.model.polizas[0]);
                
                
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
        
    }
      

      
        
	
    
    
  }
})();