/**=========================================================
 * Module: app.entrega.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.entrega')
    .controller('entregaController', entregaController);

  entregaController.$inject = ['$scope', '$filter', '$state', 'LDataSource','base64', 'entregaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION','SERVER_URL','$window'];


  function entregaController($scope, $filter, $state, LDataSource,base64, entregaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION,SERVER_URL,$window) {
      
     
      var entrega = $state.params.entrega;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.entrega = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,
              tipo: '',
              consecutivo: '',
			  asunto: '',
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
              fechaEmision: '',
              fechaVencimiento: ''
              
		},  
        fechaEmision : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.entrega.fechaEmision.isOpen = true;
        }
      },  
        fechaVencimiento : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.entrega.fechaVencimiento.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.entrega.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.entrega.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.entrega', params : { filters : {procesoSeleccionado:entrega.tipo}, data : []} });
        $state.go('app.entrega');
          
      },
      save : function() {		  
       
		        if($scope.entrega.fechaEmision.value==""){message.show("warning", "Fecha de emisión requerida");return;}
				else{$scope.entrega.model.fechaEmision=Date.parse(new Date($scope.entrega.fechaEmision.value));}	
          
                if($scope.entrega.fechaVencimiento.value==""){message.show("warning", "Fecha de vencimiento requerida");return;}
				else{$scope.entrega.model.fechaVencimiento=Date.parse(new Date($scope.entrega.fechaVencimiento.value));}	
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.entrega.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.entrega.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.entrega.model.estado= $scope.estados.current.value;}
			
          
			//INSERTAR
            if($scope.entrega.model.id==0){
				
				$rootScope.loadingVisible = true;
				entregaHttp.save({}, $scope.entrega.model, function (data) { 
					
						entregaHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.entrega.model=data;
							entrega.id=$scope.entrega.model.id; 
							if($scope.entrega.model.fechaReg){$scope.entrega.fechaReg.value=new Date(parseFloat($scope.entrega.model.fechaReg));}    
							if($scope.entrega.model.fechaAct){$scope.entrega.fechaAct.value=new Date(parseFloat($scope.entrega.model.fechaAct));} 		
                            
                                $scope.btnAdjunto=true;
                                $scope.divProductosentrega=true;
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.entrega.model.id){
					$state.go('app.entrega');
				}else{
					
					entregaHttp.update({}, $scope.entrega.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.entrega.model=data;
						if($scope.entrega.model.fechaReg){$scope.entrega.fechaReg.value=new Date(parseFloat($scope.entrega.model.fechaReg));}    
						if($scope.entrega.model.fechaAct){$scope.entrega.fechaAct.value=new Date(parseFloat($scope.entrega.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.entrega.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.entrega.model.id,
                         referencia : 'entrega' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/entrega/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.entrega.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.entrega.model.id,
                         referencia : 'entrega' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      } ,imprimir : function() {            
        	entregaHttp.getFormato({},{ id : $scope.entrega.model.id,formato:'entrega'}, function (data) {
							
                            var urlDownload = SERVER_URL + base64.decode(data.url);
                            $window.open(urlDownload);                           
											
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
          
          
          
      },
          verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.entrega.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          }, 
   
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'PA', descripcion: 'Pagada'});             
        }        
    }

    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            entregaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.entrega.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.entrega.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.entrega.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.entrega.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                entregaHttp.getContactosEmpresa({}, { terceroId: $scope.entrega.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.entrega.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.entrega.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.entrega.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    
      //PRODUCTOS
    $scope.productosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            entregaHttp.getProductos({}, {}, function(response) {   
                
                $scope.productosOp.data = response ;                 
                $scope.productosOp.current=$scope.productosOp.data[0];
                $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
    
    
    $scope.productoentrega = {
    model : {  
            productoId: 0,
            oportunidadId :0,
            total :0,
            totalPendiente :0,
            valor :0,
            cantidad :0,
            valorIva :0,
            producto: '',
            comentario: ''
			
		},
      current : {},
      data:[],
      getData : function() {
          
          
          if(entrega){
              
            if($scope.entrega.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              entregaHttp.getProductosentrega({}, { entregaId: $scope.entrega.model.id }, function(response) {
                  
                  $scope.productoentrega.data = response;
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },        
        add: function(){           
            
              
            if($scope.entrega.model.id!=0){
                if($scope.productosOp.current){
                    $rootScope.loadingVisible = true;                    
                    
                    var data ={
                         productoId: $scope.productosOp.current.id,
                         oportunidadId :$scope.entrega.model.id,
                         total :  $scope.productosOp.current.valor,
                         totalPendiente :  $scope.productosOp.current.valor,
                         valor :$scope.productosOp.current.valor,
                         cantidad :1,
                         valorIva :0,
                         producto: $scope.productosOp.current.nombre,
                         comentario: ''
                        
                    } ;
                      entregaHttp.addProductoentrega({}, data, function(response) {

                          $scope.productoentrega.getData();
                          $rootScope.loadingVisible = false;
                          message.show("success", "Producto agregado satisfactoriamente");	
                      }, function(faild) {
                          $rootScope.loadingVisible = false;
                          message.show("error", faild.Message);
                      });    
                }
                else{message.show("warning", "Debe seleccionar un producto");}

            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
          
        }
        ,        
        update: function(data){
            
            
            if($scope.entrega.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              entregaHttp.editProductoentrega({}, data, function(response) {
                  
                  $scope.productoentrega.getData();
                  $rootScope.loadingVisible = false;
                  message.show("success", "Producto actualizado satisfactoriamente");	
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
        }
        ,delete: function(data){            
            
            $rootScope.loadingVisible = true;    
            debugger;
            entregaHttp.removeProductoentrega({}, {oportunidadId: $scope.entrega.model.id,productoId: data.productoId }, function(response){
             $scope.productoentrega.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
        }
    }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	    
    $scope.tercerosOp.getData();   
    $scope.productosOp.getData();
	
	//CARGAMOS LOS DATOS DEL entrega	
	
	if(entrega.id==0){
        
      $scope.estados.current=$scope.estados.data[0];             
      $scope.entrega.model.tipo=entrega.tipo;             
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            entregaHttp.read({},$state.params.entrega, function (data) { 
                
            $scope.entrega.model = data;	
            $scope.btnAdjunto=true;
            $scope.divProductosentrega=true;
               
			if($scope.entrega.model.fechaReg){$scope.entrega.fechaReg.value=new Date(parseFloat($scope.entrega.model.fechaReg));}    
			if($scope.entrega.model.fechaAct){$scope.entrega.fechaAct.value=new Date(parseFloat($scope.entrega.model.fechaAct));}  
            
                
            if($scope.entrega.model.fechaEmision){$scope.entrega.fechaEmision.value=new Date(parseFloat($scope.entrega.model.fechaEmision));}    
			if($scope.entrega.model.fechaVencimiento){$scope.entrega.fechaVencimiento.value=new Date(parseFloat($scope.entrega.model.fechaVencimiento));}     
                
                
                
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.entrega.model.terceroId })[0];  
            $scope.contactosOp.getData();
            $scope.productoentrega.getData();                
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.entrega.model.contactoId })[0];    
                
			//$scope.estados.current=$scope.entrega.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.entrega.model.estado})[0];
               
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();