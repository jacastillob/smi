/**=========================================================
 * Module: app.entrega.js
 =========================================================*/

(function() {
  'use strict';
  angular
    .module('app.entrega')
    .controller('entregaListController', entregaListController);

  entregaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'entregaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function entregaListController($scope, $filter, $state, ngDialog, tpl, entregaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
    
    $scope.PROCESO = 1;
    $scope.CERRADA = 2;

    $scope.entregas = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreentregas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.entregas.selectedAll = !$scope.entregas.selectedAll; 
        for (var key in $scope.entregas.selectedItems) {
          $scope.entregas.selectedItems[key].check = $scope.entregas.selectedAll;
        }
      },
      add : function() {
        
       
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: 'FAC',
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',                  
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.entrega_add', params : { entrega: oport } });
            $state.go('app.entrega_add');
      
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.entrega_edit', params : { entrega: item } });
        $state.go('app.entrega_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            entregaHttp.remove({}, { id: id }, function(response) {
                $scope.entregas.getData();
                message.show("success", "entrega eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.entregas.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    entregaHttp.remove({}, { id: id }, function(response) {
                        $scope.entregas.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.entregas.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.entregas.filterText,
          "precision": false
        }];
        $scope.entregas.selectedItems = $filter('arrayFilter')($scope.entregas.dataSource, paramFilter);
        $scope.entregas.paginations.totalItems = $scope.entregas.selectedItems.length;
        $scope.entregas.paginations.currentPage = 1;
        $scope.entregas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.entregas.paginations.currentPage == 1 ) ? 0 : ($scope.entregas.paginations.currentPage * $scope.entregas.paginations.itemsPerPage) - $scope.entregas.paginations.itemsPerPage;
        $scope.entregas.data = $scope.entregas.selectedItems.slice(firstItem , $scope.entregas.paginations.currentPage * $scope.entregas.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.entregas.data = [];
        $scope.entregas.loading = true;
        $scope.entregas.noData = false;
        var parametros= {
            "proceso":'FAC'                
        };
          
        entregaHttp.getList({}, parametros,function(response) {
          $scope.entregas.selectedItems = response;
          $scope.entregas.dataSource = response;
          for(var i=0; i<$scope.entregas.dataSource.length; i++){
            $scope.entregas.nombreentregas.push({id: i, nombre: $scope.entregas.dataSource[i]});
          }
          $scope.entregas.paginations.totalItems = $scope.entregas.selectedItems.length;
          $scope.entregas.paginations.currentPage = 1;
          $scope.entregas.changePage();
          $scope.entregas.loading = false;
          ($scope.entregas.dataSource.length < 1) ? $scope.entregas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }

	//CARGAMOS LOS TIPOS DE PROCESO
    
    $scope.entregas.getData();  

        
	}
  
  
  })();