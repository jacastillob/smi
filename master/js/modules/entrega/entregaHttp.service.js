/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.entrega')
    .service('entregaHttp', entregaHttp);

  entregaHttp.$inject = ['$resource', 'END_POINT'];


  function entregaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            proceso : '@proceso'           
          },
          'url' : END_POINT + '/Comercial.svc/entrega'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/entrega/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/entrega'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/entrega'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/entrega'
      }  
    };
    return $resource( END_POINT + '/Comercial.svc/entrega', {}, actions, {}); 
  }

})();