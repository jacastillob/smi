/**=========================================================
 * Module: app.funcionario.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .service('funcionarioHttp', funcionarioHttp);

  funcionarioHttp.$inject = ['$resource', 'END_POINT'];


  function funcionarioHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/funcionario/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/funcionario/:id'
      }        
     
    };
    return $resource( END_POINT + '/General.svc/funcionario', {}, actions, {}); 
  }

})();