/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.seguimiento')
    .service('seguimientoHttp', seguimientoHttp);

  seguimientoHttp.$inject = ['$resource', 'END_POINT'];


  function seguimientoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
        
        'addSeguimiento': {
        'method':'POST',
        'url' : END_POINT + '/General.svc/addSeguimiento'
        },
      getSeguimientos : {
        method : 'POST',
        isArray : true
      },
      getSeguimiento : {
        method : 'GET',
        url : END_POINT + '/General.svc/seguimiento/:id'
      },
      removeSeguimiento : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/seguimiento/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/seguimiento', paramDefault, actions);
  }

})();
