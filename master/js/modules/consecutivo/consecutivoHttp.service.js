/**=========================================================
 * Module: app.consecutivo.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .service('consecutivoHttp', consecutivoHttp);

  consecutivoHttp.$inject = ['$resource', 'END_POINT'];


  function consecutivoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            tipo : '@tipo'           
          },
          'url' : END_POINT + '/Cliente.svc/consecutivo/tipo/:tipo'
          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/consecutivo/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/consecutivo'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/consecutivo'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/consecutivo/:id/tipo'
      },
        'getTipoConsecutivos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Cliente.svc/tipoConsecutivo'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroconsecutivo'
      },
        getFuncionarios : {
        method : 'GET',
        isArray : true,
        url : END_POINT + '/General.svc/funcionarioBusqueda'
      }
     
    };
    return $resource( END_POINT + '/Cliente.svc/consecutivo/tipo', {}, actions, {}); 
  }

})();