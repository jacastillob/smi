/**=========================================================
 * Module: constants.js
 * Define constants to inject across the application
 https://crossorigin.me/https://ec2-54-213-134-94.us-west-2.compute.amazonaws.com/LyqService/Services
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('APP_MEDIAQUERY', {
          'desktopLG':             1200,
          'desktop':                992,
          'tablet':                 768,
          'mobile':                 480
        })
    .constant('END_POINT',  'https://www.segurosmedicosinternacionales.com.co/rs/Services')
    //.constant('END_POINT','http://localhost:20559/Services')   
    .constant('REPORT_URL',  'http://reports.segurosmedicosinternacionales.com.co:82/ReportServer?/ServidorInformes/')   
    .constant('SERVER_URL', 'https://www.segurosmedicosinternacionales.com.co/rs/')
    .constant('TO_STATE', '')
    .constant('REGULAR_EXPRESION', {
      POSITIVES_INTEGERS : /^\d{0,10}?$/g,
      POSTIVES_DECIMALS : /(^[0-9]+(\.[0-9]+)?$)|(^[0-9]+\.$)/g, 
      ONLY_UPPERCASE : /\b^[A-Z]+$\b/g 
    });
})();