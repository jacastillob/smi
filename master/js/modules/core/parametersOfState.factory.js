/**=========================================================
 * Module: app.core.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('parametersOfState', parametersOfState);

  parametersOfState.$inject = [];


  function parametersOfState() {
    
    var _parametersOfState = {};
      
    return {
      set: function (state) {
        _parametersOfState[state.name] = JSON.stringify(state.params);
      },
      get : function (state) {
        return _parametersOfState[state.name] != undefined ? JSON.parse(_parametersOfState[state.name]) : state.params;
      }
    }
  }

})();