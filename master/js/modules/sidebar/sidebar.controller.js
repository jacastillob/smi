/**=========================================================
 * Module: sidebar-menu.js
 * Handle sidebar collapsible elements
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .controller('SidebarController', SidebarController);

  SidebarController.$inject = ['$rootScope', '$scope', '$state', 'Utils', 'sidebarHttp', 'parametersOfState', 'TO_STATE'];
  function SidebarController($rootScope, $scope, $state, Utils, sidebarHttp, parametersOfState, TO_STATE) {

    activate();

    function activate() {
      var collapseList = [];

      // demo: when switch from collapse to hover, close all items
      $rootScope.$watch('app.layout.asideHover', function(oldVal, newVal) {
        if ( newVal  === false && oldVal === true ) {
          closeAllBut(-1);
        }
      });

      $scope.changeOptions = function(item) {
        TO_STATE = '';
        if (item.sref != '') {
          var param = (item.param != '') ? JSON.parse(item.param) : null;
          parametersOfState.set({ name : item.sref, params : param });
          $state.go(item.sref);
          if ($state.current.name == 'app.reporte') {
            $rootScope.$broadcast('changeOptions', null);
          }
        }
      }


      // Load menu from json file
      // ----------------------------------- 

      sidebarHttp.getMenu({},{},function(response) {
        $scope.menuItems = response; 
      }, function(faild) {
        $scope.menuItems = [];
      })

      // Handle sidebar and collapse items
      // ----------------------------------

      $scope.getMenuItemPropClasses = function(item) {
        return (item.heading ? 'nav-heading' : '') +
          (isActive(item) ? ' active' : '') ;
      };

      $scope.addCollapse = function($index, item) {
        collapseList[$index] = $rootScope.app.layout.asideHover ? true : !isActive(item);
      };

      $scope.isCollapse = function($index) {
        return (collapseList[$index]);
      };

      $scope.toggleCollapse = function($index, isParentItem, param) {
        // collapsed sidebar doesn't toggle drodopwn
        if( Utils.isSidebarCollapsed() || $rootScope.app.layout.asideHover ) return true;

        // make sure the item index exists
        if( angular.isDefined( collapseList[$index] ) ) {
          if ( ! $scope.lastEventFromChild ) {
            collapseList[$index] = !collapseList[$index];
            closeAllBut($index);
          }
        }
        else if ( isParentItem ) {
          closeAllBut(-1);
        }

        $scope.lastEventFromChild = isChild($index);

        return true;

      };

      // Controller helpers
      // ----------------------------------- 

      // Check item and children active state
      function isActive(item) {

        if(!item) return;

        if( !item.sref || item.sref === '#') {
          var foundActive = false;
          angular.forEach(item.submenu, function(value) {
            if(isActive(value)) foundActive = true;
          });
          return foundActive;
        }
        else
          return $state.is(item.sref) || $state.includes(item.sref);
      }

      function closeAllBut(index) {
        index += '';
        for(var i in collapseList) {
          if(index < 0 || index.indexOf(i) < 0)
            collapseList[i] = true;
        }
      }

      function isChild($index) {
        /*jshint -W018*/
        return (typeof $index === 'string') && !($index.indexOf('-') < 0);
      }


    } // activate
  }

})();
