(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .controller('TopnavbarController', TopnavbarController);

  TopnavbarController.$inject = ['$scope', '$state', 'tokenManager', 'ngDialog'];
  function TopnavbarController($scope, $state, tokenManager, ngDialog) {

    $scope.logout = function() {

      ngDialog.openConfirm({
        template: 'app/views/templates/logout_confirmation.html',
        className: 'ngdialog-theme-default',
        scope: $scope
      }).then(function (value) {
        tokenManager.resetToken();
        $state.go('page.login');
      });        
    }

  }

})();