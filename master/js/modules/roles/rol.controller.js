/**=========================================================
 * Module: app.report.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .controller('rolController', rolController);

  rolController.$inject = ['$scope', '$rootScope', '$state', '$q', '$filter', 'roltHttp', 'base64', '$stateParams', 'parametersOfState', 'message', '$modal', 'ngDialog', 'tpl'];


  function rolController($scope, $rootScope, $state, $q, $filter, roltHttp, base64, $stateParams, parametersOfState, message, $modal, ngDialog, tpl) {
      
      
      $scope.gridOptions = {
        rowHeight: 34,
        data: [],
        subsections: [],
        setData: function(){
          $rootScope.loadingVisible = true;
          $scope.gridOptions.data = []; 
          roltHttp.getList({}, {}, function(response) {
              $scope.gridOptions.data = $filter('orderBy')(response, 'nombre');
              $rootScope.loadingVisible = false;
          }, function(faild) {
              $rootScope.loadingVisible = false;
              message.show("error", "No se Pudo Obtener el Listado de Roles: " + faild.Message);
          });
        },
        edit: function (item){
          var parameter = {datos: item, option: 0};
          var modalInstance = $modal.open({
              templateUrl: 'app/views/roles/roles_edit.html',
              controller: 'rolEditController',
              size: 'sm',
              resolve: {
                parameters: function () { return parameter; }
              }
          });
          modalInstance.result.then(function (parameters) {
            $scope.gridOptions.setData();
          }, function (parameters) {
          });
        },
        add: function (){
          var model = {id: 0, nombre: '', estado: 0};
          var parameter = {datos: model, option: 1};
          var modalInstance = $modal.open({
              templateUrl: 'app/views/roles/roles_edit.html',
              controller: 'rolEditController',
              size: 'sm',
              resolve: {
                parameters: function () { return parameter; }
              }
          });
          modalInstance.result.then(function (parameters) {
            $scope.gridOptions.setData();
          }, function (parameters) {
          });
        },
        remove: function(item){
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
              $rootScope.loadingVisible = true;
              roltHttp.removeRol({}, { id: item.id }, function(response) {
                  $scope.gridOptions.setData();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se Pudo Eliminar el Registro: " + faild.Message);
              });
          });
        }
      };
      $scope.gridOptions.setData();
  }

})();