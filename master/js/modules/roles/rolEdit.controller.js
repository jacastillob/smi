/**=========================================================
 * Module: app.report.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .controller('rolEditController', rolEditController);

  rolEditController.$inject = ['$scope', '$rootScope', '$state', '$q', '$filter', 'roltHttp', 'base64', '$stateParams', 'parametersOfState', 'message', 'parameters', '$modalInstance'];


  function rolEditController($scope, $rootScope, $state, $q, $filter, roltHttp, base64, $stateParams, parametersOfState, message, parameters, $modalInstance) {
    
      $scope.estados= {
          current:{},
          data: [],
          setData: function() {
              $scope.estados.data.push({id: 0, value: 'Inactivo'});
              $scope.estados.data.push({id: 1, value: 'Activo'});
              $scope.estados.current = $scope.estados.data[0];
          }
      };
      $scope.estados.setData();
      
      $scope.rol={
          model:{
              id: 0,
              nombre: '',
              estado: 0
          },
          option: 0,
          secciones: [],
          save: function() {
            
            if(!$scope.rol.model.nombre){message.show("warning", "Nombre del Rol Requerido");return;}
            if($scope.rol.model.nombre == ""){message.show("warning", "Nombre del Rol Requerido");return;}
            if(!$scope.estados.current){message.show("warning", "Estado del Rol Requerido");return;}
            $scope.rol.model.estado = $scope.estados.current.id;
            
            if($scope.rol.option == 0){
              $rootScope.loadingVisible = true;
              var parametros = $scope.rol.model;
              roltHttp.editRol({}, parametros,  function(response) {
                  $scope.rol.close();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se pudo guardar el registro: " + faild.Message);
              });
            }
            if($scope.rol.option == 1){
              
              var parametros = {
                  nombre: $scope.rol.model.nombre,
                  estado: $scope.rol.model.estado
              };
              
              var _Http = new roltHttp(parametros);
              _Http.$newrol( function(response) {
                  $scope.rol.close();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });
            }
          },
          dismiss : function() {
              $modalInstance.dismiss('cancel');
          },
          close : function() {
              $modalInstance.close('close');
          }
      };
      
      $scope.rol.model = parameters.datos;
      $scope.rol.option = parameters.option;
      
      if($scope.rol.option==0){
        $scope.estados.current= $filter('filter')($scope.estados.data, {id: $scope.rol.model.estado})[0];
      }
  }

})();