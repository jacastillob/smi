/**=========================================================
 * Module: modals.js
 * Provides a simple way to implement bootstrap modals from templates
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.usuario')
        .controller('ModalGmapController', ModalGmapController);

    ModalGmapController.$inject =     
    ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'usuarioHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION','$timeout'];
      
    function ModalGmapController($scope, $filter, $state, $modalInstance, LDataSource, usuarioHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION,$timeout) { 
        
        var  objUbi = parameters.objUbicacion;
        var registroUbicacion = parameters.registroUbicacion;   
        var vm = this;
        
        
        
        function activate(longitude ,latitude) {     
            
                 
            var position = new google.maps.LatLng( latitude,longitude);
            
                $scope.mapOptionsModal = {
                    zoom: 16,
                    center:position,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                  };
            
              $timeout(function(){
                // 1. Add a marker at the position it was initialized
                new google.maps.Marker({
                  map: $scope.myMapModal,
                  position: position
                });
                // 2. Trigger a resize so the map is redrawed
                google.maps.event.trigger($scope.myMapModal, 'resize');
                // 3. Move to the center if it is misaligned
                $scope.myMapModal.panTo(position);
              });          
        }

        $scope.registroUbicacionModal={               
                     
            model : {
                id:0,
                contratoId:0,
                funcionarioId:0,
                fecha: '',
                tipo: '',
                comentario: '',
                longitude : 0,
                latitude : 0,
                accuracy : 0
		},             
            
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          save : function() {
                             
                if($scope.tipos.current.value==''){message.show("warning", "Tipo registro requerido");return;}
                else{$scope.registroUbicacionModal.model.tipo= $scope.tipos.current.value;}   
               
                    
                $rootScope.loadingVisible = true;
				usuarioHttp.addRegistroUbicacion({}, $scope.registroUbicacionModal.model, function (data) { 
					
						usuarioHttp.getRegistroUbicacion({},{ id : data.id}, function (data) {                           
							$scope.registroUbicacionModal.model=data;                         
								
							message.show("success", "Ubicación Registrada satisfactoriamente!!");
                             $modalInstance.close();                       
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});              
          } 
        }
        
    //TIPOS
    $scope.tipos = {
        current : {}, data:[],
        getData : function() {
            $scope.tipos.data.push({value:'', descripcion: 'Seleccione'});
            $scope.tipos.data.push({value:'E', descripcion: 'Entrada'});
            $scope.tipos.data.push({value:'S', descripcion: 'Salida'});
            $scope.tipos.current=$scope.tipos.data[0];
        }        
    } 
    
    
   //INICIALIZAMOS EL MAPA
 

   //activate(150.644,34.397);
    
    
    //CARGAMOS LISTADO
    $scope.tipos.getData();        
    if(registroUbicacion.id==0){   
       if(objUbi.load){ 
            $scope.registroUbicacionModal.model.longitude=objUbi.long;
            $scope.registroUbicacionModal.model.latitude=objUbi.lat;
            $scope.registroUbicacionModal.model.accuracy=objUbi.accu;
            $scope.registroUbicacionModal.model.contratoId=registroUbicacion.contratoId;
            $scope.registroUbicacionModal.model.funcionarioId=registroUbicacion.funcionarioId;           
        }
           else{               
               message.show("warning", "Debe cargar la ubicación!!!");
               $modalInstance.dismiss('cancel');               
           }
             //activate(objUbi.long,objUbi.lat);
             //activate2();
    }
    else{ 
         //activate(objUbi.long,objUbi.lat);
         usuarioHttp.getRegistroUbicacion({}, { id : registroUbicacion.id} , function (data) { 
         $scope.registroUbicacionModal.model = data;	
             
           objUbi.long=$scope.registroUbicacionModal.model.longitude;
           objUbi.lat=$scope.registroUbicacionModal.model.latitude;
           objUbi.accu=$scope.registroUbicacionModal.model.accuracy;
           objUbi.load=true;
             
            $scope.tipos.current = $filter('filter')($scope.tipos.data, {value : $scope.registroUbicacionModal.model.tipo})[0];
       
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });  
           //if(objUbi.load)
             //  activate(objUbi.long,objUbi.lat);
      
    }   
      
}
    
    
    


    })();
