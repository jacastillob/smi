/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.usuario')
    .controller('registroUbicacionListController', registroUbicacionListController);

  registroUbicacionListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'usuarioHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION','$interval'];
    


  
    function registroUbicacionListController($scope, $filter, $state,$modal, ngDialog, usuarioHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION,$interval) {       
        
        
                
         
        var options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };        
        var ubicacion={
            long:0,
            lat:0,
            accu:0,            
            load:false            
        };       
               
      
       
        
            
    $scope.registroUbicacion={
        //listado           
     paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.registroUbicacion.fechaInicial.isOpen = true;
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.registroUbicacion.fechaFinal.isOpen = true;
        }
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreregistroUbicacion:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.registroUbicacion.selectedAll = !$scope.registroUbicacion.selectedAll; 
        for (var key in $scope.registroUbicacion.selectedItems) {
          $scope.registroUbicacion.selectedItems[key].check = $scope.registroUbicacion.selectedAll;
        }
      },
      add : function() {          
          
            if(!ubicacion.load){message.show("warning", "Debe cargar su ubicación");return;}
            if($scope.procesoAsociado.current.id==-1){message.show("warning", "Debe seleccionar un contrato");return;}     
            if($scope.responsableProceso.current.funcionarioId==-1){message.show("warning", "Debe seleccionar un responsable");return;}
              
         var  registroUbicacion = {
                id:0,
                contratoId:$scope.procesoAsociado.current.id,
                funcionarioId:$scope.responsableProceso.current.funcionarioId,
                fecha: '',
                tipo: '',
                comentario: '',
                longitude : 0,
                latitude : 0,
                accuracy : 0
		}
          
        var parameter= { objUbicacion : ubicacion,
                          registroUbicacion:registroUbicacion }
         
         var modalInstance = $modal.open({
          templateUrl: 'app/views/usuario/registroUbicacion_modal.html',
          controller: 'ModalGmapController',
          size: 'lg',
            resolve: {
                parameters: function () { return parameter; }
            }
        });
          
        modalInstance.result.then(function (parameters) {
            
            $scope.btnRegistroUbicacion=false; 
            $scope.registroUbicacion.getData();
            
              ubicacion.long=0;            
              ubicacion.lat=0,
              ubicacion.accu=0,            
              ubicacion.load=false;
            
             
            
            
          
        });
                      
      },
      edit : function(item) {
          
          message.show("warning", "Por el momento no se puede editar la ubicación");
          /*
        var modalInstance = $modal.open({
          templateUrl: 'app/views/usuario/registroUbicacion_modal.html',
          controller: 'ModalGmapController',
          size: 'lg',
          resolve: {            parameters: { objUbicacion : ubicacion,registroUbicacion: item}         }
        });
        modalInstance.result.then(function (parameters) {
            $scope.registroUbicacion.getData();
              
              ubicacion.long=0;            
              ubicacion.lat=0,
              ubicacion.accu=0,            
              ubicacion.load=false;
            
             $scope.registroUbicacion.obtenerUbicacion()
            
        });  */        
      
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.registroUbicacion.filterText,
          "precision": false
        }];
        $scope.registroUbicacion.selectedItems = $filter('arrayFilter')($scope.registroUbicacion.dataSource, paramFilter);
        $scope.registroUbicacion.paginations.totalItems = $scope.registroUbicacion.selectedItems.length;
        $scope.registroUbicacion.paginations.currentPage = 1;
        $scope.registroUbicacion.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.registroUbicacion.paginations.currentPage == 1 ) ? 0 : ($scope.registroUbicacion.paginations.currentPage * $scope.registroUbicacion.paginations.itemsPerPage) - $scope.registroUbicacion.paginations.itemsPerPage;
        $scope.registroUbicacion.data = $scope.registroUbicacion.selectedItems.slice(firstItem , $scope.registroUbicacion.paginations.currentPage * $scope.registroUbicacion.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.registroUbicacion.data = [];
        $scope.registroUbicacion.loading = true;
        $scope.registroUbicacion.noData = false;
          var objF={};
          
            if(!$scope.responsableProceso.current){message.show("warning", "Debe seleccionar un responsable");return;}
            if($scope.registroUbicacion.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{objF.fechaInicial=Date.parse(new Date($scope.registroUbicacion.fechaInicial.value));}	
          
            if($scope.registroUbicacion.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{objF.fechaFinal=Date.parse(new Date($scope.registroUbicacion.fechaFinal.value));}	  
              
              objF.idResponsable=$scope.responsableProceso.current.funcionarioId;              
              objF.idReferencia=$scope.procesoAsociado.current.id;    
          
        usuarioHttp.getRegistrosUbicacion({}, objF,function(response) {
          $scope.registroUbicacion.selectedItems = response;
          $scope.registroUbicacion.dataSource = response;
          for(var i=0; i<$scope.registroUbicacion.dataSource.length; i++){
            $scope.registroUbicacion.nombreregistroUbicacion.push({id: i, nombre: $scope.registroUbicacion.dataSource[i]});
          }
          $scope.registroUbicacion.paginations.totalItems = $scope.registroUbicacion.selectedItems.length;
          $scope.registroUbicacion.paginations.currentPage = 1;
          $scope.registroUbicacion.changePage();
          $scope.registroUbicacion.loading = false;
          ($scope.registroUbicacion.dataSource.length < 1) ? $scope.registroUbicacion.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }, 
        obtenerUbicacion : function() {
                //tryGeolocation();
            
            if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition( $scope.registroUbicacion.setUbicacion);
                } else {
                   message.show("error", "No se puede cargar la ubicación!");  
                    $scope.btnRegistroUbicacion=false; 
                }
                
        },
        setUbicacion : function(pos) {            
            
             var crd = pos.coords;  
            
              ubicacion.lat=crd.latitude;
              ubicacion.long=crd.longitude;
              ubicacion.accu= crd.accuracy;
              ubicacion.load= true;               
              message.show("success", "Ubicación cargada con exito!");        
              $scope.btnRegistroUbicacion=true; 
        }
        
        
        
        
        
        
        
    }
    
   
        
    //RESPONSABLE
    $scope.responsableProceso = {
      current : {}, data:[],
      getData : function() {        
          
          if($scope.procesoAsociado.current){              
              
                var obj= {
                    idProceso : $scope.procesoAsociado.current.id,
                    proceso : "CONTRATO"
                }  
              
                $rootScope.loadingVisible = true;
                usuarioHttp.getResponsablesProceso({}, obj, function(response) {                     
                    
                
                    
                    if(response.length==1){      
                       $scope.responsableProceso.data = response ;   
                       $scope.responsableProceso.current = $scope.responsableProceso.data[0];
                    }
                    else{
                        
                        $scope.responsableProceso.data = response ;   
                        $scope.responsableProceso.data.push({funcionarioId:(-1), nombre:"Todos"});
                        $scope.responsableProceso.data=$filter('orderBy')($scope.responsableProceso.data, 'id'); 
                        $scope.responsableProceso.current = $scope.responsableProceso.data[0];
                        }
                    
                    $rootScope.loadingVisible = false;
                    
                    
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      }
    } 
    
    $scope.procesoAsociado = {
      current : {}, data:[],
      getData : function() {
          
              
                $rootScope.loadingVisible = true;
                usuarioHttp.getProcesosAsociado({}, {proceso:"CONTRATO"}, function(response) {                       
                     
                    
                  if(response.length==1){     
                        $scope.procesoAsociado.data = response ;   
                        $scope.procesoAsociado.current = $scope.procesoAsociado.data[0];
                        $scope.responsableProceso.getData();
                      
                      
                    }
                    else{

                         $scope.procesoAsociado.data = response ;   
                         $scope.procesoAsociado.data.push({id:(-1), descripcion:"Todos",proceso:""});
                         $scope.procesoAsociado.data=$filter('orderBy')($scope.procesoAsociado.data, 'id');  
                         $scope.procesoAsociado.current=$scope.procesoAsociado.data[0];  
                         $scope.responsableProceso.getData();
                    }
                    
                    $rootScope.loadingVisible = false;
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });          
      },
        setProcesoAsociado: function() {             
           $scope.responsableProceso.getData();            
      }
     }
    
    //CARGAMOS LISTADO
    $scope.procesoAsociado.getData();
    $scope.registroUbicacion.obtenerUbicacion();
    // INICIALIZAMOS FECHAS
    var date = new Date();    
    $scope.registroUbicacion.fechaInicial.value=new Date(date.getFullYear(), date.getMonth(), 1);
    $scope.registroUbicacion.fechaFinal.value=new Date(date.getFullYear(), date.getMonth() + 1, 0);        
    
        
        
        
    }
  
  
  })();