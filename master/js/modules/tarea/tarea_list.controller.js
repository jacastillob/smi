/**=========================================================
 * Module: app.tarea.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tarea')
    .controller('tareaListController', tareaListController);

  tareaListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'tpl', 'tareaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function tareaListController($scope, $filter,$state,$modal, ngDialog, tpl, tareaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
         
    $scope.tareas = {
      model : {        
        tarea : 0    
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },     
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombretareas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.tareas.selectedAll = !$scope.tareas.selectedAll; 
        for (var key in $scope.tareas.selectedItems) {
          $scope.tareas.selectedItems[key].check = $scope.tareas.selectedAll;
        }
      },      
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.tareas.filterText,
          "precision": false
        }];
        $scope.tareas.selectedItems = $filter('arrayFilter')($scope.tareas.dataSource, paramFilter);
        $scope.tareas.paginations.totalItems = $scope.tareas.selectedItems.length;
        $scope.tareas.paginations.currentPage = 1;
        $scope.tareas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.tareas.paginations.currentPage == 1 ) ? 0 : ($scope.tareas.paginations.currentPage * $scope.tareas.paginations.itemsPerPage) - $scope.tareas.paginations.itemsPerPage;
        $scope.tareas.data = $scope.tareas.selectedItems.slice(firstItem , $scope.tareas.paginations.currentPage * $scope.tareas.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.tareas.data = [];
        $scope.tareas.loading = true;
        $scope.tareas.noData = false;
          
          
        tareaHttp.getList({}, {tipoTarea: $scope.filtrotarea.current.value},function(response) {
          $scope.tareas.selectedItems = response;
          $scope.tareas.dataSource = response;
          for(var i=0; i<$scope.tareas.dataSource.length; i++){
            $scope.tareas.nombretareas.push({id: i, nombre: $scope.tareas.dataSource[i]});
          }
          $scope.tareas.paginations.totalItems = $scope.tareas.selectedItems.length;
          $scope.tareas.paginations.currentPage = 1;
          $scope.tareas.changePage();
          $scope.tareas.loading = false;
          ($scope.tareas.dataSource.length < 1) ? $scope.tareas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      },edit : function(item) {
          
          var modalInstance = $modal.open({
                                      templateUrl: 'app/views/agenda/agendaCalendario_form.html',
                                      controller: 'agendaCalendarioController',
                                      size: 'lg',
                                      resolve: {
                                        parameters: { id : item.id,referencia:item.referencia}
                                      }
                                    });
                                    modalInstance.result.then(function (parameters) {
                                    });
          
      }
    }  
        
    //FILTRO TAREA
    $scope.filtrotarea = {             
        current : {}, data:[],
        getData : function() {            
            $scope.filtrotarea.data.push({value:'1', descripcion: 'Todos'});
            $scope.filtrotarea.data.push({value:'2', descripcion: 'Mes'});
            $scope.filtrotarea.data.push({value:'3', descripcion: 'Día'});
            $scope.filtrotarea.current =$scope.filtrotarea.data[2];
            $scope.tareas.getData();
            
        }          
    }
   // $scope.filtrotarea.getData();
	//CARGAMOS DATA
            
   $scope.filtrotarea.getData();
        
	}
  
  
  })();