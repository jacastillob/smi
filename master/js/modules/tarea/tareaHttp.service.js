/**=========================================================
 * Module: app.tarea.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tarea')
    .service('tareaHttp', tareaHttp);

  tareaHttp.$inject = ['$resource', 'END_POINT'];


  function tareaHttp($resource, END_POINT) {

    var paramDefault = {
      tipoTarea : '@tipoTarea'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true, 
        'params': {
            tipoTarea:'@tipoTarea'
        },
        'url' : END_POINT + '/General.svc/tarea/:tipoTarea'          
      },     
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/tarea'
      }        
    };
    return $resource( END_POINT + '/General.svc/tarea', {}, actions, {}); 
  }

})();